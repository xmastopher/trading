package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Ignore;
import org.junit.Test;

import gsplatform.servlet.database.PortfolioInfo;
import gsplatform.servlet.database.PortfolioInfoDAO;

public class PortfolioInfoDAOTest {

	@Test
	public void getPortfolioInfo( )
	{
		PortfolioInfoDAO portfolioInfoDAO = new PortfolioInfoDAO( "danimal" );
		
		try
		{
			portfolioInfoDAO.openConnection( );
			PortfolioInfo portfolioInfo = portfolioInfoDAO.getPortfolioInfo( );
			
			assertEquals( portfolioInfo.getAmountInvested( ).doubleValue( ), 
							new BigDecimal( 1000 ).doubleValue( ), 2 ); 
			
			assertEquals( portfolioInfo.getFiatType( ), "USD" );
			
			portfolioInfoDAO.closeConnection( );
		}
		catch( Exception e )
		{
			e.getStackTrace( );
			assertEquals( true, false );
		}
	}
	
	@Test
	public void testInitPortfolioInfo( ) 
	{
		PortfolioInfoDAO portfolioInfoDAO = new PortfolioInfoDAO( "testman2" );

		try
		{
			portfolioInfoDAO.openConnection( );
			portfolioInfoDAO.initPortfolioInfo("EUR", new BigDecimal( 10000 ) );
			PortfolioInfo portfolioInfo = portfolioInfoDAO.getPortfolioInfo( );
			
			assertEquals( portfolioInfo.getAmountInvested( ).doubleValue( ), 
							new BigDecimal( 10000 ).doubleValue( ), 2 ); 
	
			assertEquals( portfolioInfo.getFiatType( ), "EUR" );
			
			portfolioInfoDAO.closeConnection( );
		}
		catch( Exception e )
		{
			e.getStackTrace( );
			assertEquals( true, false );
		}
	}
	
	@Test
	public void testMakeDeposit( ) 
	{
		PortfolioInfoDAO portfolioInfoDAO = new PortfolioInfoDAO( "testman" );

		try
		{
			BigDecimal deposit = new BigDecimal( 1000 );
			
			portfolioInfoDAO.openConnection( );
			portfolioInfoDAO.initPortfolioInfo("EUR", new BigDecimal( 10000 ) );
			portfolioInfoDAO.makeDeposit( deposit );
			PortfolioInfo portfolioInfo = portfolioInfoDAO.getPortfolioInfo( );
			assertEquals( portfolioInfo.getAmountInvested( ).doubleValue( ), 
							new BigDecimal( 11000 ).doubleValue( ), 2 ); 
			portfolioInfoDAO.closeConnection( );
		}
		catch( Exception e )
		{
			e.getStackTrace( );
			assertEquals( true, false );
		}
	}
	
	@Test
	@Ignore
	public void testChangeFiatType( ) {
		fail("Not yet implemented");
	}

}
