package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.servlet.database.BotTradeMetrics;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;
import gsplatform.utilities.TestDatabaseInitializer;

public class UserBotsDAOTest {

	@Test
	public void testAddUserBot( ) 
	{
	}
	
	@Test
	public void testLockUserBot( )
	{
	}
	
	@Test
	public void testUnlockUserBot( )
	{
	}
	
	@Test
	public void testStartUserBot( )
	{
	}
	
	@Test
	public void testStopUserBot( )
	{
	}
	
	@Test
	public void testMakePublic( )
	{
	}
	
	@Test
	public void testMakePrivate( )
	{
	}
	
	@Test
	public void testRemoveUserBot( ) throws ClassNotFoundException, SQLException{
		
		TestDatabaseInitializer.resetTestDatabase( );
		UserBotsDAO userBotsDAO = new UserBotsDAO( "daniel", 1 );
		userBotsDAO.openConnection( );
		assertTrue( userBotsDAO.deleteBot( ) );
		assertNull( userBotsDAO.grabBotFromId( 1 ) );
		
		BotLeaderboardsDAO botLeaderboards = new BotLeaderboardsDAO( "daniel" );
		botLeaderboards.openConnection( );
		
		BotLogsDAO botLogs = new BotLogsDAO( "daniel", 1 );
		botLogs.openConnection( );
		
		List< BotTradeMetrics > tradeMetrics = botLeaderboards.getBotTradeMetrics( 10 );
		
		for( BotTradeMetrics btm : tradeMetrics ) {
			assertNotEquals( btm.getBotId( ), 1 );
		}
		
		List< BotLog > logs = botLogs.getLogsFromBot( );
		
		for( BotLog botLog : logs ) {
			assertNotEquals( botLog.getBotId( ), 1 );
		}
		
		userBotsDAO.closeConnection();
		botLeaderboards.closeConnection();
		botLogs.closeConnection();
		TestDatabaseInitializer.resetTestDatabase( );
	}
	
	@Test
	public void testGetAllUserBots( ) throws ClassNotFoundException, SQLException {
		TestDatabaseInitializer.resetTestDatabase( );
		UserBotsDAO userBotsDAO = new UserBotsDAO( "daniel" );
		userBotsDAO.openConnection( );
		List< UserBot > userBots = userBotsDAO.grabAllBots( );
		assertEquals( userBots.size( ), 8 );
		userBotsDAO.closeConnection( );
	}
	
}
