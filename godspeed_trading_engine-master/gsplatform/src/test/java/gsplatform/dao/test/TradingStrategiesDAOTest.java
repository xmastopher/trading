package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserStrategiesPackage;

/**
 * Test class for TradingStrategiesDAO object
 * @author danielanderson
 *
 */
public class TradingStrategiesDAOTest {

	@Test
	/**
	 * Unit test to test querying for the most recent ID
	 */
	public void testGetMaxStrategyID() 
	{
		TradingStrategiesDAO tsd = new TradingStrategiesDAO( "danimal" );
		
		int id;
		try 
		{
			tsd.openConnection( );
			id = tsd.getMaxStrategyID( );
			tsd.closeConnection( );
			assertEquals( id, 3 );
		} 
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Unit test for testing saving a strategy with params
	 */
	public void testSaveStrategy() throws JSONException
	{
		TradingStrategiesDAO tsd = new TradingStrategiesDAO( "danimal" );
		
		JSONArray buySignals = new JSONArray( );
		buySignals.put( new JSONObject( "{signal:SMA-BUY,smaSmaller:5,smaLarger:20}" ) );
		
		JSONArray sellSignals = new JSONArray( );
		sellSignals.put( new JSONObject( "{signal:SMA-SELL,smaSmaller:5,smaLarger:20}" ) );
		
		String targets = null;
		
		try 
		{
			tsd.openConnection( );
			tsd.saveStrategy("SMA Strategy", buySignals.toString( ), sellSignals.toString( ), targets, null );
			tsd.closeConnection( );
			assertEquals( 0, 0 );
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			e.printStackTrace();
			assertEquals( 0, 1 );
		}
	}
	
	@Test
	/**
	 * Unit test for testing saving a strategy with params
	 */
	public void testGetUserStrategies() throws JSONException
	{
		TradingStrategiesDAO tsd = new TradingStrategiesDAO( "danimal" );
		try 
		{
			tsd.openConnection( );
			UserStrategiesPackage usp = tsd.getUserStrategies( );
			tsd.closeConnection( );
			
			assertEquals( usp.getStrategy( 0 ), "SMA-CROSS-BUY" );
			assertEquals( usp.getStrategy( 1 ), "SMA-BUY" );
			
			JSONObject first  = usp.getStrategyByIdAsJSON( 1 );
			assertEquals( first.get( "name"), "SMA-CROSS-BUY" );
			JSONObject second = usp.getStrategyByIdAsJSON( 2 );
			assertEquals( second.get( "name"), "SMA-BUY" );

		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			e.printStackTrace();
			assertEquals( 0, 1 );
		}
	}

}
