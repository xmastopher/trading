package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.junit.Test;

import gsplatform.servlet.database.Conversation;
import gsplatform.servlet.database.Message;
import gsplatform.servlet.database.MessagesDAO;

public class MessagesDAOTest {

	@Test
	public void testGetConversations( ) 
	{
		MessagesDAO messagesDAO = new MessagesDAO( "danimal" );
		
		try 
		{
			messagesDAO.openConnection( );
			List< Conversation > conversations = messagesDAO.getConversations( );
			
			//Grab oldest conversation
			Conversation conversation = conversations.get( 1 );
			
			//Check id, timestamp and conversationee
			assertEquals( conversation.getConversationId( ), 1 );
			assertEquals( conversation.getTimestamp( ).toString( ), "2018-03-09 23:59:59.0" );
			assertEquals( conversation.getUsername( ), "testman" );
			
			//Grab the messages
			Message messageOne = conversation.getMessage( 0 );
			Message messageTwo = conversation.getMessage( 1 );
			
			//Check the subjects
			assertEquals( messageOne.getSubject( ), "HI" );
			assertEquals( messageTwo.getSubject( ), "HI" );
			
			//Check the message text
			assertEquals( messageTwo.getBody( ), "Hello testman!" );
			assertEquals( messageOne.getBody( ), "Whats up danimal?" );
			
			//Check the timestamps
			assertEquals( messageTwo.getTimestamp( ).toString( ), "2018-03-08 23:59:59.0" );
			assertEquals( messageOne.getTimestamp( ).toString( ), "2018-03-09 23:59:59.0" );
			
			//Check to recipients
			assertEquals( messageTwo.getToUser( ), "testman" );
			assertEquals( messageOne.getToUser( ).toString( ), "danimal" );
			
			//Check the originators
			assertEquals( messageTwo.getFromUser( ), "danimal" );
			assertEquals( messageOne.getFromUser( ), "testman" );
			
			
			//Grab the newest conversation
			conversation = conversations.get( 0 );
			
			//Check the id, timestamp and conversationee
			assertEquals( conversation.getConversationId( ), 2 );
			assertEquals( conversation.getTimestamp( ).toString( ), "2018-03-10 12:59:59.0" );
			assertEquals( conversation.getUsername( ), "testman" );
			
			//Update the message - there is only one
			messageOne   = conversation.getMessage( 0 );
			
			//Check all message fields
			assertEquals( messageOne.getSubject( ), "Reminder" );
			assertEquals( messageOne.getBody( ), "We have a secret meeting tomorrow a 9 PM EST" );
			assertEquals( messageOne.getToUser( ), "testman" );
			assertEquals( messageOne.getFromUser( ), "danimal" );
			assertEquals( messageOne.getTimestamp( ).toString( ), "2018-03-10 12:59:59.0" );
			
			messagesDAO.closeConnection( );
		}
		catch (ClassNotFoundException | SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals( true, false );
		}

	}
	
	@Test
	public void testSendAndDeleteMessage( ) 
	{
		MessagesDAO messagesDAO = new MessagesDAO( "danimal" );
		
		try 
		{
			int messageId 	   = 2;
			int conversationId = 2;
			String recipient   = "testman";
			String message     = "Sorry testman, I haven't been on travas in weeks! I missed the meeting :( ROFL!";
			String subject     = "Reminder";
			String timestamp   = new Timestamp( new Date( ).getTime( ) ).toString();
			
			messagesDAO.openConnection( );
			messagesDAO.sendMessage(messageId, conversationId, recipient, message, subject, timestamp);
			messagesDAO.deleteMessage(messageId, conversationId);
			messagesDAO.closeConnection( );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
			assertEquals( true, false );
		}
	}

}
