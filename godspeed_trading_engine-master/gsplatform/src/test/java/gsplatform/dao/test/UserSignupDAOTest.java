package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import gsplatform.servlet.database.UserSignupDAO;
import gsplatform.servlet.database.UserSignupPackage;

public class UserSignupDAOTest {

	@Test
	/**
	 * Test a user signup w/ regular success
	 */
	public void testSignupUserSuccess( ) 
	{
		UserSignupDAO usd = new UserSignupDAO( "christian", "christian@travas.io", "password" );
		
		try 
		{
			usd.openConnection( );
			UserSignupPackage usp = usd.signupUser( );
			assertEquals( true, true );
			usd.closeConnection( );
			
		} 
		
		catch (ClassNotFoundException | SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Test a user signup when the username already exists
	 */
	public void testSignupUsernameAlreadyExists( ) 
	{
		UserSignupDAO usd = new UserSignupDAO( "danimal", "christian@travas.io", "password" );
		
		try 
		{
			usd.openConnection( );
			UserSignupPackage usp = usd.signupUser( );
			assertEquals(usp.getSuccess( ), false );  
			assertEquals( usp.getMessage( ), "0002: Username is already taken" );
			usd.closeConnection( );
		} 
		catch ( ClassNotFoundException | SQLException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace( );
		}
	}
	
	@Test
	/**
	 * Test a user signup when the email already exists
	 */
	public void testSignupEmailAlreadyExists( ) 
	{
		UserSignupDAO usd = new UserSignupDAO( "fdaogewajf", "dtanderson005@gmail.com", "password" );
		
		try 
		{	
			usd.openConnection( );
			UserSignupPackage usp = usd.signupUser( );
			assertEquals(usp.getSuccess( ), false );  
			assertEquals( usp.getMessage( ), "0001: Email is already taken" );
			usd.closeConnection( );
		}
		
		catch ( ClassNotFoundException | SQLException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace( );
		}
	}
	
	@Test
	/**
	 * Test a user verification when the tokens match
	 * @throws SQLException
	 */
	public void testVerifyUserSuccess( )
	{
		
		UserSignupDAO usd = new UserSignupDAO( "testman", "testman@gmail.com" );
		
		try 
		{
			usd.openConnection( );
			Boolean success = usd.verifyUser( "testman@gmail.com", "1f23" ); 
			assertEquals( success, true );
			usd.closeConnection( );
		}
		
		catch ( SQLException | ClassNotFoundException e ) 
		{
			assertEquals( true, false );
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Test a user verification when the tokens don't match
	 */
	public void testVerifyUserFail( )
	{
		UserSignupDAO usd = new UserSignupDAO( "testman", "testman@gmail.com" );
		
		try 
		{
			usd.openConnection( );
			Boolean success = usd.verifyUser( "testman@gmail.com", "1f20" );
			assertEquals( success, false );
			usd.closeConnection( );
		} 
		
		catch ( SQLException e ) 
		{
			assertEquals( true, false );
			e.printStackTrace();
		} 
		
		catch ( ClassNotFoundException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
