package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.servlet.database.PortfolioStateDAO;

public class PortfolioStateDAOTest {

	/**
	 * Test grabbing totalValue, totalInvestment and fiatType from PortfolioState table
	 * @throws JSONException 
	 */
	@Test
	public void testGetPortfolioSnapshot( ) throws JSONException 
	{
		String snapshot = "2018-02-25";
		String badDate  = "2018-02-24";
		String username = "danimal";
		String database = "godspeedTest";
		
		try 
		{
			PortfolioStateDAO psd  = new PortfolioStateDAO( username, snapshot,  database );
			psd.openConnection( );
			String portfolio       = psd.getPortfolio( );
			
			JSONObject portfolioJSON = new JSONObject( portfolio );
			
			JSONArray data = portfolioJSON.getJSONArray( "data" );
			
			JSONObject omg = data.getJSONObject( 0 );
			JSONObject eth = data.getJSONObject( 1 );
			
			String omgSymbol     = omg.getString( "symbol" );
			String omgProportion = omg.getJSONArray( "tokenProportions" ).getString( 0 );
			double tokenBalance  = omg.getDouble("tokenBalance");
			String totalBalance  = omg.getString("totalBalance");
			String portionOfPort = omg.getString("portionOfPortfolio");
			String tokenLocation = omg.getJSONArray("tokenLocations").getString( 0 );
			double balanceAtLoc  = omg.getJSONArray("tokenBalances").getDouble( 0 );
			
			assertEquals( omgSymbol, "OMG" );
			assertEquals( omgProportion, "100.00%" );
			assertEquals( tokenBalance, 25.5, 3 );
			assertEquals( totalBalance, "$433.5" );
			assertEquals(portionOfPort, "37.00%" );
			assertEquals( tokenLocation, "Binance" );
			assertEquals( balanceAtLoc, 25.5, 3);
			
			assertNotNull( eth );
			
			//Check for failure when snap does not exist
			PortfolioStateDAO psd2    = new PortfolioStateDAO( username, badDate,  database );
			String portfolio2         = psd2.getPortfolio( );
			
			assertEquals( portfolio2, "Failure:snapshot does not exist" );
			psd.closeConnection( );
			
			
		} 
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
