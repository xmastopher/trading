package gsplatform.dao.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import gsplatform.servlet.database.UserLoginDAO;
import gsplatform.servlet.database.UserLoginPackage;

/**
 * Test cases for {@link UserLoginDAO} class
 * @author danielanderson
 *
 */
public class UserLoginDAOTest {


	
	/**
	 * Test logging in a user with success using username
	 */
	@Test
	public void testLoginUsername( ) 
	{
		String username = "danimal";
		String password = "asstits";
		
		UserLoginDAO userLoginDao = new UserLoginDAO( username, password );
		UserLoginPackage userLoginPackage;
		try 
		{
			userLoginDao.openConnection( );
			userLoginPackage = userLoginDao.authenticateUser( );
			
			String usernameAfter = userLoginPackage.getUsername( );
			String emailAfter    = userLoginPackage.getEmail( );
			Boolean success      = userLoginPackage.getIsSuccessful( );
			String message       = userLoginPackage.getMessage( );
			
			assertEquals( usernameAfter, "danimal" );
			assertEquals( emailAfter, "dtanderson005@gmail.com" );
			assertEquals( success, true );
			assertEquals( message, "0000: User login successful" );
			
			userLoginDao.closeConnection( );
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	/**
	 * Test logging in a user with success using email address
	 */
	public void testLoginEmail( ) 
	{
		String username = "dtanderson005@gmail.com";
		String password = "asstits";
		
		UserLoginDAO userLoginDao = new UserLoginDAO( username, password );
		
		UserLoginPackage userLoginPackage;
		try 
		{
			userLoginDao.openConnection( );
			userLoginPackage = userLoginDao.authenticateUser( );
			
			String usernameAfter = userLoginPackage.getUsername( );
			String emailAfter    = userLoginPackage.getEmail( );
			Boolean success      = userLoginPackage.getIsSuccessful( );
			String message       = userLoginPackage.getMessage( );
			
			assertEquals( usernameAfter, "danimal" );
			assertEquals( emailAfter, "dtanderson005@gmail.com" );
			assertEquals( success, true );
			assertEquals( message, "0000: User login successful" );
			userLoginDao.closeConnection( );
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Test
	/**
	 * Test attempting to log a user in with the wrong username
	 */
	public void testLoginNoUsername( ) 
	{
		String username = "daniman";
		String password = "asstits";
		
		UserLoginDAO userLoginDao = new UserLoginDAO( username, password );
		
		UserLoginPackage userLoginPackage;
		try 
		{
			userLoginDao.openConnection( );
			userLoginPackage = userLoginDao.authenticateUser( );
			Boolean success      = userLoginPackage.getIsSuccessful( );
			String message       = userLoginPackage.getMessage( );
			assertEquals( success, false );
			assertEquals( message, "0003: Username does not exist in system" );
			userLoginDao.closeConnection( );
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Test attempting to log a user in with wrong email
	 */
	public void testLoginNoEmail( ) 
	{
		String username = "daniel@tuts.com";
		String password = "asstits";
		
		UserLoginDAO userLoginDao = new UserLoginDAO( username, password );
		
		UserLoginPackage userLoginPackage;
		try 
		{
			userLoginDao.openConnection( );
			userLoginPackage = userLoginDao.authenticateUser( );
			Boolean success      = userLoginPackage.getIsSuccessful( );
			String message       = userLoginPackage.getMessage( );
			assertEquals( success, false );
			assertEquals( message, "0002: Email does not exist in system");
			userLoginDao.closeConnection( );
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Test attempting to log a user in with the wrong password
	 */
	public void testLoginWrongPasswordEmail( ) 
	{
		String username = "dtanderson005@gmail.com";
		String password = "asstats";
		
		UserLoginDAO userLoginDao = new UserLoginDAO( username, password );
		
		UserLoginPackage userLoginPackage;
		try 
		{
			userLoginDao.openConnection( );
			userLoginPackage = userLoginDao.authenticateUser( );
			Boolean success      = userLoginPackage.getIsSuccessful( );
			String message       = userLoginPackage.getMessage( );
			assertEquals( success, false );
			assertEquals( message, "0001: Incorrect password" );
			userLoginDao.closeConnection( );
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		}
	}

	@Test
	/**
	 * Test attempting to log a user in with the wrong password
	 */
	public void testLoginWrongPasswordUsername( ) 
	{
		String username = "danimal";
		String password = "asstats";
		
		UserLoginDAO userLoginDao = new UserLoginDAO( username, password );
		
		UserLoginPackage userLoginPackage;
		try 
		{
			userLoginDao.openConnection( );
			userLoginPackage = userLoginDao.authenticateUser( );
			Boolean success      = userLoginPackage.getIsSuccessful( );
			String message       = userLoginPackage.getMessage( );
			assertEquals( success, false );
			assertEquals( message, "0001: Incorrect password" );
			userLoginDao.closeConnection( );
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			assertEquals( true, false );
			e.printStackTrace();
		}
	}
}
