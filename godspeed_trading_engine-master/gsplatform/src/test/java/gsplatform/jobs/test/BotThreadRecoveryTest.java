package gsplatform.jobs.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import gsplatform.jobs.BotThreadRecovery;
import gsplatform.services.BotThread;
import gsplatform.services.BotThreadHandler;
import gsplatform.services.SimulatedAlgorithmicTradingBot;
import gsplatform.utilities.TestDatabaseInitializer;

public class BotThreadRecoveryTest {

	@Before
	public void setupBefore( ) {
		TestDatabaseInitializer.resetTestDatabase( );
		BotThreadHandler.resetSingleton();
	}
	
	@After
	public void setupAfter( ) {
		TestDatabaseInitializer.resetTestDatabase( );
		BotThreadHandler.resetSingleton();
	}
	
	@Test
	public void testBotRecoveryBuySell( ) {
		
		String exchange = "bittrex";
		String market   = "BTC-USDT";
		String kline    = "1d";
		new BotThreadRecovery( ).run( );
		
		BotThread botThread = BotThreadHandler.getSingletonInstance( ).getThread( exchange, kline, market, true );
		
		SimulatedAlgorithmicTradingBot sim = (SimulatedAlgorithmicTradingBot) botThread.getBots( ).get( 0 );

		assertEquals( "Simulated trader '6' should be added to thread bittrex-usdt-btc", 
					   botThread.getBots( ).size( ), 1 );
		
		assertEquals( "Simulated trader '6' is the bot on thread bittrex-usdt-btc", 
				   	   sim.getBotId( ), 6 );
		
		assertEquals( "Quote currency should be updated with value from database.", 
					  sim.getUserWallet( ).getBalance( "quote" ).doubleValue( ), 1.5, 0 );
		
		assertEquals( "Base currency should be updated with value from database.", 
				  	  sim.getUserWallet( ).getBalance( "" ).doubleValue( ), 0, 0 );
		
		assertTrue( "Simulted trader should be waiting to buy.", 
				    sim.getTradingAlgorithm( ).getWaitingToBuy( ) );
		

	}
	
	@Test
	public void testBotRecoveryBuyOnly( ) {
		
		String exchange = "gemini";
		String market   = "ETH-USD";
		String kline    = "1h";
		new BotThreadRecovery( ).run( );
		
		BotThread botThread = BotThreadHandler.getSingletonInstance( ).getThread( exchange, kline, market, true );
		
		SimulatedAlgorithmicTradingBot sim = (SimulatedAlgorithmicTradingBot) botThread.getBots( ).get( 0 );

		assertEquals( "Simulated trader '7' should be added to thread gemini-ethusd", 
					   botThread.getBots( ).size( ), 1 );
		
		assertEquals( "Simulated trader '7' is the bot on thread gemini-ethusd", 
				   	   sim.getBotId( ), 7 );
		
		assertEquals( "Quote currency should be updated with value from database.", 
					  sim.getUserWallet( ).getBalance( "quote" ).doubleValue( ), 0, 0 );
		
		assertEquals( "Base currency should be updated with value from database.", 
				  	  sim.getUserWallet( ).getBalance( "" ).doubleValue( ), 26.30793659, 0 );
		
		assertTrue( "Simulted trader should be waiting to sell.", 
				    sim.getTradingAlgorithm( ).getWaitingToSell( ) );

	}
	
	@Test
	public void testBotRecoveryNullAction( ) {
	
		String exchange = "gemini";
		String market   = "BTC-USD";
		String kline    = "1d";
		new BotThreadRecovery( ).run( );

		BotThread botThread = BotThreadHandler.getSingletonInstance( ).getThread( exchange, kline, market, true );
		
		SimulatedAlgorithmicTradingBot sim = (SimulatedAlgorithmicTradingBot) botThread.getBots( ).get( 0 );

		assertEquals( "Simulated trader '8' should be added to thread gemini-btc-usd", 
					   botThread.getBots( ).size( ), 1 );
		
		assertEquals( "Simulated trader '8' is the bot on thread gemini-btc-usd", 
				   	   sim.getBotId( ), 8 );
		
		assertEquals( "Quote currency should be updated with value from database.", 
					  sim.getUserWallet( ).getBalance( "quote" ).doubleValue( ), 1, 0 );
		
		assertEquals( "Base currency should be updated with value from database.", 
				  	  sim.getUserWallet( ).getBalance( "" ).doubleValue( ), 0, 0 );
		
		assertTrue( "Simulted trader should be waiting to buy.", 
				    sim.getTradingAlgorithm( ).getWaitingToBuy( ) );

	}
	
	@Test
	public void testBotRecoveryHold( ) {
	
		String exchange = "bittrex";
		String market   = "ETH-USDT";
		String kline    = "1h";
		new BotThreadRecovery( ).run( );

		BotThread botThread = BotThreadHandler.getSingletonInstance( ).getThread( exchange, kline, market, true );
		
		SimulatedAlgorithmicTradingBot sim = (SimulatedAlgorithmicTradingBot) botThread.getBots( ).get( 0 );

		assertEquals( "Simulated trader '5' should be added to thread gemini-btc-usd", 
					   botThread.getBots( ).size( ), 1 );
		
		assertEquals( "Simulated trader '5' is the bot on thread gemini-btc-usd", 
				   	   sim.getBotId( ), 5 );
		
		assertEquals( "Quote currency should be updated with value from database.", 
					  sim.getUserWallet( ).getBalance( "quote" ).doubleValue( ), 1, 0 );
		
		assertEquals( "Base currency should be updated with value from database.", 
				  	  sim.getUserWallet( ).getBalance( "" ).doubleValue( ), 0, 0 );
		
		assertTrue( "Simulted trader should be waiting to buy.", 
				    sim.getTradingAlgorithm( ).getWaitingToBuy( ) );

	}

}
