package gsplatform.services.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.BotSummaryAPI;

public class BotSummaryAPITest {

	@Test
	public void testApiCall( ) throws JSONException, IOException 
	{
		JSONObject request = new JSONObject( );
		request.put( "username", "daniel" );
		
		BotSummaryAPI botSummaryApi = new BotSummaryAPI( request );
		
		JSONObject response = botSummaryApi.createJSON( );
		assertNotNull( response );
	}

}
