package gsplatform.services.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import gsplatform.algorithms.SignalStrategy;
import gsplatform.services.AutomatedTrader;
import gsplatform.services.BotThread;
import gsplatform.services.BotThreadHandler;
import gsplatform.services.SimulatedAlgorithmicTradingBot;
import gsplatform.signals.SMABuySignal;
import gsplatform.signals.SMASellSignal;
import gsplatform.utilities.TestDatabaseInitializer;

public class BotThreadHandlerTest {

	@Before
	public void setupBefore( ) {
		TestDatabaseInitializer.resetTestDatabase( );
		BotThreadHandler.resetSingleton( );
	}
	
	@After
	public void setupAfter( ) {
		TestDatabaseInitializer.resetTestDatabase( );
		BotThreadHandler.resetSingleton( );
	}
	
	@Test
	public void testAddBot( ) throws Exception 
	{
		String exchange = "binance";
		String market   = "ETH-USDT";
		String kline    = "1h";
		
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ),
											   new SMABuySignal( 20 ), 
											   new SMASellSignal( 20 )  );
		
		SignalStrategy ss1 = new SignalStrategy( new BigDecimal( .1 ),
											    new SMABuySignal( 20 ), 
											    new SMASellSignal( 20 )  );
		
		SignalStrategy ss2 = new SignalStrategy( new BigDecimal( .1 ),
				   							    new SMABuySignal( 20 ), 
				   							    new SMASellSignal( 20 )  );

		SimulatedAlgorithmicTradingBot sim  = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "danimal", 1, "binance", null );
		SimulatedAlgorithmicTradingBot sim2 = new SimulatedAlgorithmicTradingBot( ss1, "ETH", "USDT", "danimal", 2, "binance", null );
		SimulatedAlgorithmicTradingBot sim3 = new SimulatedAlgorithmicTradingBot( ss2, "ETH", "USDT", "danimal", 3, "binance", null );
		
		//BotThreadHandler.resetSingletonInstance( );
		BotThreadHandler bth = BotThreadHandler.getSingletonInstance( );
		
		bth.addBot(exchange, kline, market, sim, true );
		bth.addBot(exchange, kline, market, sim2, true );
		bth.addBot(exchange, kline, market, sim3, true );
		
		BotThread botThread = bth.getThread( exchange, kline, market, true );
		List< AutomatedTrader > list = botThread.getBots( );
		assertEquals( list.size( ), 3 );
	}
	
	@Test
	public void testRemoveBot( ) throws Exception 
	{
		String exchange = "binance";
		String market   = "ETH-USDT";
		String kline    = "1h";
		
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ),
											   new SMABuySignal( 20 ), 
											   new SMASellSignal( 20 )  );
		
		SignalStrategy ss1 = new SignalStrategy( new BigDecimal( .1 ),
											    new SMABuySignal( 20 ), 
											    new SMASellSignal( 20 )  );
		
		SignalStrategy ss2 = new SignalStrategy( new BigDecimal( .1 ),
				   							    new SMABuySignal( 20 ), 
				   							    new SMASellSignal( 20 )  );

		SimulatedAlgorithmicTradingBot sim  = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "daniel", 1, "binance", null );
		SimulatedAlgorithmicTradingBot sim2 = new SimulatedAlgorithmicTradingBot( ss1, "ETH", "USDT", "daniel", 2, "binance", null );
		SimulatedAlgorithmicTradingBot sim3 = new SimulatedAlgorithmicTradingBot( ss2, "ETH", "USDT", "daniel", 3, "binance", null );
		
		BotThreadHandler bth = BotThreadHandler.getSingletonInstance( );
		
		bth.addBot(exchange, kline, market, sim, true );
		bth.addBot(exchange, kline, market, sim2, true );
		bth.addBot(exchange, kline, market, sim3, true );
		
		BotThread botThread = bth.getThread( exchange, kline, market, true );
		List< AutomatedTrader > list = botThread.getBots( );
		assertEquals( list.size( ), 3 );
		
		bth.removeBot( "daniel", 2, exchange, kline, market, true );
		
		list = botThread.getBots( );
		assertEquals( list.size( ), 2 );
		
		for( AutomatedTrader at : list )
		{
			int botId = at.getBotId( );
			assertNotEquals( 2, botId );
		}
	} 
	
	
	@Test
	public void testGenerateKey( ) 
	{
		String exchange = "binance";
		String market   = "ETH-USDT";
		String kline    = "1h";
		BotThreadHandler bth = BotThreadHandler.getSingletonInstance( );
		
		assertEquals( bth.generateKey(exchange, kline, market), "binance-1h-ETH-USDT" );
	}
	
	@Test
	public void stopAllThreads( ) throws Exception {
		String exchange = "binance";
		String market   = "ETH-USDT";
		String kline    = "1h";
		
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ),
											   new SMABuySignal( 20 ), 
											   new SMASellSignal( 20 )  );
		
		SignalStrategy ss1 = new SignalStrategy( new BigDecimal( .1 ),
											    new SMABuySignal( 20 ), 
											    new SMASellSignal( 20 )  );
		
		SignalStrategy ss2 = new SignalStrategy( new BigDecimal( .1 ),
				   							    new SMABuySignal( 20 ), 
				   							    new SMASellSignal( 20 )  );

		SimulatedAlgorithmicTradingBot sim  = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "danimal", 1, "binance", null );
		SimulatedAlgorithmicTradingBot sim2 = new SimulatedAlgorithmicTradingBot( ss1, "ETH", "USDT", "danimal", 2, "binance", null );
		SimulatedAlgorithmicTradingBot sim3 = new SimulatedAlgorithmicTradingBot( ss2, "ETH", "USDT", "danimal", 3, "binance", null );
		
		
		BotThreadHandler bth = BotThreadHandler.getSingletonInstance( );
		
		bth.addBot(exchange, kline, market, sim, true );
		bth.addBot(exchange, kline, market, sim2, true );
		bth.addBot(exchange, kline, market, sim3, true );
		
		bth.stopAlBotThreads( );
		
		assertTrue( bth.getThread( exchange, kline, market, true ).getBots( ).size( ) == 0 );
	}

}
