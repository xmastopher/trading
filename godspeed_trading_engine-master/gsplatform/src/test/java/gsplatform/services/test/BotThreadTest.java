package gsplatform.services.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.junit.Test;
import gsplatform.algorithms.SignalStrategy;
import gsplatform.services.AutomatedTrader;
import gsplatform.services.BotThread;
import gsplatform.services.SimulatedAlgorithmicTradingBot;
import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.signals.SMABuySignal;
import gsplatform.signals.SMASellSignal;
import gsplatform.utilities.ISO8601;
import gsplatform.utilities.TestDatabaseInitializer;

public class BotThreadTest {

	
	@Test
	public void testAddBot( ) throws Exception 
	{
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ),
				   		    					   new SMABuySignal( 20 ), 
				   		    					   new SMASellSignal( 20 )  );

		SignalStrategy ss1 = new SignalStrategy( new BigDecimal( .1 ),
												new SMABuySignal( 20 ), 
												new SMASellSignal( 20 )  );
		
		SignalStrategy ss2 = new SignalStrategy( new BigDecimal( .1 ),
						     					new SMABuySignal( 20 ), 
						     					new SMASellSignal( 20 )  );

		SimulatedAlgorithmicTradingBot sim  = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "daniel", 1, "binance", null );
		SimulatedAlgorithmicTradingBot sim2 = new SimulatedAlgorithmicTradingBot( ss1, "ETH", "USDT", "daniel", 2, "binance", null );
		SimulatedAlgorithmicTradingBot sim3 = new SimulatedAlgorithmicTradingBot( ss2, "ETH", "USDT", "daniel", 3, "binance", null );
		
		BotThread botThread = new BotThread( "1h", "binance", "ETH-USDT" );
		
		botThread.addBot( sim );
		botThread.addBot( sim2 );
		botThread.addBot( sim3 );
		
		List< AutomatedTrader > automatedTraders = botThread.getBots( );
		assertEquals( automatedTraders.size( ), 3 );
		
		
	}

	@Test
	public void testRemoveBot( ) throws Exception 
	{
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ),
											   new SMABuySignal( 20 ), 
											   new SMASellSignal( 20 )  );

		SimulatedAlgorithmicTradingBot sim  = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "daniel", 1, "binance", null );
		SimulatedAlgorithmicTradingBot sim2 = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "daniel", 2, "binance", null );
		SimulatedAlgorithmicTradingBot sim3 = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "daniel", 3, "binance", null );
		
		BotThread botThread = new BotThread( "1h", "binance", "ETH-USDT" );
		
		botThread.addBot( sim );
		botThread.addBot( sim2 );
		botThread.addBot( sim3 );
		botThread.removeBot( "daniel", 2 );
		
		List< AutomatedTrader > automatedTraders = botThread.getBots( );
		assertEquals( automatedTraders.size( ), 2 );
		
		for( AutomatedTrader at : automatedTraders )
		{
			int botId = at.getBotId( );
			assertNotEquals( 2, botId );
		}
		
	}
	
	@Test
	public void testAssessTrades( ) throws Exception
	{
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ),
											   new SMABuySignal( 20 ), 
											   new SMASellSignal( 20 )  );

		SignalStrategy ss1 = new SignalStrategy( new BigDecimal( .1 ),
												new SMABuySignal( 20 ), 
												new SMASellSignal( 20 )  );

		SignalStrategy ss2 = new SignalStrategy( new BigDecimal( .1 ),
												new SMABuySignal( 20 ), 
												new SMASellSignal( 20 ) );

		SimulatedAlgorithmicTradingBot sim  = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USDT", "daniel", 1, "binance", null );
		SimulatedAlgorithmicTradingBot sim2 = new SimulatedAlgorithmicTradingBot( ss1, "ETH", "USDT", "daniel", 2, "binance", null );
		SimulatedAlgorithmicTradingBot sim3 = new SimulatedAlgorithmicTradingBot( ss2, "ETH", "USDT", "daniel", 3, "binance", null );
		
		BotThread botThread = new BotThread( "1h", "binance", "ETH-USDT" );
		
		botThread.addBot( sim );
		botThread.addBot( sim2 );
		botThread.addBot( sim3 );
		
		botThread.assessTrades( );
		assertEquals( true, true );
		
		botThread.assessTrades( );
		assertEquals( true, true );
		
		botThread.assessTrades( );
		assertEquals( true, true );
		
	}
	
	@Test
	public void testCalculateStartTime() throws ClassNotFoundException, IOException, SQLException, ParseException, JSONException 
	{
		
		BotThread botThread = new BotThread( "1m", "binance", "ETH-USDT" );
		Date startTime      = botThread.calculateStartTime( "2018-04-22T03:00:00Z" );
		Date checkedTime    = ISO8601.toCalendar( "2018-04-22T03:01:00Z" ).getTime( );
		assertEquals( startTime, checkedTime );
		
	}
	
	@Test(timeout=11000)
	public void testSynchronization( ) throws ParseException, ClassNotFoundException, SQLException, JSONException, IOException
	{
		String now   = ISO8601.fromCalendar( Calendar.getInstance( ) );
		Calendar cal = ISO8601.toCalendar( now );
		cal.add( Calendar.MILLISECOND, 10000 );
		Date startTime = cal.getTime( );
		BotThread botThread = new BotThread( "1m", "binance", "ETH-USDT" );
		botThread.synchronize(  startTime );
	}
	
	@Test
	public void testWriteDB( ) throws ClassNotFoundException, SQLException, JSONException, IOException {

		TestDatabaseInitializer.resetTestDatabase( );
		
		BotLog botLogOne = new BotLog( 1, "daniel", "BUY", new BigDecimal( 20000 ), new BigDecimal( 0 ), 
				new BigDecimal( 120492089 ), new BigDecimal( .1 ), "{}", new Timestamp( new Date( ).getTime( ) ) );
		
		BotLog botLogTwo = new BotLog( 2, "daniel", "BUY", new BigDecimal( 20000 ), new BigDecimal( 0 ), 
				new BigDecimal( 120492089 ), new BigDecimal( .1 ), "{}", new Timestamp( new Date( ).getTime( ) ) );
		
		BigDecimal roiOne = new BigDecimal( 100 );
		
		BigDecimal roiTwo = new BigDecimal( -.100 );
		
		List< BotLog > botLogs = new ArrayList< BotLog >( );
		botLogs.add( botLogOne );
		botLogs.add( botLogTwo );
		
		List< BigDecimal > rois = new ArrayList< BigDecimal >( );
		rois.add( roiOne );
		rois.add( roiTwo );
		
		BotThread botThread = new BotThread( "1d","gdax","ETH-USD" );
		botThread.writeDB( botLogs, rois );
		
		//Setup DAO
		BotLogsDAO botLogsDAO = new BotLogsDAO( );
		BotLeaderboardsDAO botLeaderboardsDAO = new BotLeaderboardsDAO( "daniel" );
		botLogsDAO.openConnection( );
		botLeaderboardsDAO.openConnection( );
		
		//Grab new values
		botLogsDAO.setUsernameAndId( "daniel", 1 );
		List< BotLog > botLogsReturned = botLogsDAO.getLogsFromBot( );
		assertEquals( botLogsReturned.size( ), 12 );
		
		TestDatabaseInitializer.resetTestDatabase( );
	}

	
}
