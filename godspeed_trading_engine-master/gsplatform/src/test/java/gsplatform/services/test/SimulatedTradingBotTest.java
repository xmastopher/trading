package gsplatform.services.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import org.junit.Test;
import gsplatform.algorithms.SignalStrategy;
import gsplatform.services.SimulatedAlgorithmicTradingBot;
import gsplatform.servlet.database.BotLog;
import gsplatform.signals.TargetBuySignal;
import gsplatform.signals.TargetSellSignal;
import gsplatform.utilities.TradingParameters;

public class SimulatedTradingBotTest {

	@Test
	public void testTradeComplete( ) throws Exception 
	{
		//Setup strategy and sim
		SignalStrategy ss = new SignalStrategy( new BigDecimal( .1 ), new TargetBuySignal( .1 ), new TargetSellSignal( .1 ) );
		ss.setWaitingToBuy( true );
		SimulatedAlgorithmicTradingBot sim = new SimulatedAlgorithmicTradingBot( ss, "ETH", "USD", "daniel", 1, "gdax", "MARKET" );
		
		TradingParameters tradingParameters =  new TradingParameters( );
		
		//Set close - last sell is automatically set internally - so we should auto buy!
		tradingParameters.updateValue( "close", "10.1" );
		tradingParameters.updateValue( "last_price", "10.1" );
		tradingParameters.updateValue( "timestamp", "2018-03-19T10:00:00Z" );
		sim.doIteration( tradingParameters );
		assertEquals( sim.tradeComplete( ), false );
		
		//Should trigger sell check for trade completion
		tradingParameters.updateValue( "close", "12" );
		tradingParameters.updateValue( "last_price", "12" );
		tradingParameters.updateValue( "timestamp", "2018-03-19T10:00:00Z" );
		BotLog botLog = sim.doIteration( tradingParameters );
		assertEquals( sim.tradeComplete( ), true );
		BigDecimal tradeROI = sim.getTradeROI( );
		assertNotNull(botLog);
		assertNotNull(tradeROI);
		
	}
	

}
