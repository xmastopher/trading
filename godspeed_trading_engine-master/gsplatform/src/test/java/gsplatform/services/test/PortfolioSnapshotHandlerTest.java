package gsplatform.services.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.services.PortfolioSnapshotHandler;

public class PortfolioSnapshotHandlerTest {

	@Test
	/**
	 * Test grabbing the historical snapshot via SnaphshotHandler service
	 */
	public void testGetSnapshot( ) 
	{
		//Grab portfolio from service provider
		PortfolioSnapshotHandler psh;
		try 
		{
			psh = new PortfolioSnapshotHandler( "danimal", "2018-02-25" );
			String portfolio             = psh.getPortfolioSnapshot( );
			
			//Convert to json
			JSONObject portfolioJSON = new JSONObject( portfolio );
			
			//Grab all fields
			JSONArray data = portfolioJSON.getJSONArray( "data" );
			
			JSONObject omg = data.getJSONObject( 0 );
			JSONObject eth = data.getJSONObject( 1 );
			
			String omgSymbol     = omg.getString( "symbol" );
			String omgProportion = omg.getJSONArray( "tokenProportions" ).getString( 0 );
			double tokenBalance  = omg.getDouble("tokenBalance");
			String totalBalance  = omg.getString("totalBalance");
			String portionOfPort = omg.getString("portionOfPortfolio");
			String tokenLocation = omg.getJSONArray("tokenLocations").getString( 0 );
			double balanceAtLoc  = omg.getJSONArray("tokenBalances").getDouble( 0 );
			
			//Assert all fields
			assertEquals( omgSymbol, "OMG" );
			assertEquals( omgProportion, "100.00%" );
			assertEquals( tokenBalance, 25.5, 3 );
			assertEquals( totalBalance, "$433.5" );
			assertEquals(portionOfPort, "37.00%" );
			assertEquals( tokenLocation, "Binance" );
			assertEquals( balanceAtLoc, 25.5, 3);
			
			assertNotNull( eth );
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
