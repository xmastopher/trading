package gsplatform.services.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.ArbitrageAPI;

public class TestArbitrageMaster {

	@Test
	public void testArbitrageSpread( )
	{
		JSONObject request = new JSONObject( );
		
		try {
			request.put( "exchangeOne", "poloniex" );
			request.put( "exchangeTwo", "binance" );
			request.put( "count", 10 );
			
			ArbitrageAPI arbitrageAPI = new ArbitrageAPI(request  ); 
			JSONObject response 		 = arbitrageAPI.createJSON( );
			System.out.println( response );
			assertEquals( true, true );
		} 
		catch ( JSONException | IOException e )
		{
			assertEquals( true, false );
		}
	}

}
