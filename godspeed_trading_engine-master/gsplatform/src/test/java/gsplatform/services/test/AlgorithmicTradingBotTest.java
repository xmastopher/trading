package gsplatform.services.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import org.junit.Test;
import org.knowm.xchange.binance.service.BinanceCancelOrderParams;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.trade.TradeService;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParamCurrencyPair;
import gsplatform.services.AlgorithmicTradingBot;

public class AlgorithmicTradingBotTest {

	@Test
	public void testBuyOrderBinance( ) throws Exception {
		
		AlgorithmicTradingBot atb = new AlgorithmicTradingBot( "danimal", 1, "binance", "BNB", "ETH" );
		BigDecimal amount = new BigDecimal( 100 ).setScale( 8, BigDecimal.ROUND_DOWN );
		BigDecimal rate   = new BigDecimal( .0001 ).setScale( 4, BigDecimal.ROUND_DOWN );;
		atb.doBuyOrder( amount, rate, new CurrencyPair( "BNB", "ETH" ) );
		
		TradeService ts                           = atb.getTradeService( );
		DefaultOpenOrdersParamCurrencyPair params = new DefaultOpenOrdersParamCurrencyPair( new CurrencyPair( "BNB", "ETH" ) );
	
		 
		LimitOrder limitOrder = ts.getOpenOrders( params ).getOpenOrders( ).get( 0 );
		BigDecimal orderAmount = limitOrder.getOriginalAmount( );
		
		assertEquals( orderAmount.compareTo( amount ), 0 );
		BinanceCancelOrderParams bcop = new BinanceCancelOrderParams( new CurrencyPair( "BNB", "ETH" ), limitOrder.getId( ) );
		ts.cancelOrder( bcop );
	}
	
	@Test
	public void testSellOrder( ) throws Exception
	{
		AlgorithmicTradingBot atb = new AlgorithmicTradingBot( "danimal", 1, "binance", "ETH", "BTC" );
		BigDecimal amount = new BigDecimal( .01 ).setScale( 8, BigDecimal.ROUND_DOWN );
		BigDecimal rate   = new BigDecimal( 1 ).setScale( 4, BigDecimal.ROUND_DOWN );;
		atb.doSellOrder( amount, rate, new CurrencyPair( "ETH", "BTC" ) );
		
		TradeService ts                           = atb.getTradeService( );
		DefaultOpenOrdersParamCurrencyPair params = new DefaultOpenOrdersParamCurrencyPair( new CurrencyPair( "ETH", "BTC" ) );
	
		 
		LimitOrder limitOrder = ts.getOpenOrders( params ).getOpenOrders( ).get( 0 );
		BigDecimal orderAmount = limitOrder.getOriginalAmount( );
		
		assertEquals( orderAmount.compareTo( amount ), 0 );
		BinanceCancelOrderParams bcop = new BinanceCancelOrderParams( new CurrencyPair( "ETH", "BTC" ), limitOrder.getId( ) );
		ts.cancelOrder( bcop );
	}
	
	@Test
	public void testCalculateTradingAmount( ) throws Exception
	{
		AlgorithmicTradingBot atb = new AlgorithmicTradingBot( "danimal", 1, "binance", "ETH", "BTC" );
		atb.setTradingPortion( .5 );
		
		//amount should be 10 - using half of the stack
		BigDecimal amount = atb.calculateTradingAmount( new BigDecimal( .1 ), new Currency( "BTC" ), new BigDecimal( 2 ) );
		
		assertEquals( amount.toPlainString( ), "10" );
		
		//Try for sell - should sell all ten at .2 so => 10 
		amount = atb.calculateTradingAmount( new BigDecimal( .2 ), new Currency( "ETH" ), new BigDecimal( 10 ) );
		assertEquals( amount.toPlainString( ), "10" );
	}
	
	@Test
	public void testWaitForFill( ) throws Exception
	{
		AlgorithmicTradingBot atb = new AlgorithmicTradingBot( "danimal", 1, "binance", "ETH", "BTC" );
		BigDecimal amount = new BigDecimal( .01 ).setScale( 8, BigDecimal.ROUND_DOWN );
		BigDecimal rate   = new BigDecimal( 1 ).setScale( 4, BigDecimal.ROUND_DOWN );;
		atb.doSellOrder( amount, rate, new CurrencyPair( "ETH", "BTC" ) );
		
		for( int i = 0; i < 2; i ++ )
		{
			assertEquals( atb.checkFill( ), true );
		}
			
		TradeService ts                           = atb.getTradeService( );
		DefaultOpenOrdersParamCurrencyPair params = new DefaultOpenOrdersParamCurrencyPair( new CurrencyPair( "ETH", "BTC" ) );
		
		LimitOrder limitOrder         = ts.getOpenOrders( params ).getOpenOrders( ).get( 0 );
		BinanceCancelOrderParams bcop = new BinanceCancelOrderParams( new CurrencyPair( "ETH", "BTC" ), limitOrder.getId( ) );
		ts.cancelOrder( bcop );
		assertEquals( atb.checkFill( ), false );
		 
	}
	
	

}
