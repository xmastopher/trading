package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.SMACrossBuySignal;
import gsplatform.signals.SMACrossSignal;
import gsplatform.utilities.TradingParameters;

public class SMACrossBuySignalTest {

	@Test
	public void testBuySignal( )
	{
		
		SMACrossSignal smaSignal = new SMACrossBuySignal( 5, 7 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		Integer initialOpen  = 1;
		Integer initialClose = 2;
		Integer largerOpen   = 4;
		Integer largerClose  = 5;
		
		//Init first two iterations with (4,5) - 4.5 & last five iterations with (1,2) - 1.5
		//SMA( 5 ) after loop == 2.1
		//[ 4.5, 1.5, 1.5, 1.5, 1.5 ]
		//SMA( 7 ) after loop == 2.785
		//[ 4.5, 4.5, 4.5, 1.5, 1.5, 1.5, 1.5 ]
		for( int i = 0; i < 7; i ++ )
		{
			if( i < 3 )
			{
				tradingParameters.updateValue( "open", largerOpen.toString( ) );
				tradingParameters.updateValue( "close", largerClose.toString( ) );
			}
			
			else
			{
				tradingParameters.updateValue( "open", initialOpen.toString( ) );
				tradingParameters.updateValue( "close", initialClose.toString( ) );
			}
			
			smaSignal.update( tradingParameters );
			assertEquals( smaSignal.triggered( ), false );
		}
		
		//SMA(5) is 2.7 [ 1.5, 1.5, 1.5, 1.5, 7.5 ]
		//SMA(7) is 3.21 [ 4.5, 4.5, 1.5, 1.5, 1.5, 1.5, 7.5 ]
		tradingParameters.updateValue( "open", "7" );
		tradingParameters.updateValue( "close", "8");
		smaSignal.update( tradingParameters );
		assertEquals( smaSignal.triggered( ), false );
		
		//SMA(5) is 3.9 [ 1.5, 1.5, 1.5, 7.5, 7.5 ]
		//SMA(7) is 3.64 [ 4.5, 1.5, 1.5, 1.5, 1.5, 7.5, 7.5 ]
		//SHOULD TRIGGER SIGNAL
		tradingParameters.updateValue( "open", "7" );
		tradingParameters.updateValue( "close", "8");
		smaSignal.update( tradingParameters );
		assertEquals( smaSignal.triggered( ), true );
	}

}
