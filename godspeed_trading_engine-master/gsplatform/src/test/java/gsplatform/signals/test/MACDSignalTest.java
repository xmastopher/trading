package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.MACDBuySignal;
import gsplatform.signals.MACDSellSignal;
import gsplatform.signals.MACDSignal;
import gsplatform.utilities.TradingParameters;

public class MACDSignalTest {

	@Test
	public void testMACDCalculations() {

		MACDSignal macdSignal = new MACDSignal( 12, 26, 9 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		Double[] closePrices =  new Double[]{
			459.99,
			448.85,
			446.06,
			450.81,
			442.8,
			448.97,
			444.57,
			441.4,
			430.47,
			420.05,
			431.14,
			425.66,
			430.58,
			431.72,
			437.87,
			428.43,
			428.35,
			432.5,
			443.66,
			455.72,
			454.49,
			452.08,
			452.73,
			461.91,
		};
		
		for( int i = 0; i < closePrices.length; i++) {
			tradingParameters.updateValue( "close", String.valueOf( closePrices[i] ) );
			macdSignal.update( tradingParameters );
			assertTrue( !macdSignal.isReady( ) );
		}
		
		tradingParameters.updateValue( "close", "463.58" );
		macdSignal.update( tradingParameters );
		assertTrue( !macdSignal.isReady( ) );
		
		tradingParameters.updateValue( "close", "461.14" );
		macdSignal.update( tradingParameters );
		assertTrue( !macdSignal.isReady( ) );
		assertEquals( macdSignal.getMACD( ), 8.27526950390762, 2 );
		
		closePrices =  new Double[]{ 452.08, 442.66, 428.91, 429.79, 431.99, 427.72 };
		
		for( int i = 0; i < closePrices.length; i++) {
			tradingParameters.updateValue( "close", String.valueOf( closePrices[i] ) );
			macdSignal.update( tradingParameters );
			assertTrue( !macdSignal.isReady( ) );
		}
		
		tradingParameters.updateValue( "close", "423.20" );
		macdSignal.update( tradingParameters );
		assertTrue( !macdSignal.isReady( ) );
		
		tradingParameters.updateValue( "close", "426.21" );
		macdSignal.update( tradingParameters );
		assertTrue( macdSignal.isReady( ) );
		assertEquals( macdSignal.getMACD( ), -2.07055819009491, 2 );
		assertEquals( macdSignal.getSignalLine( ), 3.03752586873394, 2 );
	}
	
	@Test
	public void testBuySignal( ) {
		
		MACDSignal macdSignal = new MACDBuySignal( 12, 26, 9 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		Double[] closePrices =  new Double[]{
			459.99,
			448.85,
			446.06,
			450.81,
			442.8,
			448.97,
			444.57,
			441.4,
			430.47,
			420.05,
			431.14,
			425.66,
			430.58,
			431.72,
			437.87,
			428.43,
			428.35,
			432.5,
			443.66,
			455.72,
			454.49,
			452.08,
			452.73,
			461.91,
			463.58,
			461.14,
			452.08,
			442.66,
			428.91,
			429.79,
			431.99,
			427.72,
			423.2,
			426.21,
			426.98,
			435.69,
			434.33,
			429.8,
			419.85,
			426.24,
			402.8,
			392.05,
			390.53,
			398.67,
			406.13,
			405.46,
			408.38,
			417.2
		};
		
		
		for( int i = 0; i < closePrices.length; i++) {
			tradingParameters.updateValue( "close", String.valueOf( closePrices[i] ) );
			macdSignal.update( tradingParameters );
			assertTrue( !macdSignal.triggered( ) );
		}
		
		tradingParameters.updateValue( "close", "430.12" );
		macdSignal.update( tradingParameters );
		assertTrue( macdSignal.triggered( ) );
		
	}
	
	@Test
	public void testSellSignal( ) {
		
		MACDSignal macdSignal = new MACDSellSignal( 12, 26, 9 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		Double[] closePrices =  new Double[]{
			459.99,
			448.85,
			446.06,
			450.81,
			442.8,
			448.97,
			444.57,
			441.4,
			430.47,
			420.05,
			431.14,
			425.66,
			430.58,
			431.72,
			437.87,
			428.43,
			428.35,
			432.5,
			443.66,
			455.72,
			454.49,
			452.08,
			452.73,
			461.91,
			463.58,
			461.14,
			452.08,
			442.66,
			428.91,
			429.79,
			431.99,
			427.72,
			423.2,
			426.21,
			426.98,
			435.69,
			434.33,
			429.8,
			419.85,
			426.24,
			402.8,
			392.05,
			390.53,
			398.67,
			406.13,
			405.46,
			408.38,
			417.2,
			430.12,
			442.78,
			439.29,
			445.52,
			449.98,
			460.71,
			458.66,
			463.84,
			456.77,
			452.97,
			454.74,
			443.86,
			428.85
		};
		
		
		for( int i = 0; i < closePrices.length; i++) {
			tradingParameters.updateValue( "close", String.valueOf( closePrices[i] ) );
			macdSignal.update( tradingParameters );
			if( macdSignal.triggered( ) )
			{
				System.out.println("");
			}
			assertTrue( !macdSignal.triggered( ) );
		}
		
		tradingParameters.updateValue( "close", "434.58" );
		macdSignal.update( tradingParameters );
		assertTrue( macdSignal.triggered( ) );
	}
	

}
