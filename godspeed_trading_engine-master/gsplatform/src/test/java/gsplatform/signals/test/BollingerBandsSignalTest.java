package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.BollingerBandsBuySignal;
import gsplatform.signals.BollingerBandsSellSignal;
import gsplatform.signals.BollingerBandsSignal;
import gsplatform.utilities.TradingParameters;

public class BollingerBandsSignalTest {

	@Test
	public void testCalcuations( ) 
	{
		BollingerBandsSignal bollingers = new BollingerBandsSignal( 5, 2.00 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		tradingParameters.updateValue( "open", "1" );
		tradingParameters.updateValue( "close", "2" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.isReady( ), false );
		
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.isReady( ), false );
		
		tradingParameters.updateValue( "open", "4" );
		tradingParameters.updateValue( "close", "5" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.isReady( ), false );
		
		tradingParameters.updateValue( "open", "5" );
		tradingParameters.updateValue( "close", "6" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.isReady( ), true );
		
		tradingParameters.updateValue( "open", "7" );
		tradingParameters.updateValue( "close", "8" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.isReady( ), true );
		assertEquals( bollingers.getStandardDeviation( ), 2.38, 2 );
		assertEquals( bollingers.getSMA( ), 4.8, 2 );
	}
	
	@Test
	public void testSellSignal( )
	{
		BollingerBandsSignal bollingers = new BollingerBandsSellSignal( 5, 2.00 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		tradingParameters.updateValue( "open", "1" );
		tradingParameters.updateValue( "close", "2" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "4" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "4" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "3" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "6" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), true );
		
	}
	
	@Test
	public void testBuySignal( )
	{
		BollingerBandsSignal bollingers = new BollingerBandsBuySignal( 5, 2.00 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "4" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "3" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "4" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "4" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), false );
		
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "1" );
		bollingers.update( tradingParameters );
		assertEquals( bollingers.triggered( ), true );
	}

}
