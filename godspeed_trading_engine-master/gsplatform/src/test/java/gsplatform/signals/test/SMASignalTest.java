package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.SMASignal;
import gsplatform.utilities.TradingParameters;

public class SMASignalTest {

	@Test
	public void testSMACalculation( ) 
	{
		SMASignal smaSignal = new SMASignal( 5 );
		
		TradingParameters tradingParameters = new TradingParameters( );
		
		//Update 1
		tradingParameters.updateValue( "open", "1" );
		tradingParameters.updateValue( "close", "2" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 2
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 3
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "4" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 4
		tradingParameters.updateValue( "open", "4" );
		tradingParameters.updateValue( "close", "5" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), true );
		
		//Update 5
		tradingParameters.updateValue( "open", "5" );
		tradingParameters.updateValue( "close", "6" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), true );
		
		//Check the SMA
		double sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		//Test SMA after
		tradingParameters.updateValue( "open", "1" );
		tradingParameters.updateValue( "close", "2" );
		smaSignal.update( tradingParameters );
		assertEquals( smaSignal.isReady( ), true );
		sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
	}

}
