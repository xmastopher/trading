package gsplatform.signals.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import gsplatform.signals.EMASignal;
import gsplatform.utilities.TradingParameters;

public class EMASignalTest {

	@Test
	public void testSMACalculation( ) 
	{
		//Setup object
		EMASignal emaSignal = new EMASignal( 10 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		//Update 1
		tradingParameters.updateValue( "close", "22.27" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 2
		tradingParameters.updateValue( "close", "22.19" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 3
		tradingParameters.updateValue( "close", "22.08" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 4
		tradingParameters.updateValue( "close", "22.17" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 5
		tradingParameters.updateValue( "close", "22.18" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 6
		tradingParameters.updateValue( "close", "22.13" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 7
		tradingParameters.updateValue( "close", "22.23" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );

		
		//Update 8
		tradingParameters.updateValue( "close", "22.43" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 9
		tradingParameters.updateValue( "close", "22.24" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		
		//Update 10
		tradingParameters.updateValue( "close", "22.29" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		
		//Check the EMA - will be SMA first iteration
		double ema = emaSignal.getEMA( );
		assertEquals( new BigDecimal( ema ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 22.22, 2 );
		
		//Update 11 - Check EMA
		tradingParameters.updateValue( "close", "22.15" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		ema = emaSignal.getEMA( );
		assertEquals( new BigDecimal( ema ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 22.21, 2 );
		
		//Update 12 - Check EMA
		tradingParameters.updateValue( "close", "22.39" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		ema = emaSignal.getEMA( );
		assertEquals( new BigDecimal( ema ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 22.24, 2 );
		
	}

}
