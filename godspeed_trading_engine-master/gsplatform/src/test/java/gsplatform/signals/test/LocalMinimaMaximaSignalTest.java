package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.LocalMaximaBuySignal;
import gsplatform.signals.LocalMaximaSellSignal;
import gsplatform.signals.LocalMinimaBuySignal;
import gsplatform.signals.LocalMinimaSellSignal;
import gsplatform.utilities.TradingParameters;

public class LocalMinimaMaximaSignalTest {

	@Test
	public void testLocalMinima( ) {
		
		LocalMinimaBuySignal localMinima      = new LocalMinimaBuySignal( 5 );
		LocalMinimaSellSignal localMinimaSell = new LocalMinimaSellSignal( 5 );
		TradingParameters tradingParameters =  new TradingParameters( );
		
		tradingParameters.updateValue( "low", "5.0" );
		tradingParameters.updateValue( "close", "6.0" );
		localMinima.update( tradingParameters );
		localMinimaSell.update( tradingParameters );
		assertEquals( localMinima.getMinimumPrice( ).doubleValue( ), 5.0, 1 );
		assertEquals( localMinima.triggered( ), false );
		assertEquals( localMinimaSell.getMinimumPrice( ).doubleValue( ), 5.0, 1 );
		assertEquals( localMinimaSell.triggered( ), false );
		
		tradingParameters.updateValue( "low", "4.0" );
		tradingParameters.updateValue( "close", "5.0" );
		localMinima.update( tradingParameters );
		localMinimaSell.update( tradingParameters );
		assertEquals( localMinima.getMinimumPrice( ).doubleValue( ), 4.0, 1 );
		assertEquals( localMinima.triggered( ), false );
		assertEquals( localMinimaSell.getMinimumPrice( ).doubleValue( ), 4.0, 1 );
		assertEquals( localMinimaSell.triggered( ), false );
		
		tradingParameters.updateValue( "low", "3.0" );
		tradingParameters.updateValue( "close", "4.0" );
		localMinima.update( tradingParameters );
		localMinimaSell.update( tradingParameters );
		assertEquals( localMinima.getMinimumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMinima.triggered( ), false );
		assertEquals( localMinimaSell.getMinimumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMinimaSell.triggered( ), false );
		
		tradingParameters.updateValue( "low", "4.0" );
		tradingParameters.updateValue( "close", "5.0" );
		localMinima.update( tradingParameters );
		localMinimaSell.update( tradingParameters );
		assertEquals( localMinima.getMinimumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMinima.triggered( ), false );
		assertEquals( localMinimaSell.getMinimumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMinimaSell.triggered( ), false );
		
		tradingParameters.updateValue( "low", "3.0" );
		tradingParameters.updateValue( "close", "4.0" );
		localMinima.update( tradingParameters );
		localMinimaSell.update( tradingParameters );
		assertEquals( localMinima.getMinimumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMinima.triggered( ), false );
		assertEquals( localMinimaSell.getMinimumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMinimaSell.triggered( ), false );
		
		tradingParameters.updateValue( "low", "2.0" );
		tradingParameters.updateValue( "close", "1.0" );
		localMinima.update( tradingParameters );
		localMinimaSell.update( tradingParameters );
		assertEquals( localMinima.getMinimumPrice( ).doubleValue( ), 2.0, 1 );
		assertEquals( localMinima.triggered( ), true );
		assertEquals( localMinimaSell.getMinimumPrice( ).doubleValue( ), 2.0, 1 );
		assertEquals( localMinimaSell.triggered( ), true );
		
	}
	
	@Test
	public void testLocalMaxima( ) {
		
		LocalMaximaSellSignal localMaxima    = new LocalMaximaSellSignal( 5 );
		LocalMaximaBuySignal localMaximaBuy  = new LocalMaximaBuySignal( 5 );
		
		TradingParameters tradingParameters =  new TradingParameters( );
		tradingParameters.updateValue( "high", "1.0" );
		tradingParameters.updateValue( "close", "1.0" );
		localMaxima.update( tradingParameters );
		localMaximaBuy.update( tradingParameters );
		assertEquals( localMaxima.getMaximumPrice( ).doubleValue( ), 1.0, 1 );
		assertEquals( localMaxima.triggered( ), false );
		assertEquals( localMaximaBuy.getMaximumPrice( ).doubleValue( ), 1.0, 1 );
		assertEquals( localMaximaBuy.triggered( ), false );
		
		tradingParameters.updateValue( "high", "2.0" );
		tradingParameters.updateValue( "close", "1.0" );
		localMaxima.update( tradingParameters );
		localMaximaBuy.update( tradingParameters );
		assertEquals( localMaxima.getMaximumPrice( ).doubleValue( ), 2.0, 1 );
		assertEquals( localMaxima.triggered( ), false );
		assertEquals( localMaximaBuy.getMaximumPrice( ).doubleValue( ), 2.0, 1 );
		assertEquals( localMaximaBuy.triggered( ), false );
		
		tradingParameters.updateValue( "high", "3.0" );
		tradingParameters.updateValue( "close", "1.0" );
		localMaxima.update( tradingParameters );
		localMaximaBuy.update( tradingParameters );
		assertEquals( localMaxima.getMaximumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMaxima.triggered( ), false );
		assertEquals( localMaximaBuy.getMaximumPrice( ).doubleValue( ), 3.0, 1 );
		assertEquals( localMaximaBuy.triggered( ), false );
		
		tradingParameters.updateValue( "high", "2.0" );
		tradingParameters.updateValue( "close", "1.0" );
		localMaxima.update( tradingParameters );
		localMaximaBuy.update( tradingParameters );
		assertEquals( localMaxima.getMaximumPrice( ).doubleValue( ), 3, 1 );
		assertEquals( localMaxima.triggered( ), false );
		assertEquals( localMaximaBuy.getMaximumPrice( ).doubleValue( ), 3, 1 );
		assertEquals( localMaximaBuy.triggered( ), false );
		
		tradingParameters.updateValue( "high", "2.0" );
		tradingParameters.updateValue( "close", "1.0" );
		localMaxima.update( tradingParameters );
		localMaximaBuy.update( tradingParameters );
		assertEquals( localMaxima.getMaximumPrice( ).doubleValue( ), 2, 1 );
		assertEquals( localMaxima.triggered( ), false );
		assertEquals( localMaximaBuy.getMaximumPrice( ).doubleValue( ), 2, 1 );
		assertEquals( localMaximaBuy.triggered( ), false );		
		
		tradingParameters.updateValue( "high", "5.0" );
		tradingParameters.updateValue( "close", "6.0" );
		localMaxima.update( tradingParameters );
		localMaximaBuy.update( tradingParameters );
		assertEquals( localMaxima.getMaximumPrice( ).doubleValue( ), 5, 1 );
		assertEquals( localMaxima.triggered( ), true );
		assertEquals( localMaximaBuy.getMaximumPrice( ).doubleValue( ), 5, 1 );
		assertEquals( localMaximaBuy.triggered( ), true );
	}

}
