package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.TargetBuySignal;
import gsplatform.signals.TargetSellSignal;
import gsplatform.utilities.TradingParameters;

public class TargetSignalsTest {

	@Test
	public void testTargetBuy() {
		
		TargetBuySignal targetBuy      = new TargetBuySignal( 0.1 );

		TradingParameters tradingParameters =  new TradingParameters( );
		
		tradingParameters.updateValue( "close", "10.1" );
		tradingParameters.updateValue( "last_sell_price", "10" );
		targetBuy.update( tradingParameters );
		
		assertEquals( targetBuy.getBuyTarget( ).doubleValue( ), 9, 2 );
		assertEquals( targetBuy.triggered( ), false );
		
		tradingParameters.updateValue( "close", "8.9" );
		tradingParameters.updateValue( "last_sell_price", "10" );
		targetBuy.update( tradingParameters );
		
		assertEquals( targetBuy.getBuyTarget( ).doubleValue( ), 9.9, 2 );
		assertEquals( targetBuy.triggered( ), true );
	}
	
	@Test
	public void testTargetSell() {
		
		TargetSellSignal targetSell      = new TargetSellSignal( 0.1 );

		TradingParameters tradingParameters =  new TradingParameters( );
		
		tradingParameters.updateValue( "close", "9.8" );
		tradingParameters.updateValue( "last_buy_price", "10" );
		targetSell.update( tradingParameters );
		
		assertEquals( targetSell.getSellTarget( ).doubleValue( ), 11, 2 );
		assertEquals( targetSell.triggered( ), false );
		
		tradingParameters.updateValue( "close", "11.1" );
		tradingParameters.updateValue( "last_buy_price", "10" );
		targetSell.update( tradingParameters );
		
		assertEquals( targetSell.getSellTarget( ).doubleValue( ), 11, 2 );
		assertEquals( targetSell.triggered( ), true );
	}
	

}
