package gsplatform.signals.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import gsplatform.signals.EMACrossBuySignal;
import gsplatform.signals.EMACrossSellSignal;
import gsplatform.signals.EMACrossSignal;
import gsplatform.utilities.TradingParameters;

public class EMACrossSignalTest {

	@Test
	public void testEMACalculations( ) 
	{
		//Setup object
		EMACrossSignal emaSignal = new EMACrossSignal( 5, 10 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		//Update 1
		tradingParameters.updateValue( "close", "22.27" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 2
		tradingParameters.updateValue( "close", "22.19" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 3
		tradingParameters.updateValue( "close", "22.08" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 4
		tradingParameters.updateValue( "close", "22.17" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 5
		tradingParameters.updateValue( "close", "22.18" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 6
		tradingParameters.updateValue( "close", "22.13" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 7
		tradingParameters.updateValue( "close", "22.23" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );

		
		//Update 8
		tradingParameters.updateValue( "close", "22.43" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), false );
		
		//Update 9
		tradingParameters.updateValue( "close", "22.24" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		
		//Update 10
		tradingParameters.updateValue( "close", "22.29" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		
		//Check the EMA - will be SMA first iteration
		double ema = emaSignal.getEmaLarger( );
		assertEquals( new BigDecimal( ema ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 22.22, 2 );
		
		//Update 11 - Check EMA
		tradingParameters.updateValue( "close", "22.15" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		ema = emaSignal.getEmaLarger( );
		assertEquals( new BigDecimal( ema ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 22.21, 2 );
		
		//Update 12 - Check EMA
		tradingParameters.updateValue( "close", "22.39" );
		emaSignal.update( tradingParameters );
		assertEquals( emaSignal.isReady( ), true );
		ema = emaSignal.getEmaLarger( );
		assertEquals( new BigDecimal( ema ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 22.24, 2 );
		
	}
	
	@Test
	public void testBuySignal( ) {
		
		//Setup object
		EMACrossBuySignal emaSignal = new EMACrossBuySignal( 10, 25 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		for( int i = 26; i > 0; i-- ) {
			tradingParameters.updateValue( "close", String.valueOf( i ) );
			emaSignal.update( tradingParameters );
			System.out.println( emaSignal.getEMA( ) + ", " + emaSignal.getEmaLarger( ) );
			assertFalse( emaSignal.triggered( ) );
		}
		
		tradingParameters.updateValue( "close", "26" );
		emaSignal.update( tradingParameters );
		System.out.println( emaSignal.getEMA( ) + ", " + emaSignal.getEmaLarger() );
		assertTrue( emaSignal.triggered( ) );
		
	}
	
	@Test
	public void testSellSignal( ) {
		
		//Setup object
		EMACrossSellSignal emaSignal = new EMACrossSellSignal( 10, 25 );
		TradingParameters tradingParameters = new TradingParameters( );
		
		for( int i = 0; i < 26; i++ ) {
			tradingParameters.updateValue( "close", String.valueOf( i ) );
			emaSignal.update( tradingParameters );
			System.out.println( emaSignal.getEMA( ) + ", " + emaSignal.getEmaLarger( ) );
			assertFalse( emaSignal.triggered( ) );
		}
		
		tradingParameters.updateValue( "close", "1" );
		emaSignal.update( tradingParameters );
		System.out.println( emaSignal.getEMA( ) + ", " + emaSignal.getEmaLarger() );
		assertTrue( emaSignal.triggered( ) );
		
	}
	
	

}
