package gsplatform.signals.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import gsplatform.signals.RSIBuySignal;
import gsplatform.signals.RSISellSignal;
import gsplatform.signals.RSISignal;
import gsplatform.utilities.TradingParameters;

public class RSISignalTest {

	@Test 
	public void testRSISignalCalculations( ) {
		
		RSISignal rsiSignal = new RSISignal( 14, 50 );
		
		TradingParameters tradingParameters = new TradingParameters( );
		
		tradingParameters.updateValue("close", "44.34");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "44.09");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "44.15");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "43.61");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "44.33");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "44.83");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "45.10");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "45.42");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "45.84");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "46.08");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "45.89");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "46.03");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "45.61");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "46.28");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		
		tradingParameters.updateValue("close", "46.28");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		assertEquals( new BigDecimal( rsiSignal.getAvgGain( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.24, 2 );
		assertEquals( new BigDecimal( rsiSignal.getAvgLoss( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.10, 2 );
		assertEquals( new BigDecimal( rsiSignal.getRsi( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 70.53, 2 );
		
		tradingParameters.updateValue("close", "46.00");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		assertEquals( new BigDecimal( rsiSignal.getAvgGain( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.22, 4 );
		assertEquals( new BigDecimal( rsiSignal.getAvgLoss( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.11, 4 );
		assertEquals( new BigDecimal( rsiSignal.getRsi( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 66.32, 4 );
		
		tradingParameters.updateValue("close", "46.03");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		assertEquals( new BigDecimal( rsiSignal.getAvgGain( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.21, 4 );
		assertEquals( new BigDecimal( rsiSignal.getAvgLoss( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.10, 4 );
		assertEquals( new BigDecimal( rsiSignal.getRsi( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 66.55, 4 );
		
		tradingParameters.updateValue("close", "46.41");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		assertEquals( new BigDecimal( rsiSignal.getAvgGain( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.22, 4 );
		assertEquals( new BigDecimal( rsiSignal.getAvgLoss( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 0.10, 4 );
		assertEquals( new BigDecimal( rsiSignal.getRsi( ) ).setScale( 2, BigDecimal.ROUND_UP ).doubleValue( ), 69.41, 4 );
	}
	
	@Test
	public void testSellSignal( ) {
		
		RSISignal rsiSignal = new RSISellSignal( 14, 70 );
		
		TradingParameters tradingParameters = new TradingParameters( );
		
		tradingParameters.updateValue("close", "44.34");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "44.09");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "44.15");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "43.61");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "44.33");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "44.83");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "45.10");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "45.42");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "45.84");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "46.08");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "45.89");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "46.03");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "45.61");
		rsiSignal.update( tradingParameters );
		assertTrue( !rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "46.28");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		assertTrue( !rsiSignal.triggered( ) );
		
		tradingParameters.updateValue("close", "46.28");
		rsiSignal.update( tradingParameters );
		assertTrue( rsiSignal.isReady( ) );
		assertTrue( rsiSignal.triggered( ) );
	}
	
	@Test
	public void testBuySignalAndZeroGain( ) {
	
		RSISignal rsiSignal = new RSIBuySignal( 14, 1 );
		
		TradingParameters tradingParameters = new TradingParameters( );
		
		for( int i = 15; i > 1; i-- ) {
			tradingParameters.updateValue( "close", String.valueOf( i ) );
			rsiSignal.update( tradingParameters );
			assertTrue( !rsiSignal.triggered( ) );
		}
		
		tradingParameters.updateValue( "close", "1.0" );
		rsiSignal.update( tradingParameters );
		assertEquals( rsiSignal.getRsi( ), 0.0, 0 );
		assertTrue( rsiSignal.triggered( ) );

	}

}
