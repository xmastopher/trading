package gsplatform.signals.test;

import static org.junit.Assert.*;

import org.junit.Test;

import gsplatform.signals.SMACrossSignal;
import gsplatform.utilities.TradingParameters;

public class SMACrossSignalTest {

	@Test
	public void testSMACalculations( ) 
	{
		SMACrossSignal smaSignal = new SMACrossSignal( 5, 10 );
		
		TradingParameters tradingParameters = new TradingParameters( );
		
		//Update 1
		// SMA( 5 )  = 1.5
		// SMA( 10 ) = 1.5
		tradingParameters.updateValue( "open", "1" );
		tradingParameters.updateValue( "close", "2" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 2
		// SMA( 5 )  = 1.5 + 2.5
		// SMA( 10 ) = 1.5 + 2.5
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 3
		// SMA( 5 )  = 1.5 + 2.5 + 3.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "4" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 4
		// SMA( 5 )  = 1.5 + 2.5 + 3.5 + 4.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5 + 4.5
		tradingParameters.updateValue( "open", "4" );
		tradingParameters.updateValue( "close", "5" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Update 5
		// SMA( 5 )  = ( 1.5 + 2.5 + 3.5 + 4.5 + 5.5 ) / 5 == 3.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5 + 4.5 + 5.5
		tradingParameters.updateValue( "open", "5" );
		tradingParameters.updateValue( "close", "6" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Check the SMA
		double sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		//Update 6
		// SMA( 5 )  = ( 2.5 + 3.5 + 4.5 + 5.5 + 1.5 ) / 5 == 3.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5 + 4.5 + 5.5 + 1.5
		tradingParameters.updateValue( "open", "1" );
		tradingParameters.updateValue( "close", "2" );
		smaSignal.update( tradingParameters );
		
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Check SMA
		sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		//Update 7
		// SMA( 5 )  = ( 3.5 + 4.5 + 5.5 + 1.5 + 2.5 ) / 5 == 3.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5 + 4.5 + 5.5 + 1.5 + 2.5
		tradingParameters.updateValue( "open", "2" );
		tradingParameters.updateValue( "close", "3" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Check SMA
		sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		//Update 8
		// SMA( 5 )  = ( 4.5 + 5.5 + 1.5 + 2.5 + 3.5 ) / 5 == 3.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5 + 4.5 + 5.5 + 1.5 + 2.5 + 3.5
		tradingParameters.updateValue( "open", "3" );
		tradingParameters.updateValue( "close", "4" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Check SMA
		sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		//Update 9
		// SMA( 5 )  = ( 5.5 + 1.5 + 2.5 + 3.5 + 4.5 ) / 5 == 3.5
		// SMA( 10 ) = 1.5 + 2.5 + 3.5 + 4.5 + 5.5 + 1.5 + 2.5 + 3.5 + 4.5
		tradingParameters.updateValue( "open", "4" );
		tradingParameters.updateValue( "close", "5" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), false );
		
		//Check SMA
		sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		// Update 10
		// SMA( 5 )  = ( 1.5 + 2.5 + 3.5 + 4.5 + 5.5 ) / 5 == 3.5
		// SMA( 10 ) = ( 1.5 + 2.5 + 3.5 + 4.5 + 5.5 + 1.5 + 2.5 + 3.5 + 4.5 + 5.5 ) / 10
		tradingParameters.updateValue( "open", "5" );
		tradingParameters.updateValue( "close", "6" );
		smaSignal.update( tradingParameters );
		
		assertEquals( smaSignal.isReady( ), true );
		
		//Check SMA
		sma = smaSignal.getSMA( );
		assertEquals( sma, 3.5, 2 );
		
		double longerSMA = smaSignal.getSMALarger( );
		assertEquals( longerSMA, 3.5, 2 );
		
	}
	
	

}
