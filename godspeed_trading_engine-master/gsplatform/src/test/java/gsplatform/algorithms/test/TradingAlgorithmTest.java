package gsplatform.algorithms.test;

import org.junit.Test;

public class TradingAlgorithmTest {

	@Test
	public void testUpdateTargetsAndStopLoss() 
	{
		/*
		List< BigDecimal > targets = new ArrayList< BigDecimal >( );
		targets.add( new BigDecimal( .05 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		targets.add( new BigDecimal( .1 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		targets.add( new BigDecimal( .151 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		
		TradingAlgorithm tradingAlgorithmOne = new BuyAndHoldStrategy( TradingAlgorithm.GOAL.SELL, targets, new BigDecimal( .1 ) );
		tradingAlgorithmOne.updateStopLoss( -1, new BigDecimal( 1 ) );
		tradingAlgorithmOne.updateTargets( -1, new BigDecimal( 1 ) );
				
		TradingAlgorithm tradingAlgorithmTwo = new BuyAndHoldStrategy( TradingAlgorithm.GOAL.BUY, targets, new BigDecimal( .1 ) );
		tradingAlgorithmTwo.updateStopLoss( 1, new BigDecimal( 1 ) );
		tradingAlgorithmTwo.updateTargets( 1, new BigDecimal( 1 ) );
		
		List< BigDecimal > buyTargets  = tradingAlgorithmOne.getTargetValues();
		List< BigDecimal > sellTargets = tradingAlgorithmTwo.getTargetValues();
		
		assertEquals( tradingAlgorithmOne.getStopLossPrice( ).doubleValue( ), 1.10, .001 );
		assertEquals( buyTargets.get( 0 ).doubleValue( ), .95, .001 );
		assertEquals( buyTargets.get( 1 ).doubleValue( ), .90, .001 );
		assertEquals( buyTargets.get( 2 ).doubleValue( ), .85, .001 );
		
		assertEquals( tradingAlgorithmTwo.getStopLossPrice( ).doubleValue( ), 0.9, .001 );
		assertEquals( sellTargets.get( 0 ).doubleValue( ), 1.05, .001 );
		assertEquals( sellTargets.get( 1 ).doubleValue( ), 1.1, .001 );
		assertEquals( sellTargets.get( 2 ).doubleValue( ), 1.15, .001 );
		
	}
	
	@Test
	public void testCheckTargetsHitBuyMode( )
	{
		List< BigDecimal > targets = new ArrayList< BigDecimal >( );
		targets.add( new BigDecimal( .05 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		targets.add( new BigDecimal( .1 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		targets.add( new BigDecimal( .151 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		
		//Create algo and update params
		TradingAlgorithm tradingAlgorithmOne = new BuyAndHoldStrategy( new BigDecimal( .1 ) );
		tradingAlgorithmOne.updateStopLoss( 0, new BigDecimal( 1 ) );
		tradingAlgorithmOne.updateTargets( 1, new BigDecimal( 1 ) );
		tradingAlgorithmOne.setWaitingToSell( true );
		tradingAlgorithmOne.setWaitingToBuy( false );
		
		//Set price and check that no targets were hit
		BigDecimal currentPrice = new BigDecimal( 1 ).setScale( 2, BigDecimal.ROUND_DOWN );;
		tradingAlgorithmOne.setLastPrice( currentPrice );
		Boolean hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertFalse( hitTarget );
		
		//Update and check that target was hit
		currentPrice = new BigDecimal( 1.05 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		
		//Update and check that target was hit
		tradingAlgorithmOne.setLastPrice( currentPrice );
		currentPrice = new BigDecimal( 1.10 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		
		//Update and check that target was hit
		tradingAlgorithmOne.setLastPrice( currentPrice );
		currentPrice = new BigDecimal( 1.151 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		
		//Create new strategy and try fulfilling all targets, check that they are hit
		tradingAlgorithmOne = new BuyAndHoldStrategy( TradingAlgorithm.GOAL.BUY, targets, new BigDecimal( .1 ) );
		tradingAlgorithmOne.updateStopLoss( 0, new BigDecimal( 1 ) );
		tradingAlgorithmOne.updateTargets( 1, new BigDecimal( 1 ) );
		tradingAlgorithmOne.setWaitingToSell( true );
		tradingAlgorithmOne.setWaitingToBuy( false );
		
		tradingAlgorithmOne.setLastPrice( currentPrice );
		currentPrice = new BigDecimal( 1.151 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		*/
		
	}
	
	@Test
	public void testCheckTargetsHitSellMode( )
	{
		/*
		List< BigDecimal > targets = new ArrayList< BigDecimal >( );
		targets.add( new BigDecimal( .05 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		targets.add( new BigDecimal( .1 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		targets.add( new BigDecimal( .151 ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		
		//Create algo and update params
		TradingAlgorithm tradingAlgorithmOne = new BuyAndHoldStrategy( TradingAlgorithm.GOAL.SELL, targets, new BigDecimal( .1 ) );
		tradingAlgorithmOne.updateStopLoss( 0, new BigDecimal( 1 ) );
		tradingAlgorithmOne.updateTargets( -1, new BigDecimal( 1 ) );
		tradingAlgorithmOne.setWaitingToSell( false );
		tradingAlgorithmOne.setWaitingToBuy( true );
		
		//Set price and check that no targets were hit
		BigDecimal currentPrice = new BigDecimal( 1 ).setScale( 2, BigDecimal.ROUND_DOWN );;
		tradingAlgorithmOne.setLastPrice( currentPrice );
		Boolean hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertFalse( hitTarget );
		
		//Update and check that target was hit
		currentPrice = new BigDecimal( .95 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		
		//Update and check that target was hit
		tradingAlgorithmOne.setLastPrice( currentPrice );
		currentPrice = new BigDecimal( .9 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		
		//Update and check that target was hit
		tradingAlgorithmOne.setLastPrice( currentPrice );
		currentPrice = new BigDecimal( .851 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		
		//Create new strategy and try fulfilling all targets, check that they are hit
		tradingAlgorithmOne = new BuyAndHoldStrategy( TradingAlgorithm.GOAL.SELL, targets, new BigDecimal( .1 ) );
		tradingAlgorithmOne.updateStopLoss( 0, new BigDecimal( 1 ) );
		tradingAlgorithmOne.updateTargets( -1, new BigDecimal( 1 ) );
		tradingAlgorithmOne.setWaitingToSell( false );
		tradingAlgorithmOne.setWaitingToBuy( true );
		
		tradingAlgorithmOne.setLastPrice( currentPrice );
		currentPrice = new BigDecimal( .851 ).setScale( 2, BigDecimal.ROUND_DOWN );
		tradingAlgorithmOne.setLastPrice( currentPrice );
		hitTarget = tradingAlgorithmOne.checkTargetsHit( );
		assertTrue( hitTarget );
		*/
		
	}

}
