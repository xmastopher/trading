package gsplatform.api.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.SaveStrategyAPI;

public class SaveStrategyAPITest {

	@Test
	/**
	 * Tests successfully saving a user strategy and the values of its resulting JSON
	 */
	public void testSavingStrategySuccess( ) 
	{
		JSONObject parameters = new JSONObject( );
		
		List< String > strategies = new ArrayList< String >( );
		strategies.add( "SMA Strategy " );
		strategies.add( "SMA Strategy" );
		
		List< String > parameterSettings = new ArrayList< String >( );
		parameterSettings.add( "25, [.025,.05,.1],.01" );
		parameterSettings.add( "20, [.02,.04,.08],.01" );
		
		try 
		{
			parameters.put( "strategies", strategies );
			parameters.put( "parameterSettings", parameterSettings );
			parameters.put( "username", "danimal" );
			
			SaveStrategyAPI ssAPI = new SaveStrategyAPI( parameters );
			JSONObject response   = ssAPI.createJSON( );
			
			JSONArray successes = response.getJSONArray( "successes" );
			JSONArray messages  = response.getJSONArray( "messages" );
			
			assertEquals( successes.get( 0 ), true );
			assertEquals( messages.get( 0 ), "success" );
			
			assertEquals( successes.get( 1 ), true );
			assertEquals( messages.get( 1 ), "success" );
			
		} 
		catch ( JSONException | IOException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
