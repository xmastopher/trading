package gsplatform.api.test;

import static org.junit.Assert.*;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.LoginAPI;

/**
 * Test Cases For {@link LoginAPI} class
 * @author danielanderson
 *
 */
public class LoginAPITest {

	@Test
	/**
	 * Test logging in a user with success using username
	 */
	public void testLoginUsername( ) 
	{

		try 
		{
			JSONObject parameters = new JSONObject();
			parameters.put( "username", "danimal" );
			parameters.put( "password", "asstits" );
			
			LoginAPI loginAPI = new LoginAPI( parameters );
			JSONObject login  = loginAPI.createJSON( );
			
			String usernameAfter = login.getString( "username" );
			String emailAfter    = login.getString( "email" );
			Boolean success      = login.getBoolean( "success" );
			String message       = login.getString( "message" );
			
			assertEquals( usernameAfter, "danimal" );
			assertEquals( emailAfter, "dtanderson005@gmail.com" );
			assertEquals( success, true );
			assertEquals( message, "0000: User login successful" );
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	/**
	 * Test logging in a user with success using email address
	 */
	public void testLoginEmail( ) 
	{
		try 
		{
			JSONObject parameters = new JSONObject();
			parameters.put( "username", "dtanderson005@gmail.com" );
			parameters.put( "password", "asstits" );
			
			LoginAPI loginAPI = new LoginAPI( parameters );
			JSONObject login  = loginAPI.createJSON( );
			
			String usernameAfter = login.getString( "username" );
			String emailAfter    = login.getString( "email" );
			Boolean success      = login.getBoolean( "success" );
			String message       = login.getString( "message" );
			
			assertEquals( usernameAfter, "danimal" );
			assertEquals( emailAfter, "dtanderson005@gmail.com" );
			assertEquals( success, true );
			assertEquals( message, "0000: User login successful" );
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Test
	/**
	 * Test attempting to log a user in with the wrong username
	 */
	public void testLoginNoUsername( ) 
	{
		try 
		{
			JSONObject parameters = new JSONObject();
			parameters.put( "username", "daniman" );
			parameters.put( "password", "asstits" );
			
			LoginAPI loginAPI = new LoginAPI( parameters );
			JSONObject login  = loginAPI.createJSON( );
			Boolean success      = login.getBoolean( "success" );
			String message       = login.getString( "message" );
			assertEquals( success, false );
			assertEquals( message, "0003: Username does not exist in system" );
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Test attempting to log a user in with wrong email
	 */
	public void testLoginNoEmail( ) 
	{
		try 
		{
			JSONObject parameters = new JSONObject();
			parameters.put( "username", "daniel@tuts.com" );
			parameters.put( "password", "asstits" );
			
			LoginAPI loginAPI = new LoginAPI( parameters );
			JSONObject login  = loginAPI.createJSON( );
			Boolean success      = login.getBoolean( "success" );
			String message       = login.getString( "message" );
			assertEquals( success, false );
			assertEquals( message, "0002: Email does not exist in system" );
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	/**
	 * Test attempting to log a user in with the wrong password
	 */
	public void testLoginWrongPassword( ) 
	{
		try 
		{
			JSONObject parameters = new JSONObject();
			parameters.put( "username", "danimal" );
			parameters.put( "password", "asstats" );
			
			LoginAPI loginAPI = new LoginAPI( parameters );
			JSONObject login  = loginAPI.createJSON( );
			Boolean success      = login.getBoolean( "success" );
			String message       = login.getString( "message" );
			assertEquals( success, false );
			assertEquals( message, "0001: Incorrect password" );
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
