package gsplatform.api.test;

import static org.junit.Assert.*;

import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.ChangeLockAPI;
import gsplatform.api.ChangePrivacyAPI;
import gsplatform.api.DeleteBotAPI;
import gsplatform.api.StartStopBotAPI;
import gsplatform.api.SaveBotAPI;

public class BotAPITest 
{

	@Test
	public void testSaveBotAPI( )
	{
		JSONObject jsonObject = new JSONObject( );
		
		try
		{
			jsonObject.put( "username", "testman" );
			jsonObject.put( "botId", 1 );
			jsonObject.put( "strategyId", 1 );
			jsonObject.put( "botName", "matty teets" );
			jsonObject.put( "private", true );
			jsonObject.put( "tradingLimit", .75 );
			
			SaveBotAPI saveBotAPI = new SaveBotAPI( jsonObject );
			JSONObject response   = saveBotAPI.createJSON( );
			
			assertEquals( response.get( "success" ), true );
		}
		catch( Exception e )
		{
			assertEquals( true, false );
		}
	}
	
	@Test
	public void testChangeLockAPI( )
	{
		JSONObject jsonObject = new JSONObject( );
		
		try
		{
			jsonObject.put( "username", "testman" );
			jsonObject.put( "botId", 1 );
			jsonObject.put( "strategyId", 1 );
			jsonObject.put( "locked", false );	
			ChangeLockAPI changeLockAPI = new ChangeLockAPI( jsonObject );
			JSONObject response      = changeLockAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot unlocked successfully" );
			
			JSONObject jsonObjectTwo = new JSONObject( );
			jsonObjectTwo.put( "username", "testman" );
			jsonObjectTwo.put( "botId", 1 );
			jsonObjectTwo.put( "strategyId", 1 );
			jsonObjectTwo.put( "locked", true );	
			changeLockAPI = new ChangeLockAPI( jsonObjectTwo );
			response      = changeLockAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot locked successfully" );
			
		}
		catch( Exception e )
		{
			assertEquals( true, false );
		}
	}
	
	@Test
	public void testPrivacyAPI( )
	{
		JSONObject jsonObject = new JSONObject( );
		
		try
		{
			jsonObject.put( "username", "testman" );
			jsonObject.put( "botId", 1 );
			jsonObject.put( "strategyId", 1 );
			jsonObject.put( "private", true );	
			ChangePrivacyAPI changePrivacyAPI = new ChangePrivacyAPI( jsonObject );
			JSONObject response      = changePrivacyAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot privatized successfully" );
			
			JSONObject jsonObjectTwo = new JSONObject( );
			jsonObjectTwo.put( "username", "testman" );
			jsonObjectTwo.put( "botId", 1 );
			jsonObjectTwo.put( "strategyId", 1 );
			jsonObjectTwo.put( "private", false );	
			changePrivacyAPI = new ChangePrivacyAPI( jsonObjectTwo );
			response      = changePrivacyAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot publicized successfully" );
			
		}
		catch( Exception e )
		{
			assertEquals( true, false );
		}	
	}
	
	@Test
	public void tesStartStopAPI( )
	{
		JSONObject jsonObject = new JSONObject( );
		
		try
		{
			jsonObject.put( "username", "testman" );
			jsonObject.put( "botId", 1 );
			jsonObject.put( "strategyId", 1 );
			jsonObject.put( "command", "start" );	
			StartStopBotAPI StartStopBotAPI = new StartStopBotAPI( jsonObject );
			JSONObject response      = StartStopBotAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot is now running" );
			
			JSONObject jsonObjectTwo = new JSONObject( );
			jsonObjectTwo.put( "username", "testman" );
			jsonObjectTwo.put( "botId", 1 );
			jsonObjectTwo.put( "strategyId", 1 );
			jsonObjectTwo.put( "command", "stop" );	
			StartStopBotAPI = new StartStopBotAPI( jsonObjectTwo );
			response      = StartStopBotAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot removed from running threads" );
			
		}
		catch( Exception e )
		{
			assertEquals( true, false );
		}	
	}
	
	@Test
	public void testDeletBotAPI( )
	{
		JSONObject jsonObject = new JSONObject( );
		
		try
		{
			jsonObject.put( "username", "testman" );
			jsonObject.put( "botId", 2 );
			jsonObject.put( "strategyId", 1 );
			jsonObject.put( "command", "start" );	
			DeleteBotAPI deleteBotAPI = new DeleteBotAPI( jsonObject );
			JSONObject response      = deleteBotAPI.createJSON( );
			assertEquals( response.get( "success" ), true );
			assertEquals( response.get( "message"), "0000: bot successfully deleted" );
			
		}
		catch( Exception e )
		{
			assertEquals( true, false );
		}	
	}
}
