package gsplatform.api.test;

import static org.junit.Assert.*;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.DeleteMessageAPI;
import gsplatform.api.GetMessagesAPI;
import gsplatform.api.SendMessageAPI;

public class MessagingAPITest {

	@Test
	public void testSendMessageAPI( )
	{
		JSONObject parameters = new JSONObject( );
		
		try 
		{
			parameters.put( "username", "testman" );
			parameters.put( "messageId", 1 );
			parameters.put( "conversationId", 3 );
			parameters.put( "recipient", "testman" );
			parameters.put( "message", "helllllo" );
			parameters.put( "subject", "WAKE UP" );
			parameters.put( "timestamp", "2018-03-26 23:59:59.0" );
			
			SendMessageAPI sendMessageAPI = new SendMessageAPI( parameters );
			JSONObject response           = sendMessageAPI.createJSON( );
			
			assertEquals( response.getBoolean( "success" ), true );
		} 
		
		catch (JSONException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testDeleteMessageAPI( )
	{
		JSONObject parameters = new JSONObject( );
		
		try 
		{
			
			parameters.put( "username", "testman" );
			parameters.put( "messageId", 1 );
			parameters.put( "conversationId", 3 );
			
			DeleteMessageAPI deleteMessageAPI = new DeleteMessageAPI( parameters );
			JSONObject response               = deleteMessageAPI.createJSON( );
			
			assertEquals( response.getBoolean( "success" ), true );
		} 
		
		catch (JSONException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetMessagesAPI( )
	{
		try
		{
			JSONObject parameters = new JSONObject( );
			parameters.put( "username", "danimal" ); 
			GetMessagesAPI messagesAPI = new GetMessagesAPI(  parameters );
			
			JSONObject response = messagesAPI.createJSON( );
			
			JSONArray conversations = response.getJSONArray( "data" );
			
			//Grab the first message
			JSONObject message      = conversations.getJSONObject( 0 )
										.getJSONArray( "messages" )
											.getJSONObject( 0 );
			
			//Check id, timestamp and conversationee
			assertEquals( message.get( "sender"), "danimal" );
			assertEquals( message.get( "recipient"), "testman" );
			assertEquals( message.get( "timestamp" ), "2018-03-10 12:59:59.0" );
			assertEquals( message.get( "message" ), "We have a secret meeting tomorrow a 9 PM EST" );
		}
		catch( Exception e )
		{
			assertEquals( true, false );
		}
	}
	
}
