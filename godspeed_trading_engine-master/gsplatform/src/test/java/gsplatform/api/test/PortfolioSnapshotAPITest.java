package gsplatform.api.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.PortfolioSnapshotAPI;

/**
 * JUNIT test class for testing the snapshot portfolio API (requesting a historical snapshot by date)
 * @author danielanderson
 *
 */
public class PortfolioSnapshotAPITest {

	@Test
	public void testGetSnapshot( )
	{
		JSONObject requestParameters = new JSONObject( );
		JSONObject requestParameters2 = new JSONObject( );
		
		try 
		{
			requestParameters.put( "username", "danimal" );
			requestParameters.put( "snapshot", "2018-02-25" );
			
			requestParameters2.put( "username", "danimal" );
			requestParameters2.put( "snapshot", "2018-02-24" );
			
			PortfolioSnapshotAPI psAPI = new PortfolioSnapshotAPI( requestParameters );
			JSONObject portfolioJSON   = psAPI.createJSON( );
			
			
			JSONArray data = portfolioJSON.getJSONArray( "data" );
			
			JSONObject omg = data.getJSONObject( 0 );
			JSONObject eth = data.getJSONObject( 1 );
			
			String omgSymbol     = omg.getString( "symbol" );
			String omgProportion = omg.getJSONArray( "tokenProportions" ).getString( 0 );
			double tokenBalance  = omg.getDouble("tokenBalance");
			String totalBalance  = omg.getString("totalBalance");
			String portionOfPort = omg.getString("portionOfPortfolio");
			String tokenLocation = omg.getJSONArray("tokenLocations").getString( 0 );
			double balanceAtLoc  = omg.getJSONArray("tokenBalances").getDouble( 0 );
			
			assertEquals( omgSymbol, "OMG" );
			assertEquals( omgProportion, "100.00%" );
			assertEquals( tokenBalance, 25.5, 3 );
			assertEquals( totalBalance, "$433.5" );
			assertEquals(portionOfPort, "37.00%" );
			assertEquals( tokenLocation, "Binance" );
			assertEquals( balanceAtLoc, 25.5, 3);
			
			assertNotNull( eth );
			
			//Test with bad snapshot date for failure message
			PortfolioSnapshotAPI psAPI2 = new PortfolioSnapshotAPI( requestParameters2 );
			JSONObject portfolioJSON2   = psAPI2.createJSON( );
			
			assertEquals( portfolioJSON2.get( "success" ), false );
			assertEquals( portfolioJSON2.get( "message" ), "snapshot does not exist" );
			
		} catch (JSONException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
