package gsplatform.api.test;

import static org.junit.Assert.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import gsplatform.api.BacktestAPI;

public class BacktestAPITest {
	
	@Test
	public void testCreateJSON() 
	{
		JSONObject testParams = new JSONObject( );
		JSONArray backtestJobs = new JSONArray( );
		
		JSONObject backtestOne = new JSONObject();
		JSONObject backtestTwo = new JSONObject();
		
		try 
		{
			
			backtestOne.put( "strategy", "Simple Moving Average Strategy" );
			backtestOne.put( "currencyPair", "ETH/USDT" );
			backtestOne.put( "candleSize", "1Hour" );
			backtestOne.put( "initialInvestment", "100" );
			backtestOne.put( "parameters", new JSONArray( new String[] { "Candle Size", "Targets", "Stop Loss" } ) );
			backtestOne.put( "parameterValues", new JSONArray( new String[] { "5", "0.05, 0.10, 0.15", "0.10" } ) );
			backtestOne.put( "exchangeName", "Binance" );
			
			backtestTwo.put( "strategy", "Simple Moving Average Strategy" );
			backtestTwo.put( "currencyPair", "BTC/USDT" );
			backtestTwo.put( "candleSize", "1Hour" );
			backtestTwo.put( "initialInvestment", "100" );
			backtestTwo.put( "parameters", new JSONArray( new String[] { "Candle Size", "Targets", "Stop Loss" } ) );
			backtestTwo.put( "parameterValues", new JSONArray( new String[] { "5", "0.05, 0.10, 0.15", "0.10" } ) );
			backtestTwo.put( "exchangeName", "Binance" );
			
			backtestJobs.put( backtestOne );
			backtestJobs.put( backtestTwo );

			testParams.put( "data", backtestJobs );
			
			BacktestAPI backtestAPI = null;
			backtestAPI = new BacktestAPI( testParams );
			
			JSONObject results = backtestAPI.createJSON( );
			
			System.out.println( testParams );
			System.out.println( results );
			assertNotNull( results );
			
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
	}

}
