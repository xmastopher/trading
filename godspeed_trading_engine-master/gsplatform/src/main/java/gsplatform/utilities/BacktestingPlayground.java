package gsplatform.utilities;

/**
 * Playground class for testing signals independantly and generating CSVs
 * @author danielanderson
 *
 */
public class BacktestingPlayground 
{
	
	/**
	 * Based on command line arg calls the appropriate test function
	 * even arguments (including 0) are the strategies, odd arguments are the parameters
	 * in the form: useUSD,base,counter,filename,exchange,candle
	 *
	public static void main( String args[] )
	{
		//Iterate over all tests and call static function
		for( int i = 0; i < args.length; i += 2 )
		{
			String testCase = args[ i ];
			
			switch( testCase )
			{
			case "SMAS":
				testSMASignals( args[ i ], args[ i + 1 ] );
				break;
			case "SMACS":
				testSMACrossSignals( args[ i ], args[ i + 1 ] );
				break;
			case "SMACS-BUY":
				testSMACrossSignalsBuy( args[ i ], args[ i + 1 ] );
				break;
			case "BBS":
				testBollingerSignals( args[ i ], args[ i + 1 ] );
				break;
			case "LMMS":
				testMinimaMaximaSignals( args[ i ], args[ i + 1 ] );
				break;
				
			}
		}
	}
	
	/**
	 * Call to test SMA Cross Signals : test case SMAS
	 *
	public static void testSMASignals( String name, String params )
	{
		
		String[] paramList = params.split( "," );
		Boolean useUSD     = Boolean.valueOf( paramList[ 0 ] );
		String base		   = paramList[ 1 ];
		String counter	   = paramList[ 2 ];
		String exchange    = paramList[ 3 ];
		String candle      = paramList[ 4 ];
		String filename    = name + "_" + base + counter + "_" + exchange + "_" + candle + "_" + useUSD;
		
		//CSV Headers
		String headers[] = new String[] {
					"open",
					"close",
					"sma(20)",
					"signal",
					"stop_loss_triggered",
					"amount_bought",
					"amount_sold",
					"quote_currency_held",
					"assets_held",
					"current_networth",
					"timestamp"
				};
		
		String parameters[ ] = new String[] {
					"sma",
					"stop_loss_percent"
				};
		
		String parameterValues[ ] = new String[] {
				"20",
				".10"
			};
		
		
		//Test simple moving strat
		Signal smaBuySignal  = new SMABuySignal( 20 );
		Signal smaSellSignal = new SMASellSignal( 20 );
		
		//TEST SMA CROSS STRATEGY
		SignalStrategy sma = new SignalStrategy( new BigDecimal( .1 ).setScale(2, BigDecimal.ROUND_DOWN ), smaBuySignal, smaSellSignal );
		sma.setWaitingToBuy( true );
		
		try
		{
			Backtest backtest  = new Backtest( sma,  exchange, candle, base, counter, 10, null, true, useUSD );
			backtest.setupCSVReport( headers, parameters, parameterValues, filename );
			BacktestReport bp = backtest.run( );
			
			bp.generateReport( false );
			backtest.completeCSVReport( bp );
		}
		
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
	
	/**
	 * Call to test SMA Cross Signals : test case SMACS
	 *
	public static void testSMACrossSignals( String name, String params )
	{
		String[] paramList = params.split( "," );
		Boolean useUSD     = Boolean.valueOf( paramList[ 0 ] );
		String base		   = paramList[ 1 ];
		String counter	   = paramList[ 2 ];
		String exchange    = paramList[ 3 ];
		String candle      = paramList[ 4 ];
		String filename    = name + "_" + base + counter + "_" + exchange + "_" + candle + "_" + useUSD;
		
		//CSV Headers
		String headers[] = new String[] {
					"open",
					"close",
					"sma(7)",
					"sma(25)",
					"signal",
					"stop_loss_triggered",
					"amount_bought",
					"amount_sold",
					"quote_currency_held",
					"assets_held",
					"current_networth",
					"timestamp"
				};
		
		String parameters[ ] = new String[] {
					"short_sma",
					"long_sma",
					"stop_loss_percent"
				};
		
		String parameterValues[ ] = new String[] {
				"7",
				"25",
				".10"
			};
		
		
		//Test simple moving strat
		Signal smaBuySignal  = new SMACrossBuySignal( 7, 25 );
		Signal smaSellSignal = new SMACrossSellSignal( 7, 25 );
		
		//TEST SMA CROSS STRATEGY
		SignalStrategy sma = new SignalStrategy( new BigDecimal( .1 ).setScale(2, BigDecimal.ROUND_DOWN ), smaBuySignal, smaSellSignal );
		sma.setWaitingToBuy( true );
		
		try
		{
			Backtest backtest  = new Backtest( sma,  exchange, candle, base, counter, 10, null, true, useUSD );
			backtest.setupCSVReport( headers, parameters, parameterValues, filename );
			BacktestReport bp = backtest.run( );
			bp.generateReport( false );
			backtest.completeCSVReport( bp );
		}
		
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
	
	public static void testSMACrossSignalsBuy( String name, String params )
	{
		String[] paramList = params.split( "," );
		Boolean useUSD     = Boolean.valueOf( paramList[ 0 ] );
		String base		   = paramList[ 1 ];
		String counter	   = paramList[ 2 ];
		String exchange    = paramList[ 3 ];
		String candle      = paramList[ 4 ];
		String filename    = name + "_" + base + counter + "_" + exchange + "_" + candle + "_" + useUSD;
		
		//CSV Headers
		String headers[] = new String[] {
					"open",
					"close",
					"sma(7)",
					"sma(25)",
					"target 1",
					"target 2",
					"target 3",
					"signal",
					"stop_loss_triggered",
					"amount_bought",
					"amount_sold",
					"quote_currency_held",
					"assets_held",
					"current_networth",
					"timestamp"
				};
		
		String parameters[ ] = new String[] {
					"short_sma",
					"long_sma",
					"stop_loss_percent",
					"target 1",
					"target 2",
					"target 3"
				};
		
		String parameterValues[ ] = new String[] {
				"7",
				"25",
				".10",
				"5.00%",
				"10.00%",
				"15.00%"
			};
		
		
		//Test simple moving strat
		Signal smaBuySignal  = new SMACrossBuySignal( 7, 25 );
		
		List< BigDecimal > targets = new ArrayList< BigDecimal >( );
		targets.add( new BigDecimal( .05 ) );
		targets.add( new BigDecimal( .1 ) );
		targets.add( new BigDecimal( .15 ) );
		
		//TEST SMA CROSS STRATEGY
		TargetStrategy sma = new BuyStrategy( targets, new BigDecimal( .1 ).setScale(2, BigDecimal.ROUND_DOWN ), smaBuySignal );
		sma.setWaitingToBuy( true );
		
		try
		{
			Backtest backtest  = new Backtest( sma,  exchange, candle, base, counter, 10, targets, true, useUSD );
			backtest.setupCSVReport( headers, parameters, parameterValues, filename );
			BacktestReport bp = backtest.run( );
			bp.generateReport( false );
			backtest.completeCSVReport( bp );
		}
		
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
	
	/**
	 * Call to test Bollinger Bands Signals : test case BBS
	 *
	public static void testBollingerSignals( String name, String params )
	{
		String[] paramList = params.split( "," );
		Boolean useUSD     = Boolean.valueOf( paramList[ 0 ] );
		String base		   = paramList[ 1 ];
		String counter	   = paramList[ 2 ];
		String exchange    = paramList[ 3 ];
		String candle      = paramList[ 4 ];
		String filename    = name + "_" + base + counter + "_" + exchange + "_" + candle + "_" + useUSD;
		
		//CSV Headers
		String headers[] = new String[] {
					"open",
					"close",
					"sma(20) - BUY Signal",
					"std",
					"upper band",
					"lower band",
					"signal",
					"stop_loss_triggered",
					"amount_bought",
					"amount_sold",
					"quote_currency_held",
					"assets_held",
					"current_networth",
					"timestamp"
				};
		
		String parameters[ ] = new String[] {
					"sma",
					"std_multiplier",
					"stop_loss_percent"
				};
		
		String parameterValues[ ] = new String[] {
				"20",
				"2",
				".10"
			};
		
		
		//Test simple moving strat
		Signal bollingerBuySignal  = new BollingerBandsBuySignal( 20, 2.00 );
		Signal bollingerSellSignal = new BollingerBandsSellSignal( 20, 2.00 );
		
		//TEST SMA CROSS STRATEGY
		SignalStrategy bollingers = new SignalStrategy( new BigDecimal( .1 ).setScale(2, BigDecimal.ROUND_DOWN ), bollingerBuySignal, bollingerSellSignal );
		bollingers.setWaitingToBuy( true );
		
		try
		{
			Backtest backtest  = new Backtest( bollingers,  exchange, candle, base, counter, 10000, null, true, useUSD );
			backtest.setupCSVReport( headers, parameters, parameterValues, filename );
			BacktestReport bp = backtest.run( );
			bp.generateReport( false );
			backtest.completeCSVReport( bp );
		}
		
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
	
	/**
	 * Call to test Local minima/maxima Signals : test case LMMS
	 *
	public static void testMinimaMaximaSignals( String name, String params )
	{
		String[] paramList = params.split( "," );
		Boolean useUSD     = Boolean.valueOf( paramList[ 0 ] );
		String base		   = paramList[ 1 ];
		String counter	   = paramList[ 2 ];
		String exchange    = paramList[ 3 ];
		String candle      = paramList[ 4 ];
		String filename    = name + "_" + base + counter + "_" + exchange + "_" + candle + "_" + useUSD;
		
		//CSV Headers
		String headers[] = new String[] {
					"open",
					"close",
					"local_min",
					"local_max",
					"signal",
					"stop_loss_triggered",
					"amount_bought",
					"amount_sold",
					"quote_currency_held",
					"assets_held",
					"current_networth",
					"timestamp"
				};
		
		String parameters[ ] = new String[] {
					"window_size",
					"stop_loss_percent"
				};
		
		String parameterValues[ ] = new String[] {
				"25",
				".10"
			};
		
		
		//Test simple moving strat
		Signal minimaSignal  = new LocalMinimaBuySignal( 25 );
		Signal maximaSignal  = new LocalMaximaSellSignal( 25 );
		
		//TEST SMA CROSS STRATEGY
		SignalStrategy sma = new SignalStrategy( new BigDecimal( .1 ).setScale(2, BigDecimal.ROUND_DOWN ), minimaSignal, maximaSignal );
		sma.setWaitingToBuy( true );
		
		try
		{
			Backtest backtest  = new Backtest( sma,  exchange, candle, base, counter, 10, null, true, useUSD );
			backtest.setupCSVReport( headers, parameters, parameterValues, filename );
			BacktestReport bp = backtest.run( );
			bp.generateReport( false );
			backtest.completeCSVReport( bp );
		}
		
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
	**/
}
