package gsplatform.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import gsplatform.services.BacktestingData;

public class CSVParser extends DataParser {

	public CSVParser(String[] fieldNames, String filepath) 
	{
		super(fieldNames, filepath);
	}

	@Override
	public BacktestingData createBacktestingData( ) throws IOException
	{
        final String cvsSplitBy = ",";
        
        BufferedReader bufferedReader = null;
        String line = "";
		
        bufferedReader = new BufferedReader( new FileReader( filepath ) );
				
        boolean isHeaders = true;
        
	    while ( ( line = bufferedReader.readLine( ) ) != null ) 
	    {
	    		String[] values = line.split( cvsSplitBy );
	    		
	    		if( isHeaders )
	    		{
	    			isHeaders = false;
	    			continue;
	    		}
	    		
	    		int i = 0; for( i = 0; i < values.length; i++ )
	    		{
	    			this.backtestingData.put( this.fieldNames.get( i ), values[ i ] );
	    		}
	    		
	    		this.backtestingData.put( "LAST_CANDLE", "No" );
	    		this.backtestingData.incrementExamples( );
	    }
	    
	    int size = this.backtestingData.get("LAST_CANDLE").size( );
	    this.backtestingData.get("LAST_CANDLE").set( size-1, "Yes" );
	    bufferedReader.close( );
		return backtestingData;
	 }
}
