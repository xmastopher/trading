package gsplatform.utilities;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.Statement;

import gsplatform.servlet.database.GodspeedDatabaseConnect;

public class TestDatabaseInitializer {

	public static String sqlFile 			    = "/Users/danielanderson/git/godspeed_trading/gsplatform/scripts/create_test_database.sql";
	public static String testDbConnect 			= "jdbc:mysql://localhost/godspeedTest";
	public static String testDatabaseUser 		= "davewottle";
	public static String testDatabasePassword 	= "godspeed";
		
	public static void main( String[] args ) throws Exception {
		resetTestDatabase( );
	}
	/**
	 * Used for unit testing w/ test DB call before and after
	 * every unit test to reset the DB state
	 */
	public static void resetTestDatabase( ) {
		
		
		
		try {
			
			//Setup everything
			Connection conn 			     = GodspeedDatabaseConnect.manualConnection( testDbConnect, testDatabaseUser, testDatabasePassword );
			File file 					 = new File( sqlFile );
			FileReader fileReader 		 = new FileReader( file );
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer( );
			String line;
			
			//Grab each line, and process sql statement
			boolean concatentateSql 	   = false;
			boolean comment             = false;
			StringBuilder stringBuilder = new StringBuilder( );
			
			while ((line = bufferedReader.readLine()) != null) {
				
				//Nothing
				if( line.isEmpty( ) || comment ) {
					continue;
				}
				if( line.contains( "/**") ) {
					comment = true;
					continue;
				}
				else if( line.contains( "**/" ) ) {
					 comment = false;
					 continue;
				}
				
				
				//Setup create tables
				if( line.contains( "CREATE TABLE" ) ) {
					stringBuilder.append( line );
					concatentateSql = true;
					continue;
				}
				
				//Handle create tables
				if( concatentateSql ) {
					stringBuilder.append( line );
					if( line.contains(");") ) {
						concatentateSql = false;
						processSql( stringBuilder.toString( ), conn );
						stringBuilder = new StringBuilder( );
					}
				}
				
				//Normal insert
				else if( !concatentateSql ) {
					processSql( line, conn );
				}
			}
		
			conn.close( );
			fileReader.close();
			
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	 }
	
	private static void processSql( String sql, Connection conn ) throws Exception {
		String sqlStatement = sql.replace( ";", "" );
		Statement statement = null; 
		statement 		    = conn.createStatement( );
		statement.execute( sqlStatement );
		System.out.println( sqlStatement );
	}
}
