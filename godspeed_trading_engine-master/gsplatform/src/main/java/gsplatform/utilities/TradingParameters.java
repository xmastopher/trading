package gsplatform.utilities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gsplatform.services.BacktestingData;

public class TradingParameters 
{
	private Map< String, String > parameters;
	
	public TradingParameters( )
	{
		this.parameters = new HashMap< String, String >( );
		initMap( );
	}
	
	public void updateParameterValues( BacktestingData backtestData, double currentBaseCurrency, double currentTradedCurrency, int epoch )
	{
		for( Map.Entry< String, ArrayList< String > > entry : backtestData.getData( ).entrySet() )
		{
			String key = entry.getKey( );
			this.parameters.put( key, backtestData.getData( ).get( key ).get( epoch ) );
		}
		
		this.parameters.put( "CURRENT_BASE_CURRENCY", String.valueOf( currentBaseCurrency ) );
		this.parameters.put( "CURRENT_TRADED_CURRENCY", String.valueOf( currentTradedCurrency ) );
	}
	
	public String get( String key )
	{
		return parameters.get( key );
	}
	
	public double getAsDouble( String key )
	{
		return Double.parseDouble( this.parameters.get( key ) );
	}
	
	public BigDecimal getAsBigDecimal( String key, int precision )
	{
		return new BigDecimal( getAsDouble( key ) ).setScale( precision, BigDecimal.ROUND_DOWN );
	}
	
	public Boolean getAsBool( String key )
	{
		return Boolean.parseBoolean( this.parameters.get( key ) );
	}
	
	public int getAsInt( String key )
	{
		return Integer.parseInt( this.parameters.get( key ) );
	}
	
	public void updateValue( String key, String value )
	{
		parameters.put( key, value );
	}
	
	public List< String > getKeySet( )
	{
		return new ArrayList<String>( this.parameters.keySet( ) );
	}
	private void initMap( )
	{
		this.parameters.put("date", "0.0");
		this.parameters.put("open_time", "0.0");
		this.parameters.put("close_time", "0.0");
		this.parameters.put("last_price", "0.0");
		this.parameters.put("open", "0.0");
		this.parameters.put("high", "0.0");
		this.parameters.put("low", "0.0");
		this.parameters.put("close", "0.0");
		this.parameters.put("volume", "0.0");
		this.parameters.put("quote asset volume", "0.0");
		this.parameters.put("nu_of_trades", "0");
		this.parameters.put("taker_buy_base_asset_volume", "0.0");
		this.parameters.put("taker_buy_quote_asset_volume", "0.0");
		this.parameters.put("last_buy_price", "0.0");
		this.parameters.put("last_sell_price", "0.0");
		//this.parameters.put("VIX", "0.0");
	}
	
}
