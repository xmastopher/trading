package gsplatform.utilities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import gsplatform.services.BacktestingData;

public abstract class DataParser {
	
	protected BacktestingData backtestingData;
	protected List<String> fieldNames;
	protected String filepath;
	
	public DataParser( String[] fieldNames, String filepath )
	{
		this.fieldNames      = Arrays.asList( fieldNames );
		this.filepath        = filepath;
		this.backtestingData = new BacktestingData( fieldNames );
	}
	
	public abstract BacktestingData createBacktestingData(  ) 
	
	throws FileNotFoundException, IOException;

}
