package gsplatform.utilities;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import java.util.Random;

public class SendEmail {
	
	/**
	 * The body of the email
	 */
	public static final String emailMessage     = "Hello %s thank you for signing up for traderspot!  In order to complete the registration process please go to the "
											   + "verification link below and enter your password! \n\n %s \n \n Cheers, \n\n Lambo Moon";
   
	/**
	 * The dynamic verifcation link following REST protocol
	 */
	public static final String verificationLink = ".../pathto/page.html?verificationToken=%d";
	
	/**
	 * The subject of the email
	 */
	public static final String subject          = "Verify your profile at traderspot";
	
	/**
    * Use the password used at signup to create a verification token for email verification
    * @return A random number between 0 and MAX_INT
    */
   public int makeVerificationToken( )
   {
	   Random rand = new Random();
	   int  n = rand.nextInt( Integer.MAX_VALUE ) + 1;
	   return n;
   }
   
   /**
    * Call to buld the rest verification link with a randomized token
    * @return	A string with the token filled in
    */
   public String buildVerificationLink( )
   {
	   return String.format( verificationLink, makeVerificationToken( ) );
   }
   
   /**
    * Call to build the message for the email body
    * @param username	The username the email address belongs to
    * @return			A string with the message body
    */
   public String buildMessage( String username )
   {
	   return String.format( emailMessage, username, buildVerificationLink( ) );
   }

   public static void main( String [] args ) throws AddressException, MessagingException {    
	   
       final String username = "dtanderson005@gmail.com";
       final String password = "*********";

       Properties props = new Properties();
       props.put("mail.smtp.starttls.enable", "true");
       props.put("mail.smtp.auth", "true");
       props.put("mail.smtp.host", "smtp.gmail.com");
       props.put("mail.smtp.port", "587");

       Session session = Session.getInstance(props, new javax.mail.Authenticator() {
    	    @Override
			protected PasswordAuthentication getPasswordAuthentication() {
    	        return new PasswordAuthentication(username, password);
    	    }
    	});

       try {

           Message message = new MimeMessage(session);
           message.setFrom(new InternetAddress("dtanderson005@gmail.com"));
           message.setRecipients(Message.RecipientType.TO,
               InternetAddress.parse("dtanderson005@gmail.com"));
           message.setSubject("Testing Subject");
           message.setText("Dear Mail Crawler,"
               + "\n\n No spam to my email, please!");

           Transport.send(message);

           System.out.println("Done");

       } catch (MessagingException e) {
           throw new RuntimeException(e);
       }
   }
}