package gsplatform.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.currency.*;

import gsplatform.factories.AlgorithmParameter;
import gsplatform.services.ExchangeHandler;

public class GS_Utils 
{
	public static String convertToMonetary( double value )
	{
		BigDecimal bigDecimal = new BigDecimal( value ).setScale( 2, BigDecimal.ROUND_HALF_UP );
		return "$" + bigDecimal.toString( );
	}
	public static String convertToMonetary( BigDecimal bigDecimal )
	{
		return convertToMonetary( bigDecimal.doubleValue( ) );
	}
	
	public static String convertToPercent( double value )
	{
		BigDecimal bigDecimal = new BigDecimal( value * 100 ).setScale( 2, BigDecimal.ROUND_HALF_UP );
		return bigDecimal.toString( ) + "%";
	}
	
	public static String convertToPercent( BigDecimal bigDecimal )
	{
		return convertToPercent( bigDecimal.doubleValue( ) );
	}
	
	public static BigDecimal convertToDecimalFromPercentString( String percent ) {
		percent = percent.replace( "%", "" );
		return new BigDecimal( percent ).divide( new BigDecimal( 100 ) ).setScale( 2, BigDecimal.ROUND_DOWN );
	}
	
	public static String[] toStringArray( JSONArray array ) 
	{
	    if(array.length() == 0 )
	        return new String[] {};

	    String[] arr=new String[array.length()];
	    for(int i=0; i<arr.length; i++) {
	        arr[i]=array.optString(i);
	    }
	    return arr;
	}
	
	public static Double[] toDoubleArray( JSONArray array ) 
	{
	    if(array.length() == 0 )
	        return new Double[] {};

	    Double[] arr= new Double[array.length()];
	    
	    for(int i=0; i<arr.length; i++) {
	        arr[i ]= Double.valueOf( array.optString(i) );
	    }
	    return arr;
	}
	
    /**
     * Get token balances via ethplorer.io
     *
     * @param address Ether address
     * @param b       Network callback to @see rehanced.com.simpleetherwallet.fragments.FragmentDetailOverview#update()
     * @param force   Whether to force (true) a network call or use cache (false). Only true if user uses swiperefreshlayout
     * @throws IOException Network exceptions
     * @throws JSONException 
     */
    public static JSONObject getTokenBalancesEtherIO( String address ) throws IOException, JSONException
    {
		String url = "https://api.ethplorer.io/getAddressInfo/" + address + "?apiKey=freekey";

		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		con.addRequestProperty("User-Agent", "Mozilla/5.0");
		//int responseCode = con.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + url);
		//System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream( ) ) );
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) 
		{
			response.append( inputLine );
		}
		
		
		in.close();

		return new JSONObject( response.toString( ) );
    }
    
    /**
     * Easy helper function to get the index of a value in a plain array
     * @param value
     * @param array
     * @return
     */
    public static int indexOf( String value, AlgorithmParameter[ ] array )
    {
    		for( int i = 0; i < array.length; i++ )
    		{
    			String parameterName = array[ i ].getName( ); 
    			if( parameterName.equals( value ) ) return i;
    		}
    		
    		return -1;
    }
    
	public static String getDatabaseFromExchange( String exchangeName )
	{
		switch( exchangeName )
		{
			case "Binance":
				return "historicalbinancedata";
		}
		
		return "historicalbinancedata";
	}
	
	public static String getTableFromCandles( String candleSize )
	{
		switch( candleSize )
		{
			case "1Hour":
				return "1HourCandles";
			case "2Hour":
				return "2HourCandles";
		}
		
		return "1HourCandles";
	}
	
	public static List< CurrencyPair >  getCurrencyPairsFromExchange( String exchangeName )
	{
		Exchange binance = ExchangeHandler.getExchangePublic( exchangeName );
		return binance.getExchangeSymbols( );
	}
	
	public static String getJsonFromRequest( HttpServletRequest request ) throws IOException
	{
		// 1. get received JSON data from request
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String json = "";
        if(br != null){
            json = br.readLine();
        }
        return json;
	}
	
	public static List< String > jsonArrayToList( JSONArray jsonArray ) throws JSONException
	{
		List< String > list = new ArrayList< String >( ); 
		
		if ( jsonArray != null ) 
		{ 
		   int len = jsonArray.length( );
		   
		   for ( int i=0; i < len; i++ )
		   { 
			   list.add (jsonArray.get( i ).toString( ) );
		   } 
		} 
		
		return list;
	}
	
	/**
	 * Takes in a list of string and returns the same list with whitespace removed from every value
	 * @param inputArray
	 * @return
	 */
	public static String[] removeAllWhitespace( String[] inputArray )
	{
		String[] outputArray = new String[ inputArray.length ];
		
		for( int i = 0; i < inputArray.length; i++ )
		{
			outputArray[ i ] = inputArray[ i ].replaceAll("\\s+","");
		}
		
		return outputArray;
	}
	
	/**
	 * Returns a random hex string of length numChars
	 * @param numChars
	 * @return
	 */
	public static String generateRandomHexString( int numChars )
	{
        Random r = new Random( );
        StringBuffer sb = new StringBuffer( );
        
        while(sb.length() < numChars) {
            sb.append( Integer.toHexString( r.nextInt( ) ) );
        }

        return sb.toString().substring( 0, numChars );
	}
	
	public static double calculateAverage( List < Double > marks ) {
		  Double sum = 0.0;
		  if(!marks.isEmpty()) {
		    for (Double mark : marks) {
		        sum += mark;
		    }
		    return sum / marks.size();
		  }
		  return sum;
		}
	
	public static BigDecimal getMinimum( List< BigDecimal > bigDecimals )
	{
		BigDecimal minimum = new BigDecimal( Double.MAX_VALUE );
		
		for( BigDecimal value : bigDecimals )
		{
			minimum = GS_Utils.min( value, minimum );
		}
		
		return minimum.setScale( 8, BigDecimal.ROUND_DOWN );
	}
	
	public static BigDecimal getMaximum( List< BigDecimal > bigDecimals )
	{
		BigDecimal maximum = new BigDecimal( Double.MIN_VALUE );
		
		for( BigDecimal value : bigDecimals )
		{
			maximum = GS_Utils.max( value, maximum );
		}
		
		return maximum.setScale( 8, BigDecimal.ROUND_DOWN );
	}
	
	public static BigDecimal max( BigDecimal left, BigDecimal right )
	{
		return new BigDecimal( Math.max( left.doubleValue( ), right.doubleValue( ) ) );
	}
	
	public static BigDecimal min( BigDecimal left, BigDecimal right )
	{
		return new BigDecimal( Math.min( left.doubleValue( ), right.doubleValue( ) ) );
	}
	
	
	public static boolean sameClassFromJSON( JSONObject left, JSONObject right ) throws JSONException
	{
		return left.getString( "class" ).equalsIgnoreCase( right.getString( "class" ) );
	}
	
	public static String createFunctionBasedString( String functionName, String param )
	{
		return functionName + "(" + param + ")";
	}
	
	public static long compareTwoTimeStamps(java.sql.Timestamp currentTime, java.sql.Timestamp oldTime)
	{
	    long milliseconds1 = oldTime.getTime();
	  long milliseconds2 = currentTime.getTime();

	  long diff = milliseconds2 - milliseconds1;
	  long diffSeconds = diff / 1000;
	  long diffMinutes = diff / (60 * 1000);
	  long diffHours = diff / (60 * 60 * 1000);
	  long diffDays = diff / (24 * 60 * 60 * 1000);

	  return diffMinutes;
	}
	
	public static BigDecimal divideIntsAsBigDecimals( int left, int right ){
		return new BigDecimal( left )
					.setScale( 2, BigDecimal.ROUND_DOWN )
					  .divide( new BigDecimal( right )
					    .setScale( 2, BigDecimal.ROUND_DOWN ), 2, RoundingMode.DOWN )
					      .setScale( 2, BigDecimal.ROUND_DOWN );
	}

	
}
