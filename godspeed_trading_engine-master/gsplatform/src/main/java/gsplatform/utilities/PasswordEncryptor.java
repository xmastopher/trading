package gsplatform.utilities;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordEncryptor 
{
	public static String encryptPassword( String password )
	{
	    String generatedSecuredPasswordHash = BCrypt.hashpw( password, BCrypt.gensalt( 12 ) );
	    return ( generatedSecuredPasswordHash );
	}
	
	public static Boolean passwordsMatch( String password, String hashedPassword )
	{
		return BCrypt.checkpw( password, hashedPassword );
	}
	
	public static void main( String[] args )
	{
		String username = "chillafresh";
		String password = "password";
		
		String encryptedPassword = PasswordEncryptor.encryptPassword( password );
		
		System.out.println( encryptedPassword );
	}
}
