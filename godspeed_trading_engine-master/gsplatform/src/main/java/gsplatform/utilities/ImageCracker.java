package gsplatform.utilities;
import java.awt.image.BufferedImage;
import net.sourceforge.tess4j.*;

public class ImageCracker 
{
    public static String crackImage( BufferedImage image ) {
        ITesseract instance = new Tesseract();
        instance.setDatapath("/usr/local/Cellar/tesseract/3.05.01/share/");
        try {
            String result = instance.doOCR( image );
            return result;
        } catch (TesseractException e) {
            System.err.println(e.getMessage());
            return "Error while reading image";
        }
    }
}