package gsplatform.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class CryptoCurrencyLookup 
{
	private static final String binance_path = "/Users/danielanderson/desktop/crypto_markets/binance_eth_markets.txt";
	private static final String bittrex_path = "/Users/danielanderson/desktop/crypto_markets/bittrex_eth_markets.txt";
	
	private static HashMap<String, String> binanceEthereumMarkets = new HashMap<String, String>();
	private static HashMap<String, String> bittrexEthereumMarkets = new HashMap<String, String>();
	
	private static HashMap<String, String> binanceEthereumMarketsInverted = new HashMap<String, String>();
	private static HashMap<String, String> bittrexEthereumMarketsInverted = new HashMap<String, String>();
	
	public static void buildAllMarkets() throws IOException
	{
		File binanceFile = new File( binance_path );
		File bittrexFile = new File( bittrex_path );
		
		fillLookups( binanceFile, binanceEthereumMarkets, binanceEthereumMarketsInverted );
		fillLookups( bittrexFile, bittrexEthereumMarkets, bittrexEthereumMarketsInverted );
	}
	
	private static void fillLookups( File file, HashMap<String, String> lookup, HashMap<String, String> iLookup ) throws IOException
	{
		FileReader fileReader = new FileReader( file );
		BufferedReader bufferedReader = new BufferedReader( fileReader );
		
		StringBuffer stringBuffer = new StringBuffer( );
		String line;
		
		while ( (line = bufferedReader.readLine( ) ) != null ) 
		{
			String symbol = line.split( "\t")[0].trim( );
			String name   = line.split( "\t")[1].trim( ); 
			iLookup.put( symbol.toLowerCase( ), name.toLowerCase( ) );
			lookup.put( name.toLowerCase( ), symbol.toLowerCase( ) );	
		}
		
		fileReader.close();
	}
	
	public static HashMap< String, String > getMarket( String service, boolean invert ) throws IOException
	{
		if( service == "BITTREX" )
		{
			if( invert )
			{
				return bittrexEthereumMarketsInverted;
			}
			else
			{
				return bittrexEthereumMarkets;
			}
		}
		
		else if( service == "BINANCE" )
		{
			if( invert )
			{
				return binanceEthereumMarketsInverted;
			}
			else
			{
				return binanceEthereumMarkets;
			}
		}
		
		return null;
	}
	
	public static ArrayList< String > getAllTokensAsList( HashMap< String, String > tokenMap )
	{
		ArrayList< String > keys   = new ArrayList< String >( );
		ArrayList< String > values = new ArrayList< String >( );
		
	    for( Entry< String, String > entry : tokenMap.entrySet( ) ) 
	    {
	        keys.add( entry.getKey() );
	        values.add( entry.getValue() );
	    }

		
		keys.addAll( values );
		return keys;
	}
}
