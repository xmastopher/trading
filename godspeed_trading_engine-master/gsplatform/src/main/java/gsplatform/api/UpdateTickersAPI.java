package gsplatform.api;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.UserTickersDAO;

public class UpdateTickersAPI extends PlatformAPI {
	
	/**
	 * Grabs a user's tickers for dashboard
	 */
	private UserTickersDAO userTickersDAO;
	
	public UpdateTickersAPI( JSONObject parameters ) throws JSONException 
	{
		super(parameters);
		this.userTickersDAO 		 = new UserTickersDAO( parameters.getString( "username" ) );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry("API", "UpdateTickersAPI", "request: " + this.parameters.toString( ) );
		JSONObject response = new JSONObject( );
		response.put( "success", true);
		
		try
		{
			this.userTickersDAO.openConnection( );
			this.userTickersDAO.updateTicker( this.parameters.getString( "ticker"), this.parameters.getInt( "tickerBox" ) );
			response.put( "success", true );
			response.put( "message", "Succesfully updated ticker!" );
			this.userTickersDAO.closeConnection( );
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API", "UpdateTickersAPI", e.getMessage( ) );
			response.put( "success", false );
			response.put( "message", e.getMessage() );
		}
		
		LOGGER.addMessageEntry( "API", "UpdateTickersAPI", "response: " + response.toString( ) );
		return response;
	}

}
