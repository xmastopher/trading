package gsplatform.api;

import java.math.BigDecimal;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.BotTradeMetrics;
import gsplatform.utilities.GS_Utils;

/**
 * Query for a user's bot's historical daily performances.
 * 
 * REQUEST:
 * {
 * 		username: 'username',
 * 	    botId: int
 * }
 * 
 * RESPONSE: array list of bot trade metrics
 * @author danielanderson
 *
 */
public class HistoricalBotPerformanceAPI extends PlatformAPI {
	
	private BotLeaderboardsDAO botLeaderboardsDAO;
	
	/**
	 * Only provided if grabbing rank
	 */
	private String username;
	
	/**
	 * Only provided if grabbing rank
	 */
	private int botId;
	
	/**
	 */
	public HistoricalBotPerformanceAPI( JSONObject parameters ) throws JSONException
	{
		super( parameters ); 
		
		try {
			this.botId 		       = parameters.getInt( "botId" );
			this.username    	   = parameters.getString( "username" );
			this.botLeaderboardsDAO = new BotLeaderboardsDAO( this.username );
		}
		catch( Exception e ){
			this.botLeaderboardsDAO = new BotLeaderboardsDAO( );
			return;
		}

		
	}
	
	@Override
	public JSONObject createJSON( ) throws JSONException
	{
		
		JSONObject response      = new JSONObject( );
		response.put("success", true);
		JSONArray responseArray  = new JSONArray( );
		LOGGER.addMessageEntry( "API", "HistoricalBotPerformanceAPI", "request: " + this.parameters );
		
		try
		{
			this.botLeaderboardsDAO.openConnection( );
			
			List< BotTradeMetrics > botTradeMetrics = this.botLeaderboardsDAO.getUserHistoricalTradeMetrics( this.botId );

			for( BotTradeMetrics btm : botTradeMetrics )
			{
				//Calculate W/L
				BigDecimal winLossRatio = btm.getNumWins( ) != 0 ? 
										  new BigDecimal( ((double)btm.getNumWins( )) / ((double)btm.getNumLosses( )) ) :
									      BigDecimal.ZERO;
										  
				//Calculate ROI as percent
				BigDecimal roi			= btm.getNetworth( ).
										   divide( BigDecimal.ONE ).
										    subtract( BigDecimal.ONE ).
											 setScale( 4, BigDecimal.ROUND_DOWN );	
				
				BigDecimal avgRoi = BigDecimal.ZERO;
				
				if( btm.getNumTrades( ) != 0 ) {
					double tRoi   = btm.getTotalROI().doubleValue();
					double numT   = btm.getNumTrades();
					avgRoi = new BigDecimal( tRoi / numT );
				}
				else {
					roi = BigDecimal.ZERO;
				}
				
				JSONObject botEntry = new JSONObject( );
				
				botEntry.put("username", btm.getUsername( ) );
				botEntry.put( "botName", btm.getBotName( ) );
				botEntry.put( "exchange", btm.getExchange( ) );
				botEntry.put( "market", btm.getMarket( ) );
				botEntry.put( "avgRoi", GS_Utils.convertToPercent( avgRoi ) );
				botEntry.put( "roi", GS_Utils.convertToPercent( roi ) );
				botEntry.put( "numTrades", btm.getNumTrades( ) );
				botEntry.put( "winLoss", winLossRatio );
				botEntry.put( "bestTrade", GS_Utils.convertToPercent( btm.getBestTrade( ) ) );
				responseArray.put( botEntry );
			}
			
			this.botLeaderboardsDAO.closeConnection( );
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "HistoricalBotPerformanceAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}

		response.put( "bots", responseArray );
		LOGGER.addMessageEntry( "API", "HistoricalBotPerformanceAPI", "response: " + response );
		return response;
	}

}
