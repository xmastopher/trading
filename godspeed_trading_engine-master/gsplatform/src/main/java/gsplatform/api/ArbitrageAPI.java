package gsplatform.api;

import java.io.IOException;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.services.ArbitrageAnalysis;
import gsplatform.services.ArbitrageAnalysis.ArbPair;
import gsplatform.utilities.GS_Utils;

public class ArbitrageAPI extends PlatformAPI{
	
	/**
	 * Associated service, the arbitrage master :)
	 */
	private ArbitrageAnalysis arbitrageAnalysis;
	
	/**
	 * The first exchange to check
	 */
	private JSONArray exchangePairs;
	
	
	/**
	 * 
	 * @param parameters
	 * @throws JSONException
	 */
	public ArbitrageAPI( JSONObject parameters ) throws JSONException {
		
		super( parameters );
		this.exchangePairs = parameters.getJSONArray( "exchanges" );
		arbitrageAnalysis  = new ArbitrageAnalysis( GS_Utils.jsonArrayToList( this.exchangePairs ) );
	}

	@Override
	/**
	 * Returns JSON object list of arbitrage pairs
	 */
	public JSONObject createJSON( ) throws IOException, JSONException { 
		
		JSONObject response = new JSONObject( );
		JSONArray arbs      = new JSONArray( );
		response.put( "success", true );
	    
		LOGGER.addMessageEntry( "API", "ArbitrageAPI", "request: " + this.parameters );
		
		try {
			List< ArbPair > arbPairs = this.arbitrageAnalysis.doAnalysis( );
		    for( ArbPair arbPair : arbPairs )
		    {
		    		//Setup object and grab pair
		    		JSONObject arbitrageObject = new JSONObject( );
		    		
		    		//Add the key - market
		    		arbitrageObject.put( "market", arbPair.getMarket( ) );
		    		
		    		//Add min
		    		arbitrageObject.put ("min", arbPair.getExchangeOneName( ) );
		    		
		    		//Add max
		    		arbitrageObject.put ("max", arbPair.getExchangeTwoName( ) );
		    		
		    		//Add min
		    		arbitrageObject.put ("minVolume", arbPair.getDailyVolumeMin( ) );
		    		
		    		//Add max
		    		arbitrageObject.put ("maxVolume", arbPair.getDailyVolumeMax( ) );
		        
		    		//Add percent spread
		    		arbitrageObject.put( "spread", GS_Utils.convertToPercent( arbPair.getPercentSpread( ) ) );
		    		arbs.put( arbitrageObject );
		    }
		    
			response.put( "arbitrages", arbs );
		}
		catch( Exception e ) {
			LOGGER.addMessageEntry( "API-EXCPETIONS", "ArbitrageAPI", e.getMessage( ) );
			response.put("success", false);
			response.put("message", e.getMessage( ) );
		}
		
		LOGGER.addMessageEntry( "API", "ArbitrageAPI", "response: " + response.toString( ) );
		return response;

	}

	
}
