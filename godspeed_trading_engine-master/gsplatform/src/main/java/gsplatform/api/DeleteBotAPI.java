package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;


import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.UserBotsDAO;

public class DeleteBotAPI extends PlatformAPI {

	
	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	/**
	 * The ID associated with the bot
	 */
	protected int botId;
	
	/**
	 * The ID associated with the bot
	 */
	protected int strategyId;
	
	/**
	 * Reference to user bot object
	 */
	protected UserBotsDAO userBotsDAO;
	
	
	public DeleteBotAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);

		this.username     = parameters.getString( "username" );
		this.botId        = parameters.getInt( "botId" );
		this.userBotsDAO  = new UserBotsDAO( this.username, this.botId );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "DeleteBotAPI", "request: " + this.parameters );

		
		try
		{
			
			this.userBotsDAO.openConnection( );
			this.userBotsDAO.deleteBot( ); 
			this.userBotsDAO.closeConnection( );
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			successObject.put( "message", "0000: bot successfully deleted" );
			LOGGER.addMessageEntry( "API", "DeleteBotAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "DeleteBotAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}

}
