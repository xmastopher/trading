package gsplatform.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.servlet.database.BotTradeMetrics;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;
import gsplatform.servlet.database.UserSignalsDAO;
import gsplatform.servlet.database.UserTickersDAO;
import gsplatform.utilities.GS_Utils;

public class UserDashboardAPI extends PlatformAPI {

	/**
	 * Bot logs used for grabbing last N signals 
	 */
	private BotLogsDAO botLogsDAO;
	
	/**
	 * Grabs a user's tickers for dashboard
	 */
	private UserTickersDAO userTickersDAO;
	
	/**
	 * Used to grab user's top N bots
	 */
	private BotLeaderboardsDAO botLeaderboardsDAO;
	
	/**
	 * Used to grab user bots
	 */
	private UserBotsDAO userBotsDAO;
	
	/**
	 * Used to grab user bots
	 */
	private UserSignalsDAO userSignalsDAO;
	
	public UserDashboardAPI( JSONObject parameters ) throws JSONException 
	{
		super(parameters);
		this.botLogsDAO 			 = new BotLogsDAO( parameters.getString( "username" ) );
		this.userTickersDAO 		 = new UserTickersDAO( parameters.getString( "username" ) );
		this.botLeaderboardsDAO 	 = new BotLeaderboardsDAO( parameters.getString( "username" ) );
		this.userBotsDAO			 = new UserBotsDAO( parameters.getString( "username" ) );
		this.userSignalsDAO       = new UserSignalsDAO( parameters.getString( "username") );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry("API", "UserDashboardAPI", "request: " + this.parameters.toString( ) );
		JSONObject response = new JSONObject( );
		response.put( "success", true);
		
		try
		{

			//List< Integer > userSignals	   = userSignalsDAO.getSignals( );
			
			//Grab user bots and map them by ID
			this.userBotsDAO.openConnection( );
			List< UserBot > userBots = userBotsDAO.grabAllBots( );
			this.userBotsDAO.closeConnection( );
			Map< Integer, UserBot > userBotMap = toMap( userBots );
			
			//Add bot signals
			JSONArray botSignals = getBotSignals( userBotMap );
			response.put( "botSignals", botSignals );
			
			//Add user tickers
			JSONArray userTickers = getUserTickers( );
			response.put( "tickers", userTickers );
			
			JSONArray botMetrics = getBotMetrics( );
			response.put( "botMetrics", botMetrics );
			
		}
		
		catch( Exception e )
		{ 
			LOGGER.addMessageEntry( "API", "UserDashboardAPI", e.getMessage( ) );
			response.put( "success", false );
			response.put( "message", e.getMessage() );
		}
		
		LOGGER.addMessageEntry( "API", "UserDashboardAPI", "response: " + response.toString( ) );
		return response;
	}
	
	private Map< Integer, UserBot > toMap( List< UserBot > userBots ){
		
		Map< Integer, UserBot > userBotMap = new HashMap< Integer, UserBot >( );
		
		for( UserBot userBot : userBots ) {
			userBotMap.put( userBot.getBotId( ), userBot );
		}
		
		return userBotMap;
	}
	
	/**
	 * Internal method to grab all bot signals
	 * @param userBotMap
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws JSONException
	 */
	private JSONArray getBotSignals( Map< Integer, UserBot > userBotMap ) throws ClassNotFoundException, SQLException, JSONException {
		this.botLogsDAO.openConnection( );
		JSONArray botSignals = new JSONArray( );
		List< BotLog > botLogs = botLogsDAO.getLastNSignals( parameters.getInt( "numSignals" ) );
		for( BotLog botLog : botLogs ) {
			int id = botLog.getBotId( );
			JSONObject botLogJson = new JSONObject( );
			botLogJson.put( "timestamp", botLog.getDated( ) );
			botLogJson.put( "action", botLog.getAction( ) );
			botLogJson.put( "botName", userBotMap.get( id ).getBotName( ) );
			botLogJson.put( "exchange", userBotMap.get( id ).getExchange( ) );
			botLogJson.put( "base", userBotMap.get( id ).getMarketBase( ) );
			botLogJson.put( "quote", userBotMap.get( id ).getMarketQuote( ) );
			botLogJson.put( "signalStates", new JSONObject( botLog.getSignalStates( ) ) );
			botSignals.put( botLogJson );
		}
		this.botLogsDAO.closeConnection( );
		return botSignals;
	}
	
	/**
	 * Internal method to grab all a user's tickers
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws JSONException
	 */
	private JSONArray getUserTickers( ) throws SQLException, ClassNotFoundException, JSONException {
		this.userTickersDAO.openConnection( );
		List< String > tickers = userTickersDAO.getTickers( );
		JSONArray jsonTickers  = new JSONArray( );
		for( String ticker : tickers ) {
			jsonTickers.put( new JSONObject( ticker ) );
		}
		this.userTickersDAO.closeConnection( );
		return jsonTickers;
	}
	
	/**
	 * Internal method to grab a user's personal bot trade metrics
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws JSONException
	 */
	private JSONArray getBotMetrics( ) throws SQLException, ClassNotFoundException, JSONException  {
		this.botLeaderboardsDAO.openConnection( );
		List< BotTradeMetrics > botMetrics = botLeaderboardsDAO.getBotTradeMetrics( 10 );
		JSONArray jsonMetrics = new JSONArray( );
		int rank = 1;
		for( BotTradeMetrics btm : botMetrics ) {
			JSONObject btmObject = new JSONObject( );
			btmObject.put( "botName", btm.getBotName( ) );
			btmObject.put( "roi", getRoiAsPercent( btm ) );
			btmObject.put( "numTrades", btm.getNumTrades( ) );
			btmObject.put( "exchange", btm.getExchange( ) );
			btmObject.put("rank", rank);
			rank++;
			jsonMetrics.put( btmObject );
		}
		this.botLeaderboardsDAO.closeConnection( );
		return jsonMetrics;
	}
	
	private String getRoiAsPercent( BotTradeMetrics btm ) {
		
		if( btm.getNumTrades( ) == 0 ) {
			return "0.00%";
		}
		
		//Calculate ROI as percent
		BigDecimal roi = btm.getNetworth( ).
						  divide( BigDecimal.ONE ).
							subtract( BigDecimal.ONE ).
							  setScale( 4, BigDecimal.ROUND_DOWN );	
		
		return GS_Utils.convertToPercent( roi );
	}

}
