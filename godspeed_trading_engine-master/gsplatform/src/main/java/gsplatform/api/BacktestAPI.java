package gsplatform.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.factories.BacktestFactory;
import gsplatform.services.Backtest;

public class BacktestAPI extends PlatformAPI 
{
	
	/**
	 * Internal member to save the backtesting results
	 */
	private JSONObject returnJSON;
	
	/**
	 * 
	 * @param parameters
	 * @throws JSONException
	 */
	public BacktestAPI(JSONObject parameters) throws JSONException 
	{
		super(parameters);

	}

	@SuppressWarnings("unchecked")
	/**
	 * Provided a list of JSON objects, run a backtest
	 * @param backtestJobs
	 * @return
	 * @throws Exception
	 */
	public void runBacktests( ) throws Exception
	{
		//The results that will be returned
		JSONArray backtestResults  = new JSONArray( );
		
		for ( int i = 0; i < parameters.getJSONArray( "markets" ).length( ); i++ )
		{
		    Backtest backtest = BacktestFactory.createBacktest( this.parameters, i );
		    backtestResults.put( backtest.run( ).generateReportJson( true ) );
		}
		
		//Return backtestResults object list
		JSONObject backtestingJobResults = new JSONObject( );
		backtestingJobResults.put( "backtestResults", backtestResults );
		
		//Set the internal return json
		this.returnJSON = backtestingJobResults;
	}

	@Override
	public JSONObject createJSON() throws JSONException {
		LOGGER.addMessageEntry( "BACKTESTS", "BacktestAPI", "request: " + this.parameters );
		
		try 
		{
			runBacktests();
			this.returnJSON.put( "success", true);
		} 
		catch (Exception e) 
		{
			this.returnJSON.put("success", false);
			this.returnJSON.put("message", e.getMessage( ) );
			// TODO Auto-generated catch block
			LOGGER.addMessageEntry( "BACKTESTS-EXCEPTIONS", "BacktestAPI", "response: " + this.returnJSON );
			e.printStackTrace();
		}
		
		
		LOGGER.addMessageEntry( "BACKTESTS", "BacktestAPI", "response: " + this.returnJSON );
		return this.returnJSON;
	}
}
