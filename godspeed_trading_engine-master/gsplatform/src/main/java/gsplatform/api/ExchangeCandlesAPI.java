package gsplatform.api;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;

import gsplatform.jobs.Constants;
import gsplatform.servlet.database.NomicsMirrorDAO;
import nomics.core.NomicsExchangeCandles;

public class ExchangeCandlesAPI extends PlatformAPI {
	
	private static final String HOST = Constants.instance( ).getHost( );
	private NomicsExchangeCandles nomicsExchangeCandles;
	
	private String exchange;
	
	public ExchangeCandlesAPI(JSONObject parameters) throws JSONException {
		super(parameters);
		this.nomicsExchangeCandles = new NomicsExchangeCandles( );
		this.exchange = parameters.getString( "exchange" );  
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "ExchangeCandlesAPI", "request: " + this.parameters );
		JSONObject response = new JSONObject( );
		response.put("success", true);
		
		NomicsMirrorDAO nomicsMirror = new NomicsMirrorDAO( );

		//Grab the pair given the base and quote
		try {
			nomicsMirror.openConnection( );
			
			//Get the cline candles from the provided request
			JSONArray candles =  null;
			
			//Grab from mirrored DB
			if( this.parameters.getString( "interval").equals( "1d" ) && HOST.equalsIgnoreCase( "ovh" ) ) {
				candles = new JSONArray( nomicsMirror.getDailyCandles( this.exchange.toLowerCase( ), 
													   this.parameters.getString( "base"), 
													   this.parameters.getString( "quote") ) );
			}
			
			//Grab from nomics API
			else {
				String pair = nomicsMirror.getMarketsFromExchangeBaseQuote( this.exchange.toLowerCase( ), 
			 			 this.parameters.getString( "base"),
			 			 this.parameters.getString( "quote") );
				candles = new JSONArray( this.nomicsExchangeCandles.getExchangeCandles( Constants.instance().getNomicsApiKey( ), 
																				   	   this.parameters.getString("interval"), 
																				       this.parameters.getString( "exchange"), 
																				       pair ) );
			}
			nomicsMirror.closeConnection( );
			response.put( "priceData", candles );
		
		}
		catch( Exception e )
		{
			response.put("success", false);
			response.put("message", e.getMessage());
			return response;
		}
		
		LOGGER.addMessageEntry( "API", "ExchangeCandlesAPI", "response: " + response.toString( ).substring(0, 25 ) + "...}" );
		return response;
		
	}

}
