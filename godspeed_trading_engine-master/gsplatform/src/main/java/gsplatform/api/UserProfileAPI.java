package gsplatform.api;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.ExchangeAPI;
import gsplatform.servlet.database.ExchangeAPIDAO;
import gsplatform.servlet.database.UserProfileDAO;
import gsplatform.servlet.database.UserProfileUpdateDAO;

public class UserProfileAPI extends PlatformAPI {

	UserProfileDAO userProfileDAO;
	UserProfileUpdateDAO userProfileUpdateDAO;
	ExchangeAPIDAO exchangeAPIDAO;
	
	public UserProfileAPI( JSONObject parameters ) throws JSONException 
	{
		super(parameters);
		this.userProfileDAO       = new UserProfileDAO( parameters.getString( "username" ) );
		this.exchangeAPIDAO       = new ExchangeAPIDAO( parameters.getString( "username" ) );
		this.userProfileUpdateDAO = new UserProfileUpdateDAO( parameters.getString( "username") );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry("API", "UserProfileAPI", "request: " + this.parameters.toString( ) );
		JSONObject response = new JSONObject( );
		response.put( "success", true);
		try
		{
			//Open conns
			this.userProfileDAO.openConnection( );
			this.exchangeAPIDAO.openConnection( );
			this.userProfileUpdateDAO.openConnection( );
			
			//Grab profile information
			List< String > followers = this.userProfileDAO.getFollowers( );
			int numFollowers = followers.size( );
			int credibility  = this.userProfileDAO.getUserCredibility( );
			String email     = this.userProfileDAO.getEmail( );
			Boolean hasGrant = this.userProfileUpdateDAO.hasDailyGrant( );
			String bio       = this.userProfileDAO.getUserBio( );
			List< ExchangeAPI > exchangeAPIs = this.exchangeAPIDAO.getLinkedExchanges( );
			
			response.put( "followers", followers );
			response.put( "credibility", credibility );
			response.put( "numFollowers", numFollowers );
			response.put( "bio", bio );
			response.put( "email", email );
			response.put( "username", this.parameters.get( "username" ) );
			response.put( "hasGrant", hasGrant );
			
			JSONArray exchangeAPIsJsonArray = new JSONArray( );
			
			for( ExchangeAPI exchangeAPI : exchangeAPIs)
			{
				JSONObject exchangeAPIJsonObject = new JSONObject( );
				exchangeAPIJsonObject.put( "name", exchangeAPI.getExchange( ) );
				exchangeAPIJsonObject.put( "isActive", exchangeAPI.isLinked( ) );
				exchangeAPIsJsonArray.put( exchangeAPIJsonObject );
			}
			
			response.put( "linkedExchanges", exchangeAPIsJsonArray );
			
		}
		
		catch( Exception e)
		{
			LOGGER.addMessageEntry( "API", "UserProfileAPI", e.getMessage( ) );
			response.put( "success", false );
			response.put( "message", e.getMessage() );
		}
		
		LOGGER.addMessageEntry( "API", "UserProfileAPI", "response: " + response.toString( ) );
		return response;
	}

}
