package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserStrategiesPackage;

/**
 * API Specification for saving a strategy in a user's profile. Request JSON comes in the format of the following:
 * 
 * {
 * 		username: ["string"..."string"],
 * 		strategies: "string",
 * 		parameterSettings:	
 * 		[\["param1,param2",\],...\["param1,param2",]]
 * }
 * 
 * Response JSON comes in following format
 * {
 * 		successes: [true,true,...false]
 * 		messages: ["success","error",..."success"] 
 * }
 * 
 * @author danielanderson
 *
 */
public class GetStrategiesAPI extends PlatformAPI 
{
	
	/**
	 * Private reference to service provider 
	 */
	private TradingStrategiesDAO tradingStrategiesDAO;
	
	/**
	 * JSON request object that describes parameters to save
	 * @param parameters
	 * @throws JSONException
	 */
	public GetStrategiesAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);
		this.tradingStrategiesDAO = new TradingStrategiesDAO( parameters.getString( "username" ) );
		
	}

	@Override
	/**
	 * Returns a JSON for saving strategy - a size 1 list with failure may contain exception
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "GetStrategiesAPI", "request: " + this.parameters );
		UserStrategiesPackage usp = new UserStrategiesPackage( );
		JSONObject response = new JSONObject( );
		
		try 
		{
			JSONArray responseArraySupported = new JSONArray( );
			this.tradingStrategiesDAO.openConnection( );
			usp = this.tradingStrategiesDAO.getUserStrategies( );
			
			for( int i = 0; i < usp.size( ); i++ )
			{
				responseArraySupported.put( usp.getStrategyByIdAsJSON( usp.getId( i ) ) );
			}
			
			response.put( "strategies", responseArraySupported );
			this.tradingStrategiesDAO.closeConnection( );
		} 
		
		catch ( ClassNotFoundException | SQLException e ) 
		{
			response.put( "success", false );
			response.put( "message", e.getMessage() );
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "GetStrategiesAPI", e.getMessage( ) );
			return response;
		}

		response.put( "success", true );
		LOGGER.addMessageEntry( "API", "GetStrategiesAPI", "response: " + response.toString( ) );
		return response;
	}

}
