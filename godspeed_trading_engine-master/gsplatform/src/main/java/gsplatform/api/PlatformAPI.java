package gsplatform.api;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import gsplatform.jobs.GS_Logger;

public abstract class PlatformAPI {
	
	/**
	 * Logger
	 */
	protected static final GS_Logger LOGGER = GS_Logger.getLogger( );
	
	/**
	 * Logger - exceptions
	 */
	//final protected static Logger LOGGER_EXCEPTIONS  = Logger.getRootLogger( );
	
	protected JSONObject parameters;
	
	public PlatformAPI( JSONObject parameters )  throws JSONException 
	{
		this.parameters = parameters;
	}
	
	public abstract JSONObject createJSON() throws IOException, JSONException;
	{
		
	}

}
