package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.Conversation;
import gsplatform.servlet.database.Message;
import gsplatform.servlet.database.MessagesDAO;

public class GetMessagesAPI extends PlatformAPI {
	
	/**
	 * Messages object for DB access
	 */
	protected MessagesDAO messagesDAO;
	
	/**
	 * API constructor for passing parameter set via JSON containing username
	 * @param parameters
	 * @throws JSONException
	 */
	public GetMessagesAPI(JSONObject parameters) throws JSONException 
	{
		super(parameters);
		this.messagesDAO = new MessagesDAO( parameters.getString( "username" ) );
	}

	@Override
	/**
	 * Call to retreive a JSON object of conversations w/ messages
	 */
	public JSONObject createJSON( ) throws IOException, JSONException
	{
		LOGGER.addMessageEntry( "API", "GetMessagesAPI", "request: " + this.parameters );

		try
		{
			messagesDAO.openConnection( );
			List< Conversation > conversations = messagesDAO.getConversations( );
			messagesDAO.closeConnection( );
			//Data wrapper
			JSONObject data = new JSONObject( );
			data.put("success", true);
			//List of all conversations
			JSONArray conversationArray = new JSONArray( );
			
			for( Conversation conversation : conversations )
			{
				//Grab all the messages and conversation information
				List< Message > messages = conversation.getMessages( );
				int id = conversation.getConversationId( );
				String subject = messages.get( 0 ).getSubject( );
				
				//JSON for conversation
				JSONObject conversationJSON = new JSONObject( );
				conversationJSON.put( "id", id ); 
				conversationJSON.put( "subject", subject );
				
				//Array for conversation messages
				JSONArray messagesArray = new JSONArray( );
				
				//Wrap up all the messages and add to array
				for( Message message : messages )
				{
					JSONObject messageJSON = new JSONObject( );
					String sender          = message.getFromUser( );
					String recipient       = message.getToUser( );
					String messageBody     = message.getBody( );
					String timestamp       = message.getTimestamp( ).toString( );
					
					messageJSON.put( "sender", sender );
					messageJSON.put( "recipient", recipient );
					messageJSON.put( "message", messageBody );
					messageJSON.put( "timestamp", timestamp );
					
					messagesArray.put( messageJSON );
					
				}
				
				//Add list of messages to conversation and conversation to converstion list
				conversationJSON.put( "messages", messagesArray );
				conversationArray.put( conversationJSON );
			}
			
			data.put( "data", conversationArray );
			LOGGER.addMessageEntry( "API", "GetMessagesAPI", "response: " + data.toString( ) );
			return data;
		}
		
		//If a failure occurs return the error
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "GetMessagesAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}

}
