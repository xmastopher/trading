package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.SignalsLookupDAO;

/**
 * API Specification for saving a strategy in a user's profile. Request JSON comes in the format of the following:
 * 
 * {
 * 		username: ["string"..."string"],
 * 		strategies: "string",
 * 		parameterSettings:	
 * 		[\["param1,param2",\],...\["param1,param2",]]
 * }
 * 
 * Response JSON comes in following format
 * {
 * 		successes: [true,true,...false]
 * 		messages: ["success","error",..."success"] 
 * }
 * 
 * @author danielanderson
 *
 */
public class GetTradingSignalsAPI extends PlatformAPI 
{
	
	/**
	 * Private reference to service provider 
	 */
	private SignalsLookupDAO signalsLookupDAO;
	
	/**
	 * JSON request object that describes parameters to save
	 * @param parameters
	 * @throws JSONException
	 */
	public GetTradingSignalsAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);
		this.signalsLookupDAO = new SignalsLookupDAO( );
		
	}

	@Override
	/**
	 * Returns a JSON for saving strategy - a size 1 list with failure may contain exception
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "GetTradingSignalsAPI", "request: " + this.parameters );
		JSONObject response = new JSONObject( );
		try 
		{
			this.signalsLookupDAO.openConnection();
			JSONArray responseArray = new JSONArray(  );
		
			for( String[] pair :  this.signalsLookupDAO.getAllSignals( ) )
			{
				String name   = pair[0];
				String params = pair[1];
				
				JSONObject json = new JSONObject( );
				json.put( "name", name );
				json.put("parameters", new JSONArray( params ) );
				responseArray.put( json );
			}
			
			List< JSONObject > preconfigured   = this.signalsLookupDAO.getPreconfiguredSignals( );
			JSONArray preconfiguredSellSignals = new JSONArray( );
			JSONArray preconfiguredBuySignals  = new JSONArray( );
			for( JSONObject strategy : preconfigured ) {
				if( strategy.getString( "signal" ).contains( "SELL" ) ) {
					preconfiguredSellSignals.put( strategy );
				}
				else {
					preconfiguredBuySignals.put( strategy );
				}
			}
			response.put( "signals", responseArray );
			response.put( "preconfiguredBuySignals", preconfiguredBuySignals );
			response.put( "preconfiguredSellSignals", preconfiguredSellSignals );
			this.signalsLookupDAO.closeConnection();
			
		} 
		
		catch ( ClassNotFoundException | SQLException e ) 
		{
			response.put( "success", false );
			response.put( "message", e.getMessage() );
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "GetTradingSignalsAPI", e.getMessage( ) );
			return response;
		}

		response.put( "success", true );
		LOGGER.addMessageEntry( "API", "GetTradingSignalsAPI", "response: " + response.toString( ) );
		return response;
	}

}
