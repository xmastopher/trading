package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;

public class DeleteStrategyAPI extends PlatformAPI {

	
	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	/**
	 * The ID associated with the bot
	 */
	protected int strategyId;
	
	/**
	 * Reference to user bot object
	 */
	protected TradingStrategiesDAO tradingStrategiesDAO;
	
	/**
	 * Reference to trading bots DAO - for checking on strategy - bot dependancy
	 */
	protected UserBotsDAO userBotsDAO;
	
	
	public DeleteStrategyAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);

		this.username     = parameters.getString( "username" );
		this.strategyId   = parameters.getInt( "strategyId" );
		this.userBotsDAO  = new UserBotsDAO( this.username );
		this.tradingStrategiesDAO = new TradingStrategiesDAO( this.username );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "DeleteStrategyAPI", "request: " + this.parameters );
		
		try
		{
			String[] botsUsingStrategy = botExistsForStrategy( this.strategyId );
			
			if( botsUsingStrategy.length > 0 ) {
				String message = String.join( "\n ", botsUsingStrategy );
				throw new Exception( "There are currently bot(s) using this strategy:\n " + message );
			}
			
			//If previous check passses - go ahead and delete the strategy
			this.tradingStrategiesDAO.openConnection( );
			this.tradingStrategiesDAO.deleteStrategy( this.strategyId ); 
			this.tradingStrategiesDAO.closeConnection( );
			
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			successObject.put( "message", "0000: strategy successfully deleted" );
			LOGGER.addMessageEntry( "API", "DeleteStrategyAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		
		catch( Exception e)
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "DeleteStrategyAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		} 
	}
	
	/**
	 * Returns list of bot names using this strategy
	 * @param strategyId
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private String[] botExistsForStrategy( int strategyId ) throws ClassNotFoundException, SQLException {
		
		this.userBotsDAO.openConnection( );
		List< UserBot > userBots = this.userBotsDAO.grabBotsUsingStrategy( strategyId );
		this.userBotsDAO.closeConnection( );
		
		String[] returnList = new String[ userBots.size( ) ];
		
		for( int i = 0; i < userBots.size( ); i++ )
		{
			returnList[ i ] = userBots.get( i ).getBotName( );
		}
		
		return returnList;
	}

}
