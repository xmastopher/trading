package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.factories.AlgorithmicTraderFactory;
import gsplatform.factories.TradingStrategyFactory;
import gsplatform.services.BotThreadHandler;
import gsplatform.services.SimulatedAlgorithmicTradingBot;
import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;
import gsplatform.servlet.database.UserStrategiesPackage;

public class StartStopBotAPI extends PlatformAPI {

	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	/**
	 * The ID associated with the bot
	 */
	protected int botId;
	
	/**
	 * The name of the bot - made by user
	 */
	protected String command;
	
	/**
	 * Reference to user bot object
	 */
	protected UserBotsDAO userBotsDAO;
	
	/**
	 * Reference to trading strategies DAO
	 * @param parameters
	 * @throws JSONException
	 */
	protected TradingStrategiesDAO tradingStrategiesDAO;
	
	public StartStopBotAPI( JSONObject parameters ) throws JSONException {
		
		super( parameters );
		
		this.username     = parameters.getString( "username" );
		this.botId        = parameters.getInt( "botId" );
		this.command      = parameters.getString( "command" );
		this.userBotsDAO  = new UserBotsDAO( this.username, this.botId );
		this.tradingStrategiesDAO = new TradingStrategiesDAO( this.username );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "StartStopBotAPI", "request: " + this.parameters );

		try
		{
			
			this.userBotsDAO.openConnection( );
			this.tradingStrategiesDAO.openConnection( );
			
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			
			//Lock bot if specified
			if( this.command.equalsIgnoreCase( "start" ) )
			{
				this.userBotsDAO.startBot( );
				addBotThread( );
				successObject.put( "message", "0000: bot is now running" );
			}
			
			//Unlock otherwise
			else if( this.command.equalsIgnoreCase( "stop" ) )
			{
				this.userBotsDAO.stopBot( );
				removeBotThread( );
				successObject.put( "message", "0000: bot removed from running threads" );
			}
			else
			{
				throw new Exception( "invalid command" );
			}
			
			this.userBotsDAO.closeConnection( );
			LOGGER.addMessageEntry( "API", "StartStopBotAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "BotLogsAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}
	
	/**
	 * Add bot to corresponding runnable
	 * @throws Exception 
	 */
	public void addBotThread( ) throws Exception
	{
		
		//Grab the user bot
		UserBotsDAO userBotsDAO = new UserBotsDAO( this.username );
		userBotsDAO.openConnection( );
		UserBot userBot = userBotsDAO.grabBotFromId( this.botId );
		userBotsDAO.closeConnection( );
		
		//Grab the trading strategy
		TradingStrategiesDAO tradingStrategiesDAO = new TradingStrategiesDAO( this.username );
		tradingStrategiesDAO.openConnection( );
		UserStrategiesPackage usp = tradingStrategiesDAO.getUserStrategyById( userBot.getStrategyId( ) );
		tradingStrategiesDAO.closeConnection( );

		//Grab the strategy
		TradingAlgorithm algorithm = new TradingStrategyFactory( ).createObject( usp, 0 );
		SimulatedAlgorithmicTradingBot atb  = new AlgorithmicTraderFactory( ).createObject( userBot, algorithm );
		BotThreadHandler.getSingletonInstance( ).addBot( userBot, atb );
	}
	
	/**
	 * Remove bot from corresponding runnable
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws JSONException
	 * @throws IOException
	 */
	protected void removeBotThread( ) throws ClassNotFoundException, SQLException, JSONException, IOException
	{		
		//Grab the user bot
		UserBotsDAO userBotsDAO = new UserBotsDAO( this.username );
		userBotsDAO.openConnection( );
		UserBot userBot = userBotsDAO.grabBotFromId( this.botId );
		userBotsDAO.closeConnection( );

		BotThreadHandler.getSingletonInstance( ).removeBot( userBot.getUsername( ), botId, userBot.getExchange(), userBot.getInterval(), userBot.getMarketBase( ) + "-" + userBot.getMarketQuote( ), true );
		
	}
	
}
