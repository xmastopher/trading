package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.UserBotsDAO;


public class SaveBotAPI extends PlatformAPI {
	
	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	/**
	 * The ID associated with the bot
	 */
	protected int botId;
	
	/**
	 * The ID associated with the bot
	 */
	protected int strategyId;
	
	/**
	 * The name of the bot - made by user
	 */
	protected String botName;
	
	/**
	 * The name of the bot - made by user
	 */
	protected Boolean isPrivate;
	
	/**
	 * The name of the bot - made by user
	 */
	protected Double tradingLimit;
	
	/**
	 * The base currency traded
	 */
	protected String baseCurrency;
	
	/**
	 * Quote currency traded against
	 */
	protected String counterCurrency;
	
	/**
	 * The exchange the bot is trading on
	 */
	protected String exchange;
	
	/**
	 * Reference to user bot object
	 */
	protected UserBotsDAO userBotsDAO;
	
	/**
	 * Reference to user bot object
	 */
	protected BotLeaderboardsDAO botLeaderboardsDAO;
	
	/**
	 * Trading candle - interval ie. 1h
	 */
	protected String interval;
	
	/**
	 * API call to save a bot to one's profile
	 * @param parameters
	 * @throws JSONException
	 */
	
	public SaveBotAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);

		this.username           = parameters.getString( "username" );
		this.strategyId         = parameters.getInt( "strategyId" );
		this.botName            = parameters.getString( "botName" );
		this.isPrivate          = parameters.getBoolean( "private" );
		this.tradingLimit       = parameters.getDouble( "tradingLimit" );
		this.exchange           = parameters.getString( "exchange" );
		this.baseCurrency       = parameters.getString( "baseCurrency" );
		this.counterCurrency    = parameters.getString( "counterCurrency");
		this.interval           = parameters.getString( "interval" );
		this.userBotsDAO        = new UserBotsDAO( this.username, this.botId );
		this.botLeaderboardsDAO = new BotLeaderboardsDAO( this.username, this.botId );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "SaveBotAPI", "request: " + this.parameters );

		
		try
		{
			
			this.userBotsDAO.openConnection( );
			this.botLeaderboardsDAO.openConnection( );
			boolean isPublic = this.isPrivate == false;
			int id = this.userBotsDAO.addBot( this.botName, this.strategyId, true, false, isPublic, this.baseCurrency, this.counterCurrency, this.exchange, tradingLimit, this.interval );
			this.botLeaderboardsDAO.setBotId( id );
			this.botLeaderboardsDAO.initBotTrades( this.botName, this.exchange, this.baseCurrency + "-" + this.counterCurrency );
			this.userBotsDAO.closeConnection( );
			this.botLeaderboardsDAO.closeConnection( );
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			successObject.put( "message", "0000: bot saved successfully" );
			LOGGER.addMessageEntry( "API", "SaveBotAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "SaveBotAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}

}
