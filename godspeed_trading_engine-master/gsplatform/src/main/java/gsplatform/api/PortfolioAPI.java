package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.services.Portfolio;
import gsplatform.services.PortfolioAggregator;
import gsplatform.services.Portfolio.CoinAsset;
import gsplatform.utilities.GS_Utils;

public class PortfolioAPI extends PlatformAPI 
{
	
	private PortfolioAggregator portfolioAggregator;
	private String fiatType;
	
	/**
	 * Explicit constructor that takes in JSON params for API call
	 * @param parameters
	 * @throws JSONException
	 */
	public PortfolioAPI( JSONObject parameters ) throws JSONException 
	{
		super( parameters );
		String username = this.parameters.getString( "username" );
		String fiatType = this.parameters.getString( "fiatType" );
		
		try 
		{
			this.portfolioAggregator = new PortfolioAggregator( fiatType, username );
		} 
		catch (ClassNotFoundException | SQLException | IOException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Iterates over list of all user's tokens and calculate proportions at the exchange level.  Wraps information
	 * in JSON fields and returns associated JSON representation of user's aggregated portfolio.
	 */
	@Override
	public JSONObject createJSON( ) throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "PortfolioAPI", "request: " + this.parameters );

		List< Portfolio.CoinAsset > sortedAssets = portfolioAggregator.getPortfolio( );
		JSONObject returnJSON  = new JSONObject( );
		returnJSON.put( "success", true );
		JSONArray tokenObjects = new JSONArray( );
		
		for( int i = 0; i < sortedAssets.size(); i++ )
		{
			JSONObject tokenObject     = new JSONObject( );
			JSONArray tokenLocations   = new JSONArray( );
			JSONArray tokenBalances    = new JSONArray( );
			JSONArray tokenProportions = new JSONArray( );
			
			int totalTokenLocations = 1;
			
			List< String > locationOfCoin   = new ArrayList< String >( );
			List< Double > amountAtLocation = new ArrayList< Double >( );
			
			CoinAsset coinAsset = sortedAssets.get( i );
			
			String symbol = sortedAssets.get( i ).getCoin( );
			tokenObject.put( "symbol", symbol );
			locationOfCoin.add( coinAsset.getLocation( ) );
			amountAtLocation.add( coinAsset.getAmount().doubleValue( ) );
			
			double totalOwnership = coinAsset.getAmount( ).doubleValue( );
			tokenObject.put( this.fiatType, GS_Utils.convertToMonetary( coinAsset.toFiat( this.fiatType ) ) );
			
			while( i+1 != sortedAssets.size() && sortedAssets.get( i+1 ).getCoin().equals( symbol ) )
			{
				coinAsset       = sortedAssets.get( i+1 );
				totalOwnership += coinAsset.getAmount( ).doubleValue( );
				locationOfCoin.add( coinAsset.getLocation( ) );
				amountAtLocation.add( coinAsset.getAmount().doubleValue( ) );
				totalTokenLocations++;
				++i;
			}
			
			tokenObject.put("tokenBalance", GS_Utils.convertToMonetary( coinAsset.getFiatValue( ) ) );
			tokenObject.put( "totalValue", totalOwnership );
			tokenObject.put("portionOfPortfolio", GS_Utils.convertToPercent( coinAsset.getPercentOfPortfolio( ) ) );
			
			for( int t = 0; t < totalTokenLocations; t++ )
			{
				String percentOfTotal = GS_Utils.convertToPercent( amountAtLocation.get( t )  / totalOwnership );
				
				tokenLocations.put( locationOfCoin.get( t ) );
				tokenBalances.put( amountAtLocation.get( t ) );
				tokenProportions.put( percentOfTotal );
			}
			
			tokenObject.put( "tokenLocations", tokenLocations );
			tokenObject.put( "tokenBalances", tokenBalances );
			tokenObject.put( "tokenProportions", tokenProportions );
			
			tokenObjects.put( tokenObject ); 
		}
		
		returnJSON.put( "totalBalance", GS_Utils.convertToMonetary( this.portfolioAggregator.getTotalCombinedFiat( ) ) );
		returnJSON.put( "data", tokenObjects );
		LOGGER.addMessageEntry( "API", "PortfolioAPI", "response: " + returnJSON.toString( ) );
		return returnJSON;
		
	}

}
