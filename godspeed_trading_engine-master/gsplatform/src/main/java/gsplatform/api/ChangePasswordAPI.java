package gsplatform.api;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.UserLoginDAO;
import gsplatform.servlet.database.UserLoginPackage;

/**
 * Platform API for loging in a user, returns a JSON w/ additional information.  The actual LoginServlet sets
 * the user in the system as authenticated
 * @author danielanderson
 *
 */
public class ChangePasswordAPI extends PlatformAPI {
	

	/**
	 * Explicit constructor for handlig user login API
	 * @param parameters
	 * @throws JSONException
	 */
	public ChangePasswordAPI(JSONObject parameters) throws JSONException {
		super(parameters);
	}

	@Override
	/**
	 * Call to create json for user authentication/login
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		JSONObject returnObject = new JSONObject( );
		LOGGER.addMessageEntry( "API", "ChangePasswordAPI", "request: " + this.parameters );
		

		String[] inputCheck = confirmInputs( this.parameters.getString( "password"), this.parameters.getString( "passwordConfirmed"),
										  this.parameters.getString( "newPassword"), this.parameters.getString( "newPasswordConfirmed") );
		
		if( !new Boolean( inputCheck[ 0 ] ) ) {
			returnObject.put( "success", false );
			returnObject.put( "message", inputCheck[ 1 ] );
			return returnObject;
		}
		
		try
		{
			UserLoginDAO userLoginDAO = new UserLoginDAO( this.parameters.getString( "username" ), 
														 this.parameters.getString( "password") );
			
			userLoginDAO.openConnection( );
			UserLoginPackage userLoginPackage = userLoginDAO.authenticateUser( );
			
			//Tries to login, if successful, change password
			if( userLoginPackage.getIsSuccessful( ) ) {
				userLoginDAO.changePassword( this.parameters.getString( "newPassword" ) );
				returnObject.put( "success", true );
				returnObject.put( "message", "Password has succesfully been changed!" );
			}
			else {
				returnObject.put( "success", false );
				returnObject.put( "message", "Old passwords provided are incorrect, please try again with correct passwords" );
			}
			LOGGER.addMessageEntry( "API", "ChangePasswordAPI", "response: " + returnObject.toString( ) );
			userLoginDAO.closeConnection( );
		} 
		
		//Return w/ 0004 fail message and message
		catch (Exception e) 
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "ChangePasswordAPI", e.getMessage( ) );
			returnObject.put( "success", false );
			returnObject.put( "message", "0004: " + e.getMessage( ) );
			e.printStackTrace();
		} 
		
		return returnObject;
	}

	/**
	 * Internal method to sanity check the input
	 * @param currentPassword			The user's submitted curr password
	 * @param currentPasswordConfirmed	The user's submitted confirmed curr password
	 * @param newPassword				The user's new submitted password
	 * @param newPasswordConfirmed		The user's confirmed new password
	 * @return
	 */
	private String[] confirmInputs( String currentPassword, String currentPasswordConfirmed, 
								   String newPassword, String newPasswordConfirmed ) {
		
		if( !currentPassword.equals( currentPasswordConfirmed ) ) {
			return new String[] { "false", "current passwords do not match, please match current passwords." };
		}
		else if( !newPassword.equals( newPasswordConfirmed ) ) {
			return new String[] { "false", "new passwords do not match, please match new passwords." };
		}
		else if( currentPassword.equals( newPassword ) ) {
			return new String[] { "false", "current password entry is the same as new password, please submit a new password" };
		}
		else if( newPassword.length( ) < 8 ) {
			return new String[] { "false", "new password must be larger than eight characters... Come on! Get some better security!" };
		}
		
		return new String[] { "true", "success" };
		
	}
}
