package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.MessagesDAO;

public class SendMessageAPI extends PlatformAPI {

	/**
	 * Messages object for DB access
	 */
	protected MessagesDAO messagesDAO;
	
	/**
	 * Id associated with the message - should always be last message id + 1 
	 */
	protected int messageId;
	
	/**
	 * Id for the conversation to bucket message
	 */
	protected int conversationId;
	
	/**
	 * Who is recieving this message
	 */
	protected String toUser;
	
	/**
	 * Who is the message from
	 */
	protected String fromUser;
	
	/**
	 * The message contents
	 */
	protected String message;
	
	/**
	 * The subject of the message
	 */
	protected String subject;
	
	/**
	 * When was this message sent - should always be NOW( )
	 */
	protected String timestamp;
	
	/**
	 * Main constructor for send message API
	 * @param parameters
	 * @throws JSONException
	 */
	public SendMessageAPI( JSONObject parameters ) throws JSONException 
	{
		super(parameters);
		this.messagesDAO = new MessagesDAO( parameters.getString( "username" ) );
		
		//Grab all params
		this.messageId      = parameters.getInt( "messageId" );
		this.conversationId = parameters.getInt( "conversationId" );
		this.toUser         = parameters.getString( "recipient" );
		this.fromUser       = parameters.getString( "username" );
		this.message        = parameters.getString( "message" );
		this.subject        = parameters.getString( "subject" );
		this.timestamp      = parameters.getString( "timestamp" );
	}

	@Override
	/**
	 * Returns success or failure json if message send was successful
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "SendMessageAPI", "request: " + this.parameters );
		try
		{
			messagesDAO.openConnection( );
			messagesDAO.sendMessage( this.messageId, this.conversationId, this.toUser, 
											this.message, this.subject, this.timestamp);
			messagesDAO.closeConnection();
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			successObject.put( "message", "0000: message sent successfully" );
			LOGGER.addMessageEntry( "API", "SendMessageAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "SendMessageAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}

}
