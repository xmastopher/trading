package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.UserBotsDAO;

public class ChangePrivacyAPI extends PlatformAPI {
	
	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	/**
	 * The ID associated with the bot
	 */
	protected int botId;
	
	/**
	 * The ID associated with the bot
	 */
	protected int strategyId;
	
	/**
	 * The name of the bot - made by user
	 */
	protected Boolean isPrivate;
	
	/**
	 * Reference to user bot object
	 */
	protected UserBotsDAO userBotsDAO;
	
	public ChangePrivacyAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);
		this.username     = parameters.getString( "username" );
		this.botId        = parameters.getInt( "botId" );
		this.isPrivate    = parameters.getBoolean( "private" );
		this.userBotsDAO  = new UserBotsDAO( this.username, this.botId );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "ChangePrivacyAPI", "request: " + this.parameters );
		
		try
		{
			
			this.userBotsDAO.openConnection( );
			
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			
			//Lock bot if specified
			if( this.isPrivate )
			{
				this.userBotsDAO.makePrivate( );
				successObject.put( "message", "0000: bot privatized successfully" );
			}
			
			//Unlock otherwise
			else
			{
				this.userBotsDAO.makePublic( );
				successObject.put( "message", "0000: bot publicized successfully" );
			}
			
			this.userBotsDAO.closeConnection( );
			LOGGER.addMessageEntry( "API", "ChangePrivacyAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "ChangePrivacyAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}

}
