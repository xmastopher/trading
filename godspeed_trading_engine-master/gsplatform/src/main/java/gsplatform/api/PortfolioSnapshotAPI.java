package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.services.PortfolioSnapshotHandler;

/**
 * API specification for requesting a portfolio snapshot 
 * @author danielanderson
 *
 */
public class PortfolioSnapshotAPI extends PlatformAPI 
{
	
	/**
	 * Internal service provider for grabbing snapshot
	 */
	private PortfolioSnapshotHandler portfolioSnapshotHandler;
	
	/**
	 * Explicit constructor for snapshot API
	 * @param parameters						JSON object with fields 'username' and 'snapshot'
	 * @throws JSONException					
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public PortfolioSnapshotAPI(JSONObject parameters) throws JSONException, ClassNotFoundException, SQLException {
		super(parameters);
		//Grab username and snapshot
		String username = parameters.getString("username");
		String snapshot = parameters.getString("snapshot");
		
		//Create service provider for snapshots
		this.portfolioSnapshotHandler = new PortfolioSnapshotHandler( username, snapshot );
		
	}

	/**
	 * Generate JSON with snapshot of portfolio
	 */
	@Override
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "PortfolioSnapshotAPI", "request: " + this.parameters );

		String response = this.portfolioSnapshotHandler.getPortfolioSnapshot( );
		
		if( response.contains( "Failure" ) )
		{
			String message = response.split( ":" )[ 1 ];
			
			JSONObject errorJSON = new JSONObject();
			
			errorJSON.put( "success", false );
			errorJSON.put( "message", message );
			return errorJSON;
		}
		
		JSONObject successfulJSON = new JSONObject( response );
		successfulJSON.put( "success", true );
		LOGGER.addMessageEntry( "API", "PortfolioSnapshotAPI", "response: " + successfulJSON.toString( ) );
		return successfulJSON;
	}
	
	
}
