package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.jobs.Constants;
import gsplatform.servlet.database.SessionTokensDAO;
import gsplatform.servlet.database.UserLoginDAO;
import gsplatform.servlet.database.UserLoginPackage;

/**
 * Platform API for loging in a user, returns a JSON w/ additional information.  The actual LoginServlet sets
 * the user in the system as authenticated
 * @author danielanderson
 *
 */
public class LoginAPI extends PlatformAPI {
	
	/**
	 * The DAO object for user login
	 */
	private UserLoginDAO userLoginDAO;
	
	/**
	 * Ref to session tokens
	 */
	private SessionTokensDAO sessionTokensDAO;
	
	/**
	 * username associated
	 */
	private String username;
	
	/**
	 * The password associated
	 */
	private String password;
	
	/**
	 * Explicit constructor for handlig user login API
	 * @param parameters
	 * @throws JSONException
	 */
	public LoginAPI(JSONObject parameters) throws JSONException {
		super(parameters);
		this.username         = this.parameters.getString( "username" );
		this.password         = this.parameters.getString( "password" );
		this.userLoginDAO     = new UserLoginDAO( username, password );
		this.sessionTokensDAO = new SessionTokensDAO( this.parameters.getString( "username" ) );
	}

	@Override
	/**
	 * Call to create json for user authentication/login
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		JSONObject returnObject = new JSONObject( );
		LOGGER.addMessageEntry( "API", "LoginAPI", "request: " + this.parameters );
		
		try
		{
			//Grab new session token
			String sessionToken = updateAndGetSessionToken( );
			
			this.userLoginDAO.openConnection( );
			UserLoginPackage userLoginPackage = this.userLoginDAO.authenticateUser( );
			returnObject.put( "username", userLoginPackage.getUsername( ) );
			returnObject.put( "email",  userLoginPackage.getEmail( ) );
			returnObject.put( "success", userLoginPackage.getIsSuccessful( ) );
			returnObject.put( "message", userLoginPackage.getMessage( ) );
			returnObject.put( "redirect", Constants.instance().getRedirectAfterLogin( ) );
			returnObject.put( "sessionToken", sessionToken );
			LOGGER.addMessageEntry( "API", "LoginAPI", "response: " + returnObject.toString( ) );
			this.userLoginDAO.closeConnection( );
		} 
		
		//Return w/ 0004 fail message and message
		catch (ClassNotFoundException e) 
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "LoginAPI", e.getMessage( ) );
			returnObject.put( "success", false );
			returnObject.put( "message", "0004: " + e.getMessage( ) );
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "LoginAPI", e.getMessage( ) );
			returnObject.put( "success", false );
			returnObject.put( "message", "0004: " + e.getMessage( ) );
			e.printStackTrace( );
		}
		
		return returnObject;
	}
	
	/**
	 * Internal method to grab 64 character session token
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private String updateAndGetSessionToken( ) throws ClassNotFoundException, SQLException
	{
		this.sessionTokensDAO.openConnection( );
		String sessionToken = this.sessionTokensDAO.resetSession( this.username );
		this.sessionTokensDAO.closeConnection( );
		return sessionToken;
	}

}
