package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.MessagesDAO;

public class DeleteMessageAPI extends PlatformAPI {
	
	/**
	 * Messages object for DB access
	 */
	protected MessagesDAO messagesDAO;
	
	/**
	 * Id associated with the message - should always be last message id + 1 
	 */
	protected int messageId;
	
	/**
	 * Id for the conversation to bucket message
	 */
	protected int conversationId;
	
	/**
	 * Who is recieving this message
	 */
	protected String toUser;
	
	/**
	 * Who is the message from
	 */
	protected String fromUser;
	
	/**
	 * The message contents
	 */
	protected String message;
	
	/**
	 * The subject of the message
	 */
	protected String subject;
	
	/**
	 * When was this message sent - should always be NOW( )
	 */
	protected String timestamp;
	
	/**
	 * Main constructor for send message API
	 * @param parameters
	 * @throws JSONException
	 */
	public DeleteMessageAPI( JSONObject parameters ) throws JSONException 
	{
		super( parameters );
		this.messagesDAO = new MessagesDAO( parameters.getString( "username" ) );
		
		//Grab all params
		this.messageId      = parameters.getInt( "messageId" );
		this.conversationId = parameters.getInt( "conversationId" );
	}

	@Override
	/**
	 * Returns success or failure json if message send was successful
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "DeleteMessageAPI", "request: " + this.parameters );

		try
		{
			messagesDAO.openConnection( );
			messagesDAO.deleteMessage( this.messageId, this.conversationId );
			messagesDAO.closeConnection( );
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			successObject.put( "message", "0000: message was set to delete" );
			LOGGER.addMessageEntry( "API", "DeleteMessageAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "DeleteMessageAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}
	
}
