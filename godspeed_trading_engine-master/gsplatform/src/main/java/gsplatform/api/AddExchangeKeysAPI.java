package gsplatform.api;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.ExchangeAPIDAO;

public class AddExchangeKeysAPI extends PlatformAPI {
	
	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	protected ExchangeAPIDAO exchangeAPIDAO;
	
	public AddExchangeKeysAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);
		this.username       = parameters.getString( "username" );
		this.exchangeAPIDAO = new ExchangeAPIDAO( parameters.getString( "username") );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "AddExchangeKeysAPI", "request: " + this.parameters );
		JSONObject response = new JSONObject( );
		
		try
		{
			//Open conns
			this.exchangeAPIDAO.openConnection( );
			this.exchangeAPIDAO.AddExchangeAPIKey( this.parameters.getString( "privateKey" ),
												  this.parameters.getString( "publicKey" ),
												  this.parameters.getString( "exchange" ) );
			response.put( "success", true );
			response.put( "message", "api keys have been successfully added to user profile");
			
		}
		
		catch( Exception e)
		{
			LOGGER.addMessageEntry( "API", "AddExchangeKeysAPI", e.getMessage( ) );
			response.put( "success", false );
			response.put( "message", e.getMessage( ) );
		}
		
		LOGGER.addMessageEntry( "API", "AddExchangeKeysAPI", "response: " + response.toString( ) );
		return response;
	}

}
