package gsplatform.api;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;

public class BotTradesAPI extends PlatformAPI {
	
	private BotLogsDAO botLogsDAO;
	private int botId;
	
	/**
	 * Create api object with param object 
	 * {
	 * 	username: "username",
	 *  botId: someInt
	 * }
	 * @param parameters
	 * @throws JSONException 
	 */
	public BotTradesAPI( JSONObject parameters ) throws JSONException
	{
		super( parameters );
		this.botId = parameters.getInt( "botId" );
		this.botLogsDAO  = new BotLogsDAO( parameters.getString( "botOwner" ), parameters.getInt( "botId" ) ); 
	}
	
	@Override
	public JSONObject createJSON( ) throws JSONException
	{
		
		JSONObject response      = new JSONObject( );
		response.put("success", true);
		JSONArray responseArray  = new JSONArray( );
		LOGGER.addMessageEntry( "API", "BotTradesAPI", "request: " + this.parameters );
		
		try
		{
			this.botLogsDAO.openConnection( );
			List< BotLog > botLogs = this.botLogsDAO.getLastNSignalsById( );
			BigDecimal lastBuy   = BigDecimal.ZERO;
			BigDecimal lastSell  = BigDecimal.ZERO;
			
			for( BotLog botLog : botLogs )
			{
				if( botLog.getAction( ).equalsIgnoreCase( "BUY") ) {
					lastBuy  = botLog.getLastPrice( );
				}
				else {
					lastSell = botLog.getLastPrice( );
				}
				
				if( !lastBuy.equals( BigDecimal.ZERO ) && !lastSell.equals( BigDecimal.ZERO ) ) {
					JSONObject trade = new JSONObject( );
					double roi       = ( lastSell.doubleValue( ) / lastBuy.doubleValue( ) ) - 1.00;
					trade.put( "timestamp", botLog.getDated( ) );
					trade.put( "roi", new BigDecimal( roi ).setScale( 4, BigDecimal.ROUND_DOWN ) );
					responseArray.put( trade );
					lastBuy  = BigDecimal.ZERO;
					lastSell = BigDecimal.ZERO;
				}
			}
			this.botLogsDAO.closeConnection( );
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "BotTradesAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
		response.put( "trades", responseArray );
		LOGGER.addMessageEntry( "API", "BotTradesAPI", "response: " + response );
		return response;
	}

}
