package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.UserBotsDAO;


public class ChangeLockAPI extends PlatformAPI {

	
	/**
	 * Username associated with the bot
	 */
	protected String username;
	
	/**
	 * The ID associated with the bot
	 */
	protected int botId;
	
	/**
	 * The ID associated with the bot
	 */
	protected int strategyId;
	
	/**
	 * The name of the bot - made by user
	 */
	protected Boolean isLocked;
	
	/**
	 * Reference to user bot object
	 */
	protected UserBotsDAO userBotsDAO;
	
	public ChangeLockAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);
		this.username     = parameters.getString( "username" );
		this.botId        = parameters.getInt( "botId" );
		this.isLocked    = parameters.getBoolean( "locked" );
		this.userBotsDAO  = new UserBotsDAO( this.username, this.botId  );
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "ChangeLockAPI", "request: " + this.parameters );

		
		try
		{
			
			this.userBotsDAO.openConnection( );
			
			JSONObject successObject = new JSONObject( );
			successObject.put( "success", true );
			//Lock bot if specified
			if( this.isLocked )
			{
				this.userBotsDAO.lockBot( );
				successObject.put( "message", "0000: bot locked successfully" );
			}
			
			//Unlock otherwise
			else
			{
				this.userBotsDAO.unlockBot( );
				successObject.put( "message", "0000: bot unlocked successfully" );
			}
			
			this.userBotsDAO.closeConnection( );
			LOGGER.addMessageEntry( "API", "ChangeLockAPI", "response: " + successObject.toString( ) );
			return successObject;
		}
		catch( SQLException | ClassNotFoundException e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "ChangeLockAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}
	}
}
