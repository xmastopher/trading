package gsplatform.api;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.utilities.GS_Utils;

/**
 * API Specification for saving a strategy in a user's profile. Request JSON comes in the format of the following:
 * 
 * {
 * 		username: ["string"..."string"],
 * 		strategies: "string",
 * 		parameterSettings:	
 * 		[\["param1,param2",\],...\["param1,param2",]]
 * }
 * 
 * Response JSON comes in following format
 * {
 * 		successes: [true,true,...false]
 * 		messages: ["success","error",..."success"] 
 * }
 * 
 * @author danielanderson
 *
 */
public class SaveStrategyAPI extends PlatformAPI 
{
	
	TradingStrategiesDAO tradingStrategiesDAO;
	
	/**
	 * JSON request object that describes parameters to save
	 * @param parameters
	 * @throws JSONException
	 */
	public SaveStrategyAPI( JSONObject parameters ) throws JSONException {
		
		super(parameters);
		this.tradingStrategiesDAO = new TradingStrategiesDAO( parameters.getString( "username" ) );
		
	}

	@Override
	/**
	 * Returns a JSON for saving strategy - a size 1 list with failure may contain exception
	 */
	public JSONObject createJSON() throws IOException, JSONException 
	{
		LOGGER.addMessageEntry( "API", "SaveStrategyAPI", "request: " + this.parameters );
		JSONObject returnObject = new JSONObject( );
		
		try 
		{
			this.tradingStrategiesDAO.openConnection();
			this.tradingStrategiesDAO.saveStrategy(  this.parameters.getString( "strategyName"),
													this.parameters.getString( "buySignals"),
													this.parameters.getString( "sellSignals"),
													this.parameters.getString( "targets"), 
													GS_Utils.convertToDecimalFromPercentString( 
															this.parameters.getString( "stopLoss" ) ) );
			this.tradingStrategiesDAO.closeConnection( );
		} 
		catch ( ClassNotFoundException | SQLException e ) 
		{
			JSONObject failObject = new JSONObject();
			failObject.put( "success", false );
			failObject.put( "message", e.getMessage( ) );
			return failObject;
		}

		returnObject.put( "success", true );
		returnObject.put( "message", "succesfully save strategy: " + this.parameters.getString( "strategyName" ) );
		
		LOGGER.addMessageEntry( "API", "SaveStrategyAPI", "response: " + returnObject.toString( ) );
		return returnObject;
	}

}
