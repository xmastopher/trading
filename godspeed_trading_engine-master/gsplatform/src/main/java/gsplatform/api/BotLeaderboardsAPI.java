package gsplatform.api;

import java.math.BigDecimal;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.BotTradeMetrics;
import gsplatform.utilities.GS_Utils;

public class BotLeaderboardsAPI extends PlatformAPI {
	
	private BotLeaderboardsDAO botLeaderboardsDAO;
	
	/**
	 * How many record are you grabbing?
	 */
	private int numberToFetch;
	
	/**
	 * Set to true if the user wants to grab their
	 * bot rank instead of the entire leaderboard
	 */
	private boolean grabBotRank;
	
	/**
	 * Pass in which leaderboard to grab
	 */
	private String leaderboard;
	
	/**
	 * Only provided if grabbing rank
	 */
	private String username;
	
	/**
	 */
	public BotLeaderboardsAPI( JSONObject parameters ) throws JSONException
	{
		super( parameters ); 
		this.leaderboard = parameters.getString( "leaderboard" );
		
		try {
			this.grabBotRank 		= parameters.getBoolean( "grabBotRank" );
			this.username = parameters.getString( "username" );
			this.botLeaderboardsDAO  = new BotLeaderboardsDAO( this.username );
		}
		catch( Exception e ){
			this.botLeaderboardsDAO = new BotLeaderboardsDAO( );
			return;
		}

		
	}
	
	@Override
	public JSONObject createJSON( ) throws JSONException
	{
		
		JSONObject response      = new JSONObject( );
		response.put("success", true);
		JSONArray responseArray  = new JSONArray( );
		LOGGER.addMessageEntry( "API", "BotLeaderboardsAPI", "request: " + this.parameters );
		
		try
		{
			this.botLeaderboardsDAO.openConnection( );
			
			//Fetch only the bot's rank information
			if( this.grabBotRank ) {
				String[] foundBot = this.botLeaderboardsDAO.getRank( );
				response.put( "rank", new Integer( foundBot[0] ) );
				response.put( "botName", foundBot[1] );
				response.put( "botId", foundBot[2] );
				this.botLeaderboardsDAO.closeConnection( );
				return response;
			}
			
			List< BotTradeMetrics > botTradeMetrics = null; 
			
			//Fetch top N rows
			if( !this.parameters.getString( "leaderboard" ).equalsIgnoreCase( "historical" ) ) {
				this.numberToFetch = parameters.getInt( "numberToFetch" );
				botTradeMetrics    = this.botLeaderboardsDAO.getTopBotTradeMetrics( this.numberToFetch, getTableFromString( this.leaderboard ) );
			}
			
			//Fetch historical datapoints by username/id
			else {
				this.username   = parameters.getString( "botOwner" );
				this.botLeaderboardsDAO.setUsernameAndId(this.username, this.parameters.getInt( "botId" ) );
				botTradeMetrics = this.botLeaderboardsDAO.getUserHistoricalTradeMetrics( this.parameters.getInt( "botId") );
			}
			
			double maxRoi = Double.MIN_VALUE;
			
			for( BotTradeMetrics btm : botTradeMetrics )
			{
				System.out.println( btm.getBotName( ) );
				
				//Calculate W/L
				BigDecimal winLossRatio = BigDecimal.ZERO;
				
				if( btm.getNumLosses( ) == 0 ) {
					winLossRatio = new BigDecimal( btm.getNumWins( ) );
				}
				else if( btm.getNumWins( ) == 0 ) {
					winLossRatio = new BigDecimal( btm.getNumLosses( ) );
				}
				else {
					winLossRatio = GS_Utils.divideIntsAsBigDecimals( btm.getNumWins( ), btm.getNumLosses( ) );
				}
										  
				//Calculate ROI as percent
				BigDecimal roi			= btm.getNetworth( ).
										   divide( BigDecimal.ONE ).
										    subtract( BigDecimal.ONE ).
											 setScale( 4, BigDecimal.ROUND_DOWN );	
				
				BigDecimal avgRoi = BigDecimal.ZERO;
				
				if( btm.getNumTrades( ) != 0 ) {
					double tRoi   = btm.getTotalROI().doubleValue();
					double numT   = btm.getNumTrades();
					avgRoi = new BigDecimal( tRoi / numT );
					maxRoi = Math.max( roi.doubleValue( ), maxRoi );
				}
				else {
					roi = BigDecimal.ZERO;
				}
				
				JSONObject botEntry = new JSONObject( );
				
				botEntry.put("username", btm.getUsername( ) );
				botEntry.put( "botName", btm.getBotName( ) );
				botEntry.put( "botId", btm.getBotId(  ) );
				botEntry.put( "exchange", btm.getExchange( ) );
				botEntry.put( "market", btm.getMarket( ) );
				botEntry.put( "avgRoi", GS_Utils.convertToPercent( avgRoi ) );
				botEntry.put( "roi", GS_Utils.convertToPercent( roi ) );
				botEntry.put( "numTrades", btm.getNumTrades( ) );
				botEntry.put( "winLoss", winLossRatio );
				botEntry.put( "bestTrade", GS_Utils.convertToPercent( btm.getBestTrade( ) ) );
				responseArray.put( botEntry );
			}
			
			if( this.parameters.getString( "leaderboard" ).equals( "historical" ) ) {
				response.put( "athRoi", GS_Utils.convertToPercent( maxRoi ) );
			}
			
			this.botLeaderboardsDAO.closeConnection( );
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "BotLeaderboardsAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}

		if( this.parameters.getString( "leaderboard" ).equals( "historical" ) ) {
			response.put( "historicalPerformances", responseArray );
		}
		else {
			response.put( "bots", responseArray );
		}
		LOGGER.addMessageEntry( "API", "BotLeaderboardsAPI", "response: " + response );
		return response;
	}
	
	private String getTableFromString( String leaderboard ) {
		
		if( leaderboard.equalsIgnoreCase( "alltime") ) {
			return "BotLeaderboards";
		}
		else if( leaderboard.equalsIgnoreCase( "weekly") ) {
			return "BotLeaderboardsWeekly";
		}
		else if( leaderboard.equalsIgnoreCase( "daily") ) {
			return "BotLeaderboardsDaily";
		}
		return "BotLeaderboards";
	}

}
