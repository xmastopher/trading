package gsplatform.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.servlet.database.BotTradeMetrics;
import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;
import gsplatform.servlet.database.UserStrategiesPackage;
import gsplatform.utilities.GS_Utils;


public class BotSummaryAPI extends PlatformAPI {
	
	
	protected UserBotsDAO userBotsDAO;
	
	protected BotLeaderboardsDAO botLeaderboardsDAO;
	
	protected TradingStrategiesDAO tradingStrategiesDAO;

	
	/**
	 * 
	 * @param parameters
	 * @throws JSONException
	 * @throws IOException 
	 */
	public BotSummaryAPI( JSONObject parameters ) throws JSONException, IOException {
		super(parameters);
		//this.botSummarization     = new BotSummerizationService( parameters.getString( "username" ) );
		this.tradingStrategiesDAO   = new TradingStrategiesDAO( parameters.getString( "username" ) );
		this.botLeaderboardsDAO     = new BotLeaderboardsDAO( parameters.getString( "username" ) );
		this.userBotsDAO 		   = new UserBotsDAO( parameters.getString( "username" ) );
	}
	
	@Override
	public JSONObject createJSON( ) throws IOException, JSONException {

		JSONObject response       = new JSONObject( );
		JSONArray botSummaryArray = new JSONArray( );
		response.put("success", true);
		
		LOGGER.addMessageEntry( "API", "BotSummaryAPI", "request: " + this.parameters );
		
		try
		{
			//List< BotSummary > botSummaries = this.botSummarization.createBotSummaries( );
			this.tradingStrategiesDAO.openConnection( );
			this.userBotsDAO.openConnection( );
			this.botLeaderboardsDAO.openConnection( );
			UserStrategiesPackage usp        = this.tradingStrategiesDAO.getUserStrategies( );
			List< UserBot > userBots         = this.userBotsDAO.grabAllBots( );
			List< BotTradeMetrics > btms     = this.botLeaderboardsDAO.getBotTradeMetrics( 0 );
			this.tradingStrategiesDAO.closeConnection( );
			this.userBotsDAO.closeConnection( );
			this.botLeaderboardsDAO.closeConnection( );

			
			for( UserBot userBot : userBots )
			{      
				
				//Grab bot performance object via ID
				BotTradeMetrics btm     = getBotPerformanceFromId( btms, userBot.getBotId( ) );
				
				//Init with empty btm
				if( btm == null ) {
					btm = new BotTradeMetrics( userBot.getBotId(), userBot.getUsername( ), userBot.getBotName(), 
											  userBot.getExchange( ), userBot.getMarketBase( ) + "-" + userBot.getMarketQuote( ) );
				}
				
				//Calculate W/L
				BigDecimal winLossRatio = btm.getNumLosses() != 0 ? 
										  new BigDecimal( ((double)btm.getNumWins( )) / ((double)btm.getNumLosses( )) ) :
									      BigDecimal.ZERO;
										  
				//Calculate ROI as percent
				BigDecimal roi			= btm.getNetworth( ).
										   divide( BigDecimal.ONE ).
										    subtract( BigDecimal.ONE ).
											 setScale( 4, BigDecimal.ROUND_DOWN );	
				
				//Wrap up everything in obj
				JSONObject botSummaryJson = new JSONObject( );
				botSummaryJson.put( "name", userBot.getBotName( ) );
				botSummaryJson.put( "id", userBot.getBotId( ) );
				botSummaryJson.put( "running", userBot.getRunning( ) );
				botSummaryJson.put( "exchange", userBot.getExchange( ) );
				botSummaryJson.put( "strategyId", userBot.getStrategyId( ) );
				botSummaryJson.put( "strategy", usp.getStrategyByIdAsJSON( userBot.getStrategyId( ) ) );
				botSummaryJson.put( "market", userBot.getMarketBase( ) + "-" + userBot.getMarketQuote( ) );
				
				if( btm.getNumTrades( ) == 0 ) {
					botSummaryJson.put( "averageROI", 0 );
					botSummaryJson.put( "roi", new BigDecimal( 0 ) );
				}
				else {
					double tRoi   = btm.getTotalROI().doubleValue();
					double numT   = btm.getNumTrades();
					double avgRoi = tRoi / numT;
					botSummaryJson.put( "averageROI", GS_Utils.convertToPercent( avgRoi ) );
					botSummaryJson.put( "roi", GS_Utils.convertToPercent( roi ) );
				}
				
				botSummaryJson.put( "totalWins", btm.getNumWins( ) );
				botSummaryJson.put( "totalLosses", btm.getNumLosses( ) );
				botSummaryJson.put( "numTrades", btm.getNumTrades( ) );
				
				botSummaryJson.put( "winLoss", winLossRatio.setScale( 4, BigDecimal.ROUND_DOWN ) );
				botSummaryJson.put( "tradingLimit", userBot.getTradingLimit( ) );
				botSummaryJson.put( "interval", userBot.getInterval( ) );
				botSummaryArray.put( botSummaryJson );
			}
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "BotSummaryAPI", e.getMessage( ) );
			response.put( "success", false );
			response.put( "message", e.getMessage( ) );
		}
		
		
		response.put( "botSummaries", botSummaryArray );
		LOGGER.addMessageEntry( "API", "BotSummaryAPI", "response: " + response.toString( ) );
		return response;
		
	}
	
	protected BotTradeMetrics getBotPerformanceFromId( List< BotTradeMetrics > metrics, int id ) {
		
		for( BotTradeMetrics btm : metrics ) {
			if( btm.getBotId( ) == id )
				return btm;
		}  
		
		return null;
	}
	/***
	protected JSONArray createJSONArrayFromMetrics( List< BotTradeMetrics > botTradeMetrics ) throws JSONException
	{
		JSONArray jsonArray = new JSONArray( );

		for( BotTradeMetrics btm : botTradeMetrics )
		{
			JSONObject jsonObject = new JSONObject( );
			jsonObject.put( "botId", btm.getBotId( ) );
			jsonObject.put( "strategyId", btm.getStrategyId( ) );
			jsonObject.put( "returnOnTrade", btm.getReturnOnTrade( ) );
			jsonObject.put( "botName", btm.getBotName( ) );
			jsonObject.put( "username", btm.getUsername( ) );
			jsonObject.put( "timestamp", btm.getTimestamp( ) );
			jsonArray.put( jsonObject );
		}
		
		return jsonArray;
	}
	***/
	
	public static void main( String[ ] args ) throws JSONException, IOException
	{
		JSONObject o = new JSONObject( );
		o.put( "username", "danimal" );
		new BotSummaryAPI( o ).createJSON( );
	}
}
