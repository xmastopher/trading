package gsplatform.api;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;

import gsplatform.servlet.database.NomicsMirrorDAO;

public class ExchangeMarketsAPI extends PlatformAPI {
	
	private String exchange;
	
	public ExchangeMarketsAPI(JSONObject parameters) throws JSONException {
		super(parameters);
		this.exchange = parameters.getString( "exchange" );  
	}

	@Override
	public JSONObject createJSON() throws IOException, JSONException {
		
		LOGGER.addMessageEntry( "API", "ExchangeMarketsAPI", "request: " + this.parameters );
		JSONObject response = new JSONObject( );
		
		try {
			NomicsMirrorDAO nomicsMirror = new NomicsMirrorDAO( );
			nomicsMirror.openConnection( );
			List< String > markets = nomicsMirror.getMarketsFromExchange( this.exchange );
			JSONArray marketsArray = new JSONArray( );
			for( String market : markets ) {
				marketsArray.put( new JSONObject( market ) );
			}
			response.put( "success", true );
			response.put( "markets", marketsArray );
			response.put( "exchange", this.exchange );
			nomicsMirror.closeConnection( );
		}
		catch( Exception e ){
			response.put("success", false);
			response.put("message", e.getMessage());
			return response;
		}
		LOGGER.addMessageEntry( "API", "ExchangeMarketsAPI", "response: " + response.toString( ) );
		return response;
		
	}

}
