package gsplatform.api;

import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;

public class BotLogsAPI extends PlatformAPI {
	
	private BotLogsDAO botLogsDAO;
	private UserBotsDAO userBotsDAO;
	private int botId;
	
	/**
	 * Create api object with param object 
	 * {
	 * 	username: "username",
	 *  botId: someInt
	 * }
	 * @param parameters
	 * @throws JSONException 
	 */
	public BotLogsAPI( JSONObject parameters ) throws JSONException
	{
		super( parameters );
		this.botId = parameters.getInt( "botId" );
		
		if( parameters.optString( "botOwner" ).equals( "" ) ) {
			this.botLogsDAO  = new BotLogsDAO( parameters.getString( "username" ), parameters.getInt( "botId" ) ); 
			this.userBotsDAO = new UserBotsDAO( parameters.getString( "username" ), parameters.getInt( "botId" ) ); 
		}
		else {
			this.botLogsDAO  = new BotLogsDAO( parameters.getString( "botOwner" ), parameters.getInt( "botId" ) ); 
			this.userBotsDAO = new UserBotsDAO( parameters.getString( "botOwner" ), parameters.getInt( "botId" ) ); 
		}
	}
	
	@Override
	public JSONObject createJSON( ) throws JSONException
	{
		
		JSONObject response      = new JSONObject( );
		response.put("success", true);
		JSONArray responseArray  = new JSONArray( );
		JSONObject userBotObject = new JSONObject( );
		LOGGER.addMessageEntry( "API", "BotLogsAPI", "request: " + this.parameters );
		
		try
		{
			this.botLogsDAO.openConnection( );
			this.userBotsDAO.openConnection( );
			List< BotLog > botLogs = this.botLogsDAO.getLogsFromBot( );

			for( BotLog botLog : botLogs )
			{
				JSONObject logObject = new JSONObject( );
				
				String message = createMessage( botLog );
				logObject.put( "timestamp", botLog.getDated( ) );
				logObject.put( "action", botLog.getAction( ) );
				logObject.put( "indicators", new JSONObject( botLog.getSignalStates( ) ) );
				logObject.put( "message", message );
				logObject.put( "closePrice", botLog.getLastPrice( ) );
				logObject.put( "networth", botLog.getNetworth( ) );
				logObject.put( "assets", botLog.getAsssetsOwned( ) );
				logObject.put("quotes", botLog.getQuoteOwned( ) );
				responseArray.put( logObject );
			}
			
			UserBot userBot = userBotsDAO.grabBotFromId( this.botId );
			userBotObject.put( "botName", userBot.getBotName( ) );
			userBotObject.put( "botId", userBot.getBotId( ) );
			userBotObject.put( "market", userBot.getMarketBase( ) + "-" + userBot.getMarketQuote( ) );
			userBotObject.put( "exchange", userBot.getExchange( ) );
			userBotObject.put( "interval", userBot.getInterval( ) );
			this.botLogsDAO.closeConnection( );
			this.userBotsDAO.closeConnection( );
		}
		
		catch( Exception e )
		{
			LOGGER.addMessageEntry( "API-EXCEPTIONS", "BotLogsAPI", e.getMessage( ) );
			JSONObject errorObject = new JSONObject( );
			errorObject.put( "success", false );
			errorObject.put( "message", e.getMessage( ) );
			return errorObject;
		}

		response.put( "userBot", userBotObject );
		response.put( "botLogs", responseArray );
		LOGGER.addMessageEntry( "API", "BotLogsAPI", "response: " + response );
		return response;
	}
	
	/**
	 * 2341, 0, 1234, .55, '[{signal:SMA-CROSS,smaSmaller:1,smaLarger:2}]', '2018-04-29 13:59:59'
	 * Creates a message in the form of (ie): " Last price is [lastPrice] with 
	 * signal values of [signal values].  [botName] has a current networth of X and is holding Y assets"
	 * @param botLog
	 * @return
	 * @throws JSONException 
	 */
	protected String createMessage( BotLog botLog ) throws JSONException
	{
		
		StringBuilder stringBuilder = new StringBuilder( );
		stringBuilder.append( "<i><b>Last price:</b></i> " );
		stringBuilder.append( botLog.getLastPrice( ).toPlainString( ) );
		stringBuilder.append( "<br>" );
		stringBuilder.append( "<i><b>Indicators:</b></i> " );
		stringBuilder.append( signalValuesToString( botLog.getSignalStates( ) ) );
		stringBuilder.append( "<br>" );
		stringBuilder.append( "<i><b>Holdings:</b></i> Networth = " );
		stringBuilder.append( botLog.getNetworth( ).toPlainString( ) );
		stringBuilder.append( ", Assets = ");
		stringBuilder.append( botLog.getAsssetsOwned( ).toPlainString( ) );
		return stringBuilder.toString( );
	}
	
	/**
	 * Takes in json array of signals to string 
	 * @param jsonArrayString
	 * @return
	 * @throws JSONException 
	 */
	protected String signalValuesToString( String jsonArrayString ) throws JSONException
	{
		StringBuilder stringBuilder = new StringBuilder( );
		
		JSONObject signalsArray = new JSONObject( jsonArrayString );
		
		JSONObject signalObjectBuy = signalsArray.getJSONObject( "buyIndicators" );
		
		Iterator< ? > keys = signalObjectBuy.keys( );
		
		while( keys.hasNext( ) ) {
		    String key = ( String ) keys.next( );
		    
		    if( key.equals( "class" ) )
		    		continue;
		    
    			stringBuilder.append( " " );
    			stringBuilder.append( key );
    			stringBuilder.append( " = " );
    			stringBuilder.append( signalObjectBuy.get( key ).toString( ) );
		}
		
		JSONObject signalObjectSell = signalsArray.getJSONObject( "sellIndicators" );
		
		keys = signalObjectSell.keys( );
		
		while( keys.hasNext( ) ) {
		    String key = ( String ) keys.next( );
		    
		    if( key.equals( "class" ) )
		    		continue;
		    
    			stringBuilder.append( " " );
    			stringBuilder.append( key );
    			stringBuilder.append( " = " );
    			stringBuilder.append( signalObjectSell.get( key ).toString( ) );
		}

		
		return stringBuilder.toString( );
		
	}

}
