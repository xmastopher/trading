package gsplatform.algorithms;

import java.math.BigDecimal;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.exceptions.BuyAndSellException;
import gsplatform.signals.Signal;
import gsplatform.utilities.TradingParameters;

/**
 * Abstract class that acts as template for all derived algos.
 * @author danielanderson
 *
 */
public abstract class TradingAlgorithm 
{
	
	/**
	 * Constant for how much precision to show for trading
	 * values, crypto commonly uses 8, in some cases
	 * we may want to use 16
	 */
	public static final int DECIMALS_FOR_DISPLAY = 8;
	
	/**
	 * Public enum to define the signal
	 * @author danielanderson
	 *
	 */
	public enum SIGNAL{ BUY, SELL, HOLD };
	
	/**
	 * The current signal based on this iterations
	 * asssessment
	 */
	private SIGNAL signal;
	
	/**

	public class SignalPortionPair
	{
		
		private int targetsHit;
		
		public SignalPortionPair( SIGNAL signal, int targetsHit )
		{
			this.signal = signal;
			this.targetsHit = targetsHit;
		}
		
		public SIGNAL getSignal( )
		{
			return this.signal;
		}
		
		public int getTargetsHit( )
		{
			return this.targetsHit;
		}
		
		public void setTargetsHit( int targetsHit )
		{
			this.targetsHit = targetsHit;
		}
		
		@Override
		public String toString( )
		{
			return this.signal.toString( );
		}
	}**/
	
	/**
	 * The name of the algo - should match front end
	 */
	protected String name;
	
	/**
	 * Flag for triggering sell signal
	 */
	protected boolean sellSignal;
	
	/**
	 * Flag for triggering buySignal
	 */
	protected boolean buySignal;
	
	/**
	 * Holds the last signal
	 */
	protected SIGNAL lastSignal;
	
	/**
	 * Stop loss value
	 */
	protected BigDecimal stopLoss;
	
	/**
	 * Portion to buy or sell
	 *
	protected BigDecimal portion;*/
	
	/**
	 * Message that should be updated in onX( ) functions
	 */
	protected String logMessage;
	
	/**
	 * Holds the last price 
	 */
	protected BigDecimal lastPrice;
	
	/**
	 * Last sell price
	 */
	protected BigDecimal lastSellPrice;
	
	/**
	 * Last buy price 
	 */
	protected BigDecimal lastBuyPrice;
	
	/**
	 * Last buy price 
	 */
	protected BigDecimal stopLossPrice;
	
	/**
	 * Are we waiting to buy?
	 */
	protected Boolean waitingToBuy;
	
	/**
	 * Are we waiting to sell?
	 */
	protected Boolean waitingToSell;
	
	/**
	 * The external flag for seeing if a stop loss has been triggered
	 */
	protected Boolean externalStopLossFlag;
	
	/**
	 * Default constructor
	 */
	public TradingAlgorithm()
	{
		this.buySignal     		 = false;
		this.sellSignal    		 = false;
		this.lastPrice     		 = new BigDecimal( 0 );
		this.lastBuyPrice 		 = new BigDecimal( 0 );
		this.lastSellPrice 		 = new BigDecimal( 0 );
		this.stopLoss      		 = new BigDecimal( 0 );
		this.stopLossPrice        = new BigDecimal( 0 );
		this.externalStopLossFlag = false;
	}
	
	
	/**
	 * Pass in contradicting flags to designate a starting point
	 * @param waitingToSell
	 * @param waitingToBuy
	 */
	public void initStartingPoint( Boolean waitingToSell, Boolean waitingToBuy )
	{
		this.waitingToBuy  = waitingToBuy;
		this.waitingToSell = waitingToSell;
	}
	
	/**
	 * Explicit constructor called in children constructors
	 * 
	 * @param goal		Are we mainly trying to buy, sell or neither
	 * @param targets	What are our buy or sell targets, can also be empty
	 * @param stopLoss	Stop loss percentage below last buy or sell
	 */
	public TradingAlgorithm( BigDecimal stopLoss )
	{
		this();
		this.stopLoss = stopLoss.setScale( 2, BigDecimal.ROUND_DOWN );
	}
	
	/**
	 * Returns the logged message, should set this at specific points
	 * @return a string for the log message
	 */
	public String getLogMessage( )
	{
		return this.logMessage;
	}
	
	/**
	 * 
	 * @param tradingParameters		Parameters for deciding the signal
	 * @return						Returns the trading signal
	 * @throws BuyAndSellException	Throws exception if buy and sell are both triggered
	 */
	public SIGNAL doIteration( TradingParameters tradingParameters ) throws BuyAndSellException
	{
		
		this.externalStopLossFlag = this.lastSignal == SIGNAL.BUY && this.stopLossTriggered( ) ? true : false;
		
		this.lastPrice = new BigDecimal( tradingParameters.getAsDouble( "last_price" ) );
		update( tradingParameters );
		
		this.buySignal  = assessBuySignal( );
		this.sellSignal = this.externalStopLossFlag || assessSellSignal( );
		
		if( this.externalStopLossFlag ) {
			System.out.println("stopLossTriggered");
		}
		if( this.buySignal ) 
		{
			this.lastSignal = SIGNAL.BUY;
			onBuy( );
			updateStopLoss( );
			completeBuy( false );
		}
		if( this.sellSignal ) 
		{ 
			this.lastSignal = SIGNAL.SELL;
			onSell( );
			completeSell( false );

		}
		
		if( !this.buySignal && !this.sellSignal ) onHold( );
		
		if( sellSignal && buySignal ) throw new BuyAndSellException();
		
		return getSignal();
	}
	
	
	/**
	 * Called at the end of the update cycle to update stop loss value
	 * @param action		0 == sell, 1 == buy
	 */
	public void updateStopLoss( )
	{
		BigDecimal difference = this.lastPrice.multiply( this.stopLoss ).setScale( 8, BigDecimal.ROUND_DOWN );
		this.stopLossPrice    = this.lastPrice.subtract( difference ).setScale(8, BigDecimal.ROUND_DOWN );
	}

	/**
	 * Override to update specific log message for algo
	 */
	protected void onBuy( )
	{
		if( this.externalStopLossFlag )
		{
			this.logMessage = "STOP LOSS TRIGGERED " + this.lastPrice.setScale( TradingAlgorithm.DECIMALS_FOR_DISPLAY, BigDecimal.ROUND_CEILING );
		}
		else
		{
			this.logMessage = "BUYING ASSET "  + this.lastPrice.setScale( TradingAlgorithm.DECIMALS_FOR_DISPLAY, BigDecimal.ROUND_CEILING );
		}
		
	}
	
	/**
	 * Override to update specific log message for algo
	 */
	protected void onSell( )
	{
		if( this.externalStopLossFlag )
		{
			this.logMessage = "STOP LOSS TRIGGERED " + this.lastPrice.setScale( TradingAlgorithm.DECIMALS_FOR_DISPLAY, BigDecimal.ROUND_CEILING );
		}
		else
		{
			this.logMessage = "SELLING ASSET "  + this.lastPrice.setScale( TradingAlgorithm.DECIMALS_FOR_DISPLAY, BigDecimal.ROUND_CEILING );

		}
	
	}
	
	/**
	 * Override to update specific log message for algo
	 */
	protected void onHold( )
	{
		this.logMessage = "HODL AT " + this.lastPrice.setScale( TradingAlgorithm.DECIMALS_FOR_DISPLAY, BigDecimal.ROUND_CEILING );
	}
	
	/**
	 * Override to define what parameters need to be updated
	 */
	protected abstract void update( TradingParameters tradingParameters );
	
	
	/**
	 * Parent level assess - checks for stop loss and targets depending on algorithm modes
	 * @return	True or False if signal is complete, null if more logic needs to be assessed in child
	 */
	protected Boolean assessSellSignal( )
	{
		return waitingToSell;
	}
	
	/**
	 * Call this function in derived class if sell period completely ends to flip flags back
	 */
	protected void completeSell( Boolean optionalFlag )
	{
		//If we make it to this point, go ahead and flip switch - stop loss or all targets were met
		this.lastSellPrice  = this.lastPrice;
		this.waitingToBuy  = true;
		this.waitingToSell = false;
	}
	
	/**
	 * Parent level assess - checks for stop loss and targets depending on algorithm modes
	 * @return	True or False if signal is complete, null if more logic needs to be assessed in child
	 */
	protected Boolean assessBuySignal( )
	{
		return waitingToBuy;
	}
	
	/**
	 * Call in derived class when buy phase completely ends
	 */
	protected void completeBuy( Boolean optionalFlag )
	{
		this.lastBuyPrice  = this.lastPrice;
		this.waitingToBuy  = false;
		this.waitingToSell = true;
		
		if( this.externalStopLossFlag )
		{
			this.stopLossPrice = new BigDecimal( Double.MAX_VALUE );
		}
	}
	
	/**
	 * Override to return personal static name for class - these should match the string retrieved form front end.
	 */
	public abstract String getName( );
	
	/**
	 * Was a stop loss triggered?
	 * @return true if stop loss was triggered, false otherwise
	 */
	public Boolean stopLossTriggered( )
	{
		if( this.lastSignal == SIGNAL.BUY && 
			this.lastPrice.doubleValue( ) <= this.stopLossPrice.doubleValue( ) )
		{
			this.externalStopLossFlag = true;
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the current signal enum based on which signal object was triggered
	 * @return SignalPortionPair the enum for the signal paired with the portion or amount to buy or sell
	 */
	public SIGNAL getSignal()
	{
		if( this.buySignal ){
			return SIGNAL.BUY;
		}
		else if( this.sellSignal ){
			return SIGNAL.SELL;
		}
		return SIGNAL.HOLD;
	}
	
	/**
	 * Conversion function primarilly used for testing
	 * @return SIGNAL enum
	 *
	public static SIGNAL stringToSignal( String signalString )
	{
		switch( signalString )
		{
		case "BUY":
		case "buy":
			return SIGNAL.BUY;
		case "SELL":
		case "sell":
			return SIGNAL.SELL;
		}
		
		return SIGNAL.HOLD;
	}*/

	/**
	 * Returns true if the signal is a sell signal
	 * @return Boolean
	 */
	public boolean isSellSignal() {
		return sellSignal;
	}

	/**
	 * Set the sell signal
	 * @param sellSignal
	 */
	public void setSellSignal(boolean sellSignal) {
		this.sellSignal = sellSignal;
	}

	/**
	 * Returns true if its a buy signal
	 * @return Boolean
	 */
	public boolean isBuySignal() {
		return buySignal;
	}

	/**
	 * Set the buy signal
	 * @param buySignal 
	 */
	public void setBuySignal(boolean buySignal) {
		this.buySignal = buySignal;
	}

	/**
	 * Returns true if this is the last signal
	 * @return
	 */
	public SIGNAL isLastSignal() {
		return lastSignal;
	}

	/**
	 * Called to set the last signal
	 * @param lastSignal
	 */
	public void setLastSignal(SIGNAL lastSignal) {
		this.lastSignal = lastSignal;
	}

	/**
	 * Return the current stop loss
	 * @return BigDecimal
	 */
	public BigDecimal getStopLoss() {
		return stopLoss;
	}

	/**
	 * Set the current stop loss
	 * @param stopLoss BigDecimal
	 */
	public void setStopLoss(BigDecimal stopLoss) {
		this.stopLoss = stopLoss;
	}

	/**
	 * Return the last close price
	 * @return
	 */
	public BigDecimal getLastPrice() {
		return lastPrice;
	}

	/**
	 * Set the last close price
	 * @param lastPrice
	 */
	public void setLastPrice(BigDecimal lastPrice) {
		this.lastPrice = lastPrice;
	}

	/**
	 * Get the last sell price
	 * @return
	 */
	public BigDecimal getLastSellPrice() {
		return lastSellPrice;
	}

	/**
	 * Set the last sell price
	 * @param lastSellPrice
	 */
	public void setLastSellPrice(BigDecimal lastSellPrice) {
		this.lastSellPrice = lastSellPrice;
	}

	/**
	 * Get the last buy price
	 * @return
	 */
	public BigDecimal getLastBuyPrice() {
		return lastBuyPrice;
	}

	/**
	 * Set the last buy price
	 * @param lastBuyPrice
	 */
	public void setLastBuyPrice(BigDecimal lastBuyPrice) {
		this.lastBuyPrice = lastBuyPrice;
	}

	/**
	 * Get the number of decimals used for display
	 * @return
	 */
	public static int getDecimalsForDisplay() {
		return DECIMALS_FOR_DISPLAY;
	}

	/**
	 * Set the strategy name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the log message
	 * @param logMessage
	 */
	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}
	
	/**
	 * Get the current stop loss price
	 * @return
	 */
	public BigDecimal getStopLossPrice( )
	{
		return this.stopLossPrice;
	}
	
	/**
	 * Set the current awaited action - sell
	 * @param waitingToSell
	 */
	public void setWaitingToSell( Boolean waitingToSell )
	{
		this.waitingToSell = waitingToSell;
	}
	
	/**
	 * Get current awaited action - is sell
	 * @return
	 */
	public Boolean getWaitingToSell( )
	{
		return this.waitingToSell;
	}
	
	/**
	 * Set current awaited action - buy
	 * @param waitingToBuy
	 */
	public void setWaitingToBuy( Boolean waitingToBuy )
	{
		this.waitingToBuy = waitingToBuy;
	}
	
	/**
	 * Get current awaited action - is buy
	 * @return
	 */
	public Boolean getWaitingToBuy(  )
	{
		return this.waitingToBuy;
	}
	
	/**
	 * Returns current status of stop loss
	 * @return Boolea if stop loss is triggered
	 */
	public Boolean getExternalStopLossFlag( )
	{
		return this.externalStopLossFlag;
	}
	
	/**
	 * Sets the stop loss flag to false
	 */
	public void resetExternalStopLossFlag( )
	{
		this.externalStopLossFlag = false;
	}
	
	/**
	 * Implement in child to describe which signals to
	 * return 
	 * @return	A list of signals
	 */
	public abstract List< Signal > getSignals( );
	
	/**
	 * Implement in child to describe how long to
	 * prepocess the signal
	 * @return
	 */
	public abstract int getPreprocessingValue();
	
	/**
	 * Returns all ths signals in the form of JSON, implement
	 * in all children
	 * @return JSONObject description of signal
	 * @throws JSONException
	 */
	public JSONObject getSignalsAsJson( ) throws JSONException { return new JSONObject( );}

}
