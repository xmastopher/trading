package gsplatform.algorithms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.signals.Signal;
import gsplatform.utilities.TradingParameters;

/**
 * A signal based strategy looks solely for signals and only buys or sells based on the presence 
 * of a specific signal.  Each signal uses an indicator to update its state and buy or 
 * sell signals are trigger based on values of said indicators.
 * @author danielanderson
 *
 */
public class SignalStrategy extends TradingAlgorithm {

	/**
	 * Signal used to assess buys
	 */
	protected Signal buySignal;
	
	/**
	 * Signal used to asses sells
	 */
	protected Signal sellSignal;
	
	/**
	 * Explicit constructor used to create a signal strategy.  Requires
	 * at least a buy and sell signal as well as a stop loss value.
	 * @param stopLoss		Values within range 0 - 1.00
	 * @param buySignal		Signal object which defines the buy signal triggers
	 * @param sellSignal		Signal object which defines the sell signal trigger
	 */
	public SignalStrategy( BigDecimal stopLoss, Signal buySignal, Signal sellSignal )
	{
		super( stopLoss );
		
		this.buySignal     = buySignal;
		this.sellSignal    = sellSignal;
		this.waitingToSell = new Boolean( false );
		this.waitingToBuy  = new Boolean( false );
	}
	
	/**
	 * Called every iteration of a trading period to update
	 * the buy and sell signal states
	 */
	@Override
	protected void update( TradingParameters tradingParameters ) {
		sellSignal.update( tradingParameters );
		buySignal.update( tradingParameters );
	}
	
	/**
	 * Returns true when a stop loss is triggered
	 */
	@Override
	public Boolean stopLossTriggered( )
	{
		return super.stopLossTriggered( );
	}
	
	/**
	 * Override to update specific log message for algo
	 */
	@Override
	protected void onHold( )
	{
		super.onHold( );
		this.logMessage += this.buySignal.getMessage( );
	}
	

	/**
	 * Sell target is defined as the following for SMA Strategy:
	 * When the current candle closes below the defined SMA and is red
	 */
	@Override
	protected Boolean assessSellSignal() {
		
		//Validate parent and readiness of signal first
		if( !super.assessSellSignal( ) || !this.sellSignal.isReady( ) )
			return false;
		
		return this.sellSignal.triggered( );
	}
	
	/**
	 * Defines logic for a sell signal
	 */
	@Override
	protected void onSell( ) {
		super.onSell( );
		this.logMessage += " " + this.sellSignal.getMessage( );
	}
	
	@Override
	/**
	 * Buy signal is defined as the following for SMA Strategy:
	 * When the current candle closes above the defined SMA and is green
	 */
	protected Boolean assessBuySignal( ) {
		
		//Validate parent and readiness of signal first
		if( !super.assessBuySignal( ) || !this.buySignal.isReady( ) )
			return false;
		
		return this.buySignal.triggered( );
		
	}
	
	/**
	 * Defines logic for a buy signal
	 */
	@Override
	protected void onBuy( )
	{
		super.onBuy( );
		this.logMessage += this.buySignal.getMessage( );
	}
	
	/**
	 * Override to define how the signals will be returned,
	 * used for processing signals in messages.
	 */
	@Override
	public List< Signal > getSignals( )
	{
		List< Signal > signals = new ArrayList< Signal >( );
		signals.add( this.buySignal );
		signals.add( this.sellSignal );
		return signals;
	}
	
	/**
	 * Adds signal definition to json array and returns it - for logging purposes
	 * @return
	 * @throws JSONException
	 */
	@Override
	public JSONObject getSignalsAsJson( ) throws JSONException
	{
		JSONObject returnJson = new JSONObject( );
 
		//Add to json to return obj
		returnJson.put( "buyIndicators", this.buySignal.toJson( ) );
		returnJson.put( "sellIndicators", this.sellSignal.toJson( ) );
		
		return returnJson;
	}
	
	/**
	 * Returns the name of the strategy from the 
	 * java layer's perspective
	 * @return String name of the strategy
	 */
	@Override
	public String getName( ) {
		// TODO Auto-generated method stub
		return this.name;
	}
	
	/**
	 * Returns the number of iterations needed to initialize
	 * the trading signal
	 * @return int number of iterations to preprocess
	 */
	@Override
	public int getPreprocessingValue( )
	{
		return Math.max( this.buySignal.getPreprocessingValue( ), 
							this.sellSignal.getPreprocessingValue( ) );
	}

}
