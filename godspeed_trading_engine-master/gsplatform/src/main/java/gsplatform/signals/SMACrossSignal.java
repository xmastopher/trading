package gsplatform.signals;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.TradingParameters;

public class SMACrossSignal extends SMASignal {

	/**
	 * The phase corresponding to the longer SMA
	 */
	protected PHASE phaseTwo;
	
	/**
	 * List of the prices currently in the moving sum where candlesInMovingSum.size( ) == candlesInAverage
	 */
	protected Queue< Double > candlesInMovingSumLarger;
	
	/**
	 * The SMA period ie. SMA-20 will take the average over the last 20 candle averages
	 */
	protected int candlesInAverageLarger;
	
	/**
	 * The moving sum based on the number of candles
	 */
	protected double movingSumLarger;
	
	/**
	 * The moving average throughout time
	 */
	protected double movingAverageLarger;
	
	/**
	 * The last value of the shorter SMA
	 */
	protected double lastShortSMA;
	
	/**
	 * The last value of the longer SMA
	 */
	protected double lastLongSMA;
	
	/**
	 * SMA Cross constructor, also takes in candle size for larget SMA value
	 * @param candlesInShorterAverage	The number of candles in the short SMA
	 * @param candlesInLargerAverage		The number of candles in the longer SMA
	 */
	public SMACrossSignal(int candlesInShorterAverage, int candlesInLargerAverage ) 
	{
		super( candlesInShorterAverage );
		this.candlesInMovingSumLarger   = new LinkedList< Double >( );
		this.candlesInAverageLarger     = candlesInLargerAverage;
		this.movingSumLarger            = 0;
		this.movingAverageLarger        = 0;
		this.phaseTwo				   = PHASE.SETUP;
	}
	
	
	@Override
	/**
	 * Update function is same as parent SMASignal
	 */
	public void updateBefore(TradingParameters tradingParameters) {
		
		super.updateBefore( tradingParameters );
	}
	
	@Override
	/**
	 * The previous SMA prices should be updated after updating the trigger
	 */
	public void updateAfter( TradingParameters tradingParameters )
	{
		//Only update if READY state is set
		if( isReady( ) )
		{
			this.lastShortSMA = this.movingAverage;
			this.lastLongSMA  = this.movingAverageLarger;
		}
	}
	
	/**
	 * Internal method that is called in the initialization period
	 * @param tradingParameters	The trading parameters for this candle
	 */
	@Override
	protected void initMovingSum( TradingParameters tradingParameters )
	{
		super.initMovingSum( tradingParameters );
		
		//Check that second portion of phase is ready
		if( this.phaseTwo == PHASE.SETUP )
		{
			double avgPrice = ( this.currentClosePrice + this.currentOpenPrice ) / 2.00 ;
			this.candlesInMovingSumLarger.add( avgPrice );
			this.movingSumLarger += avgPrice;
		}
	}
	
	/**
	 * Calls parent update and also updates the larget moving average values
	 * @param tradingParameters	The trading parameters for this candle
	 */
	@Override
	protected void updateMovingAverage( TradingParameters tradingParameters )
	{
		super.updateMovingAverage( tradingParameters );
		
		//Check that second portion of phase is ready
		if( this.phaseTwo == PHASE.READY )
		{
			//Update the moving sum with the new candle
			double avgPrice    = ( this.currentClosePrice + this.currentOpenPrice ) / 2.00 ;
			this.movingSumLarger    += avgPrice;
			
			this.candlesInMovingSumLarger.add( avgPrice );
			this.movingAverageLarger = this.movingSumLarger / Double.valueOf( this.candlesInAverageLarger );
			
			//Remove the first candle
			double firstCandle = this.candlesInMovingSumLarger.remove( );
			this.movingSumLarger    -= firstCandle;
		}
	}
	
	/**
	 * Override phase update for child
	 */
	@Override
	protected void updatePhase( )
	{
		super.updatePhase( );
		
		//Update phase
		if( this.candlesInMovingSumLarger.size( ) == this.candlesInAverageLarger-1 )
		{
			this.phaseTwo = PHASE.READY;
		}
	}
	
	/**
	 * Returns the longer/larger of the two SMAs
	 * @return
	 */
	public double getSMALarger( )
	{
		return this.movingAverageLarger;
	}
	
	/**
	 * Override isReady function to take into account inialization of longer SMA
	 * @return
	 */
	@Override
	public boolean isReady( )
	{
		return this.phase == PHASE.READY && this.phaseTwo == PHASE.READY && this.movingAverageLarger != 0;
	}
	
	@Override
	public String getIndicatorsAsString( ) 
	{
		return new BigDecimal ( this.getSMA( ) ).setScale( 8 , BigDecimal.ROUND_DOWN ) 
				    + ", " + new BigDecimal ( this.getSMALarger( ) ).setScale( 8, BigDecimal.ROUND_DOWN );
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "SMA-CROSS" );
		jsonObject.put( GS_Utils.createFunctionBasedString( "sma", String.valueOf( this.candlesInAverage ) ), 
							new BigDecimal( this.getSMA( ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) );
		jsonObject.put( GS_Utils.createFunctionBasedString( "sma", String.valueOf( this.candlesInAverageLarger ) ), 
				new BigDecimal( this.getSMALarger( ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) );
		return jsonObject;
	}
	
	@Override
	public int getPreprocessingValue( )
	{
		return Math.max( this.candlesInAverage, this.candlesInAverageLarger );
	}
}
