package gsplatform.signals;
import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

/**
 *  Signal class for simple moving average buy signal which is
 *  triggered when a candle is green and closes above designated SMA
 */
public class RSISellSignal extends RSISignal
{
	
	/**
	 * Explicit constructor initializes candle color
	 */
	public RSISellSignal( int candlesInAverage, double threshold )
	{
		super( candlesInAverage, threshold );
	}
	
	@Override
	/**
	 * Update the candle color
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );
	}
	
	/**
	 * Buy signal is defined as the following for SMA Strategy:
	 * When the current candle closes above the defined SMA and is green
	 */
	@Override
	public void updateTrigger( )
	{
		this.isTriggered = this.rsi != Double.MAX_VALUE && this.rsi > this.threshold;
	}
	
	@Override
	public String getMessage( )
	{
		String smaShortFunction = "RSI(" + this.candlesInAverage + ")" + " = " + new BigDecimal( this.rsi ).setScale( 8, BigDecimal.ROUND_DOWN ).toPlainString( );
		return " " + smaShortFunction;
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		return super.toJson( );
	}
	
}
