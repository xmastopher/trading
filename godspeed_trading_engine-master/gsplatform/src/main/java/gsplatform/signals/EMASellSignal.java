package gsplatform.signals;
import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.TradingParameters;

/**
 *  Signal class for simple moving average sell signal which is
 *  triggered when a candle is red and closes below designated SMA
 */
public class EMASellSignal extends EMASignal {
	
	/**
	 * Is the current candle green?
	 */
	protected Boolean isRed;
	
	/**
	 * Explicit constructor initializes candle color
	 */
	public EMASellSignal( int candlesInAverage )
	{
		super( candlesInAverage );
		this.isRed = new Boolean( false );
	}
	
	@Override
	/**
	 * Update the candle color
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );
		this.isRed = this.currentClosePrice <= this.currentOpenPrice;
	}
	
	/**
	 * Sell signal is triggered when the price closes below the SMA
	 */
	@Override
	public void updateTrigger( )
	{
		this.isTriggered = this.isRed && this.currentClosePrice <= this.ema && 
						   this.currentOpenPrice >= this.ema;
	}
	
	/**
	 * Get the color of the candle - for testing
	 * @return True if candle is red false otherwise
	 */
	public Boolean isCandleRed( )
	{
		return this.isRed;
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "EMA" );
		jsonObject.put( GS_Utils.createFunctionBasedString( "ema", String.valueOf( this.candlesInAverage ) ), 
							new BigDecimal( this.ema ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) );
		return jsonObject;
	}
}
