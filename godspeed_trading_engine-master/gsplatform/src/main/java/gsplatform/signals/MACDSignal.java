package gsplatform.signals;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

/**
 * Implementation for the MACD signal, the MACD has four main components:
 * 1. min EMA - an EMA line with the shortest period
 * 2. max EMA - an EMA line with the 
 * 3. signal EMA - Used to decipher the signal
 * 4. macd line - min EMA less max EMA
 */
public class MACDSignal extends Signal {

	/**
	 * The current close price
	 */
	protected double currentClosePrice;
	
	/**
	 * The current open price
	 */
	protected double currentOpenPrice;

	/**
	 * ema signal for min period
	 */	
	protected EMASignal minEMA;

	/**
	 * ema signal for middle period (the signal one)
	 */
	protected EMASignal signalEMA;

	/**
	 * ema signal for the max period
	 */
	protected EMASignal maxEMA;

	/**
	 * The difference between min EMA and max EMA
	 */
	protected double macdLine;
	
	/**
	 * Used to reference last mac d point
	 */
	protected double lastMacdLine;

	/**
	 * Explicit constructor
	 * @param minPeriod the minimum EMA line
	 * @param midPeriod the signalling EMA line
	 * @param maxPeriod the maximum EMA line
	 */
	public MACDSignal( int minPeriod, int maxPeriod, int signalPeriod )
	{
		super( );
		this.minEMA    = new EMASignal( minPeriod );
		this.signalEMA = new EMASignal( signalPeriod );
		this.maxEMA    = new EMASignal( maxPeriod );
	}

	@Override
	public void updateBefore( TradingParameters tradingParameters ) {
		
		this.currentClosePrice = tradingParameters.getAsDouble( "close" );
		this.currentOpenPrice  = tradingParameters.getAsDouble( "open" );
		
		this.minEMA.updateBefore(tradingParameters);
		this.maxEMA.updateBefore(tradingParameters);
		
		updateMACD( );
		updateSignal( );
		updatePhase( );	
		
	}
	
	/**
	 * Called to update the difference value of the max and min EMAs
	 */
	private void updateMACD( ) {
		if( this.minEMA.getPhase( ) == PHASE.READY && this.maxEMA.getPhase( ) == PHASE.READY && this.maxEMA.getEMA( ) != 0 ) {
			this.macdLine = this.minEMA.getEMA( ) - this.maxEMA.getEMA( ); 
		}
	}
	
	/**
	 * Called to update signal line in parrallel with MACD
	 * the signal line is the moving average of the macD values
	 */
	private void updateSignal( ) {
		if( this.macdLine != 0 ) {
			
			TradingParameters macdParams = new TradingParameters( );
			macdParams.updateValue("close", String.valueOf( this.macdLine ) );
			this.signalEMA.updateBefore( macdParams );
		}
	} 


	@Override
	public void updateTrigger( ) {
		//No trigger defined for SMASignal
	}
	
	@Override
	public void updateAfter(TradingParameters tradingParameters) {
		this.minEMA.updateAfter(tradingParameters);
		this.maxEMA.updateAfter(tradingParameters);
		if( this.macdLine != 0 ) {
			
			TradingParameters macdParams = new TradingParameters( );
			macdParams.updateValue("close", String.valueOf( this.macdLine ) );
			this.signalEMA.updateAfter( macdParams );
		}
		this.lastMacdLine = this.macdLine;
	}

	/**
	 * Called each iteration to see if the phase needs to be flipped
	 */
	protected void updatePhase( )
	{
		if( this.phase == PHASE.READY ) return;
		
		this.phase = this.minEMA.getPhase( ) == PHASE.READY 		&&
			         this.signalEMA.getPhase( ) == PHASE.READY   &&
			         this.maxEMA.getPhase( ) == PHASE.READY      &&
			         this.signalEMA.getEMA( ) != 0		        ? 
			         PHASE.READY : PHASE.SETUP;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	public double getMACD( ) {
		return this.macdLine;
	}
	
	public double getSignalLine( ) {
		return this.signalEMA.getEMA( );
	}
	
	@Override
	public String getIndicatorsAsString() 
	{
		return new BigDecimal( this.macdLine ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( );
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "MACD" );
		jsonObject.put( "macd", this.getMACD( ) );
		return jsonObject;
	}
	
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.maxEMA.getPreprocessingValue( );
	}
}
