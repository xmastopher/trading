package gsplatform.signals;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

public abstract class Signal 
{
	protected enum PHASE{ SETUP, READY };
	
	protected PHASE phase;
	
	protected Boolean isTriggered;
	
	public Signal( )
	{
		this.isTriggered = false;
		this.phase       = PHASE.SETUP;
	}
	
	/**
	 * Untouched function which defined the order of operations for signal assesment
	 * @param tradingParameters
	 */
	public final void update( TradingParameters tradingParameters )
	{
		this.isTriggered = false;
		
		updateBefore( tradingParameters );
		
		if( this.isReady( ) ) {
			updateTrigger( );
		}
		
		updateAfter( tradingParameters );
	}
	
	/**
	 * Override in child to define the update sequence
	 * @param tradingParameters
	 */
	public abstract void updateBefore( TradingParameters tradingParameters );
	
	/**
	 * Override in child to define the update sequence
	 * @param tradingParameters
	 */
	public abstract void updateAfter( TradingParameters tradingParameters );

	/**
	 * Override to update the isTriggered values
	 * @return
	 */
	public abstract void updateTrigger( );
	
	public Boolean triggered( )
	{
		return this.isTriggered;
	}
	
	/**
	 * Is the signal ready to be checked, is it done with preprocessing
	 * @return
	 */
	public boolean isReady( )
	{
		return this.phase == PHASE.READY;
	}
	
    /**
     * Message for signal that must be overridden
     */
    public abstract String getMessage( );
    
    /**
     * Returns a json definition for the state of this signal
     */
    public abstract JSONObject toJson( ) throws JSONException;
    
   /**
    * 
    * @return A comma delimitted string of all indicator values
    */
    public abstract String getIndicatorsAsString( );
    
    public abstract int getPreprocessingValue( ); 
    
    /**
     * Use to retreive phase, helpful when signal contains other signals
     * @return
     */
    public PHASE getPhase( ) {
    		return this.phase;
    }
	
	
}
