package gsplatform.signals;

import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;

public class BollingerBandsSellSignal extends BollingerBandsSignal {


	public BollingerBandsSellSignal( int candlesInAverage, double standardDeviations ) 
	{
		super( candlesInAverage, standardDeviations );
	}
	
	@Override
	public void updateTrigger( ) {
		Double upperBand = this.movingAverage + ( this.numberOfDeviations * this.stdDeviation );
		this.isTriggered = this.movingAverage != 0.0 && this.currentOpenPrice < upperBand && this.currentClosePrice >= upperBand;
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "BOLLINGER-BANDS" );
		jsonObject.put( GS_Utils.createFunctionBasedString("sma", String.valueOf( this.candlesInAverage ) ), this.getSMA( ) );
		jsonObject.put( GS_Utils.createFunctionBasedString("std", new BigDecimal( this.numberOfDeviations ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) ),
							new BigDecimal( this.getStandardDeviation( ) ).setScale( 8, BigDecimal.ROUND_DOWN ) );
		return jsonObject;
	}
	
}
