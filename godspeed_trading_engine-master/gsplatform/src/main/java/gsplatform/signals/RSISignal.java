package gsplatform.signals;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

public class RSISignal extends Signal {
	
	/**
	 * N - candles used to calculate RSI
	 */
	protected int candlesInAverage;

	/**
	 * The relative strength index for the given period
	 */
	protected double rsi;
	
	/**
	 * Buy or sell when above or below threshold
	 */
	protected double threshold;
	
	/**
	 * Smooths values after initial calculation
	 */
	protected boolean doSmoothing;

	/**
	 * The tracked gains over the period
	 */
	protected Queue< Double > differences;
	
	/**
	 * Track the number of periods - because we want to include 0 periods
	 */
	protected int periods;

	/**
	 * Sum of gains up to current candle over period
	 */
	protected double sumOfGains;

	/**
	 * The average gain over the period
	 */
	protected double averageGain;
	
	/**
	 * The average gain over the period
	 */
	protected double lastAverageGain;
	
	/**
	 * Sum of the losses up to current candle over period
	 */
	protected double sumOfLosses;
	
	/**
	 * The average gain over the period
	 */
	protected double lastAverageLoss;

	/**
	 * The average loss over the period
	 */
	protected double averageLoss;

	/**
	 * The current close price
	 */
	protected double currentClosePrice;

	/**
	 * The close price of the previous candle
	 */
	protected double previousClosePrice;

	/**
	 * @param candlesInAverage - used to average
	 * @param threshold - buy or sell above or below threshold between 0 - 100
	 * Explicit constructor
	 */
	public RSISignal( int candlesInAverage, double threshold )
	{
		this.rsi              = Double.MAX_VALUE;
		this.threshold        = threshold;
		this.candlesInAverage = candlesInAverage;
		this.averageGain      = 0;
		this.averageLoss      = 0;
		this.lastAverageGain  = 0;
		this.lastAverageLoss  = 0;
		this.periods          = 0;
		this.differences      = new LinkedList< Double >( );
	}

	@Override
	public void updateBefore( TradingParameters tradingParameters ) {
		
		//Do nothing if the previous close price has not been grabbed
		if( this.previousClosePrice == 0 ){
			this.currentClosePrice = tradingParameters.getAsDouble( "close" );
			return;
		}

		this.currentClosePrice = tradingParameters.getAsDouble( "close" );		

		updateAverages( tradingParameters );

		initMovingSum( tradingParameters );

		updatePhase( );
		
	}
	

	@Override
	public void updateTrigger( ) {
		//No trigger defined for SMASignal
	}
	
	@Override
	public void updateAfter(TradingParameters tradingParameters) {
		this.previousClosePrice = this.currentClosePrice;
		this.lastAverageGain    = this.averageGain;
		this.lastAverageLoss    = this.averageLoss;
	}
	
	/**
	 * Internal method that is called in the initialization period
	 * @param tradingParameters	The trading parameters for this candle
	 */
	protected void initMovingSum( TradingParameters tradingParameters )
	{
		if( this.phase == PHASE.SETUP )
		{
			double difference = this.currentClosePrice - this.previousClosePrice;

			if( difference > 0 ) {
				this.sumOfGains  += difference;
			}
			else if( difference < 0 ){
				this.sumOfLosses += ( difference * -1 );
			}
			
			this.differences.add( difference );
		}

	}
	
	/**
	 * Called once the SMA has been set and we need to update
	 * @param tradingParameters	The trading parameters for this candle
	 */
	protected void updateAverages( TradingParameters tradingParameters )
	{
		if( this.phase == PHASE.READY  )
		{
			double difference = this.currentClosePrice - this.previousClosePrice;
			
			//Perform a regular average
			if( this.periods == this.candlesInAverage-1 && !this.doSmoothing ) {
				averageForRsi( difference );
			}
			
			//Smooth
			else if( doSmoothing ){
				smoothForRsi( difference );
			}
			
			updateRSI( );
			
			//No point in updating sums once we start smoothing
			if( !doSmoothing ) {
				double firstCandle  = this.differences.remove( );
				if( firstCandle > 0 ) {
					this.sumOfGains -= firstCandle;
				}
				else {
					this.sumOfLosses -= firstCandle;
				}
			}

		}
	}
	
	/**
	 * Called only once to set initial average
	 * @param difference
	 */
	protected void averageForRsi( double difference ) {
		this.differences.add( difference );
		if( difference > 0 ) {
			this.sumOfGains  += difference;
		}
		else if( difference < 0 ){
			this.sumOfLosses  += ( difference * -1 );
		}
		this.averageGain = sumOfGains / ( this.candlesInAverage );
		this.averageLoss = sumOfLosses / ( this.candlesInAverage );
		this.doSmoothing = true;
	}
	
	/**
	 * Perform smoothing, happens each iteration after average
	 * @param difference
	 */
	protected void smoothForRsi( double difference ) {
		if( difference > 0 ) {
			this.averageGain = ( ( this.lastAverageGain * ( this.candlesInAverage-1 ) ) + difference ) / this.candlesInAverage;
		}
		else if( difference < 0 ) {
			this.averageLoss = ( ( this.lastAverageLoss * ( this.candlesInAverage-1 ) ) + ( difference * -1 ) ) / this.candlesInAverage;
		}
	}

	/**
	 * Called when preprocessing is complete to update the relative strength index
	 */
	protected void updateRSI( ){
		
		if( this.phase == PHASE.READY ){
			double rs = this.averageGain / this.averageLoss;
			this.rsi  = this.averageGain != 0 ? 100.00 - ( 100.00 / ( 1.00 + rs ) ) : 0;
		}
	}
	
	/**
	 * Called each iteration to see if the phase needs to be flipped
	 */
	protected void updatePhase( )
	{
		this.periods++;
		//Update phase
		if( this.periods == this.candlesInAverage-1 ){
			this.phase = PHASE.READY;
		}
	}

	public double getAvgGain( ) {
		return this.averageGain;
	}
	
	public double getAvgLoss( ) {
		return this.averageLoss;
	}
	
	public double getRsi( ) {
		return this.rsi;
	}
	
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIndicatorsAsString() 
	{
		return new BigDecimal( this.rsi ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( );
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "RSI" );
		jsonObject.put( "avgLoss", this.averageLoss );
		jsonObject.put( "avgGain", this.averageGain );
		jsonObject.put( "rsi", this.rsi );
		return jsonObject;
	}
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.candlesInAverage;
	}
}
