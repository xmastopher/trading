package gsplatform.signals;
import gsplatform.utilities.TradingParameters;

public class EMACrossSellSignal extends EMACrossSignal {

	/**
	 * Explicit constructor
	 * @param candlesInShorterAverage	The shorter SMA parameter
	 * @param candlesInLargerAverage		The longer SMA parameter
	 */
	public EMACrossSellSignal( int candlesInShorterAverage, int candlesInLargerAverage ) 
	{
		super(candlesInShorterAverage, candlesInLargerAverage);
		
		//Ensures that trigger is not set on the first iteration
		this.previousEmaLarger = 0;
	}
	
	
	@Override
	/**
	 * Calls parent, work happens in updateAfter for this signal
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );	
	}
	
	@Override
	/**
	 * A buy signal is triggered when when the shorter SMA moves below the longer SMA. In other
	 * words, the last price of the shorter SMA is > the last price of the longer SMA, but the current
	 * price of the shorter SMA is < than the current price of the longer SMA
	 */
	public void updateTrigger( )
	{
		this.isTriggered = this.previousEma > this.previousEmaLarger && 
					       this.ema < this.emaLarger;
	}
	
	@Override
	public String getMessage( )
	{
		String smaShortFunction = "EMA(" + this.candlesInAverage + ")" + " = " + this.ema;
		String smaLongFunction = "EMA(" + this.candlesInAverageLarger + ")" + " = " + this.emaLarger;
		return " " + smaShortFunction + " " + smaLongFunction;
	}
	

}
