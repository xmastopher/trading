package gsplatform.signals;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.TradingParameters;

public class TargetSellSignal extends TargetSignal {

	public TargetSellSignal(Double target) {
		super(target);
		// TODO Auto-generated constructor stub
	}
	/**
	 * The last price purchased at
	 */
	protected Double lastBuyPrice;
	
	/**
	 * The last price sold at
	 */
	protected Double sellTarget;
	
	@Override
	public void updateBefore(TradingParameters tradingParameters) {
		super.updateBefore(tradingParameters);
		this.lastBuyPrice = tradingParameters.getAsDouble( "last_buy_price" );
		this.sellTarget     = ( this.target * this.lastBuyPrice) + this.lastBuyPrice;
	}

	
	@Override
	/**
	 * A buy signal is triggered when when the shorter SMA moves below the longer SMA. In other
	 * words, the last price of the shorter SMA is > the last price of the longer SMA, but the current
	 * price of the shorter SMA is < than the current price of the longer SMA
	 */
	public void updateTrigger( )
	{
		this.isTriggered = this.closePrice >= this.sellTarget;
	}
	
	
	public Double getSellTarget( ) {
		return this.sellTarget;
	}
	
	@Override
	public String getMessage( )
	{
		try {
			return toJson( ).toString( );
		}
		catch (Exception e){
			return "error";
		}

	}
	
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "TARGET" );
		if( Double.isInfinite( this.sellTarget ) ) {
			jsonObject.put( GS_Utils.createFunctionBasedString("sellTarget", String.valueOf( this.target ) ), 0 );
		}
		else {
			jsonObject.put( GS_Utils.createFunctionBasedString("sellTarget", String.valueOf( this.target ) ), this.sellTarget );
		}
		return jsonObject;
	}

}
