package gsplatform.signals;
import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

/**
 */
public class MACDSellSignal extends MACDSignal
{
	
	/**
	 * Explicit constructor initializes candle color
	 */
	public MACDSellSignal( int minPeriod, int maxPeriod, int signalPeriod )
	{
		super( minPeriod, maxPeriod, signalPeriod );
	}
	
	@Override
	/**
	 * Update the candle color
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );
	}
	
	/**
	 * Buy when mac d crosses below signal line
	 */
	@Override
	public void updateTrigger( )
	{
		this.isTriggered = this.lastMacdLine >= this.signalEMA.getEMA( ) && this.macdLine < this.signalEMA.getEMA( );
	}
	
	@Override
	public String getMessage( )
	{
		String smaShortFunction = "MACD(" + this.maxEMA.getPreprocessingValue( ) + "," + this.minEMA.getPreprocessingValue( ) + "," + 
								  this.signalEMA.getPreprocessingValue( ) + ")" + " = " + new BigDecimal( this.macdLine ).setScale( 8, BigDecimal.ROUND_DOWN ).toPlainString( );
		return " " + smaShortFunction;
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		return super.toJson( );
	}
	
	
}
