package gsplatform.signals;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

public class SMASignal extends Signal {
	
	/**
	 * List of the prices currently in the moving sum where candlesInMovingSum.size( ) == candlesInAverage
	 */
	protected Queue< Double > candlesInMovingSum;
	
	/**
	 * The SMA period ie. SMA-20 will take the average over the last 20 candle averages
	 */
	protected int candlesInAverage;
	
	/**
	 * The moving sum based on the number of candles
	 */
	protected double movingSum;
	
	/**
	 * The moving average throughout time
	 */
	protected double movingAverage;
	
	/**
	 * The current close price
	 */
	protected double currentClosePrice;
	
	/**
	 * The current open price
	 */
	protected double currentOpenPrice;
	
	public SMASignal( int candlesInAverage )
	{
		super( );
		this.candlesInMovingSum   = new LinkedList< Double >( );
		this.candlesInAverage     = candlesInAverage;
		this.movingSum            = 0;
		this.movingAverage        = 0;
	}

	@Override
	public void updateBefore( TradingParameters tradingParameters ) {
		
		this.currentClosePrice = tradingParameters.getAsDouble( "close" );
		this.currentOpenPrice = tradingParameters.getAsDouble( "open" );
		
		//Update the moving average - internally checks state
		updateMovingAverage( tradingParameters );
		initMovingSum( tradingParameters );
		updatePhase( );
	}
	

	@Override
	public void updateTrigger( ) {
		//No trigger defined for SMASignal
	}
	
	@Override
	public void updateAfter(TradingParameters tradingParameters) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Internal method that is called in the initialization period
	 * @param tradingParameters	The trading parameters for this candle
	 */
	protected void initMovingSum( TradingParameters tradingParameters )
	{
		if( this.phase == PHASE.SETUP )
		{
			this.candlesInMovingSum.add( this.currentClosePrice );
			this.movingSum += this.currentClosePrice;
		}
	}
	
	/**
	 * Called once the SMA has been set and we need to update
	 * @param tradingParameters	The trading parameters for this candle
	 */
	protected void updateMovingAverage( TradingParameters tradingParameters )
	{
		if( this.phase == PHASE.READY  )
		{
			//Update the moving sum with the new candle
			this.movingSum    += this.currentClosePrice;
			
			this.candlesInMovingSum.add( this.currentClosePrice );
			this.movingAverage = this.movingSum / Double.valueOf( this.candlesInAverage );
			
			//Remove the first candle
			double firstCandle = this.candlesInMovingSum.remove( );
			this.movingSum    -= firstCandle;
		}
	}
	
	/**
	 * Called each iteration to see if the phase needs to be flipped
	 */
	protected void updatePhase( )
	{
		//Update phase
		if( this.candlesInMovingSum.size( ) == this.candlesInAverage-1 )
		{
			this.phase = PHASE.READY;
		}
	}
	
	/**
	 * Public access for SMA value for testing
	 * @return
	 */
	public double getSMA( )
	{
		return this.movingAverage;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIndicatorsAsString() 
	{
		return new BigDecimal( this.getSMA( ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( );
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "SMA" );
		jsonObject.put( "sma", this.getSMA( ) );
		return jsonObject;
	}
	
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.candlesInAverage;
	}
}
