package gsplatform.signals;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

public class TargetSignal extends Signal {

	/**
	 * Percentage value to calculate next target
	 */
	protected Double target;
	
	/**
	 * Percentage value to calculate next target
	 */
	protected Double closePrice;
	
	public TargetSignal( Double target ) {
		this.target = target;
		this.phase  = Signal.PHASE.READY;
	}
	
	@Override
	public void updateBefore(TradingParameters tradingParameters) {
		this.closePrice = tradingParameters.getAsDouble( "close" );
	}

	@Override
	public void updateAfter(TradingParameters tradingParameters) {;}

	@Override
	public void updateTrigger() {;}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public JSONObject toJson() throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIndicatorsAsString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPreprocessingValue() {
		// TODO Auto-generated method stub
		return 0;
	}


}
