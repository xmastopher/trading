package gsplatform.signals;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.TradingParameters;

public class LocalMaximaBuySignal extends Signal {

	protected Queue< BigDecimal > window;
	
	protected BigDecimal currentClosePrice;
	
	protected int windowSize;
	
	protected BigDecimal maximumPrice;
	
	protected BigDecimal highPrice;
	
	public LocalMaximaBuySignal( int windowSize )
	{
		this.windowSize = windowSize;
		this.window     = new LinkedList< BigDecimal >( );
	}
	
	@Override
	public void updateBefore(TradingParameters tradingParameters) 
	{
		this.highPrice  		   = tradingParameters.getAsBigDecimal( "high", 8 );
		this.currentClosePrice  = tradingParameters.getAsBigDecimal( "close", 8 );
		
		if( this.phase == PHASE.READY )
		{
			this.window.remove( );
		}
		else if( this.window.size( ) == this.windowSize-1 )
		{
			this.phase = PHASE.READY;
		}
		
		this.window.add( this.highPrice );
		
	}

	@Override
	/**
	 * Buy when the close price is below the local minima
	 */
	public void updateTrigger( ) 
	{
		this.isTriggered = this.phase == PHASE.READY && this.currentClosePrice.doubleValue( ) > this.maximumPrice.doubleValue( );
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIndicatorsAsString() {
		// TODO Auto-generated method stub
		return this.maximumPrice.setScale( 8, BigDecimal.ROUND_DOWN ).toString( );
	}
	
	public BigDecimal getMaximumPrice( )
	{
		return this.maximumPrice;
	}

	@Override
	public void updateAfter(TradingParameters tradingParameters) {
		this.maximumPrice = GS_Utils.getMaximum( new ArrayList< BigDecimal >( this.window ) );
		
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "LOCAL-MAXIMA" );
		jsonObject.put( GS_Utils.createFunctionBasedString( "window", String.valueOf( this.windowSize ) ), 
							this.getMaximumPrice( ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( )  );
		return jsonObject;
	}
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.windowSize;
	}
}
