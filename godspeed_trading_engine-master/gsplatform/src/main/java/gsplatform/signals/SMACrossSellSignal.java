package gsplatform.signals;
import gsplatform.utilities.TradingParameters;

public class SMACrossSellSignal extends SMACrossSignal {

	/**
	 * Explicit constructor
	 * @param candlesInShorterAverage	The shorter SMA parameter
	 * @param candlesInLargerAverage		The longer SMA parameter
	 */
	public SMACrossSellSignal( int candlesInShorterAverage, int candlesInLargerAverage ) 
	{
		super(candlesInShorterAverage, candlesInLargerAverage);
		
		//Ensures that trigger is not set on the first iteration
		this.lastShortSMA = Double.MIN_VALUE;
		this.lastLongSMA  = Double.MAX_VALUE;
	}
	
	
	@Override
	/**
	 * Calls parent, work happens in updateAfter for this signal
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );	
	}
	
	@Override
	/**
	 * A buy signal is triggered when when the shorter SMA moves below the longer SMA. In other
	 * words, the last price of the shorter SMA is > the last price of the longer SMA, but the current
	 * price of the shorter SMA is < than the current price of the longer SMA
	 */
	public void updateTrigger( )
	{
		this.isTriggered = this.lastShortSMA > this.lastLongSMA && 
					       this.movingAverage < this.movingAverageLarger;
	}
	
	@Override
	public String getMessage( )
	{
		String smaShortFunction = "SMA(" + this.candlesInAverage + ")" + " = " + this.getSMA( );
		String smaLongFunction = "SMA(" + this.candlesInAverageLarger + ")" + " = " + this.getSMALarger( );
		return " " + smaShortFunction + " " + smaLongFunction;
	}
	

}
