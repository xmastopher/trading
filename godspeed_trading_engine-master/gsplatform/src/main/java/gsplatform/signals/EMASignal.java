package gsplatform.signals;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

public class EMASignal extends SMASignal {

	/**
	 * Applied to each EMA value
	 */
	protected double weightingMultiplier;
	
	/**
	 * Used to calculate the current EMA
	 */
	protected double previousEma;
	
	
	/**
	 * The current exponential moving average value
	 */
	protected double ema;
	
	public EMASignal( int candlesInAverage )
	{
		super( candlesInAverage );
		this.weightingMultiplier = 2.00 / ( this.candlesInAverage + 1.00 );
		this.previousEma         = Double.MIN_VALUE;
		this.ema                 = 0;
	}

	@Override
	public void updateBefore( TradingParameters tradingParameters ) {
		
		super.updateBefore( tradingParameters );

		
		if( this.phase == PHASE.READY ) {
			
			if( this.ema == 0 ) {
				this.ema = this.movingAverage;
			}
			else {
				this.ema = ( ( this.currentClosePrice - this.previousEma ) * this.weightingMultiplier ) + this.previousEma;
			}
		}
	}
	

	@Override
	public void updateTrigger( ) {
		//No trigger defined for SMASignal
	}
	
	@Override
	public void updateAfter(TradingParameters tradingParameters) {
		
		super.updateAfter( tradingParameters);
		
		if( this.phase == PHASE.READY && this.ema != 0 ) {
			this.previousEma = this.ema;
		}
	}
	
	/**
	 * Called each iteration to see if the phase needs to be flipped
	 */
	@Override
	protected void updatePhase( )
	{
		super.updatePhase( );
	}
	
	/**
	 * Public access for SMA value for testing
	 * @return
	 */
	@Override
	public double getSMA( )
	{
		return this.movingAverage;
	}
	
	/**
	 * Returns the last iterations ema value
	 * @return
	 */
	public double getPreviousEMA( ) {
		return this.previousEma;
	}
	
	/**
	 * Returns the current EMA's iteration
	 * @return
	 */
	public double getEMA( ) {
		return this.ema;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIndicatorsAsString() 
	{
		return new BigDecimal( this.getSMA( ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( );
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "EMA" );
		jsonObject.put( "ema", this.getEMA( ) );
		return jsonObject;
	}
	
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.candlesInAverage;
	}
}
