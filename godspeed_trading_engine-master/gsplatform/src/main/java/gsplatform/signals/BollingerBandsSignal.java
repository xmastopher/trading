package gsplatform.signals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gsplatform.utilities.TradingParameters;

public class BollingerBandsSignal extends SMASignal {

	/**
	 * Standard deviation for the current set
	 */
	protected Double stdDeviation;
	
	/**
	 * The numbere of deviations away from sma
	 */
	protected Double numberOfDeviations;

	/**
	 * Provide the SMA and number of deviations away
	 * @param candlesInAverage
	 * @param standardDeviations
	 */
	public BollingerBandsSignal( int candlesInAverage, Double standardDeviations ) 
	{
		super(candlesInAverage);
		this.numberOfDeviations = standardDeviations;
	}

	@Override
	/**
	 * Overriden to add in standard deviation calculation
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );
		this.stdDeviation = calculateStandardDeviation( );
	}
	
	/**
	 * Calculates the std deviation for the current set
	 * @return
	 */
	public double calculateStandardDeviation( )
	{
		List< Double > slidingWindow = new ArrayList< Double >( this.candlesInMovingSum );
		Double standardDeviation     = new Double( 0 );
		
        for( int i = 0; i < slidingWindow.size(); i++ ) 
        {
            standardDeviation += Math.pow( slidingWindow.get( i ) - this.getSMA( ), 2	);
        }

        return Math.sqrt( standardDeviation / this.candlesInAverage );
	}
	
	/**
	 * Returns the std deviation for the period
	 * @return
	 */
	public double getStandardDeviation( )
	{
		return this.stdDeviation;
	}
	
	@Override
	public String getIndicatorsAsString() 
	{
		return super.getIndicatorsAsString( ) 
				+ "," + 
			   new BigDecimal( this.stdDeviation ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) 
			   + "," + 
			   new BigDecimal( this.movingAverage + ( this.numberOfDeviations * this.stdDeviation ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( )
			   + "," +
			   new BigDecimal( this.movingAverage - ( this.numberOfDeviations * this.stdDeviation ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( );

	}
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.candlesInAverage;
	}
	
}
