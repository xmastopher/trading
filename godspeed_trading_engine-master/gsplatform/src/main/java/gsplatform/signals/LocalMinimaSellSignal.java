package gsplatform.signals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.TradingParameters;

public class LocalMinimaSellSignal extends Signal {

	protected Queue< BigDecimal > window;
	
	protected BigDecimal currentClosePrice;
	
	protected int windowSize;
	
	protected int currentCount;
	
	protected BigDecimal minimumPrice;
	
	protected BigDecimal lowPrice;
	
	public LocalMinimaSellSignal( int windowSize )
	{
		this.windowSize = windowSize;
		this.window     = new LinkedList< BigDecimal >( );
	}
	
	@Override
	public void updateBefore(TradingParameters tradingParameters) 
	{
		this.currentClosePrice = tradingParameters.getAsBigDecimal( "close", 8 );
		this.lowPrice          = tradingParameters.getAsBigDecimal( "low", 8 );
		
		if( this.phase == PHASE.READY )
		{
			this.window.remove( );
		}
		else if( this.window.size( ) == this.windowSize-1 )
		{
			this.phase = PHASE.READY;
		}
		
		this.window.add( this.lowPrice );
	}

	@Override
	/**
	 * Buy when the close price is below the local minima
	 */
	public void updateTrigger( ) 
	{
		this.isTriggered = this.phase == PHASE.READY && this.currentClosePrice.doubleValue( ) < this.minimumPrice.doubleValue( );
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getIndicatorsAsString() {
		// TODO Auto-generated method stub
		return this.minimumPrice.setScale( 8, BigDecimal.ROUND_DOWN ).toString( );
	}
	
	public BigDecimal getMinimumPrice( )
	{
		return this.minimumPrice;
	}

	@Override
	public void updateAfter(TradingParameters tradingParameters) {
		this.minimumPrice = GS_Utils.getMinimum( new ArrayList< BigDecimal >( this.window ) );
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "LOCAL-MINIMA" );
		jsonObject.put( GS_Utils.createFunctionBasedString( "window", String.valueOf( this.windowSize ) ), 
							this.getMinimumPrice( ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( )  );
		return jsonObject;
	}
	
	@Override
	public int getPreprocessingValue( )
	{
		return this.windowSize;
	}
}
