package gsplatform.signals;

import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;

public class BollingerBandsBuySignal extends BollingerBandsSignal {

	public BollingerBandsBuySignal( int candlesInAverage, double standardDeviations ) 
	{
		super( candlesInAverage, standardDeviations );
	}
	
	@Override
	public void updateTrigger( ) {
		Double lowerBand = this.movingAverage - ( this.numberOfDeviations * this.stdDeviation );
		this.isTriggered = this.movingAverage != 0.0 && this.currentOpenPrice > lowerBand && this.currentClosePrice <= lowerBand;
	}
	
	@Override
	public String getMessage( )
	{
		Double upper = this.getSMA( ) + ( this.numberOfDeviations * this.stdDeviation );
		Double lower = this.getSMA( ) - ( this.numberOfDeviations * this.stdDeviation );
		String sma       = "SMA(" + this.candlesInAverage + ")" + " = " + this.getSMA( );
		String upperBand = "BOL+ " + this.numberOfDeviations + " = " + upper;
		String lowerBand = "BOL- " + this.numberOfDeviations + " = " + lower;
		return " " + sma + " " + upperBand + " " + lowerBand;
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "BOLLINGER-BANDS" );
		jsonObject.put( GS_Utils.createFunctionBasedString("sma", String.valueOf( this.candlesInAverage ) ), this.getSMA( ) );
		jsonObject.put( GS_Utils.createFunctionBasedString("std", new BigDecimal( this.numberOfDeviations ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) ),
							new BigDecimal( this.getStandardDeviation( ) ).setScale( 8, BigDecimal.ROUND_DOWN ) );
		return jsonObject;
	}

	
}
