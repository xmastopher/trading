package gsplatform.signals;
import gsplatform.utilities.TradingParameters;

public class SMACrossBuySignal extends SMACrossSignal {

	
	/**
	 * Explicit constructor initializes candle color
	 */
	public SMACrossBuySignal( int candlesInAverage, int candlesInLongerAverage )
	{
		super( candlesInAverage, candlesInLongerAverage );
		
		//Ensures that trigger is not set on the first iteration
		this.lastShortSMA = Double.MAX_VALUE;
		this.lastLongSMA  = Double.MIN_VALUE;
	}
	
	@Override
	/**
	 * Calls parent, work happens in updateAfter for this signal
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );	
	}
	
	@Override
	/**
	 * A buy signal is triggered when when the shorter SMA moves above the longer SMA. In other
	 * words, the last price of the shorter SMA is < the last price of the longer SMA, but the current
	 * price of the shorter SMA is > than the current price of the longer SMA
	 */
	public void updateTrigger( )
	{
		this.isTriggered = this.isReady( ) && this.lastShortSMA < this.lastLongSMA && 
					       this.movingAverage > this.movingAverageLarger;
	}
	
	@Override
	public String getMessage( )
	{
		String smaShortFunction = "SMA(" + this.candlesInAverage + ")" + " = " + this.getSMA( );
		String smaLongFunction = "SMA(" + this.candlesInAverageLarger + ")" + " = " + this.getSMALarger( );
		return " " + smaShortFunction + " " + smaLongFunction;
	}
	
	
}
