package gsplatform.signals;
import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.TradingParameters;

/**
 *  Signal class for simple moving average buy signal which is
 *  triggered when a candle is green and closes above designated SMA
 */
public class SMABuySignal extends SMASignal
{
	
	/**
	 * Is the current candle green?
	 */
	protected Boolean isGreen;
	
	/**
	 * Explicit constructor initializes candle color
	 */
	public SMABuySignal( int candlesInAverage )
	{
		super( candlesInAverage );
		this.isGreen = new Boolean( false );
	}
	
	/**
	 * Return true if candle is green false otherwise
	 */
	public Boolean isCandleGreen( )
	{
		return this.isGreen;
	}
	
	@Override
	/**
	 * Update the candle color
	 */
	public void updateBefore( TradingParameters tradingParameters )
	{
		super.updateBefore( tradingParameters );
		this.isGreen = this.currentClosePrice >= this.currentOpenPrice;
	}
	
	/**
	 * Buy signal is defined as the following for SMA Strategy:
	 * When the current candle closes above the defined SMA and is green
	 */
	@Override
	public void updateTrigger( )
	{
		this.isTriggered = this.isGreen && this.currentClosePrice >= this.movingAverage && 
						   this.currentOpenPrice <= this.movingAverage;
	}
	
	@Override
	public String getMessage( )
	{
		String smaShortFunction = "SMA(" + this.candlesInAverage + ")" + " = " + new BigDecimal( this.getSMA( ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toPlainString( );
		return " " + smaShortFunction;
	}
	
	@Override
	public JSONObject toJson( ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject( );
		jsonObject.put( "class", "SMA" );
		jsonObject.put( GS_Utils.createFunctionBasedString( "sma", String.valueOf( this.candlesInAverage ) ), 
							new BigDecimal( this.getSMA( ) ).setScale( 8, BigDecimal.ROUND_DOWN ).toString( ) );
		return jsonObject;
	}
	
}
