package gsplatform.exceptions;

/**
 * 
 */
public class BuyAndSellException extends Exception 
{
	/**
	 * 
	 */
	private static String DEFAULT_ERROR_MESSAGE = "Both buy and sell signals cannot be true at the same time.";
	
	/**
	 * 
	 */
	public BuyAndSellException() 
	{
		super(DEFAULT_ERROR_MESSAGE);
	}
	
	/**
	 * 
	 */
	public BuyAndSellException(String message)
	{
		super(message);
	}

}
