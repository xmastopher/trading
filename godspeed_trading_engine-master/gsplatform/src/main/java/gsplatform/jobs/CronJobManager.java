package gsplatform.jobs;

/**
 * Driver program accessed through server cronjobs which performs
 * different automated processes based on provided command line
 * parameters;
 * @author danielanderson
 *
 */
public class CronJobManager {
   
	/**
	 * Internal reference to logger
	 */
	private static final GS_Logger LOGGER = GS_Logger.getLogger( );
	
	/**
	 * Helper function for parsing arguments
	 * @param args	Array of strings passed from command line
	 * @param arg	The arg to search for
	 * @return		String value associated with the arg
	 */
	public static String parseArg( String[] args, String arg ) {
		for( int i = 0; i < args.length; i++ ) {
			if( args[ i ].equalsIgnoreCase( arg ) ) {
				return args[ i+1 ];
			}
		}
		return "NONE";
	}
	
	/**
	 * Main entry point for running a cronjob, accepted parameters are:
	 * -NomicsMirrorManager {"true", "false"}
	 * -BotLeaderBoardsCleaner {"daily","weekly"}
	 * -LogRoller {"true","false"}
	 * @param args Command line params
	 */
	public static void main( String[] args ) {
		
		LOGGER.writeLogMessage( "AUTOATED-PROCESSES", "CronJobManager", "Kicking off Cron Job with params: " + args.toString( ) );
		String nomicsArg   = parseArg( args, "-NomicsMirrorManager");
		String botsArg     = parseArg( args, "-BotLeaderboardsCleaner");
		String rollerArg   = parseArg( args, "-LogRoller");		
		
		//Run nomics cron job
		if( nomicsArg.equalsIgnoreCase( "true" ) ) {
			new NomicsMirrorManager( ).run( );
		}
		
		if( botsArg.equalsIgnoreCase( "BotLeaderboardsDaily" ) || botsArg.equalsIgnoreCase( "BotLeaderboardsWeekly" ) ) {
			new BotLeaderboardsCleaner( ).run( botsArg ); 
		}
		
		//Add log roller class below - not implemented yet
		if( rollerArg.equalsIgnoreCase( "true" ) ) {
			new LogRoller( ).run( );
		}
		
		//Add log roller class below - not implemented yet
		if( rollerArg.equalsIgnoreCase( "true" ) ) {
			new LogRoller( ).run( );
		}
	 }
}
