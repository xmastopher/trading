package gsplatform.jobs;
import java.sql.SQLException;
import java.util.List;
import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.factories.AlgorithmicTraderFactory;
import gsplatform.factories.TradingStrategyFactory;
import gsplatform.services.AutomatedTrader;
import gsplatform.services.BotThreadHandler;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;
import gsplatform.servlet.database.UserStrategiesPackage;

/**
 * Automated process only to be ran on server startup.  This process does exactly one task,
 * search for all running bots in the database and resume their thread process.
 * @author danielanderson
 */
public class BotThreadRecovery{

	/**
	 * Retrieve singleton instance of logger
	 */
	private static final GS_Logger LOGGER = GS_Logger.getLogger( );
	
	/**
	 * Empty constructor
	 */
	public BotThreadRecovery() {;}

	/**
	 * Grabs singleton instance of BotThreadHandler class and 
	 * calls function to recover trading bots.
	 */
	public void run( ) {
		try {
			recoverTradingBots( BotThreadHandler.getSingletonInstance( ) );
		} catch (ClassNotFoundException e) {
			LOGGER.writeLogMessage( "AUTOMATED-PROCESSES", "BotThreadRecovery", "Failed recovery bots with exception " + e.getMessage( ) );
			e.printStackTrace();
		} catch (SQLException e) {
			LOGGER.writeLogMessage( "AUTOMATED-PROCESSES", "BotThreadRecovery", "Failed recovery bots with exception " + e.getMessage( ) );
			e.printStackTrace();
		}
	}
    
	/**
	 * Called when singleton is created to start any bots that aren't running
	 * which were interrupted by an application or server shutdown
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private void recoverTradingBots( BotThreadHandler botThreadHandler ) throws ClassNotFoundException, 
																			   SQLException {
		
		UserBotsDAO userBotsDAO = new UserBotsDAO( );
		BotLogsDAO botLogsDAO   = new BotLogsDAO( );
		userBotsDAO.openConnection( );
		botLogsDAO.openConnection( );
		
		List< UserBot > runningBots = userBotsDAO.getAllRunningBots( );
		for( UserBot userBot : runningBots ) {
			
			try {
				//Grab the trading strategy
				TradingStrategiesDAO tradingStrategiesDAO = new TradingStrategiesDAO( userBot.getUsername( ) );
				tradingStrategiesDAO.openConnection( );
				UserStrategiesPackage usp = tradingStrategiesDAO.getUserStrategyById( userBot.getStrategyId( ) );
				tradingStrategiesDAO.closeConnection( );
		
				//Grab the strategy object
				TradingAlgorithm algorithm = new TradingStrategyFactory( ).createObject( usp, 0 );
				AutomatedTrader atb        = new AlgorithmicTraderFactory( ).createObject( userBot, algorithm );
				
				//Grab Bot Log
				botLogsDAO.setUsernameAndId( userBot.getUsername( ), userBot.getBotId( ) );
				BotLog lastLog = botLogsDAO.getLastBotTradeActionLog( );
				
				//Recover the bots state & add to thread
				atb.recoverState( lastLog, userBot );
				
				BotThreadHandler.getSingletonInstance( ).addBot( userBot, atb );
				LOGGER.addMessageEntry( "AUTOMATED-PROCESSES", "BotThreadHandler", "Succesfully restarted bot: " +
						userBot.getUsername( ) + "-" + userBot.getBotId( ) );
			}
			catch ( Exception e ) { 
				LOGGER.addMessageEntry( "AUTOMATED-PROCESSES", "BotThreadHandler", "ERROR RESTARTING BOT: " + 
									     userBot.getUsername( ) + "-" + userBot.getBotId( ) );
				continue;
			}
		}
		
		botLogsDAO.closeConnection( );
		userBotsDAO.closeConnection( );
	}
    

}
