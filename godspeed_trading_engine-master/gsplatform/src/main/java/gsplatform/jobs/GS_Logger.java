package gsplatform.jobs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * This is the core class used for all platform logging.  We chose 
 * not to use LOG4J because screw LOG4J.
 * @author danielanderson
 */
public class GS_Logger {
	
	/**
	 * Path to logging directory
	 */
	public static final String APPLOGS_PATH = "/sb/travasresearch/applogs/travas/";
	
	/**
	 * Singleton flag - set to true to ensure single instance
	 */
	static boolean isConfigured = false;
	
	/**
	 * Reference to singleton instance
	 */
	static GS_Logger instance;
	
	/**
	 * Internal mapping to log level file paths by name
	 */
	private HashMap< String, String > configuredLogFiles;

	/**
	 * Queue holding all pending log messages
	 */
	private static Queue< LogMessage > logMessageQueue = new LinkedList< LogMessage >( );
	
	/**
	 * Reference to internal runnable instance that handles
	 * the log message queue
	 */
	private static GS_LoggerManager loggerThread;
	
	/**
	 * Wrapper to describe a log message, encapsulates the information about the class
	 * which generated the log, the associated log file, specific message and timestamp.
	 * @author danielanderson
	 *
	 */
	public class LogMessage{
		
		/**
		 * The string index for the log file
		 */
		private String log;
		
		/**
		 * Class name which is calling the log
		 */
		private String caller;
		
		/**
		 * Message associated with the log
		 */
		private String message;
		
		/**
		 * Timestamp this message was created
		 */
		private Timestamp timestamp;
		
		/**
		 * Explicit constructor
		 * @param log		String indexing to log file
		 * @param caller		The functino which wanted to perform the log
		 * @param message	The message presented at the end of the log
		 */
		public LogMessage( String log, String caller, String message ) {
			this.log 	 = log;
			this.caller  = caller;
			this.message = message;
			this.timestamp = new Timestamp( new Date( ).getTime( ) );
		}
		
		/**
		 * Returns the log associated with this message
		 * @return	String index for log map
		 */
		public String getLog( ) { return this.log; }
		
		/**
		 * Returns the class which created this log
		 * @return	String which is equivallent to class name
		 */
		public String getCaller( ) { return this.caller; }
		
		/**
		 * Returns the message associated with this log
		 * @return	String message
		 */
		public String getMessage( ) { return this.message; }
		
		/**
		 * Returns the timestamp the log was created
		 * @return	SQL Timestamp object
		 */
		public Timestamp getTimestamp( ) { return this.timestamp; }
	}
	
	/**
	 * Threaded class that runs logger.logAllPending job every X milliseconds
	 * to remove the load of logging from the main program
	 * @author danielanderson
	 *
	 */
	public class GS_LoggerManager implements Runnable{
		
		/**
		 * Keeps track of the running state of the queue manager
		 */
		private boolean running;
		
		/**
		 * Every X milliseconds this thread will call logAllPending on
		 * singleton log manager 
		 */
		@Override
		public void run() {
			
			this.running = true;
			
			while( running ) {
				
				try 
				{
					Thread.sleep( Constants.instance( ).getLoggerQueueDelay( ) );
					System.out.println( "Running logger job..." );
					this.logAllPending( );
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		/**
		 * Iterates over parent class queue of logs and writes them to 
		 * their respective logging files
		 */
		public void logAllPending( )
		{	
			while( !logMessageQueue.isEmpty( ) ){
				LogMessage logMessage = logMessageQueue.poll( );
				writeLogMessage( logMessage.getLog( ), logMessage.getCaller( ), logMessage.getMessage( ) );
			}
		}
	}

	/**
	 * Configures logger singleton on instantiation
	 */
	public GS_Logger( ){
		config( );
	}
	
	/**
	 * Static method for grabbing singleton
	 * @return	GS_Logger singlton instance
	 */
	public static GS_Logger getLogger( )
	{
		if( !isConfigured ){
			instance = new GS_Logger( );
			isConfigured = true;
			instance.beginLoggerThread( );
		}
		return instance;
	}
	
	/**
	 * Singletone level accessor to grab configured log files,
	 * should only ever be called by automated process LogRoller
	 * @return Map< String, String > mapping identifiers to filepaths
	 */
	public static Map< String, String > getConfiguredLogFiles( ){
		if( !isConfigured ){
			instance = new GS_Logger( );
			isConfigured = true;
		}
		return instance.configuredLogFiles;
	} 
	
	/**
	 * Starts internal runnable thread ( GS_LoggerManager ) to
	 * run in background and handle queued messages
	 */
	public void beginLoggerThread( ) {
		if( loggerThread == null ) {
			loggerThread = new GS_LoggerManager( );
			Thread thread     = new Thread( loggerThread );
			thread.start( );
		}
	}
	
	/**
	 * Called on server stop to write any pending logs
	 * and terminate the runnable thread
	 */
	public void stopLoggerThread( ) {
		loggerThread.logAllPending( );
		loggerThread.running = false;
	}
	
	/**
	 * Creates string, string mapping that maps logger name to logger
	 * filepath.
	 */
	private void config( )
	{
		this.configuredLogFiles   = new HashMap< String, String >( );
		this.configuredLogFiles.put( "API", Constants.instance( ).getApiLogPath( ) );
		this.configuredLogFiles.put( "API-EXCEPTIONS", Constants.instance( ).getApiExceptionsLogPath( ) );
		this.configuredLogFiles.put( "BOTS", Constants.instance( ).getBotsLogPath( ) );
		this.configuredLogFiles.put( "BOTS-EXCEPTIONS", Constants.instance( ).getBotExceptionsLogPath( ) );
		this.configuredLogFiles.put( "SIMULATIONS", Constants.instance( ).getSimulationsLogPath( ) );
		this.configuredLogFiles.put( "SIMULATIONS-EXCEPTIONS", Constants.instance( ).getSimulationExceptionsLogPath( ) );
		this.configuredLogFiles.put( "BOT-THREADS", Constants.instance( ).getBotThreadsLogPath( ) );
		this.configuredLogFiles.put( "AUTOMATED-PROCESSES", Constants.instance( ).getAutomatedProcessesLogPath( ) );
		this.configuredLogFiles.put( "BACKTESTS", Constants.instance( ).getBacktestsLogPath( ) );
		this.configuredLogFiles.put( "BACKTESTS-EXCEPTIONS", Constants.instance( ).getBacktestsExceptionsLogPath( ) );
	}
	
	/**
	 * Add a message to the log entry.  This queues the the message
	 * to be logged later.
	 * @param log		The log to to write to
	 * @param caller		The class that created the log
	 * @param message	The message associated with the log
	 */
	public void addMessageEntry( String log, String caller, String message ){
		//LogMessage logMessage = new LogMessage( log, caller, message );
		writeLogMessage( log, caller, message );
		//logMessageQueue.add( logMessage );
	}
	
	/**
	 * Wrapper function used to perform explicit writing to log files, should
	 * never be used when going through an API - should always use addMessageEntry
	 * @param log		The log file string
	 * @param caller		The class that called the log
	 * @param message	The message associated with the log
	 */
	public void writeLogMessage( String log, String caller, String message )
	{
		writeLogMessage( log, caller, message, new Timestamp( new Date( ).getTime( ) ).toString( ) );
	}
	
	/**
	 * Log message function called by GS_Logger manager class to perform interval based
	 * log writing from queue of pending log messages.  This function is always called
	 * when log messages are eventually written to their respective files.
	 * @param log		String name of log to write to (filepath)
	 * @param caller		String class that created the log
	 * @param message	String message associated with the log
	 * @param dated		String timestamp message was created
	 */
	public void writeLogMessage( String log, String caller, String message, String dated )
	{
		try 
		{
			File file = new File( this.configuredLogFiles.get( log ) );

			// if file doesnt exists, then create it
			if ( !file.exists( ) ) 
			{
				file.createNewFile( );
			}

			// true = append file
			FileWriter fw     = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write( generateLogMessage( caller, message, dated ) );
			bw.close( );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
			return;
		}
	}
	
	/**
	 * Internal function for generating a log message.
	 * @param caller		The class that called the log
	 * @param message	The log's associated message
	 * @param timestamp	The log's timestamp
	 * @return	String message combining above three components
	 */
	private String generateLogMessage( String caller, String message, String timestamp )
	{
		StringBuilder stringBuilder = new StringBuilder( );
		stringBuilder.append( timestamp );
		stringBuilder.append( " " );
		stringBuilder.append( "[" );
		stringBuilder.append( caller );
		stringBuilder.append( "] " );
		stringBuilder.append( message );
		stringBuilder.append( "\n" );
		return stringBuilder.toString( );
	}
}
