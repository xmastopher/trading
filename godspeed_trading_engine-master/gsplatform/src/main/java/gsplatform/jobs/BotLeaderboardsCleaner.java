package gsplatform.jobs;
import java.sql.Timestamp;
import java.util.Date;

import gsplatform.servlet.database.BotLeaderboardsDAO;

/**
 * Automated process only to be ran via CronJobManager driver by providing the
 * following command line parameters: -BotLeaderboardsCleaner BotLeaderboardsDaily | BotleaderboardsWeekly.
 * This process can do one of two tasks:
 * 	1. Remove all daily leaderboards and move them to historical
 *  2. Remove all weekly leaderboards
 * This is done to respectively wipe the boards at the end of each week or day
 * @author danielanderson
 */
public class BotLeaderboardsCleaner{

	/**
	 * Retrieve singlton for logging
	 */
	private static GS_Logger LOGGER = GS_Logger.getLogger( );
	
	/**
	 * Empty constructor.
	 */
	public BotLeaderboardsCleaner() {;}

	/**
	 * "BotLeaderboardsDaily" - run only daily
	 * "BotLeaderboardsWeekly" - run both
	 */
	public void run( String arg ) {
		
		this.reset( new Date( ), arg );	
	}
	
	/**
	 * Drops daily leaderboards and write them to historical
	 * @param now	Date the leaderboards are being cleared
	 */
	private void reset( Date now, String arg ) {

		LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "BotLeaderboardsCleaner", "wiping leaderboard " + arg );
		try {
			BotLeaderboardsDAO botLeaderboardsDAO = new BotLeaderboardsDAO( );
			botLeaderboardsDAO.openConnection( );
			if( arg.equalsIgnoreCase( "BotLeaderboardsDaily") ){
				botLeaderboardsDAO.copyDailyLeaderboards( new Timestamp( now.getTime( ) ) );
			}
			botLeaderboardsDAO.clearTable( arg );
			botLeaderboardsDAO.initAllRunningBots( arg );
			botLeaderboardsDAO.closeConnection( );
			LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "BotLeaderboardsCleaner", "successfully cleared leaderboard " + arg );
		}
		catch( Exception e ) {
			LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "BotLeaderboardsCleaner", "failed wiping daily leadeboards with error: " + e.getMessage( ) );
		}
	}	

}
