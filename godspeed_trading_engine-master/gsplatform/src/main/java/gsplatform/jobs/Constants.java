package gsplatform.jobs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Global level wrapper class which is initialized on sever start
 * to load all environment constants and parameters.
 * @author danielanderson
 */
public class Constants{

	/**
	* Path to properties file
	**/
	public static final String CONFIG_PATH = "/sb/travasresearch/config/";
	
	/**
	 * Name of configuration file
	 */
	private static final String CONFIG_FILE = "constants.properties";
	
	/**
	 * Path to api logger file - logs all API requests and responses
	**/
	private String apiLogPath;
	
	/**
	* Path to api exceptions logger file - logs all exceptions from API calls
	**/
	private String apiExceptionsLogPath;
	
	/**
	* Path to trading bots logger - logs all trading bot actions
	**/
	private String botsLogPath;
	
	/**
	* Path to bot exceptions - logs all trading bot exceptions
	**/
	private String botExceptionsLogPath;
	
	/**
	* Path to trading bots logger - logs all trading bot actions
	**/
	private String backtestsLogPath;
	
	/**
	* Path to bot exceptions - logs all trading bot exceptions
	**/
	private String backtestsExceptionsLogPath;

	/**
	* Path to simulations logger - logs all bot simulation actions
	**/
	private String simulationsLogPath;
	
	/**
	* Path to simulation exceptions - logs all exceptions from simulations
	**/
	private String simulationExceptionsLogPath;
	
	/**
	* Path to bot threads - logs creation, deletion, adds and removal to all bot threads
	**/
	private String botThreadsLogPath;
	
	/**
	* Path to bot threads - logs creation, deletion, adds and removal to all bot threads
	**/
	private String automatedProcessesLogPath;
	
	/**
	* Key used to decrypt json sent from login
	**/
	private String masterKey;
	
	/**
	* Key used to access the nomics API
	**/
	private String nomicsApiKey;
	
	/**
	* How long does the log manager wait before writing queue of logs
	**/
	private int loggerQueueDelay;
	
	/**
	* The database to use for the platform (web app)
	**/
	private String platformDatabase;
	
	/**
	* User to use for querying
	**/
	private String platformDatabaseUser;
	
	/**
	* Password to access database
	**/
	private String platformDatabasePassword;
	
	/**
	* Mirror database that holds subset of nomics data
	**/
	private String nomicsMirrorDatabase;
	
	/**
	* User for nomics mirror
	**/
	private String nomicsMirrorDatabaseUser;
	
	/**
	* Password for nomics mirror
	**/
	private String nomicsMirrorDatabasePassword;
	
	/**
	 * Redirect URI for after login
	 */
	private String redirectAfterLogin;
	
	/**
	 * Redirect URI for after login
	 */
	private String redirectToLogin;
	
	/**
	 * Which device is being used to host?
	 */
	private String host;
	
	/**
	 * Return the system constants singleton
	 * @return
	 */
	public static Constants getConstants() {
		return constants;
	}

	/**
	* Singleton reference
	**/
	private static Constants constants;
	
	/**
	 * has instance been created?
	 */
	private static boolean configured = false;
	
	/**
	 * Loads all environment variables into singleton
	 */
	public Constants( ) {
		System.out.println( "Configuring constants..." );
		setup( );
	}
	
	/**
	 * Singleton access
	 * @return singleton
	 */
	public static Constants instance() {
		if( configured == false ) {
			configured = true;
			constants = new Constants( );
		}
		return constants;
	}
	
	/**
	* Grabs and sets all constants for platform
	**/
	private void setup(){
		
		Properties properties = new Properties( );
		File propertiesFile   = new File( CONFIG_PATH + CONFIG_FILE );
		
		if( propertiesFile.exists( ) ){
			System.out.println( "LOADING PLATFORM PROPERTIES FROM " + ( CONFIG_PATH + CONFIG_FILE ) );
			FileInputStream fileInputStream = null;
			try
			{
				fileInputStream = new FileInputStream( propertiesFile );
				properties.load( fileInputStream );
				fileInputStream.close( );
				loadConstants( properties );
			}
			catch( Exception e )
			{
				e.printStackTrace( ); 
			}
		}
		else{
			System.out.println( "PROPERTIES FILE DOES NOT EXIST, PLEASE CREATE PROPERTIES FILE AT LOCATION: " 
							    + CONFIG_PATH + CONFIG_FILE );
		}
		
	}
	
	/**
	* Internal method to load all constants
	**/
	private void loadConstants( Properties properties )
	{
		List< String > filepaths 			= new ArrayList< String >( );
		
		this.apiLogPath 						= properties.getProperty( "apiLogPath" );
		filepaths.add( apiLogPath );
		
		this.apiExceptionsLogPath 			= properties.getProperty( "apiExceptionsLogPath" );
		filepaths.add( apiExceptionsLogPath );
		
		this.botsLogPath 					= properties.getProperty( "botsLogPath" );
		filepaths.add( botsLogPath );
		
		this.botExceptionsLogPath 			= properties.getProperty( "botExceptionsLogPath" );
		filepaths.add( botExceptionsLogPath );
		
		this.backtestsLogPath 			    = properties.getProperty( "backtestsLogPath" );
		filepaths.add( backtestsLogPath );
		
		this.backtestsExceptionsLogPath 	    = properties.getProperty( "backtestsExceptionsLogPath" );
		filepaths.add( backtestsExceptionsLogPath );
		
		this.simulationsLogPath 				= properties.getProperty( "simulationsLogPath" );
		filepaths.add( simulationsLogPath );
		
		this.simulationExceptionsLogPath 	= properties.getProperty( "simulationExceptionsLogPath" );
		filepaths.add( simulationExceptionsLogPath );
		
		this.botThreadsLogPath 				= properties.getProperty( "botThreadsLogPath" );
		filepaths.add( botThreadsLogPath );
		
		this.automatedProcessesLogPath 	    = properties.getProperty( "automatedProcessesLogPath" );
		filepaths.add( automatedProcessesLogPath );
		
		this.masterKey						= properties.getProperty( "masterKey" );
	
		this.nomicsApiKey 					= properties.getProperty( "nomicsApiKey" );
		
		this.loggerQueueDelay				= Integer.parseInt( properties.getProperty( "loggerQueueDelay" ) );
		
		this.platformDatabase				= properties.getProperty( "platformDatabase" );
		
		this.platformDatabaseUser 			= properties.getProperty( "platformDatabaseUser" );
		
		this.platformDatabasePassword 		= properties.getProperty( "platformDatabasePassword" );
		
		this.nomicsMirrorDatabase			= properties.getProperty( "nomicsMirrorDatabase" );
		
		this.nomicsMirrorDatabaseUser 		= properties.getProperty( "nomicsMirrorDatabaseUser" );
		
		this.nomicsMirrorDatabasePassword 	= properties.getProperty( "nomicsMirrorDatabasePassword" );
		
		this.redirectAfterLogin          	= properties.getProperty( "redirectAfterLogin" );
		
		this.redirectToLogin 	          	= properties.getProperty( "redirectToLogin" );
		
		this.host	          				= properties.getProperty( "host" );
		
		createFilesIfNotExists( filepaths );
		
	}
	
	/**
	 * Internal method for generating new log file
	 */
	private void createFilesIfNotExists( List< String > filepaths ) {
		for( String filepath : filepaths ) {
			File file = new File( filepath );
			if ( file.exists() ) {
			   continue;
			}
			else {
				try {
					file.createNewFile( );
				} 
				catch (IOException e) {
					System.out.println( "Failed to create log files: " + e.getMessage( ) );
				}
			}
		}
		
	}
	
	/**
	 * Path to the API logger file
	 * @return String filepath
	 */
	public String getApiLogPath() {
		return apiLogPath;
	}

	/**
	 * Path to the API_EXPETIONS logger file
	 * @return String filepath
	 */
	public String getApiExceptionsLogPath() {
		return apiExceptionsLogPath;
	}

	/**
	 * Path to the BOTS logger file
	 * @return String filepath
	 */
	public String getBotsLogPath() {
		return botsLogPath;
	}

	/**
	 * Path to the BOTS_EXCEPTIONS logger file
	 * @return String filepath
	 */
	public String getBotExceptionsLogPath() {
		return botExceptionsLogPath;
	}
	
	/**
	 * Path to the BACKTESTS log file
	 * @return String filepath
	 */
	public String getBacktestsLogPath() {
		return backtestsLogPath;
	}

	/**
	 * Path to the BACKTESTS_EXCEPTIONS file
	 * @return String filepath
	 */
	public String getBacktestsExceptionsLogPath() {
		return backtestsExceptionsLogPath;
	}

	/**
	 * Path to the SIMULATIONS log path
	 * @return String filepath
	 */
	public String getSimulationsLogPath() {
		return simulationsLogPath;
	}

	/**
	 * Path to the SIMULATIONS EXCEPTIONS logger file
	 * @return String filepath
	 */
	public String getSimulationExceptionsLogPath() {
		return simulationExceptionsLogPath;
	}

	/**
	 * Path to the BOT THREADS logger file
	 * @return String filepath
	 */
	public String getBotThreadsLogPath() {
		return botThreadsLogPath;
	}

	/**
	 * Get the system level master key
	 * @return String 64 character hex string
	 */
	public String getMasterKey() {
		return masterKey;
	}

	/**
	 * Get the nomics api key, for hitting all nomics endpoints
	 * @return String filepath
	 */
	public String getNomicsApiKey() {
		return nomicsApiKey;
	}

	/**
	 * Get the set timer delay for running the logging queue
	 * @return int millisenconds value
	 */
	public int getLoggerQueueDelay() {
		return loggerQueueDelay;
	}

	/**
	 * Get the database connection string for the 
	 * main platform database
	 * @return Database SQL connection string
	 */
	public String getPlatformDatabase() {
		return platformDatabase;
	}

	/**
	 * Get the username for database connections for the 
	 * platform database
	 * @return String username
	 */
	public String getPlatformDatabaseUser() {
		return platformDatabaseUser;
	}

	/**
	 * Get the user's associated password for the platform
	 * database
	 * @return String password
	 */
	public String getPlatformDatabasePassword() {
		return platformDatabasePassword;
	}

	/**
	 * Get the sql database connection string for the nomics
	 * mirror database
	 * @return Database connection string
	 */
	public String getNomicsMirrorDatabase() {
		return nomicsMirrorDatabase;
	}

	/**
	 * Get the user for the nomics mirror DB
	 * @return String username
	 */
	public String getNomicsMirrorDatabaseUser() {
		return nomicsMirrorDatabaseUser;
	}

	/**
	 * Get the password for the nomics mirror db
	 * @return String password
	 */
	public String getNomicsMirrorDatabasePassword() {
		return nomicsMirrorDatabasePassword;
	}
	
	/**
	 * Grab the redirect URL sent with LoginAPI response
	 * @return String url format
	 */
	public String getRedirectAfterLogin() {
		return redirectAfterLogin;
	}
	
	/**
	 * Get the redirect to login string in the form x.login.html
	 * @return String url format
	 */
	public String getRedirectToLogin() {
		return redirectToLogin;
	}
	
	/**
	 * Path to the AUTOMATED_PROCESSES logger file
	 * @return String filepath
	 */
	public String getAutomatedProcessesLogPath( ) {
		return automatedProcessesLogPath;
	}
	
	/**
	 * Get the host within the set 'LOCAL', 'OVH'
	 * @return String value of host
	 */
	public String getHost() {
		return host;
	}


}
