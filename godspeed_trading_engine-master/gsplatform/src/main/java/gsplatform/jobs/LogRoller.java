package gsplatform.jobs;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * This class is an automated process that is only to be called via 
 * CronJobManager by providing driver with the following command
 * line parameters: -LogRoller true
 * The class performs one core task, rolling all files in the main
 * applogs folder to a timestamped folder with timestamped prefixes.
 * @author danielanderson
 *
 */
public class LogRoller{

	/**
	 * Internal reference to Application LOGGER
	 */
	private static GS_Logger LOGGER = GS_Logger.getLogger( );
	
	public LogRoller() {;}

	/**
	 * Obtains configured log files from GS_Logger and rolls them. This
	 * will generate new files prefixed by timestamp and place them in a directory
	 * with this timestamp.
	 */
	public void run( ) {
		try {
			this.rollLogs( GS_Logger.getConfiguredLogFiles( ) );
			LOGGER.writeLogMessage( "AUTOMATED-PROCESSES", "LogRoller", "Yesterdays logs moved to new DIR" );
		} catch (InterruptedException e) {
			e.printStackTrace();
			LOGGER.writeLogMessage( "AUTOMATED-PROCESSES", "LogRoller", "Error rolling logs with message: " + e.getMessage( )  );
		}
	}
	
	
	/**
	 * Iterates over all log files and rolls them, should only
	 * ever be called via 
	 * @param Map< String, String > identifier filepath pairs
	 * @throws InterruptedException 
	 */
	public void rollLogs( Map< String, String > configuredLogFiles ) throws InterruptedException 
	{
		Date now						     = new Date( );
		String pattern 					 = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat( pattern );
		String pathToDirectory 			 = GS_Logger.APPLOGS_PATH + simpleDateFormat.format( now );
		createDirectoryIfNotExists( pathToDirectory );
		Thread.sleep( 10000 );
		
	    Iterator it = configuredLogFiles.entrySet().iterator();
	    while ( it.hasNext( ) ) {
	        Map.Entry< String, String > pair = ( Map.Entry< String, String > )it.next( );
	        try {
				rollLog( pair.getValue( ), simpleDateFormat.format( now ), pathToDirectory );
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}
	
	/**
	 *  All this does is rename the old file to the new one prefixed with timestamp
	 * @param fileToRoll				The filepath of the file to roll
	 * @param date					String with current timestamp
	 * @param						Absolute path to directory to move file to 
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	private void rollLog( String fileToRoll, String dateString, String pathToDirectory ) throws IOException, 
																							  InterruptedException {
		
		// File (or directory) with old name
		File file = new File( fileToRoll );

		// File (or directory) with new name
		String newFilename = generateNewFilename( fileToRoll, dateString );
		File file2 = new File( newFilename );

		//If file already exists
		if ( file2.exists( ) )
		   throw new java.io.IOException( "file exists" );

		// Rename file (or directory)
		boolean success = file.renameTo( file2 );

		//Error
		if ( !success ) {
		   System.out.println( "ERROR rolling file" );
		}
		
		//Create the current log file
		else {
			moveFile( file2.getAbsolutePath( ), pathToDirectory + "/" + newFilename.split( "/" )[ 5 ] );
			file.createNewFile( );
		}
	}
	
	/**
	 * Move a file to the associated timestamped directory
	 * @param fileToMove
	 * @param directory
	 * @throws IOException
	 */
	private void moveFile( String fileToMove, String pathToDirectory ) throws IOException {
		File sourceFile = new File( fileToMove );
		Path sourcePath = sourceFile.toPath();
		File destFile = new File( pathToDirectory );
		Path destPath = destFile.toPath();
		Files.move( sourcePath, destPath, new CopyOption[]{} );
	}
	
	/**
	 * Checks that new directory to roll files exists, if
	 * not it makes it
	 * @param pathToDirectory
	 */
	private void createDirectoryIfNotExists( String pathToDirectory ) {
	    File directory = new File( pathToDirectory );
	    if (!directory.exists()){
	        directory.mkdir();
	    }
	}
	
	/**
	 * Returns absoulte path with correct filename preceeded by date string
	 * @param dateString
	 */
	private String generateNewFilename( String path, String dateString ) {
		StringBuilder stringBuilder = new StringBuilder( );
		String[ ] splitPath = path.split( "/" );
		stringBuilder.append( splitPath[ 0 ] );
		for( int i = 1; i < splitPath.length; i++ ) {
			if( i == splitPath.length - 1 ) {
				stringBuilder.append( "/" );
				stringBuilder.append( dateString );
				stringBuilder.append( "-" );
				stringBuilder.append( splitPath[ i ] );
			}
			else {
				stringBuilder.append( "/" );
				stringBuilder.append( splitPath[ i ] );
			}
		}
		
		return stringBuilder.toString( );
	}

}
