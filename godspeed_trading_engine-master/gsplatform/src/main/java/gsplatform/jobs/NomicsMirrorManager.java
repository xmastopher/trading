package gsplatform.jobs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import nomics.core.NomicsExchangeCandles;
import nomics.core.NomicsMarkets;

/**
 * Threaded class that runs LOGGER.logAllPending job every X milliseconds
 * to remove the load of logging from the main program
 * @author danielanderson
 *
 */
public class NomicsMirrorManager {
	
	/**
	 * Platform logger reference
	 */
	private static final GS_Logger LOGGER = GS_Logger.getLogger( );
	
   /**
    * Driver string for mysql
    */
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   
   /**
    * Connection string for general database
    */
   static final String DB_URL_GS = Constants.instance( ).getNomicsMirrorDatabase( );

   /**
    * Username for nomics db
    */
   static final String USER = Constants.instance( ).getNomicsMirrorDatabaseUser( );
   
   /**
    * Password for nomics db
    */
   static final String PASS = Constants.instance( ).getNomicsMirrorDatabasePassword( );

   /**
    * Main access method to run nomics mirror job, writes all nomics markets,
    * daily candles and exchange-market intersections
    */
   public void run( ) {
	   String host = Constants.instance( ).getHost( );
	   try {
			Connection connection = getConnection( );
			JSONArray markets = writeNomicsMarkets( connection, host.equalsIgnoreCase( "ovh" ) );
			writeNomicsMarketIntersections( connection, markets );
			LOGGER.writeLogMessage( "AUTOMATED-PROCESSES", "NomicsMirrorManager", "Successfully copied all nomics data" );
	   }
	   catch ( Exception e ){
			LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "NomicsMirrorManager", "failed writing nomics mirrors " + e.getMessage( ) );
	   }
   }
	
	/**
	 * Write all the nomics markets to local database
	 * @param connection		SQL connection object
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 */
	private JSONArray writeNomicsMarkets( Connection connection, Boolean isOVH ) throws JSONException, 
																					  IOException {
		
		LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "NomicsMirrorManager","writing all markets...");
		NomicsMarkets nomicsMarkets = new NomicsMarkets( ); 
		JSONArray jsonArray         =  new JSONArray( nomicsMarkets.getAllMarkets( Constants.instance( ).getNomicsApiKey( ) ) );
		NomicsExchangeCandles nomicsCandles = new NomicsExchangeCandles( );
		JSONArray filteredMarkets = new JSONArray( );
		
		boolean writeOne = true;
		for( int i = 0; i < jsonArray.length( ); i++ ) {
			
			try {
				
				JSONObject json      = jsonArray.getJSONObject( i );
				String sql 			 = "REPLACE INTO NomicsMarkets(exchange,market,base,quote) VALUES(?,?,?,?)";
				PreparedStatement ps  = connection.prepareStatement( sql );
				
				String candles = nomicsCandles.getExchangeCandles( Constants.instance( ).getNomicsApiKey( ), 
																  "1d", 
																  json.getString( "exchange"),
																  json.getString( "market" ) );
				
				//Do not store any candles that are no longer supported - actually delete them
				if( new JSONArray( candles ).length( ) == 0 ) {
					String sqlDelete 			 = "DELETE FROM NomicsMarkets WHERE exchange = ? AND market = ? AND base = ? AND quote = ?";
					PreparedStatement psDelete  = connection.prepareStatement( sqlDelete );
					psDelete.setString( 1, json.getString( "exchange") );
					psDelete.setString( 2, json.getString( "market" ) );
					psDelete.setString( 3, json.getString( "base") );
					psDelete.setString( 4, json.getString( "quote") );
					psDelete.executeUpdate( );
					LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "NomicsMirrorManager","market: " + json.getString( "market" ) + 
										   " is being deleted because it is no longer supported on exchange: " + json.getString( "exchange" ) );
					continue;
				}
				else {
					filteredMarkets.put( json );
				}
				
				ps.setString( 1, json.getString( "exchange") );
				ps.setString( 2, json.getString( "market" ) );
				ps.setString( 3, json.getString( "base") );
				ps.setString( 4, json.getString( "quote") );
				
				ps.execute( );
				
				if( isOVH || ( writeOne && json.getString( "exchange" ).equalsIgnoreCase( "GDAX" ) ) ) {
					writeOne = false;
					LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "NomicsMirrorManager","market: " + json.getString( "market" ) + 
							   " is being mirrored at exchange " + json.getString( "exchange" ) );
					writeNomicsDailyCandles( connection, json.getString( "exchange"), json.getString( "base"), json.getString( "quote"), candles );
				}
			}
			catch( Exception e ) {
				LOGGER.addMessageEntry("AUTOMATED-PROCESSES", "NomicsMirrorManager", "Failed writing a market to nomics mirror" );
				continue;
			}
			
		}
		
		return filteredMarkets;
	}
	
	/**
	 * Write all the market intersections.  Uses previously provided JSON array of markets by exchange
	 * and creates a mapping with all intersections.  The mapping is then used to write all intersections
	 * to our local NomicsMirror DB.
	 * @param connection			SQL connection object
	 * @param markets			List of all current supported markets
	 * @throws JSONException
	 * @throws IOException
	 */
	private void writeNomicsMarketIntersections( Connection connection, JSONArray markets ) throws JSONException, 
																								 IOException {
		
		LOGGER.writeLogMessage("AUTOMATED-PROCESSES", "NomicsMirrorManager","writing all market intersections...");
		Map< String, ArrayList< String > > lookup = new HashMap< String, ArrayList< String > >( );
		
		for( int i = 0; i < markets.length( ); i++ ) {
			
			JSONObject market = markets.getJSONObject( i );
			String key = market.getString( "base" ) + "-" + market.getString( "quote" );
			
			if( !lookup.containsKey( key ) ) {
				lookup.put( key, new ArrayList< String >( ) );
			}
			
			lookup.get( key ).add( market.toString( ) );
		}
		
	    Iterator it = lookup.entrySet().iterator();
	    
	    while ( it.hasNext( ) ) {
	        Map.Entry< String, ArrayList< String > > pair = ( Map.Entry< String, ArrayList< String > > )it.next( );
	        ArrayList< String > exchanges = pair.getValue( );
	        
	        for( int i = 0; i < exchanges.size( ); i++ ) {
	        	
	        		for( int j = i+1; j < exchanges.size( ); j++ ) {
		        	
	        			JSONObject marketOne = new JSONObject( exchanges.get( i ) );
	        			JSONObject marketTwo = new JSONObject( exchanges.get( j ) );
	        			
		        		String base  = pair.getKey( ).split("-")[0];
		        		String quote = pair.getKey( ).split("-")[1];
		        		String exOneMark = marketOne.getString( "market" );
		        		String exTwoMark = marketTwo.getString( "market" );
		        		String exOne = marketOne.getString( "exchange" );
		        		String exTwo = marketTwo.getString( "exchange" );
		        		
					try {

						String sql 			 = "REPLACE INTO NomicsMarketsIntersections(exchangeOne,exchangeTwo,exchangeOneMarket,exchangeTwoMarket,base,quote) VALUES(?,?,?,?,?,?)";
						PreparedStatement ps  = connection.prepareStatement( sql );
						
						ps.setString( 1, exOne );
						ps.setString( 2, exTwo );
						ps.setString( 3, exOneMark );
						ps.setString( 4, exTwoMark );
						ps.setString( 5, base );
						ps.setString( 6, quote );
						LOGGER.addMessageEntry( "AUTOMATED-PROCESSES", "NomicsMirrorManager", "Market intersection written: " + 
												exOne + "-" + exTwo + "-" +  base + "-" +  quote );
						ps.execute( );
					}
					catch( Exception e ) {
						LOGGER.addMessageEntry("AUTOMATED-PROCESSES", "NomicsMirrorManager", "Failed writing a market to nomics mirror" );
						continue;
					}

	        		}
	        }
	    }
		
	}

	/**
	 * Helper method to write a single row of daily candles
	 * @param connection
	 */
	private void writeNomicsDailyCandles( Connection connection, String exchange, String base, String quote, String candles ) {
			
			try {

				String sql 			 = "REPLACE INTO NomicsDailyCandles(exchange,base, quote,candles) VALUES(?,?,?,?)";
				PreparedStatement ps  = connection.prepareStatement( sql );
				
				ps.setString( 1, exchange );
				ps.setString( 2, base );
				ps.setString( 3, quote );
				ps.setString( 4, candles );
				ps.execute( );
			}
			catch( Exception e ) {
				LOGGER.addMessageEntry("AUTOMATED-PROCESSES", "NomicsMirrorManager", "Failed writing a market to nomics mirror" );
			}
		
	}
	
	/**
	 * Returns SQL connection created from given parameters
	 * @return	Connection object
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
   private Connection getConnection( ) throws ClassNotFoundException, 
   											 SQLException {
	   Class.forName( JDBC_DRIVER );

	   System.out.println( "Connecting to database..." );
	   
	   return DriverManager.getConnection( DB_URL_GS, USER, PASS );
   }
   
}
