package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.PortfolioSnapshotAPI;

/**
 * Servlet implementation class PortfolioSnapshotServlet
 */
@WebServlet(description = "Get a snapshot of a user's portfolio", urlPatterns = { "/portfoliosnapshotservlet" , "/portfoliosnapshot"})
public class PortfolioSnapshotServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PortfolioSnapshotServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter( );
		String params = request.getParameter( "data" );
		JSONObject errorJSON = new JSONObject();
		
		try 
		{
			PortfolioSnapshotAPI portfolioSnapshotAPI = new PortfolioSnapshotAPI( new JSONObject( params ) );
			JSONObject results                        = portfolioSnapshotAPI.createJSON( );
			writer.println( results.toString( ) );
			
		} 
		catch ( Exception e ) 
		{
			
			try {
				errorJSON.put( "success", false );
				errorJSON.put( "message", e.getMessage( ) );
				writer.println( errorJSON.toString( ) );
				e.printStackTrace();
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

}
