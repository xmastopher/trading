package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.ChangeLockAPI;
import gsplatform.servlet.database.AuthenticationManager;

/**
 * Servlet implementation class ChangeLockServlet
 */
@WebServlet(description = "Lock/Unlock bot", urlPatterns = { "/changelockservlet" , "/changelock" } )
public class ChangeLockServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeLockServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter( );
		String params = request.getParameter( "data" );
		JSONObject errorJSON = new JSONObject();
		
		try 
		{
			JSONObject verifiedParams     = AuthenticationManager.verifySession( new JSONObject( params ) );
			
			if( !verifiedParams.getBoolean( "success") ) 
			{
				writer.println( verifiedParams.toString( ) );
			}
			else
			{
				ChangeLockAPI changeLockAPI  			 = new ChangeLockAPI( verifiedParams );
				JSONObject results                        = changeLockAPI.createJSON( );
				writer.println( results.toString( ) );
			}
			
		} 
		catch ( Exception e ) 
		{
			
			try {
				errorJSON.put( "success", false );
				errorJSON.put( "message", e.getMessage( ) );
				writer.println( errorJSON.toString( ) );
				e.printStackTrace();
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
}
