package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.PortfolioAPI;

@WebServlet(description = "Grab a user's portfolio", urlPatterns = { "/portfolioservlet" , "/portfolio" } )
public class PortfolioServlet extends HttpServlet {
	
	private static final long serialVersionUID = 2L;
	public static final String HTML_START="<html><body>";
	public static final String HTML_END="</body></html>";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PortfolioServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		String jsonData    = request.getParameter("data");

		try {
			JSONObject params       = new JSONObject( jsonData );
			PortfolioAPI portfolioAPI = new PortfolioAPI( params );
			JSONObject results      = portfolioAPI.createJSON( );
			writer.println( results.toString( ) );
		} catch (JSONException e) {
			System.out.println( jsonData );
			System.out.println( e.getMessage( ) );
			throw new ServletException();
		} catch (Exception e) {
			throw new ServletException();
		}
	}
    
}
