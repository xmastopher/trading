package gsplatform.servlet;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import gsplatform.jobs.BotThreadRecovery;
import gsplatform.jobs.Constants;
import gsplatform.jobs.GS_Logger;
import gsplatform.services.BotThreadHandler;

/**
 * Describes all functionality that happens on a system start or a system shutdown
 * @author danielanderson
 *
 */
@WebListener
public class GS_ServletContextListener implements ServletContextListener {

	public static boolean DEBUG = false;
	
	/**
	 * Grabs global level constants and recovers an bots that
	 * may have been stopped
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		//Initialize constants
		Constants.instance( );
		
		//Start all bots that stopped
		new BotThreadRecovery( ).run( );
	}

	/**
	 * Ensures that all threads are stopped before terminating
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if( !DEBUG ) {
			BotThreadHandler.getSingletonInstance( ).stopAlBotThreads( );
		}
		GS_Logger.getLogger( ).stopLoggerThread( );
	}


}
