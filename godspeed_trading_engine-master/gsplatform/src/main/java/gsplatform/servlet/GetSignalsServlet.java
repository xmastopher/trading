package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.GetTradingSignalsAPI;

/**
 * Servlet implementation class ArbitrageServlet
 */
@WebServlet(description = "Grab's signals for user's dashboard", urlPatterns = { "/getsignalsservlet" , "/getsignals" } )
public class GetSignalsServlet extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter( );
		
		try 
		{
			GetTradingSignalsAPI getSignalsAPI  	 = new GetTradingSignalsAPI( new JSONObject("{}") );
			JSONObject results                    = getSignalsAPI.createJSON( );
			writer.println( results.toString( ) );
			
		} 
		catch ( Exception e ) 
		{
			JSONObject errorJson = new JSONObject( );
			try {
				errorJson.put( "success", false );
				errorJson.put( "message", e.getMessage( ) );
				writer.println( errorJson.toString( ) );
				e.printStackTrace();
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

}
