package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.StartStopBotAPI;
import gsplatform.servlet.database.AuthenticationManager;
import gsplatform.utilities.GS_Utils;

/**
 * Servlet implementation class SaveBotServlet
 */
@WebServlet(description = "Start and stop a bot", urlPatterns = { "/startstopbotservlet" , "/startstopbot" } )
public class StartStopBotServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter( );
		String params = GS_Utils.getJsonFromRequest( request );
		JSONObject errorJSON = new JSONObject();
		
		try 
		{
			JSONObject verifiedParams = AuthenticationManager.verifySession( new JSONObject( params ) );
			
			if( !verifiedParams.getBoolean( "success") ) 
			{
				writer.println( verifiedParams.toString( ) );
			}
			else
			{
				StartStopBotAPI StartStopBotAPI  			 = new StartStopBotAPI( verifiedParams );
				JSONObject results                			 = StartStopBotAPI.createJSON( );
				writer.println( results.toString( ) );
			}
			
		} 
		catch ( Exception e ) 
		{
			
			try {
				errorJSON.put( "success", false );
				errorJSON.put( "message", e.getMessage( ) );
				writer.println( errorJSON.toString( ) );
				e.printStackTrace();
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

}
