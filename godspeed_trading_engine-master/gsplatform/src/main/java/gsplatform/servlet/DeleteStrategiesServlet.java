package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.DeleteStrategyAPI;
import gsplatform.servlet.database.AuthenticationManager;
import gsplatform.utilities.GS_Utils;

/**
 * Servlet implementation class DeleteBotServlet
 */
@WebServlet(description = "delete a user's strategy", urlPatterns = { "/deletestrategy" , "/deletestrategyservlet" } )
public class DeleteStrategiesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteStrategiesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter( );
		String params = GS_Utils.getJsonFromRequest( request );
		JSONObject errorJSON = new JSONObject();
		
		try 
		{
			
			JSONObject verifiedParams     = AuthenticationManager.verifySession( new JSONObject( params ) );
			
			if( !verifiedParams.getBoolean( "success") ) 
			{
				writer.println( verifiedParams.toString( ) );
			}
			else
			{
				DeleteStrategyAPI deleteStrategyAPI  = new DeleteStrategyAPI( verifiedParams );
				JSONObject results         			= deleteStrategyAPI.createJSON( );
				writer.println( results.toString( ) );
			}
			
		} 
		catch ( Exception e ) 
		{
			
			try 
			{
				errorJSON.put( "success", false );
				errorJSON.put( "message", e.getMessage( ) );
				writer.println( errorJSON.toString( ) );
				e.printStackTrace();
			} 
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}

		}
	}

}
