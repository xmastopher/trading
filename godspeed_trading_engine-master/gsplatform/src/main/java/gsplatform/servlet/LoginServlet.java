package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.LoginAPI;
import gsplatform.utilities.GS_Utils;

@WebServlet(description = "Log a user in", urlPatterns = { "/loginservlet" , "/login" } )
public class LoginServlet extends HttpServlet 
{
	/**
	 * Amount of time a session can be active
	 */
	public static final int SESSION_MINUTES = 120;
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try 
		{
			String json              = GS_Utils.getJsonFromRequest( request );
			JSONObject requestJson   = new JSONObject( json );
			LoginAPI loginAPI        = new LoginAPI( requestJson );
			JSONObject loginResponse = loginAPI.createJSON( );
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			
			//Write the message
			PrintWriter out = response.getWriter();
			out.print( loginResponse.toString( ) );
			out.flush();
			
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
