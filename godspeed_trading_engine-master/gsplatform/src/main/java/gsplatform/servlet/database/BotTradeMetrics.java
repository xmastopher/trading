package gsplatform.servlet.database;

import java.math.BigDecimal;

public class BotTradeMetrics
{
	
	protected String username;
	
	protected int botId;
	
	protected String botName;
	
	protected String exchange;
	
	protected String market;
	
	protected BigDecimal totalROI;

	protected BigDecimal networth;
	
	protected int numTrades;
	
	protected int numWins;
	
	protected int numLosses;
	
	protected BigDecimal bestTrade;
	
	public BotTradeMetrics(int botId, String username, String botName, String exchange, String market) {
		this(botId, username, botName, exchange, market, BigDecimal.ZERO, BigDecimal.ONE, 0,0,0,BigDecimal.ZERO );
	}
	
	public BotTradeMetrics( int botId, String username, String botName, 
							String exchange, String market, 
							BigDecimal totalROI, BigDecimal networth, int numTrades,
							int numLosses, int numWins, BigDecimal bestTrade )
	{
		this.botId       = botId;
		this.botName     = botName;
		this.username    = username;
		this.totalROI    = totalROI;
		this.networth    = networth;
		this.numTrades   = numTrades;
		this.exchange    = exchange;
		this.market      = market;
		this.numWins     = numWins;
		this.numLosses   = numLosses;
		this.bestTrade   = bestTrade;
	}

	public BigDecimal getBestTrade() {
		return bestTrade;
	}

	public void setTotalROI(BigDecimal totalROI) {
		this.totalROI = totalROI;
	}

	public void setBestTrade(BigDecimal bestTrade) {
		this.bestTrade = bestTrade;
	}

	public String getUsername() {
		return username;
	}

	public int getBotId() {
		return botId;
	}

	public String getBotName() {
		return botName;
	}

	public String getExchange() {
		return exchange;
	}

	public String getMarket() {
		return market;
	}

	public BigDecimal getTotalROI() {
		return totalROI;
	}

	public BigDecimal getNetworth() {
		return networth;
	}

	public int getNumTrades() {
		return numTrades;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public void settotalROI(BigDecimal totalROI) {
		this.totalROI = totalROI;
	}

	public void setNetworth(BigDecimal networth) {
		this.networth = networth;
	}

	public void setNumTrades(int numTrades) {
		this.numTrades = numTrades;
	}
	
	
	public int getNumWins() {
		return numWins;
	}

	public int getNumLosses() {
		return numLosses;
	}

	public void setNumWins(int numWins) {
		this.numWins = numWins;
	}

	public void setNumLosses(int numLosses) {
		this.numLosses = numLosses;
	}
	
}
