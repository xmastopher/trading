package gsplatform.servlet.database;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONException;

import gsplatform.jobs.Constants;
import nomics.core.NomicsMarkets;


public class ExchangeMarketIntersectionsDAO extends UserDAOObject
{
	
	/**
	 * Empty
	 */
	public ExchangeMarketIntersectionsDAO( ) 
	{}
	
	/**
	 * DAO function to update a user's tickers for their dashboard
	 * @throws SQLException 
	 */
	public String getMarketIntersections( String exchangeOne, String exchangeTwo ) throws SQLException
	{		
	    
		String query         = "SELECT markets FROM ExchangeMarketIntersections WHERE ( exchangeOne = ? AND exchangeTwo = ? ) OR ( exchangeOne = ? AND exchangeTwo = ? )";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, exchangeOne );
		ps.setString( 2, exchangeTwo );
		ps.setString( 3, exchangeTwo );
		ps.setString( 4, exchangeOne );
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) ) { 
			return resultSet.getString( "markets" );
		}
		
		return "{}";
		
	}
	
	
	
	/**
	 * Method used to update database reflection with nomics
	 * @throws IOException 
	 * @throws JSONException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void updateDatabase( ) throws JSONException, IOException, ClassNotFoundException, SQLException {
		
		String[] exchanges = new String[] {"bitfinex", "bittrex", "hitbtc", "kraken", "poloniex", "binance", "bitflyer", "bithumb", "gdax", "gemini"};
		String nomicsApiKey = Constants.instance().getNomicsApiKey( );
		NomicsMarkets nomicsMarkets = new NomicsMarkets( );
		
		PreparedStatement ps = null;
		this.openConnection( );

		for( int e = 0; e < exchanges.length; e++ ) {
			
			for( int ee = e+1; ee < exchanges.length; ee++ ) {
				String[] exchangePair = new String[] {exchanges[e], exchanges[ee]};
				String intersections  =  nomicsMarkets.getMarketIntersections( exchangePair, nomicsApiKey ).get( 0 );
				if( intersections.equals("[]")) continue;
				
				String query = "INSERT INTO ExchangeMarketIntersections VALUES(?,?,?)";
				ps = this.connection.prepareStatement( query );
				
				ps.setString( 1, exchanges[e] );
				ps.setString( 2, exchanges[ee] );
				ps.setString( 3, intersections );
				ps.executeUpdate( );
				
				System.out.println( getMarketIntersections( exchanges[e], exchanges[ee] ) );
			}
			
		}
		
		this.closeConnection( );
		
	}
	
	public static void main( String[] args ) {
		
		ExchangeMarketIntersectionsDAO emid = new ExchangeMarketIntersectionsDAO( );
		
		try {
			emid.updateDatabase( );
		}
		catch( Exception e ) {
			e.printStackTrace( );
		}
	}
	
}