package gsplatform.servlet.database;

import gsplatform.servlet.database.UserLoginPackage;

/**
 * Abstraction for user signup information
 * @author danielanderson
 *
 */
public class UserSignupPackage extends UserLoginPackage {

	/**
	 * Message associated with signup
	 */
	private String message;
	
	/**
	 * Random generated hex used to verify account email
	 */
	private String verificationToken;
	
	/**
	 * Explicit constructor with success flag
	 * @param isSuccessful
	 */
	UserSignupPackage( Boolean isSuccessful )
	{
		super( isSuccessful );
	}
	
	/**
	 * Explicit constructor with success flag and message
	 * @param isSuccessful
	 * @param message
	 */
	UserSignupPackage( Boolean isSuccessful, String message )
	{
		super( isSuccessful );
		this.message = message;
	}
	
	/**
	 * Explicit constructor with all fields
	 * @param username
	 * @param email
	 * @param isSuccessful
	 * @param message
	 */
	UserSignupPackage( String username, String email, Boolean isSuccessful, String message )
	{
		this(isSuccessful, message);
	}
	
	/**
	 * Grab the message from the sign up attempt
	 */
	@Override
	public String getMessage( )
	{
		return this.message;
	}
	
	/**
	 * Call in DAO to set verification token before saving signup record
	 * @param verificationToken
	 */
	public void setVerificationToken( String verificationToken )
	{
		this.verificationToken = verificationToken;
	}
	
	public Boolean getSuccess( )
	{
		return this.isSuccessful;
	}

}
