package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import gsplatform.servlet.database.SessionTokensDAO;
import gsplatform.utilities.GS_Utils;

/**
 * Data access object for user signup
 * @author danielanderson
 *
 */
public class SessionTokensDAO extends UserDAOObject {
	
	/**
	 * Constant for length of verificatino string
	 */
	private static final int SESSION_EXPIRATION_TIME_MINUTES = 120;
	
	/**
	 * Length of session token which agrees with DB spec
	 */
	private static final int SESSION_TOKEN_LENGTH = 64;
	
	/**
	 * No username
	 */
	public SessionTokensDAO(  )
	{;}
	
	/**
	 * Explicit construct with just email - should only be used when checking verification token
	 * @param email
	 */
	public SessionTokensDAO( String username )
	{
		super( username );
	}
	
	/**
	 * Returns true or false based on the state of the user's session
	 * @param token
	 * @return
	 * @throws SQLException 
	 */
	public boolean sessionIsActive( String token, Timestamp thisApiTimestamp ) throws SQLException
	{
		String query         = "SELECT lastApiCall FROM SessionTokens WHERE token = ?";
		PreparedStatement ps = null;
		
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, token );
		
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) ) {
			return tokenIsGood( resultSet.getTimestamp( "lastApiCall" ), thisApiTimestamp);
		}
		
		return false;
	}
	
	/**
	 * Returns true if a session token has expired, false o/w (helper function)
	 * @param first		The timestamp of the token in theDB
	 * @param second		The timestamp of the now API call
	 * @return
	 */
	private boolean tokenIsGood( Timestamp first, Timestamp second )
	{
		return GS_Utils.compareTwoTimeStamps( first, second ) <= SESSION_EXPIRATION_TIME_MINUTES;
	}
	
	/**
	 * 
	 * @param token
	 * @return
	 * @throws SQLException 
	 */
	public String getUserFromSessionToken( String token ) throws SQLException
	{
		String query         = "SELECT username FROM SessionTokens WHERE token = ?";
		PreparedStatement ps = null;
		
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, token );
		
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) ) {
			return resultSet.getString( "username" );
		}
		
		return "FAIL";
	}
	
	/**
	 * Call to reset or activate a session - usually on login, or when prompting user to reactivate a session...
	 * @param username
	 * @return The session token or "" if failed
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public String resetSession( String username ) throws SQLException, ClassNotFoundException {
		
		if( username.contains( "@") )
		{
			UserLoginDAO userLoginDAO = new UserLoginDAO( );
			userLoginDAO.openConnection( );
			username = userLoginDAO.getUsernameFromEmail( username );
			userLoginDAO.closeConnection( );
		}
		
		String query         = "UPDATE SessionTokens SET token = ?, lastApiCall = ? WHERE username = ?";
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		
		String sessionToken = GS_Utils.generateRandomHexString( SESSION_TOKEN_LENGTH );
		
		ps.setString( 1, sessionToken );
		ps.setTimestamp( 2, new Timestamp( System.currentTimeMillis( ) ) );
		ps.setString( 3, username );
		
		//Return token on success, session o/w
		return ps.executeUpdate( ) > 0 ? sessionToken : "";
	}
	
	/**
	 * Call to update token last use timestamp
	 */
	public String updateToken( String username, String token ) throws SQLException {
		
		String query         = "UPDATE SessionTokens SET lastApiCall = ? WHERE token = ? AND username = ?";
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		
		ps.setTimestamp( 1, new Timestamp( System.currentTimeMillis( ) ) );
		ps.setString( 2, token );
		ps.setString( 3, username );
		
		//Return token on success, session o/w
		return ps.executeUpdate( ) > 0 ? token : "";
	}
}
