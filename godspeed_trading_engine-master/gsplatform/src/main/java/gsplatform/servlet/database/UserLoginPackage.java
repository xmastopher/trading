package gsplatform.servlet.database;

/**
 * Encapsulates information about a login attempt
 * @author danielanderson
 *
 */
public class UserLoginPackage 
{
	/**
	 * Username for login
	 */
	protected String username;
	
	/**
	 * Email for login
	 */
	protected String email;
	
	/**
	 * Was the logn successful
	 */
	protected Boolean isSuccessful;
	
	/**
	 * Message associated with login attempt
	 */
	protected String message;
	
	/**
	 * Explicit constructor that takes in the success flag
	 * @param isSuccessful
	 */
	public UserLoginPackage( Boolean isSuccessful )
	{
		this.isSuccessful = isSuccessful;
	}
	
	/**
	 * Explicit constructor that takes in all fields
	 * @param username
	 * @param email
	 * @param isSuccessful
	 * @param message
	 */
	public UserLoginPackage( String username, String email, Boolean isSuccessful, String message )
	{
		this.username     = username;
		this.email        = email;
		this.isSuccessful = isSuccessful;
		this.message      = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	
	public String getMessage( )
	{
		return this.message;
	}
	
	public void setMessage( String message )
	{
		this.message = message;
	}
}
