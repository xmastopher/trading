package gsplatform.servlet.database;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PortfolioInfoDAO extends UserDAOObject {

	/**
	 * Explicit constructor to initialize with username
	 * @param username
	 */
	public PortfolioInfoDAO( String username )
	{
		super( username );
	}
	
	public PortfolioInfo getPortfolioInfo( ) throws SQLException
	{
		PreparedStatement ps = null;
		
		String sql = "SELECT * FROM PortfolioInfo WHERE username = ?";
		ps = this.connection.prepareStatement( sql );
		
		ps.setString( 1, this.username );
		
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return new PortfolioInfo( resultSet.getString( "fiatType" ), resultSet.getBigDecimal( "amountInvested" ) );
		}
		
		return null;
	}
	/**
	 * Call to initialize portfolio info for a user
	 * @throws SQLException 
	 */
	public void initPortfolioInfo( String fiatType, BigDecimal amountInvested ) throws SQLException
	{
		PreparedStatement ps = null;
		
		String sql = "INSERT INTO PortfolioInfo VALUES(?,?,?)";
		ps = this.connection.prepareStatement( sql );
		
		ps.setString( 1, this.username );
		ps.setString( 2, fiatType );
		ps.setBigDecimal( 3, amountInvested );
		
		ps.executeUpdate( );
	}
	
	
	/**
	 * Call to update amount invested for a particular user
	 * @throws SQLException 
	 */
	public void makeDeposit( BigDecimal amountInvested ) throws SQLException
	{
		PreparedStatement ps = null;
		
		//Grab portfolio info and update amount invested
		PortfolioInfo portfolioInfo = getPortfolioInfo( );
		BigDecimal currentInvested  = portfolioInfo.getAmountInvested( ).add( amountInvested );
		
		//Create update statemenet - set the new investment amount
		String sql = "UPDATE PortfolioInfo SET amountInvested=? WHERE username = ?";
		ps = this.connection.prepareStatement( sql );
		ps.setBigDecimal( 1, currentInvested );
		ps.setString( 2, this.username );
		
		//Execute statement
		ps.executeUpdate( );
	}
	
	/**
	 * Will convert current amount invested to a new fiat type and update
	 * the portfolio info for user
	 */
	public void changeFiatType( String newFiatType )
	{
		//Will have to convert fiat type
		//This will require an external call to an API
	}
}
