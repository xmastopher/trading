package gsplatform.servlet.database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.jobs.Constants;

public class AuthenticationManager {
	
	
	public static JSONObject verifySession( JSONObject request ) throws JSONException, SQLException, ClassNotFoundException {
	
		//If there is a null token, this use has no session at all
		try {
			request.getString( "token" );
		}
		catch(Exception e)
		{
			request.put("redirect", Constants.instance().getRedirectToLogin( ) );
			request.put( "success", false );
			request.put( "message", "Session inactive" );
			return request;
		}
		
		//Get NOW
		Calendar calendar = Calendar.getInstance( );
	    Timestamp now = new java.sql.Timestamp( calendar.getTime( ).getTime( ) );
	    
		SessionTokensDAO sessionTokensDAO = new SessionTokensDAO(  );
		sessionTokensDAO.openConnection( );
		String token = request.getString( "token" );
		
		String username = sessionTokensDAO.getUserFromSessionToken( token );
		request.put( "username", username );
		
		//Session is active
		if( sessionTokensDAO.sessionIsActive( request.getString( "token" ), now ) ) 
		{
			sessionTokensDAO.updateToken( username, token );
			request.put( "success", true );
			sessionTokensDAO.closeConnection( );
			return request;
		}
		
		//Session is inactive
		else 
		{
			request.put("redirect", Constants.instance().getRedirectToLogin( ) );
			request.put( "success", false );
			request.put( "message", "Session inactive" );
		}
		
		sessionTokensDAO.closeConnection( );
		return request;
	}

}
