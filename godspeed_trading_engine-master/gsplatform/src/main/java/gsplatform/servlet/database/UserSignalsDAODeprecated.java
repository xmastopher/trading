package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class UserSignalsDAODeprecated extends UserDAOObject
{
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 */
	public UserSignalsDAODeprecated( String username ) 
	{
		super( username );
	}
	
	
	/**
	* DAO function to add an additional signal to a user's dashboard
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	*
	**/
	public void addSignal( String exchange, String base, String counter, int inHouseSignalId, String kline ) throws ClassNotFoundException, SQLException
	{
		String query         = "INSERT INTO UserSignals( username, botId ) VALUES( ?, ? )";
		PreparedStatement ps = null;
		
		SignalFeedLookupDAO signalFeedLookupDAO = new SignalFeedLookupDAO( this.username );
		signalFeedLookupDAO.openConnection( );
		ResultSet resultSet = signalFeedLookupDAO.getSignalId( exchange, base, counter, inHouseSignalId, kline );

		//Signal already exists, grab its id
		int id = 0;
		if( resultSet.next( ) )
		{
			id = resultSet.getInt( "id" );
		}
		
		//Signal does not exist - create it then grab id.
		else
		{
			signalFeedLookupDAO.addNewSignal( exchange, base, counter, inHouseSignalId, kline );
			resultSet = signalFeedLookupDAO.getSignalId( exchange, base, counter, inHouseSignalId, kline );
			signalFeedLookupDAO.closeConnection( );
			resultSet.next( );
			id = resultSet.getInt( "id" );
		}
		
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, id );
		ps.executeUpdate( );
	}
	
	/**
	* DAO function to add an additional signal to a user's dashboard
	* @return false if signal does not exist or failure
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	*
	**/
	public boolean removeSignal( String exchange, String base, String counter, int inHouseSignalId, String kline ) throws ClassNotFoundException, SQLException
	{
		String query         = "DELETE FROM UserSignals WHERE signalId = ? AND username = ?";
		PreparedStatement ps = null;
		ps                   = this.connection.prepareStatement( query );
		
		SignalFeedLookupDAO signalFeedLookupDAO = new SignalFeedLookupDAO( this.username );
		signalFeedLookupDAO.openConnection( );
		ResultSet resultSet = signalFeedLookupDAO.getSignalId( exchange, base, counter, inHouseSignalId, kline );
		
		//Signal already exists, grab its id
		int id = 0;
		
		if( resultSet.next( ) )
		{
			id = resultSet.getInt( "id" );
			return false;
		}
		
		ps.setInt( 1, id );
		ps.setString( 2, this.username );
		return true;
	}
	
}