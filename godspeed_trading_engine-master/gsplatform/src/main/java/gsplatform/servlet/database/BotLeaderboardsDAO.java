package gsplatform.servlet.database;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BotLeaderboardsDAO extends UserDAOObject
{	
	/**
	 * URL for the all time leaderboard
	 */
	private static final String BOT_LEADERBOARDS_ALLTIME_TABLE  = "BotLeaderboards";
	
	/**
	 * URL for the daily leaderboard
	 */
	private static final String BOT_LEADERBOARDS_DAILY_TABLE    = "BotLeaderboardsDaily";
	
	/**
	 * URL for the weekly leaderboard
	 */
	private static final String BOT_LEADERBOARDS_WEEKLY_TABLE   = "BotLeaderboardsWeekly";
	
	/**
	 * Internal member for the bot id
	 */
	protected int botId;
	
	
	public BotLeaderboardsDAO( )
	{;}
	
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 * @param username
	 * @param stategyId
	 */
	public BotLeaderboardsDAO( String username ) 
	{
		super( username );
	}
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 * @param username
	 * @param botId
	 * @param stategyId
	 */
	public BotLeaderboardsDAO( String username, int botId ) 
	{
		super( username );
		this.botId = botId;
	}
	
	public void setUsernameAndId( String username, int botId )
	{
		this.username = username;
		this.botId    = botId;
	}
	
	public void setBotId( int botId ) {
		this.botId = botId;
	}

	/**
	 * Called after clearing daily bot performances to reset all bot
	 * metrics
	 */
	public boolean initAllRunningBots( String table ) throws SQLException {
		
		String query = "SELECT ID,username,botName,exchange,marketBase,marketQuote FROM UserBots WHERE running = true";
		PreparedStatement ps = this.connection.prepareStatement( query );

		ResultSet resultSet = ps.executeQuery( );

		boolean success = true;
		while( resultSet.next( ) ){
			
			this.username   = resultSet.getString( "username" );
			this.botId      = resultSet.getInt( "ID" );
			String botName  = resultSet.getString( "botName" );
			String exchange = resultSet.getString( "exchange" );
			String market   = resultSet.getString( "marketBase" ) + "-" + resultSet.getString( "marketQuote" );
			success         = success && _initBotTrades( botName, exchange, market, table );
		}	

		return success;
	}	

	/**
	 * Wrapper function to call all internal inits
	 */
	public boolean initBotTrades( String botName, String exchange, String market ) throws SQLException {
		return _initBotTrades( botName, exchange, market, "BotLeaderboards" ) &&
		       _initBotTrades( botName, exchange, market, "BotLeaderboardsWeekly" ) &&
		       _initBotTrades( botName, exchange, market, "BotLeaderboardsDaily" );
	}

	/**
	 * Call to setup DB with bot trades
	 * @return
	 * @throws SQLException 
	 */
	public boolean _initBotTrades( String botName, String exchange, String market, String table ) throws SQLException {
		
		String query = "INSERT INTO " + table + " VALUES( ?, ?, ?, ?, ?, 0, 1, 0, 0, 0, 0 )";
				
		//Write the log to the DB
		PreparedStatement ps = null;
		ps                   = this.connection.prepareStatement( query );
		ps.setInt( 1, this.botId );
		ps.setString( 2, this.username );
		ps.setString( 3, botName );
		ps.setString( 4, exchange );
		ps.setString( 5, market );

		return ps.executeUpdate( ) > 0;
	}
	
	/**
	 * Updates bot performance by passing the roi on the most previous trade - inserts into all three databases
	 * @param roiOnTrade
	 * @return
	 * @throws SQLException
	 */
	public boolean addTrade( BigDecimal roiOnTrade ) throws SQLException {
		return _addTrade( roiOnTrade, BOT_LEADERBOARDS_ALLTIME_TABLE ) &&
			   _addTrade( roiOnTrade, BOT_LEADERBOARDS_WEEKLY_TABLE ) &&
			   _addTrade( roiOnTrade, BOT_LEADERBOARDS_DAILY_TABLE );
	}
	
	/**
	 * Internal function which adds the specific table to add a trade to
	 * @param roiOnTrade
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	private boolean _addTrade( BigDecimal roiOnTrade, String table ) throws SQLException {
		
		//Updates average ROI, networth and bestTrade
		String query = "";
		
		//User won
		if( roiOnTrade.doubleValue( ) > 0.0 ) {
			query = "UPDATE " + table + " set totalROI = totalROI + ?, networth = networth + ( networth * ? ), " +
				    "numTrades = numTrades + 1, bestTrade = GREATEST( ?, bestTrade ), numWins = numWins + 1 WHERE username = ? AND botId = ?";
		}
		else if( roiOnTrade.doubleValue( ) <= 0.0 ) {
			query = "UPDATE " + table + " set totalROI = totalROI + ?, networth = networth + ( networth * ? ), " +
				    "numTrades = numTrades + 1, bestTrade = GREATEST( ?, bestTrade ), numLosses = numLosses + 1 WHERE username = ? AND botId = ?";
		}
		
		//Write the log to the DB
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		ps.setBigDecimal( 1, roiOnTrade );
		ps.setBigDecimal( 2, roiOnTrade );
		ps.setBigDecimal( 3, roiOnTrade );
		ps.setString( 4, this.username );
		ps.setInt( 5, this.botId );
		
		return ps.executeUpdate( ) > 0;
	}

	
	
	/**
	 * Grab a user's highest rank - returns all time rank
	 * @return
	 * @throws SQLException
	 */
	public String[] getRank( ) throws SQLException {
		
		String[] notFound = new String[]{"0","",""};
		
		List< BotTradeMetrics > leaderboard = getTopBotTradeMetrics( -1, "BotLeaderboards" );
		
		//Return first instance of username rank
		int rank = 1; for( BotTradeMetrics btm : leaderboard ) {
			
			if( btm.getUsername( ).equalsIgnoreCase( this.username ) ) {
				return new String[]{ String.valueOf( rank ), btm.getBotName( ), String.valueOf( btm.getBotId( ) ) };
			}
			else {
				rank++;
			}
		}
		
		return notFound;
	}
	
	/**
	 * Grab all trade metrics for a user
	 * @return
	 * @throws SQLException
	 */
	public List< BotTradeMetrics > getBotTradeMetrics( final int limit ) throws SQLException{
		
		String query = "";
		
		if( limit <= 0 ) {
			query = "select * from BotLeaderboards WHERE username = ? ORDER BY networth DESC";
		}
		else {
			query = "select * from BotLeaderboards WHERE username = ? ORDER BY networth DESC LIMIT ?";			
		}

		//Write the log to the DB
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		
		if( limit > 0 ) {
			ps.setInt( 2, limit );
		}
		
		
		ResultSet resultSet = ps.executeQuery( );
		List< BotTradeMetrics > botTradeMetrics = new ArrayList< BotTradeMetrics >( );
		
		while( resultSet.next( ) )
		{
			int botId 			  = resultSet.getInt( "botId" );
			String username 		  = resultSet.getString( "username" );
			String botName        = resultSet.getString( "botName" );
			String exchange       = resultSet.getString( "exchange" );
			String market         = resultSet.getString( "market" ); 
			BigDecimal totalROI = resultSet.getBigDecimal( "totalROI" );
			BigDecimal networth   = resultSet.getBigDecimal( "networth" );
			int numTrades 	      = resultSet.getInt( "numTrades" );
			int numWins 	          = resultSet.getInt( "numWins" );
			int numLosses 	      = resultSet.getInt( "numLosses" );
			BigDecimal bestTrade  = resultSet.getBigDecimal( "bestTrade" );
			
			botTradeMetrics.add( new BotTradeMetrics( botId, username, botName, exchange, market, 
							     	   totalROI, networth, numTrades, numLosses, numWins, bestTrade ) );
		}
		
		return botTradeMetrics;
	}
	
	/**
	 * Get the top trade metrics over all users
	 * @param n
	 * @return
	 * @throws SQLException
	 */
	public List< BotTradeMetrics > getTopBotTradeMetrics( int n, String leaderboard ) throws SQLException{
		
		String query = ""; 

		//Write the log to the DB
		PreparedStatement ps = null;
		
		//Fetch last N records
		if( n > 0 ) {
			query = "SELECT * from " + leaderboard + " WHERE numTrades > 0 ORDER BY networth DESC LIMIT ?";
			ps              = this.connection.prepareStatement( query );
			ps.setInt( 1, n );
		}
		
		//Fetch all records
		else {
			query = "SELECT * from " + leaderboard + " WHERE numTrades > 0 ORDER BY networth DESC";
			ps              = this.connection.prepareStatement( query );
		}
		
		ResultSet resultSet = ps.executeQuery( );
		List< BotTradeMetrics > botTradeMetrics = new ArrayList< BotTradeMetrics >( );

		while( resultSet.next( ) )
		{
			int botId 			  = resultSet.getInt( "botId" );
			String username 		  = resultSet.getString( "username" );
			String botName        = resultSet.getString( "botName" );
			String exchange       = resultSet.getString( "exchange" );
			String market         = resultSet.getString( "market" ); 
			BigDecimal totalROI   = resultSet.getBigDecimal( "totalROI" );
			BigDecimal networth   = resultSet.getBigDecimal( "networth" );
			int numTrades 	      = resultSet.getInt( "numTrades" );
			int numWins 	          = resultSet.getInt( "numWins" );
			int numLosses 	      = resultSet.getInt( "numLosses" );
			BigDecimal bestTrade  = resultSet.getBigDecimal( "bestTrade" );
			
			botTradeMetrics.add( new BotTradeMetrics( botId, username, botName, exchange, market, 
							     totalROI, networth, numTrades, numLosses, numWins, bestTrade ) );
		}
		
		return botTradeMetrics;
	}
	
	/**
	 * Get the top trade metrics over all users
	 * @param n
	 * @return
	 * @throws SQLException
	 */
	public List< BotTradeMetrics > getUserHistoricalTradeMetrics( int botId ) throws SQLException{
		
		String query = "SELECT * From BotLeaderboardsHistorical WHERE username = ? AND botId = ?"; 

		//Write the log to the DB
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, botId );
		
		
		ResultSet resultSet = ps.executeQuery( );
		List< BotTradeMetrics > botTradeMetrics = new ArrayList< BotTradeMetrics >( );

		while( resultSet.next( ) )
		{
			String username 		  = resultSet.getString( "username" );
			String botName        = resultSet.getString( "botName" );
			String exchange       = resultSet.getString( "exchange" );
			String market         = resultSet.getString( "market" ); 
			BigDecimal totalROI   = resultSet.getBigDecimal( "totalROI" );
			BigDecimal networth   = resultSet.getBigDecimal( "networth" );
			int numTrades 	      = resultSet.getInt( "numTrades" );
			int numWins 	          = resultSet.getInt( "numWins" );
			int numLosses 	      = resultSet.getInt( "numLosses" );
			BigDecimal bestTrade  = resultSet.getBigDecimal( "bestTrade" );
			
			botTradeMetrics.add( new BotTradeMetrics( botId, username, botName, exchange, market, 
							     totalROI, networth, numTrades, numLosses, numWins, bestTrade ) );
		}
		
		return botTradeMetrics;
	}
	
	/**
	 * Returns the top performing bots over user's X is following
	 * @param following
	 * @return
	 * @throws SQLException
	 */
	public List< BotTradeMetrics > getTopBotTradeMetricsFollowing( List< String > following ) throws SQLException{
		
		String query = generateFollowingQuery( following.size( ) );

		//Write the log to the DB
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		
		//Set all plaaceholders
		for( int i = 1; i < following.size( ); i++ ) {
			ps.setString( i, following.get( i-1 ) );
		}
		
		
		ResultSet resultSet = ps.executeQuery( );
		List< BotTradeMetrics > botTradeMetrics = new ArrayList< BotTradeMetrics >( );

		while( resultSet.next( ) )
		{
			int botId 			  = resultSet.getInt( "botId" );
			String username 		  = resultSet.getString( "username" );
			String botName        = resultSet.getString( "botName" );
			String exchange       = resultSet.getString( "exchange" );
			String market         = resultSet.getString( "market" ); 
			BigDecimal totalROI   = resultSet.getBigDecimal( "totalROI" );
			BigDecimal networth   = resultSet.getBigDecimal( "networth" );
			int numTrades 	      = resultSet.getInt( "numTrades" );
			int numWins 	          = resultSet.getInt( "numWins" );
			int numLosses 	      = resultSet.getInt( "numLosses" );
			BigDecimal bestTrade  = resultSet.getBigDecimal( "bestTrade" );
			
			botTradeMetrics.add( new BotTradeMetrics( botId, username, botName, exchange, market, 
							     totalROI, networth, numTrades, numWins, numLosses, bestTrade ) );
		}
		
		return botTradeMetrics;
	}
	
	/**
	 * Internal method to generate long query to query for only users X is following
	 * @param numFollowing
	 * @return
	 */
	private String generateFollowingQuery( int numFollowing ) {
	 
		StringBuilder stringBuilder = new StringBuilder( );
		
		stringBuilder.append( " SELECT * FROM BotLeaderboards WHERE username IN (" );
		
		for( int i = 0; i < numFollowing; i++ ) {
			
			stringBuilder.append( "'" );
			stringBuilder.append( "?" );
			stringBuilder.append( "'" );
			if( i != numFollowing - 1 ) {
				stringBuilder.append( "," );
			}
		}
	
		stringBuilder.append( "'" );
		stringBuilder.append( " DESC" );
		return stringBuilder.toString( );
	}
	
	/**
	 * Delete all records from table
	 * @param numFollowing
	 * @return
	 * @throws SQLException 
	 */
	public boolean clearTable( String table ) throws SQLException {
			
		String query = "DELETE FROM " + table ;
		
		//Write the log to the DB
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		
		
		return ps.executeUpdate( ) > 0;
	}
	
	/**
	 * On delete this should be calld with current timestamp to store all historical daily
	 * trade metrics to our historical DB
	 * @param timestamp
	 * @return
	 * @throws SQLException
	 */
	public boolean copyDailyLeaderboards( Timestamp timestamp ) throws SQLException {
			
		String query = "SELECT * FROM BotLeaderboardsDaily";
		PreparedStatement ps = null;
		ps = this.connection.prepareStatement( query );
		ResultSet resultSet = ps.executeQuery( );
		boolean success = true;
		while( resultSet.next( ) ) {
			int botId 			  = resultSet.getInt( "botId" );
			String username 		  = resultSet.getString( "username" );
			String botName        = resultSet.getString( "botName" );
			String exchange       = resultSet.getString( "exchange" );
			String market         = resultSet.getString( "market" ); 
			BigDecimal totalROI   = resultSet.getBigDecimal( "totalROI" );
			BigDecimal networth   = resultSet.getBigDecimal( "networth" );
			int numTrades 	      = resultSet.getInt( "numTrades" );
			BigDecimal bestTrade  = resultSet.getBigDecimal( "bestTrade" );
			int numWins 	          = resultSet.getInt( "numWins" );
			int numLosses 	      = resultSet.getInt( "numLosses" );
			
			String copyQuery = "INSERT INTO BotLeaderboardsHistorical VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			
			PreparedStatement psCopy = null;
			psCopy = this.connection.prepareStatement( copyQuery );
			psCopy.setTimestamp( 1, timestamp );
			psCopy.setInt( 2, botId );
			psCopy.setString( 3, username );
			psCopy.setString( 4, botName );
			psCopy.setString( 5, exchange );
			psCopy.setString( 6, market );
			psCopy.setBigDecimal( 7, totalROI );
			psCopy.setBigDecimal( 8, networth );
			psCopy.setInt( 9, numTrades );
			psCopy.setBigDecimal( 10, bestTrade );
			psCopy.setInt( 11, numWins );
			psCopy.setInt( 12, numLosses );
			
			success = success && ( psCopy.executeUpdate( ) > 0 );
		}
		
		return success;
	}
	
	/**
	 * Removes all bot records for a given bot in all leaderboards
	 * @return
	 * @throws SQLException 
	 */
	public boolean deleteAllBotRecords( ) throws SQLException {
		return _deleteAllBotRecords( "BotLeaderboards" ) &&
			   _deleteAllBotRecords( "BotLeaderboardsDaily" ) &&
			   _deleteAllBotRecords( "BotLeaderboardsWeekly" );
	}
	
	/**
	 * Internal method that wipes a certain leaderboard
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	private boolean _deleteAllBotRecords( String table ) throws SQLException {
		String query = "DELETE FROM " + table + " WHERE botID = ?";
		PreparedStatement ps = connection.prepareStatement( query );
		ps.setInt( 1, this.botId );
		return ps.executeUpdate( ) >= 1;
	}
	
	
}
