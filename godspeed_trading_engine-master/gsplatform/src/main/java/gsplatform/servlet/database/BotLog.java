package gsplatform.servlet.database;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class BotLog {


	protected int botId;
	
	protected String username;
	
	protected String action;
	
	protected BigDecimal networth;
	
	protected BigDecimal quoteOwned;
	
	protected BigDecimal asssetsOwned;
	
	protected BigDecimal lastPrice;
	
	protected String signalStates;
	
	protected Timestamp dated;
	
	
	public BotLog(int botId, String username, String action, BigDecimal networth, BigDecimal quoteOwned,
			BigDecimal asssetsOwned, BigDecimal lastPrice, String signalStates, Timestamp dated) {
		super();
		this.botId = botId;
		this.username = username;
		this.action = action;
		this.networth = networth;
		this.quoteOwned = quoteOwned;
		this.asssetsOwned = asssetsOwned;
		this.lastPrice = lastPrice;
		this.signalStates = signalStates;
		this.dated = dated;
	}


	public int getBotId() {
		return botId;
	}


	public void setBotId(int botId) {
		this.botId = botId;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public BigDecimal getNetworth() {
		return networth;
	}


	public void setNetworth(BigDecimal networth) {
		this.networth = networth;
	}


	public BigDecimal getQuoteOwned() {
		return quoteOwned;
	}


	public void setQuoteOwned(BigDecimal quoteOwned) {
		this.quoteOwned = quoteOwned;
	}


	public BigDecimal getAsssetsOwned() {
		return asssetsOwned;
	}


	public void setAsssetsOwned(BigDecimal asssetsOwned) {
		this.asssetsOwned = asssetsOwned;
	}


	public BigDecimal getLastPrice() {
		return lastPrice;
	}


	public void setLastPrice(BigDecimal lastPrice) {
		this.lastPrice = lastPrice;
	}


	public String getSignalStates() {
		return signalStates;
	}


	public void setSignalStates(String signalStates) {
		this.signalStates = signalStates;
	}


	public Timestamp getDated() {
		return dated;
	}


	public void setDated(Timestamp dated) {
		this.dated = dated;
	}
	
	
}
