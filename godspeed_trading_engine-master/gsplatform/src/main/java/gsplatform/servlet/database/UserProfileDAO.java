package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserProfileDAO extends UserDAOObject
{
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 */
	public UserProfileDAO( String username ) 
	{
		super( username );
	}
	
	/**
	 * DAO function - get list of user's followers
	 * @throws SQLException 
	 */
	public List< String > getFollowers( ) throws SQLException
	{		
	    
		String query         = "SELECT follower from UserFollowing WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		List< String > followers = new ArrayList< String >( );
		
		while( resultSet.next( ) )
		{
			followers.add( resultSet.getString( "follower" ) );
		}
		
		return followers;
		
	}
	
	/**
	 * Returns credibility score from given username
	 * @return
	 * @throws SQLException
	 */
	public int getUserCredibility( ) throws SQLException
	{
		String query = "SELECT credibility from UserCredibility WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getInt( "credibility" );
		}
		
		return 0;
	}
	
	/**
	 * Returns email from given username
	 * @return
	 * @throws SQLException
	 */
	public String getEmail( ) throws SQLException
	{
		String query = "SELECT email from UserLogin WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getString( "email" );
		}
		
		return "error";
	}
	
	/**
	 * Returns true if account is verified, false otherwise
	 * @return
	 * @throws SQLException
	 */
	public boolean isVerified( ) throws SQLException
	{
		String query = "SELECT verified from UserSignup WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getBoolean( "verified" );
		}
		
		return false;
	}
	
	/**
	 * Returns email from given username
	 * @return
	 * @throws SQLException
	 */
	public String getUserBio( ) throws SQLException
	{
		String query = "SELECT bio from UserBio WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getString( "bio" );
		}
		
		return "error";
	}
	
}