package gsplatform.servlet.database;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;
import nomics.core.NomicsAggregatedCandles;
import nomics.core.NomicsExchangeCandles;

public class HistoricalDataDAO 
{
	/**
	 * The base currency
	 */
	private String baseCurrency;
	
	/**
	 * The counter currency
	 */
	private String counterCurrency;
	
	/**
	 * All data fields by row
	 */
	private ResultSet resultSet;
	
	/**
	 * Internal class to integrate nomics data
	 * @author danielanderson
	 *
	 */
	private class NomicsResultSet
	{
		/**
		 * List if nomics data is chosen
		 */
		private List< TradingParameters > nomicsSet;
		
		private int index;
		
		/**
		 * Constructor for nomics result set
		 */
		public NomicsResultSet( ) 
		{
			this.index     = -1;
			this.nomicsSet = new ArrayList< TradingParameters >( );
		}
		
		/**
		 * Public method to append TradingParameters object
		 * @param tradingParameters
		 */
		public void addTradingParameters( TradingParameters tradingParameters )
		{
			this.nomicsSet.add( tradingParameters );
		}
		
		/**
		 * Grabs the next set of trading params while there is one
		 * @return
		 */
		public TradingParameters next( )
		{
			if( index+1 == this.nomicsSet.size( ) )
			{
				return null;
			}
			
			this.index++;
			return this.nomicsSet.get( this.index );
		}
		
		
	}

	/**
	 * Nomics Result set instance for using API
	 */
	private NomicsResultSet nomicsResultSet;
	
	/**
	 * API key if using nomics data
	 */
	private String apiKey;
	
	/**
	 * Will convert everything to USD
	 */
	private boolean useUSD;
	
	public HistoricalDataDAO( )
	{ 
		this.nomicsResultSet = new NomicsResultSet( );
	}
  	
	/**
	 * Constructor for using nomics dataset
	 */
	public HistoricalDataDAO( String apiKey, Boolean useUSD )
	{
		this.apiKey    		 = apiKey;
		this.nomicsResultSet = new NomicsResultSet( );
		this.useUSD = useUSD;
	}
  	
  	/**
  	 * Additional method for building data from Nomics set
  	 * @param interval
  	 * @param exchange
  	 * @param symbol
  	 * @throws IOException
  	 * @throws JSONException
  	 * @throws ParseException 
  	 */
  	public void makeNomicsData( String interval, String exchange, String symbol ) throws IOException, JSONException, ParseException
  	{
  		NomicsExchangeCandles nomicsExchangeCandles     = new NomicsExchangeCandles( );
  		
  		JSONArray candles      = new JSONArray( nomicsExchangeCandles.getExchangeCandles( this.apiKey, interval, exchange, symbol ) );
  		JSONArray candlesQuote = new JSONArray( ); 
  		
  		int stoppingPoint = candles.length( ); 
  		
  		for( int i = 0; i < stoppingPoint; i++ )
  		{
  			//{"volume":"4844.59542000","high":"380.70000000","low":"373.00000000","close":"380.00000000","open":"375.87000000","timestamp":"2018-03-30T03:00:00Z"}
  			TradingParameters tradingParameters = new TradingParameters( );
  			JSONObject candle                   = candles.getJSONObject( i );
  			
  			//Grab the USD set for price conversation
  			if( i == 0 && this.useUSD )
  			{
  				String timestamp = candle.getString( "timestamp" );
  		  		NomicsAggregatedCandles nomicsAggregatedCandles = new NomicsAggregatedCandles( );
  				candlesQuote = new JSONArray( nomicsAggregatedCandles.getCandlesFromTimestamp( this.apiKey, 
  																							 interval, 
  											 												 this.counterCurrency,
  											 												 timestamp )
  											);  
  				stoppingPoint = candlesQuote.length( );
  			}
  			
  			//If using USD, do a manual conversion here
  			if ( this.useUSD )
  			{
  				JSONObject aggragatedCandle = candlesQuote.getJSONObject( i );
  				
  				String volume = String.valueOf( new Double( candle.getString( "volume" ) )  * new Double( aggragatedCandle.getString( "volume" ) ) );
  				String high   = String.valueOf( new Double( candle.getString( "high" ) )    * new Double( aggragatedCandle.getString( "high" ) ) );
  				String low    = String.valueOf( new Double( candle.getString( "low" ) )     * new Double( aggragatedCandle.getString( "low" ) ) );
  				String close  = String.valueOf( new Double( candle.getString( "close" ) )   * new Double( aggragatedCandle.getString( "close" ) ) );
  				String open   = String.valueOf( new Double( candle.getString( "open" ) )    * new Double( aggragatedCandle.getString( "open" ) ) );
	  			
  				tradingParameters.updateValue( "volume", volume );
	  			tradingParameters.updateValue( "high", high );
	  			tradingParameters.updateValue( "low", low );
	  			tradingParameters.updateValue( "close", close );
	  			tradingParameters.updateValue( "open", open );
	  			tradingParameters.updateValue( "timestamp", candle.getString( "timestamp" ) );
	  			tradingParameters.updateValue( "last_price", close );	
  			}
  			else
  			{
	  			tradingParameters.updateValue( "volume", candle.getString( "volume" ) );
	  			tradingParameters.updateValue( "high", candle.getString( "high" ) );
	  			tradingParameters.updateValue( "low", candle.getString( "low" ) );
	  			tradingParameters.updateValue( "close", candle.getString( "close" ) );
	  			tradingParameters.updateValue( "open", candle.getString( "open" ) );
	  			tradingParameters.updateValue( "timestamp", candle.getString( "timestamp" ) );
	  			tradingParameters.updateValue( "last_price", candle.getString( "close" ) );	
  			}
  			
  			this.nomicsResultSet.addTradingParameters( tradingParameters );
  		}
  	}
  	
  	/**
  	 * Create nomics results set from internal DB - mirror
  	 * @param interval
  	 * @param exchange
  	 * @param base
  	 * @param quote
  	 * @throws SQLException 
  	 * @throws JSONException 
  	 * @throws ClassNotFoundException 
  	 */
  	public void makeNomicsDataMirror( String interval, String exchange, String base, String quote ) throws JSONException, SQLException, ClassNotFoundException {
  		
  		NomicsMirrorDAO nomicsMirror = new NomicsMirrorDAO( );
  		nomicsMirror.openConnection();
  		JSONArray priceData = new JSONArray( nomicsMirror.getDailyCandles( exchange, base, quote ) );
  		
  		for( int i = 0; i < priceData.length( ); i++ ) {
  			JSONObject candle = priceData.getJSONObject( i );
  			TradingParameters tradingParameters = new TradingParameters( );
  			tradingParameters.updateValue( "volume", candle.getString( "volume" ) );
  			tradingParameters.updateValue( "high", candle.getString( "high" ) );
  			tradingParameters.updateValue( "low", candle.getString( "low" ) );
  			tradingParameters.updateValue( "close", candle.getString( "close" ) );
  			tradingParameters.updateValue( "open", candle.getString( "open" ) );
  			tradingParameters.updateValue( "timestamp", candle.getString( "timestamp" ) );
  			tradingParameters.updateValue( "last_price", candle.getString( "close" ) );	
  			this.nomicsResultSet.addTradingParameters( tradingParameters );
  		}
  		nomicsMirror.closeConnection();
  	}
	
	/**
	 * Grabs the next column in the result set
	 * @return TradingParameter object holding all fields for current iteration
	 * @throws SQLException
	 */
	public TradingParameters doIteration( ) throws SQLException
	{
		TradingParameters tradingParameters = new TradingParameters( );

		if( this.resultSet != null && this.resultSet.next( ) )
		{
			
			String openTime  				= this.resultSet.getString( "open_time" );
			tradingParameters.updateValue("open_time", openTime.toString( ) );
			
			String closeTime 				= this.resultSet.getString( "close_time" );
			tradingParameters.updateValue("close_time", closeTime.toString( ) );
			
			Double open      				= this.resultSet.getDouble( "open" );
			tradingParameters.updateValue("open", open.toString( ) );
			
			Double high     					= this.resultSet.getDouble( "high" );
			tradingParameters.updateValue("high", high.toString( ) );
			
			Double low      					= this.resultSet.getDouble( "low" );
			tradingParameters.updateValue("low", low.toString( ) );
			
			Double close      				= this.resultSet.getDouble( "close" );
			tradingParameters.updateValue("close", close.toString( ) );
			
			Double volume      				= this.resultSet.getDouble( "volume" );
			tradingParameters.updateValue("volume", volume.toString( ) );
			
			Double quoteAssetVolume  		= this.resultSet.getDouble( "quote_asset_volume" );
			tradingParameters.updateValue("quote_asset_volume", quoteAssetVolume.toString( ) );
			
			Integer numTrades      			= this.resultSet.getInt( "num_of_trades" );
			tradingParameters.updateValue("num_of_trades", numTrades.toString( ) );
			
			Double takerBuyBaseAssetVolume   = this.resultSet.getDouble( "taker_buy_base_asset_volume" );
			tradingParameters.updateValue("taker_buy_base_asset_volume", takerBuyBaseAssetVolume.toString( ) );
			
			Double takerBuyQuoteAssetVolume  = this.resultSet.getDouble( "taker_buy_quote_asset_volume" );
			tradingParameters.updateValue("date", takerBuyQuoteAssetVolume.toString( ) );
			
			tradingParameters.updateValue( "last_price", close.toString( ) );
			
		}
		else if( this.nomicsResultSet != null  )
		{
			return this.nomicsResultSet.next( );
		}
		else
		{
			return null;
		}
		
		return tradingParameters;
	}
	
	public void setCounter( String counter )
	{
		this.counterCurrency = counter;
	}
}
