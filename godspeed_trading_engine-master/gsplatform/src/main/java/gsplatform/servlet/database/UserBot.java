package gsplatform.servlet.database;

public class UserBot {

	/**
	 * ID for the bot
	 */
	protected int botId;
	
	/**
	 * The strategy associated with the bot
	 */
	protected int strategyId;
	
	/**
	 * User associated with the bot
	 */
	protected String username;
	
	/**
	 * The name of the bot
	 */
	protected String botName;

	/**
	 * Is the bot locked?
	 */
	protected Boolean locked;
	
	/**
	 * Is this bot currently runing?
	 */
	protected Boolean running;
	
	/**
	 * is the bot public - will it be on leaderboards or no
	 */
	protected Boolean isPublic;
	
	/**
	 * The exchange the bot is on
	 */
	protected String exchange;
	
	/**
	 * The base of the market trading on
	 */
	protected String marketBase;
	
	/**
	 * The quote of the market traded on
	 */
	protected String marketQuote;
	
	/**
	 * The interval the bot is trading on 
	 */
	protected String interval;
	
	/**
	 * How much of the user's stack does he/she trade
	 */
	protected double tradingLimit;
	
	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public int getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Boolean getRunning() {
		return running;
	}

	public void setRunning(Boolean running) {
		this.running = running;
	}

	public double getTradingLimit() {
		return tradingLimit;
	}

	public void setTradingLimit(double tradingLimit) {
		this.tradingLimit = tradingLimit;
	}
	
	public void setIsPublic( Boolean isPublic )
	{
		this.isPublic = isPublic;
	}
	
	public Boolean getIsPublic( )
	{
		return this.isPublic;
	}
	
	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getMarketBase() {
		return marketBase;
	}

	public void setMarketBase(String marketBase) {
		this.marketBase = marketBase;
	}

	public String getMarketQuote() {
		return marketQuote;
	}

	public void setMarketQuote(String marketQuote) {
		this.marketQuote = marketQuote;
	}

	/**
	 * Explicit constructor to define a UserBot object
	 */
	public UserBot( int botId, int strategyId, String username, String botName, String exchange, String marketBase, String marketQuote, Boolean locked, Boolean running, Boolean isPublic, double tradingLimit, String interval )
	{
		this.botId 		 = botId;
		this.strategyId	 = strategyId;
		this.username	 = username;
		this.botName		 = botName;
		this.locked		 = locked;
		this.running      = running;
		this.isPublic     = isPublic;
		this.tradingLimit = tradingLimit;
		this.exchange     = exchange;
		this.marketBase   = marketBase;
		this.marketQuote  = marketQuote;
		this.interval     = interval;
	}

	public String getInterval( ) {
		return interval;
	}

	public void setInterval (String interval ) {
		this.interval = interval;
	}
	
	
}
