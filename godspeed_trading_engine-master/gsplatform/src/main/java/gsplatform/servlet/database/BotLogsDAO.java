package gsplatform.servlet.database;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BotLogsDAO extends UserDAOObject
{
	
	/**
	 * Internal member for the bot id
	 */
	protected int botId;
	
	public BotLogsDAO( )
	{;}
	
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 * @param username
	 * @param stategyId
	 */
	public BotLogsDAO( String username ) 
	{
		super( username );
	}
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 * @param username
	 * @param botId
	 * @param stategyId
	 */
	public BotLogsDAO( String username, int botId ) 
	{
		super( username );
		this.botId = botId;
	}
	
	public void setUsernameAndId( String username, int botId )
	{
		this.username = username;
		this.botId    = botId;
	}
	
	public void setBotId( int botId ) {
		this.botId = botId;
	}
	
	/**
	 * Public method for writing a trading bots action/history to the database.
	 * We can use this if a user wants to export their trading bot history
	 * @throws SQLException 
	 */
	public void writeLog( String action, BigDecimal networth, BigDecimal quotesOwned, BigDecimal assetsOwned, BigDecimal lastPrice, String signalStates, Timestamp timestamp ) throws SQLException
	{		
	    
		String query         = "INSERT INTO BotLogs VALUES(?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = null;

		//Write the log to the DB
		ps              = this.connection.prepareStatement( query );

		ps.setInt( 1, this.botId );
		ps.setString( 2, this.username );
		ps.setString( 3, action );
		ps.setBigDecimal( 4, networth );
		ps.setBigDecimal( 5, quotesOwned );
		ps.setBigDecimal( 6, assetsOwned );
		ps.setBigDecimal( 7, lastPrice );
		ps.setString( 8, signalStates );
		ps.setTimestamp( 9, timestamp );
		ps.executeUpdate( );
		
	}
	
	/**
	 * Returns a list of BotLog objects which contains all bot logs ascending by timestamp
	 * @return
	 * @throws SQLException
	 */
	public List< BotLog > getLogsFromBot( ) throws SQLException
	{
	    List< BotLog > botLogs = new ArrayList< BotLog >( );
	    
		String query         = "SELECT * FROM BotLogs WHERE username = ? and botId = ? ORDER BY dated ASC";
		PreparedStatement ps = null;

		//Write the log to the DB
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, this.botId );
		
		
		ResultSet resultSet = ps.executeQuery( );
		
		/**
		 * 	public BotLog(int botId, String username, String action, BigDecimal networth, BigDecimal quoteOwned,
			BigDecimal asssetsOwned, BigDecimal lastPrice, String signalStates, Timestamp dated) {
		 */
		while( resultSet.next( ) )
		{
			BotLog botLog = new BotLog( this.botId, this.username, resultSet.getString( "action" ), 
										resultSet.getBigDecimal( "networth"), resultSet.getBigDecimal( "quoteOwned"), resultSet.getBigDecimal( "assetsOwned" ),
										resultSet.getBigDecimal( "lastPrice" ), resultSet.getString( "signalStates"), resultSet.getTimestamp( "dated" ) );
			
			botLogs.add( botLog );
		}
		
		return botLogs;
	}
	
	/**
	 * Returns the bot logs only for buy and sell signals, pass in a limit to limit how many signals are grabbed
	 * @return
	 * @throws SQLException
	 */
	public List< BotLog > getLastNSignals( int limit ) throws SQLException
	{
	    List< BotLog > botLogs = new ArrayList< BotLog >( );
	    
		String query         = "SELECT * FROM BotLogs WHERE username = ? and ( action = 'SELL' or action = 'BUY' ) ORDER BY dated DESC LIMIT ?";
		PreparedStatement ps = null;

		//Write the log to the DB
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, limit );
		
		
		ResultSet resultSet = ps.executeQuery( );
		
		/**
		 * 	public BotLog(int botId, String username, String action, BigDecimal networth, BigDecimal quoteOwned,
			BigDecimal asssetsOwned, BigDecimal lastPrice, String signalStates, Timestamp dated) {
		 */
		while( resultSet.next( ) )
		{
			BotLog botLog = new BotLog( resultSet.getInt( "botId" ), this.username, resultSet.getString( "action" ), 
										resultSet.getBigDecimal( "networth"), resultSet.getBigDecimal( "quoteOwned"), resultSet.getBigDecimal( "assetsOwned" ),
										resultSet.getBigDecimal( "lastPrice" ), resultSet.getString( "signalStates"), resultSet.getTimestamp( "dated" ) );
			
			botLogs.add( botLog );
		}
		
		return botLogs;
	}
	
	/**
	 * Returns the bot logs only for buy and sell signals, pass in a limit to limit how many signals are grabbed
	 * @return
	 * @throws SQLException
	 */
	public List< BotLog > getLastNSignalsById( ) throws SQLException
	{
	    List< BotLog > botLogs = new ArrayList< BotLog >( );
	    
		String query         = "SELECT * FROM BotLogs WHERE username = ? AND botId = ? AND ( action = 'SELL' or action = 'BUY' ) ORDER BY dated ASC";
		PreparedStatement ps = null;

		//Write the log to the DB
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, this.botId );
		
		
		ResultSet resultSet = ps.executeQuery( );
		
		/**
		 * 	public BotLog(int botId, String username, String action, BigDecimal networth, BigDecimal quoteOwned,
			BigDecimal asssetsOwned, BigDecimal lastPrice, String signalStates, Timestamp dated) {
		 */
		while( resultSet.next( ) )
		{
			BotLog botLog = new BotLog( resultSet.getInt( "botId" ), this.username, resultSet.getString( "action" ), 
										resultSet.getBigDecimal( "networth"), resultSet.getBigDecimal( "quoteOwned"), resultSet.getBigDecimal( "assetsOwned" ),
										resultSet.getBigDecimal( "lastPrice" ), resultSet.getString( "signalStates"), resultSet.getTimestamp( "dated" ) );
			
			botLogs.add( botLog );
		}
		
		return botLogs;
	}
	
	/**
	 * Used to grab a bot's most recent action
	 * @return	null if no actions were made
	 * @throws SQLException 	
	 */
	public BotLog getLastBotTradeActionLog( ) throws SQLException {
		String query         = "SELECT * FROM BotLogs WHERE username = ? and botId = ? and ( action = 'SELL' or action = 'BUY' ) ORDER BY dated DESC";
		PreparedStatement ps = null;
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, this.botId );
		ResultSet resultSet = ps.executeQuery( ); 
		if( resultSet.next( ) ) {
			return new BotLog( this.botId, this.username, resultSet.getString( "action" ), 
					resultSet.getBigDecimal( "networth"), resultSet.getBigDecimal( "quoteOwned"), resultSet.getBigDecimal( "assetsOwned" ),
					resultSet.getBigDecimal( "lastPrice" ), resultSet.getString( "signalStates"), resultSet.getTimestamp( "dated" ) );
		}
		return null;
	}
	
	/**
	 * Delete all logs for a certain bot
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	public boolean removeAllBotLogs( ) throws SQLException {
		String query = "DELETE FROM BotLogs WHERE username = ? and botId = ?";
		PreparedStatement ps = connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, this.botId );
		return ps.executeUpdate( ) >= 1;
	}
	
}
