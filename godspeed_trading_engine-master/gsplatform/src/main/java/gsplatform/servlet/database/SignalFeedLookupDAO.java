package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SignalFeedLookupDAO extends UserDAOObject
{	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 */
	public SignalFeedLookupDAO(  String username ) 
	{
		super( username );
	}
	
	/**
	 * DAO function to update a user's tickers for their dashboard
	 * @throws SQLException 
	 */
	public ResultSet getSignalId( String exchange, String base, String counter, int inHouseSignalId, String kline ) throws SQLException
	{		
	    
		String query         = "SELECT id from SignalFeedLookup WHERE exchange = ? and baseCurrency = ? and counterCurrency = ? and inHouseSignal = ? and candle = ?";
		PreparedStatement ps = null;
		ps = this.connection.prepareStatement( query );
    
	    	ps.setString( 1, base );
	    	ps.setString( 1, counter );
	    	ps.setString( 1, exchange );
	    	ps.setInt( 1, inHouseSignalId );
	    	ps.setString( 1, kline );
    	
	    	return ps.executeQuery( );
		
	}
	
	/**
	** DAO function to add new signal - should only be used if signal does not already exist
	 * @throws SQLException 
	**/
	public void addNewSignal(  String exchange, String base, String counter, int inHouseSignalId, String kline ) throws SQLException
	{

		String query         = "INSERT INTO SignalFeedLookup( baseCurrency, counterCurrency, exchange, inHouseSignal, candle ) VALUES( ?, ?, ?, ?, ? )";
		PreparedStatement ps = null;
		ps = this.connection.prepareStatement( query );
		
		ps.setString( 1, base );
		ps.setString( 1, counter );
		ps.setString( 1, exchange );
		ps.setInt( 1, inHouseSignalId );
		ps.setString( 1, kline );
    	
		ps.executeUpdate( );
	}
	
}