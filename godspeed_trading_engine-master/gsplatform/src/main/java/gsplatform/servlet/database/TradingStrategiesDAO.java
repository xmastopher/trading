package gsplatform.servlet.database;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Data Access Object class for encapsulated User Trading Strategy data access
 * @author danielanderson
 *
 */
public class TradingStrategiesDAO extends UserDAOObject
{
	
	/**
	 * Explicit constructor to initialize with username
	 * @param username
	 */
	public TradingStrategiesDAO( String username )
	{
		super( username );
	}
	
	/**
	 * Public method to grab integer value of max ID for provided strategy and user
	 * @param strategy
	 * @return	and int for the max ID of this strategy
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public int getMaxStrategyID( ) throws ClassNotFoundException, SQLException
	{
		String query = "SELECT MAX( ID ) AS maxIndex from TradingStrategies  WHERE username = ?";
		
		PreparedStatement preparedStatement = null;
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getInt( "maxIndex" );
		}

		return 0;
	}
	
	/**
	 * Public method to save a strategy to user's profile in database
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * 
	 */
	public void saveStrategy( String strategyName, String buySignals, String sellSignals, String targets, BigDecimal stopLoss ) throws ClassNotFoundException, SQLException
	{
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO TradingStrategies (username,strategyName,buySignals,sellSignals,targets,stopLoss) " + 
					   "VALUES( ?, ?, ?, ?, ?, ? )";
		
		preparedStatement = this.connection.prepareStatement( query );
	
		this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setString( 2, strategyName );
		preparedStatement.setString( 3, buySignals );
		preparedStatement.setString( 4, sellSignals );
		preparedStatement.setString( 5, targets );
		preparedStatement.setBigDecimal( 6, stopLoss );
		preparedStatement.executeUpdate( );
	}
	
	/**
	 * Public method for grabbing strategies from DB
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public UserStrategiesPackage getUserStrategies( ) throws ClassNotFoundException, SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM TradingStrategies WHERE username = ?";
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		UserStrategiesPackage userStrategiesPackage = new UserStrategiesPackage(  );
		
		while( resultSet.next( ) )
		{
			String strategy		= resultSet.getString( "strategyName" );
			String buySignals   = resultSet.getString( "buySignals" );
			String sellSignals  = resultSet.getString( "sellSignals" );
			String targets      = resultSet.getString( "targets" );
			Integer id			= resultSet.getInt( "ID" );
			BigDecimal stopLoss = resultSet.getBigDecimal( "stopLoss" ).setScale( 2, BigDecimal.ROUND_DOWN );
			userStrategiesPackage.addFields( strategy, buySignals, sellSignals, targets, stopLoss, id );
		}
		
		return userStrategiesPackage;
	}

	
	/**
	 * Public method for grabbing strategies from DB
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public UserStrategiesPackage getUserStrategyById( int id ) throws ClassNotFoundException, SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM TradingStrategies WHERE username = ? and id = ?";
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setInt( 2, id );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		UserStrategiesPackage userStrategiesPackage = new UserStrategiesPackage(  );
		
		while( resultSet.next( ) )
		{
			String strategy		= resultSet.getString( "strategyName" );
			String buySignals   = resultSet.getString( "buySignals" );
			String sellSignals  = resultSet.getString( "sellSignals" );
			String targets      = resultSet.getString( "targets" );
			BigDecimal stopLoss = resultSet.getBigDecimal( "stopLoss" ).setScale( 2, BigDecimal.ROUND_DOWN );
			userStrategiesPackage.addFields( strategy, buySignals, sellSignals, targets, stopLoss, id );
		}
		
		return userStrategiesPackage;
	}
	
	/**
	 * Call to remove a bot from a user's profile
	 * @return
	 * @throws SQLException
	 */
	public boolean deleteStrategy( int id ) throws SQLException
	{
		String query = "SELECT * FROM TradingStrategies WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setInt( 2, id );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		if( resultSet.next( ) )
		{
			String addBotQuery   = "INSERT INTO DeletedTradingStrategies (username, strategyName, buySignals, sellSignals, targets, stopLoss) VALUES(?,?,?,?,?,?)";
			PreparedStatement ps = this.connection.prepareStatement( addBotQuery );

		    String username     = resultSet.getString("username");
		    String strategyName = resultSet.getString("strategyName");
		    String buySignals   = resultSet.getString("buySignals");
		    String sellSignals  = resultSet.getString("sellSignals");
		    String targets      = resultSet.getString("targets");
		    String stopLoss     = resultSet.getString("stopLoss");
			
			//Set all placeholders
			ps.setString( 1, username );
			ps.setString( 2, strategyName );
			ps.setString( 3, buySignals );
			ps.setString( 4, sellSignals );
			ps.setString( 5, targets );
			ps.setString( 6, stopLoss );

			ps.executeUpdate( );
		}
		
		//The bot does not exist
		else
		{
			return false;
		}
		
		//Query to remove bot from main table
		String deleteQuery = "DELETE FROM TradingStrategies WHERE username = ? and ID = ?";
		
		//Setup prepared statement
		PreparedStatement preparedStatementDelete = null;
		preparedStatementDelete = this.connection.prepareStatement( deleteQuery );
		preparedStatementDelete.setString( 1, this.username );
		preparedStatementDelete.setInt( 2, id );
		
		//Execute the deletion
		preparedStatementDelete.executeUpdate( );
		return true;
	}
}
