package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for getting the linke portfolios a user has
 * @author danielanderson
 *
 */
public class PortfolioLookupDAO extends UserDAOObject {
	
	/**
	 * Internal class for holding the location information ie. exchange or pubic wallet
	 * @author danielanderson
	 *
	 */
	public class Location
	{
		/**
		 * The location the coins live at ie. Binance, Ethereum Wallet
		 */
		private String location;
		
		/**
		 * The type of location ie. exchange or wallet
		 */
		private String type;
		
		/**
		 * Explicit constructor
		 * @param location
		 * @param type
		 */
		public Location( String location, String type )
		{
			this.location = location;
			this.type     = type;
		}
		
		public String getLocation( )
		{
			return this.location;
		}
		
		public String getType( )
		{
			return this.type;
		}
		
	}
	
	/**
	 * Explicit Constructor
	 * @param username
	 */
	public PortfolioLookupDAO( String username )
	{
		super( username );
	}
	
	/**
	 * Add a location to a user's lookup
	 * @param name
	 * @param type
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int addLocation( String name, String type) throws ClassNotFoundException, SQLException
	{
		PreparedStatement ps = null;
		
		String sql = "INSERT INTO PortfolioLookup VALUES(?,?,?)";
		ps = this.connection.prepareStatement( sql );
		ps.setString( 1, this.username );
		ps.setString( 2, name );
		ps.setString( 3, type );
		
		return ps.executeUpdate();
	}
	
	/**
	 * Grab all locations for a user
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List< Location > getLocations( ) throws ClassNotFoundException, SQLException
	{
		List< Location > locations = new ArrayList< Location >();
		PreparedStatement ps = null;
		
		String sql = "SELECT * FROM PortfolioLookup WHERE username = ?";
		ps = this.connection.prepareStatement( sql );
		ps.setString( 1, this.username );
		
		ResultSet rs = ps.executeQuery();
		
		while( rs.next( ) )
		{
			String location     = rs.getString( "location" );
			String locationType = rs.getString( "locationType" );
			locations.add( new Location( location, locationType ) );
		}
		
		return locations;
		
	}
	
}
