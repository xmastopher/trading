package gsplatform.servlet.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class that encapsulates values returned from TradingStrategiesDAO.getUserStrategies( )
 * @author danielanderson
 *
 */
public class UserStrategiesPackage {

	/**
	 * Parallel list of strategies
	 */
	private List< String > strategies;
	
	/**
	 * Parallel list of parameterValues
	 */
	private List< String > buySignals;
	
	/**
	 * Parallel list of parameterValues
	 */
	private List< String > sellSignals;
	
	/**
	 * Parallel list of parameterValues
	 */
	private List< String > targets;

	/**
	 * Parallel list of ids
	 */
	private List< Integer > ids;
	
	/**
	 * 
	 */
	private List< BigDecimal > stopLosses;
	
	/**
	 * Default constructor to initialize array lists
	 */
	public UserStrategiesPackage( )
	{
		this.strategies      = new ArrayList< String >( ); 
		this.buySignals      = new ArrayList< String >( ); 
		this.sellSignals      = new ArrayList< String >( ); 
		this.targets         = new ArrayList< String >( ); 
		this.stopLosses      = new ArrayList< BigDecimal >( ); 
		this.ids             = new ArrayList< Integer >( );
	}
	
	/**
	 * 
	 * @param strategy	 Name of strategy - provide by user
	 * @param buySignal	 Buy signal JSON object string
	 * @param sellSignal Sell signal JSON object string
	 * @param targets	 Target JSONArray string
	 * @param id			 id associated with the strategy
	 */
	public void addFields( String strategy, String buySignal, String sellSignal, String targets, BigDecimal stopLoss, Integer id )
	{
		this.strategies.add( strategy );
		this.buySignals.add( buySignal );
		this.sellSignals.add( sellSignal );
		this.targets.add( targets );
		this.stopLosses.add( stopLoss );
		this.ids.add( id );
	}
	
	/**
	 * Grab strategy at index i
	 * @param i	Index of strategy
	 * @return
	 */
	public String getStrategy( int i ) 
	{
		return this.strategies.get( i );
	}
	
	public JSONObject getStrategyByIdAsJSON( int id ) throws JSONException
	{
		int foundId = -1;
		
		int i = 0;
		
		for( Integer strategyId : this.ids )
		{
			if( strategyId == id )
			{
				foundId = i;
				break;
			}
			
			i++;
		}
		
		if( foundId == -1 ) return null;
		
		
		JSONObject strategyObject = new JSONObject( );
		
		strategyObject.put( "id", id );
		strategyObject.put("name", this.strategies.get( foundId ) );
		strategyObject.put("buySignals", new JSONArray( this.buySignals.get( foundId ) ) );
		strategyObject.put("sellSignals", new JSONArray( this.sellSignals.get( foundId ) ) );
		strategyObject.put("targets", new JSONArray( this.targets.get( foundId ) ) );
		strategyObject.put("stopLoss", this.stopLosses.get( foundId ) );
		
		return strategyObject;
	}

	/**
	 * Grab parameter set at index i
	 * @param i	Index of parameter set
	 * @return
	 */
	public String getBuySignal( int i ) 
	{
		return this.buySignals.get( i );
	}
	
	/**
	 * Grab parameter set at index i
	 * @param i	Index of parameter set
	 * @return
	 */
	public String getSellSignal( int i ) 
	{
		return this.sellSignals.get( i );
	}
	
	/**
	 * Grab parameter set at index i
	 * @param i	Index of parameter set
	 * @return
	 */
	public String getTargets( int i ) 
	{
		return this.targets.get( i );
	}

	/**
	 * Grab ID at index i
	 * @param i	Index of ID
	 * @return
	 */
	public Integer getId( int i ) 
	{
		return this.ids.get( i );
	}
	
	/**
	 * Returns stop loss at index
	 * @param i
	 * @return
	 */
	public BigDecimal getStopLoss( int i  )
	{
		return this.stopLosses.get( i );
	}
	
	/**
	 * Return the numbe of strategies in the set
	 * @return
	 */
	public int size( )
	{
		return this.strategies.size( );
	}
}
