package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.PasswordEncryptor;


public class UserProfileUpdateDAO extends UserDAOObject
{
	
	/**
	 * Can only grant a user every 24 hours
	 */
	public static final long GRANT_PERIOD = 1440;
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 */
	public UserProfileUpdateDAO( String username ) 
	{
		super( username );
	}
	
	/**
	 * DAO function - get list of user's followers
	 * @throws SQLException 
	 */
	public List< String > followUser( ) throws SQLException
	{		
	    
		String query         = "SELECT follower from UserFollowing WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		List< String > followers = new ArrayList< String >( );
		
		while( resultSet.next( ) )
		{
			followers.add( resultSet.getString( "follower" ) );
		}
		
		return followers;
		
	}
	
	/**
	 * Returns credibility score from given username
	 * @return False if user has no grant today
	 * @throws SQLException
	 */
	public Boolean grantCredibility( String userToGrant ) throws SQLException
	{
		if( !hasDailyGrant( ) )
		{
			return false;
		}
		
		String query = "Update UserCredibility SET credibility = crebility + 1 WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, userToGrant );
		int success = ps.executeUpdate( );
		
		if( success > 0  )
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns credibility score from given username
	 * @return False if user has no grant today
	 * @throws SQLException
	 */
	public Boolean decrementCredibility( String userToGrant ) throws SQLException
	{
		if( !hasDailyGrant( ) )
		{
			return false;
		}
		
		String query = "Update UserCredibility SET credibility = crebility - 1 WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, userToGrant );
		int success = ps.executeUpdate( );
		
		if( success > 0  )
		{
			return updateGrantDate( );
		}
		
		return false;
	}
	
	/**
	 * Updates a user's last grant date on success
	 * @throws SQLException 
	 */
	protected boolean updateGrantDate( ) throws SQLException
	{
		
		String query = "Update UserCredibility SET lastDateGranted = NOW() WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		int success = ps.executeUpdate( );
		
		if( success > 0  )
		{
			return updateGrantDate();
		}
		
		return false;
	}
	
	/**
	 * Internal function to check if user can grant
	 * @return
	 * @throws SQLException
	 */
	public boolean hasDailyGrant( ) throws SQLException
	{
		String query = "SELECT dateOfLastGrant from UserCredibility WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, username );
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			Timestamp dateOfLastGrant = resultSet.getTimestamp( "dateOfLastGrant" );

			Calendar calendar = Calendar.getInstance();

			java.util.Date now = calendar.getTime();
			
			return GS_Utils.compareTwoTimeStamps( dateOfLastGrant, new Timestamp( now.getTime( ) ) ) >= GRANT_PERIOD;
		}
		
		return false;
	}
	
	/**
	 * Resets a user's password to a new password
	 * @return	true if successful, false otherwise
	 * @throws SQLException
	 */
	public boolean resetPassword( String newPassword ) throws SQLException
	{
		String encryptedPassword = PasswordEncryptor.encryptPassword( newPassword );
		String query = "UPDATE UserLogin SET password = ? WHERE username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, encryptedPassword );
		ps.setString( 1, this.username );
		return ps.executeUpdate( ) > 0;
	}
	
}