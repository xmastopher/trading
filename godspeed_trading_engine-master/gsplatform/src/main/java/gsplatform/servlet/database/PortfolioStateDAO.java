package gsplatform.servlet.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Data abstraction object for querying the state of a user's portfolio
 * @author danielanderson
 *
 */
public class PortfolioStateDAO extends UserDAOObject
{
	
	/**
	 * Parameter to grab the snapshot of the user's portfolio should be a string in the following format 'YYYY-MM-DD'
	 */
	private String snapshot;
	
	/**
	 * The database to grab the snapshot from - test or production
	 */
	private String database;
	
	/**
	 * The JSON object for the entire portfolio
	 */
	private String portfolio;
	
	/**
	 * Constructs a PortfolioStateDAO object with provided user, database and snapshot and initializes with such parameters
	 * @param username					The username associated with the user
	 * @param snapshot					A date of the snapshot in the following format 'YYYY-MM-DD'
	 * @param database					The database to pull from - test or prod
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public PortfolioStateDAO( String username, String snapshot, String database ) throws ClassNotFoundException, SQLException
	{
		super( username );
		this.snapshot = snapshot;
		this.database = database;
		
		init( );
	}
	
	/**
	 * Internal method to set internal state
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void init( ) throws ClassNotFoundException, SQLException
	{
		String query         = "SELECT * FROM PortfolioState WHERE DATE( snapshot ) = ? AND username = ?";
		PreparedStatement ps = null;
		
		ps              = this.connection.prepareStatement( query );
		ps.setString( 1, this.snapshot );
		ps.setString( 2, this.username );
		
		ResultSet resultSet = ps.executeQuery( );
		
		if( resultSet.next( ) )
		{
			this.portfolio = resultSet.getString( "portfolio" );
		}
		else
		{
			this.portfolio = "Failure:snapshot does not exist";
		}
	}
	
	/**
	 * Called to save the state of a user's portfolio as a JSON in the database
	 * @param portfolio
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 * @throws JSONException
	 */
	public void writePortfolio( JSONObject portfolio ) throws ClassNotFoundException, SQLException, IOException, JSONException
	{
		String query         = "INSERT INTO PortfolioState VALUES(?,?,?)";
		PreparedStatement ps = null;
		
		Connection conn = this.connection;
		ps              = conn.prepareStatement( query );
		ps.setString( 1, this.snapshot );
		ps.setString( 2, this.username );
		ps.setString( 3, portfolio.toString( ) );
		ps.executeUpdate( );
		
	}
	
	/**
	 * Grab the total value measured in associated fiat type
	 * @return
	 */
	public String getPortfolio( )
	{
		return this.portfolio;
	}
	
	
	
}
