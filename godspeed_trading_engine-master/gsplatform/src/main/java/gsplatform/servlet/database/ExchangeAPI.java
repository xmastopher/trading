package gsplatform.servlet.database;

public class ExchangeAPI 
{
	protected String exchange;
	
	protected boolean isLinked;

	public ExchangeAPI(String exchange, boolean isLinked) {
		super();
		this.exchange = exchange;
		this.isLinked = isLinked;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public boolean isLinked() {
		return isLinked;
	}

	public void setLinked(boolean isLinked) {
		this.isLinked = isLinked;
	}
	
}
