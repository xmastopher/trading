package gsplatform.servlet.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import gsplatform.jobs.Constants;

/**
 * Class with static methods for grabbing connection based on database name
 * @author danielanderson
 *
 */
public class GodspeedDatabaseConnect {
	   
	   /**
	    * Driver string for mysql
	    */
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   
	   /**
	    * Connection string for general database
	    */
	   static final String DB_URL_GS = Constants.instance( ).getPlatformDatabase( );

	   /**
	    * Username
	    */
	   static final String USER = Constants.instance( ).getPlatformDatabaseUser( );
	   
	   /**
	    * Password
	    */
	   static final String PASS = Constants.instance( ).getPlatformDatabasePassword( );
	   
	   /**
	    * Provide a string and get connection for corresponding DB
	    * @param database					The name of the DB
	    * @return							Connection object
	    * @throws ClassNotFoundException		
	    * @throws SQLException
	    */
	   public static Connection getConnection( String database ) throws ClassNotFoundException, SQLException
	   {
		   Class.forName( JDBC_DRIVER );

		   System.out.println( "Connecting to database..." );
		   
		   if( database.equalsIgnoreCase( "nomicsmirror" ) ) {
			   return DriverManager.getConnection( Constants.instance( ).getNomicsMirrorDatabase( ), 
						  						  Constants.instance( ).getNomicsMirrorDatabaseUser( ),
						  						  Constants.instance( ).getNomicsMirrorDatabasePassword( ) );
		   }
		   
		   return DriverManager.getConnection( Constants.instance( ).getPlatformDatabase( ), 
				   							  Constants.instance( ).getPlatformDatabaseUser( ),
				   							  Constants.instance( ).getPlatformDatabasePassword( ) );
	   }
	   
	   public static Connection manualConnection( String dbUrl, String user, String pass ) throws SQLException {
		   return DriverManager.getConnection( dbUrl, user, pass );
	   }
	   
}
