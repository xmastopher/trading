package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.knowm.xchange.Exchange;

import gsplatform.services.ExchangeHandler;

/**
 * Data access object that grabs a user's API keys provided an exchange
 * @author danielanderson
 *
 */
public class ExchangeAPIDAO extends UserDAOObject
{
	
	/**
	 * Name of the exchange associated with API key ie. 'Binance'
	 */
	private String exchangeName;
	
	/**
	 * The private key associated with said exchange/user
	 */
	private String exchangePrivateKey;
	
	/**
	 * The public key associated with said exchange/user
	 */
	private String exchangePublicKey;
	
	
	/**
	 * Explicit constructor 
	 * @param username
	 * @param exchangeName
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ExchangeAPIDAO( String username )
	{
		super( username );
	}
	
	/**
	 * Explicit constructor 
	 * @param username
	 * @param exchangeName
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ExchangeAPIDAO( String username, String exchangeName ) throws ClassNotFoundException, SQLException
	{
		super( username );
		this.exchangeName = exchangeName;
		this.openConnection( );
		this.setKeys( );
		this.closeConnection( );
	}
	
	/**
	 * Call to initialize the interal keys for this user
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void setKeys( ) throws ClassNotFoundException, SQLException
	{
		PreparedStatement ps = null;
		
		String sql = "SELECT publicKey, privateKey FROM ApiKeys WHERE username = ? AND exchange = ? ";
		ps  = this.connection.prepareStatement( sql );
		ps.setString( 1, this.username );
		ps.setString( 2, this.exchangeName );
		
		ResultSet rs = ps.executeQuery( );
		
		if( rs.next( ) )
		{
			this.exchangePublicKey  = rs.getString( "publicKey" );
			this.exchangePrivateKey = rs.getString( "privateKey" );
		}
		
	}
	
	/**
	 * Grab list of active/non-active exchange API pairs
	 * @return
	 * @throws SQLException
	 */
	public List< ExchangeAPI > getLinkedExchanges( ) throws SQLException
	{
		PreparedStatement ps = null;
		String sql = "SELECT * FROM ApiKeys WHERE username = ?";
		ps  = this.connection.prepareStatement( sql );
		ps.setString( 1, this.username );
		
		ResultSet rs = ps.executeQuery( );
		
		List< ExchangeAPI > exchangeAPIs = new ArrayList< ExchangeAPI >( );
		
		if( rs.next( ) )
		{
			String exchange = rs.getString( "exchange" );
			Boolean isActive = rs.getBoolean( "isActive" );
			exchangeAPIs.add( new ExchangeAPI( exchange, isActive ) );
		}
		
		return exchangeAPIs;
	}
	
	public boolean AddExchangeAPIKey( String privateKey, String publicKey, String pExchangeName ) throws SQLException
	{
		try
		{
			Exchange exchange = ExchangeHandler.getExchangePrivate( pExchangeName, 
																   privateKey,
																   publicKey );
			
			if( exchange == null ) return false;
		}
		catch( Exception e )
		{
			return false;
		}
		
		PreparedStatement ps = null;
		String sql = "UPDATE ApiKeys set privateKey = ?, publicKey = ? WHERE username = ? and exchange = ? ";
		ps  = this.connection.prepareStatement( sql );
		ps.setString( 1, privateKey );
		ps.setString( 2, publicKey );
		ps.setString( 3, this.username );
		ps.setString( 4, pExchangeName );
		
		return ps.executeUpdate( ) > 0;
	}
	
	/**
	 * Change usability state of API to active/inactive
	 * @param pExchangeName
	 * @param active
	 * @return
	 * @throws SQLException
	 */
	public boolean setActive( String pExchangeName, Boolean active ) throws SQLException
	{
		
		PreparedStatement ps = null;
		String sql = "UPDATE ApiKeys set isActive = ? WHERE username = ? and exchange = ? ";
		ps  = this.connection.prepareStatement( sql );
		ps.setBoolean( 1, active );
		ps.setString( 2, this.username );
		ps.setString( 3, pExchangeName );
		
		return ps.executeUpdate( ) > 0;
	}

	
	/**
	 * Public method to grab authenticated exchange object
	 * @return	Exchange object that is authenticated
	 */
	public Exchange getExchangeObject( )
	{
		return ExchangeHandler.getExchangePrivate( this.exchangeName, this.exchangePrivateKey, this.exchangePublicKey );
	}
	
	public static void main( String args[] )
	{
		ExchangeAPIDAO exchangeAPIDAO = new ExchangeAPIDAO( "danimal" );

		try {
			exchangeAPIDAO.openConnection( );
			exchangeAPIDAO.AddExchangeAPIKey( args[0], args[1], "binance" );
			exchangeAPIDAO.closeConnection( );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
