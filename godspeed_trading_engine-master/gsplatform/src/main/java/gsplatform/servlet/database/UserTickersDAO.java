package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserTickersDAO extends UserDAOObject
{
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 */
	public UserTickersDAO( String username ) 
	{
		super( username );
	}
	
	/**
	 * DAO function to update a user's tickers for their dashboard
	 * @throws SQLException 
	 */
	public void updateTicker( String tickerJSONArray, int tickerBox ) throws SQLException
	{		
	    
		String query         = "UPDATE UserTickers SET ticker = ? WHERE tickerBox = ? and username = ?";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, tickerJSONArray);
		ps.setInt( 2, tickerBox );
		ps.setString( 3, username );
		ps.executeUpdate( );
	}
	
	/**
	 * Returns user's personal tickers
	 * @param tickerBox
	 * @return
	 * @throws SQLException
	 */
	public List< String > getTickers( ) throws SQLException
	{		
	    
		String query         = "SELECT * FROM UserTickers WHERE username = ? ORDER BY tickerBox ASC";
		PreparedStatement ps = null;

		//Update row corresponding to the specific ticker
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		
		List< String > returnList = new ArrayList< String >( );
		
		ResultSet resultSet = ps.executeQuery( );
		
		while( resultSet.next( ) ){
			returnList.add( resultSet.getString( "ticker" ) );
		}
		
		return returnList;
		
	}
	
}