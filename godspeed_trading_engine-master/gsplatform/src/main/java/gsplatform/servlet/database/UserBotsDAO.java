package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data access object for writing and reading bots
 * @author danielanderson
 *
 */
public class UserBotsDAO extends UserDAOObject {

	/**
	 * Internal member for the bot id
	 */
	protected int botId;
	
	
	/**
	 * Constructor for when username is irrelevant - ie grabbing all public bots
	 */
	public UserBotsDAO( )
	{;}
	
	/**
	 * Explicit constructor for the user bots
	 */
	public UserBotsDAO( String username ) 
	{
		super(username);
	}
	
	/**
	 * Second constructor for included the unique information about a bot
	 * @param username
	 * @param botId
	 * @param strategyId
	 */
	public UserBotsDAO( String username, int botId )
	{
		this( username );
		this.botId = botId;
	}
	
	/**
	 * Public method to grab integer value of max ID for provided user bot
	 * @param strategy
	 * @return	and int for the max ID of this strategy
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public int getMaxBotID( ) throws ClassNotFoundException, SQLException
	{
		String query = "SELECT MAX( ID ) AS maxIndex from UserBots  WHERE username = ?";
		
		PreparedStatement preparedStatement = null;
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getInt( "maxIndex" );
		}

		return 0;
	}
	/**
	 * Call to add a bot to the DB for some user 
	 * @param username
	 * @param botId
	 * @param strategyId
	 * @param name
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public int addBot( String name, int strategyId, Boolean locked, Boolean running, Boolean isPublic, String base, String counter, String exchange, double tradingLimit, String interval ) throws SQLException, ClassNotFoundException
	{
		String query         = "INSERT INTO UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)" +
							   "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		
		PreparedStatement ps = null;

		//Add the bot to the user profile
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, strategyId );
		ps.setString( 3, name );
		ps.setString( 4, exchange);
		ps.setString( 5, base);
		ps.setString( 6, counter);
		ps.setBoolean(  7, locked );
		ps.setBoolean( 8, running );
		ps.setBoolean( 9, isPublic );
		ps.setDouble( 10, tradingLimit );
		ps.setString( 11, interval );
		ps.executeUpdate( );
		return getMaxBotID( );
	}
	
	/**
	 * Get all bots from specific user
	 * @param username
	 * @throws SQLException 
	 */
	public List< UserBot > grabAllBots( ) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM UserBots WHERE username = ?";
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		List< UserBot > userBots = new ArrayList< UserBot >( );
		
		//Grab all the bots for this user and add to list
		while( resultSet.next( ) )
		{
			int botId			= resultSet.getInt( "id" );
			int strategyId	    = resultSet.getInt( "strategyId" );
			String botName		= resultSet.getString( "botName" );
			String exchange		= resultSet.getString( "exchange" );
			String marketBase    = resultSet.getString( "marketBase" );
			String marketQuote  = resultSet.getString( "marketQuote" );
			Boolean locked		= resultSet.getBoolean( "locked" );
			Boolean isRunning   = resultSet.getBoolean( "running" );
			Boolean isPublic    = resultSet.getBoolean( "isPublic" );
			String interval     = resultSet.getString( "intervalCandle" );
			double tradingLimit = resultSet.getDouble( "tradingLimit" );
			
			UserBot userBot = new UserBot( botId, strategyId, username, botName, exchange, marketBase, marketQuote, locked, isRunning, isPublic, tradingLimit, interval );
			
			userBots.add( userBot );
		}
		
		return userBots;
	}
	
	/**
	 * Get all bots from specific user
	 * @param username
	 * @throws SQLException 
	 */
	public Map< Integer, UserBot > grabAllBotsAsMap( ) throws SQLException
	{
		Map< Integer, UserBot > userBotMap = new HashMap< Integer, UserBot >( );
		List< UserBot > userBots = grabAllBots( );
		
		for( UserBot userBot : userBots ) {
			userBotMap.put( userBot.getBotId( ), userBot );
		}
	
		return userBotMap;
	}
	
	/**
	 * Public method for grabbing all publicly flagged bots - primarily for leaderboard augmentation
	 * @throws SQLException 
	 */
	public List< UserBot > getAllPublicBots( ) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM UserBots WHERE isPublic = true";
		
		preparedStatement = this.connection.prepareStatement( query );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		List< UserBot > userBots = new ArrayList< UserBot >( );
		
		//Grab all the bots for this user and add to list
		while( resultSet.next( ) )
		{
			int botId			= resultSet.getInt( "id" );
			int strategyId	    = resultSet.getInt( "strategyId" );
			String botName		= resultSet.getString( "botName" );
			String exchange		= resultSet.getString( "exchange" );
			String marketBase    = resultSet.getString( "marketBase" );
			String marketQuote  = resultSet.getString( "marketQuote" );
			Boolean locked		= resultSet.getBoolean( "locked" );
			Boolean isRunning   = resultSet.getBoolean( "running" );
			Boolean isPublic    = resultSet.getBoolean( "isPublic" );
			String interval     = resultSet.getString( "intervalCandle" );
			double tradingLimit = resultSet.getDouble( "tradingLimit" );
			
			UserBot userBot = new UserBot( botId, strategyId, username, botName, exchange, marketBase, marketQuote, locked, isRunning, isPublic, tradingLimit, interval );
			
			userBots.add( userBot );
		}
		
		return userBots;
	}
	
	/**
	 * Get all bots that are running
	 * @throws SQLException 
	 */
	public List< UserBot > getAllRunningBots( ) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM UserBots WHERE running = true";
		
		preparedStatement = this.connection.prepareStatement( query );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		List< UserBot > userBots = new ArrayList< UserBot >( );
		
		//Grab all the bots for this user and add to list
		while( resultSet.next( ) )
		{
			int botId			= resultSet.getInt( "id" );
			int strategyId	    = resultSet.getInt( "strategyId" );
			String username      = resultSet.getString( "username" );
			String botName		= resultSet.getString( "botName" );
			String exchange		= resultSet.getString( "exchange" );
			String marketBase    = resultSet.getString( "marketBase" );
			String marketQuote  = resultSet.getString( "marketQuote" );
			Boolean locked		= resultSet.getBoolean( "locked" );
			Boolean isRunning   = resultSet.getBoolean( "running" );
			Boolean isPublic    = resultSet.getBoolean( "isPublic" );
			String interval     = resultSet.getString( "intervalCandle" );
			double tradingLimit = resultSet.getDouble( "tradingLimit" );
			
			UserBot userBot = new UserBot( botId, strategyId, username, botName, exchange, marketBase, marketQuote, locked, isRunning, isPublic, tradingLimit, interval );
			
			userBots.add( userBot );
		}
		
		return userBots;
	}
	
	/**
	 * Call to set the percent of your stack that you
	 * want your bot to trade with.
	 * @param percentOfStack
	 * @throws SQLException 
	 */
	public void setBotTradingLimit( double percentOfStack ) throws SQLException
	{
		//Grab bot to update
 		String query = "UPDATE UserBots SET tradingLimit = ? WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement  = this.connection.prepareStatement( query );
		preparedStatement.setDouble( 1, percentOfStack );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		
		preparedStatement.executeUpdate( );
			
	}
	
	/**
	 * Call to lock a user's bot
	 * @throws SQLException
	 */
	public void lockBot( ) throws SQLException
	{
		//Grab user bot to lock
 		String query = "UPDATE UserBots SET locked = ? WHERE username = ? and ID = ?  ";
 		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setBoolean( 1, true );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		
		preparedStatement.executeUpdate( );
	}
	
	/**
	 * Call to unlock a user's bot
	 * @throws SQLException
	 */
	public void unlockBot( ) throws SQLException
	{
		String query = "UPDATE UserBots SET locked = ? WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setBoolean( 1, false );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		
		preparedStatement.executeUpdate( );
	}
	
	/**
	 * Call to remove a bot from a user's profile
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public boolean deleteBot( ) throws SQLException, ClassNotFoundException
	{
		String query = "SELECT * FROM UserBots WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setInt( 2, this.botId );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		boolean success = true;
		
		if( resultSet.next( ) )
		{
			String addBotQuery   = "INSERT INTO UserDeletedBots (username,strategyID,botName) VALUES(?,?,?)";
			PreparedStatement ps = this.connection.prepareStatement( addBotQuery );
			String botName      = resultSet.getString("botName");
			
			//Set all placeholders
			ps.setString( 1, this.username );
			ps.setInt( 2, this.botId );
			ps.setString( 3, botName );

			//Successfull - now delete all logs and leaderboard records
			if( ps.executeUpdate( ) > 0 ) {
				BotLeaderboardsDAO botLeaderboards = new BotLeaderboardsDAO( this.username, this.botId );
				botLeaderboards.openConnection( );
				BotLogsDAO botLogs				  = new BotLogsDAO( this.username, this.botId );
				botLogs.openConnection( );
				success = botLeaderboards.deleteAllBotRecords( ) && botLogs.removeAllBotLogs( );
			}
		}
		
		//The bot does not exist
		else
		{
			return false;
		}
		
		//Query to remove bot from main table
		String deleteQuery = "DELETE FROM UserBots WHERE username = ? and ID = ?  ";
		
		//Setup prepared statement
		PreparedStatement preparedStatementDelete = null;
		preparedStatementDelete = this.connection.prepareStatement( deleteQuery );
		preparedStatementDelete.setString( 1, this.username );
		preparedStatementDelete.setInt( 2, this.botId );
		
		//Execute the deletion
		return preparedStatementDelete.executeUpdate( ) >= 1 && success;
	}
	
	/**
	 * Call to begin running a bot on the server
	 * @throws SQLException
	 */
	public void startBot( ) throws SQLException
	{
		String query = "UPDATE UserBots SET running = ? WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setBoolean( 1, true );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		
		preparedStatement.executeUpdate( );
		
	}
	
	/**
	 * Call to stop a bot that is running on the server
	 * @throws SQLException
	 */
	public void stopBot( ) throws SQLException
	{
		String query = "UPDATE UserBots SET running = ? WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setBoolean( 1, false );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		preparedStatement.executeUpdate( );
	
	}
	
	/**
	 * Call to stop a bot that is running on the server
	 * @throws SQLException
	 */
	public void makePublic( ) throws SQLException
	{
		String query = "UPDATE UserBots SET isPublic = ? WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setBoolean( 1, true );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		preparedStatement.executeUpdate( );
	}
	
	/**
	 * Call to stop a bot that is running on the server
	 * @throws SQLException
	 */
	public void makePrivate( ) throws SQLException
	{
		String query = "UPDATE UserBots SET isPublic = ? WHERE username = ? and ID = ?  ";
		
		PreparedStatement preparedStatement = null;
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setBoolean( 1, false );
		preparedStatement.setString( 2, this.username );
		preparedStatement.setInt( 3, this.botId );
		preparedStatement.executeUpdate( );
	}
	
	/**
	 * Get all bots from specific user
	 * @param username
	 * @throws SQLException 
	 */
	public UserBot grabBotFromId( int id ) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM UserBots WHERE username = ? and id = ?";
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setInt( 2, id );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		//Grab all the bots for this user and add to list
		if( resultSet.next( ) )
		{
			int botId			= resultSet.getInt( "id" );
			int strategyId	    = resultSet.getInt( "strategyId" );
			String botName		= resultSet.getString( "botName" );
			String exchange		= resultSet.getString( "exchange" );
			String marketBase    = resultSet.getString( "marketBase" );
			String marketQuote  = resultSet.getString( "marketQuote" );
			Boolean locked		= resultSet.getBoolean( "locked" );
			Boolean isRunning   = resultSet.getBoolean( "running" );
			Boolean isPublic    = resultSet.getBoolean( "isPublic" );
			String interval     = resultSet.getString( "intervalCandle" );
			double tradingLimit = resultSet.getDouble( "tradingLimit" );
			
			UserBot userBot = new UserBot( botId, strategyId, username, botName, exchange, marketBase, marketQuote, locked, isRunning, isPublic, tradingLimit, interval );
			
			return userBot;
		}
		
		return null;
	}
	
	/**
	 * Get all bots from specific user
	 * @param username
	 * @throws SQLException 
	 */
	public List< UserBot > grabBotsUsingStrategy( int strategyId ) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		
		String query = "SELECT * FROM UserBots WHERE username = ? and strategyId = ?";
		
		preparedStatement = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setInt( 2, strategyId );
		ResultSet resultSet   = preparedStatement.executeQuery( );
		
		List< UserBot > returnList = new ArrayList< UserBot >( );
		
		//Grab all the bots for this user and add to list
		while( resultSet.next( ) )
		{
			int botId			= resultSet.getInt( "id" );
			int sId	    = resultSet.getInt( "strategyId" );
			String botName		= resultSet.getString( "botName" );
			String exchange		= resultSet.getString( "exchange" );
			String marketBase    = resultSet.getString( "marketBase" );
			String marketQuote  = resultSet.getString( "marketQuote" );
			Boolean locked		= resultSet.getBoolean( "locked" );
			Boolean isRunning   = resultSet.getBoolean( "running" );
			Boolean isPublic    = resultSet.getBoolean( "isPublic" );
			String interval     = resultSet.getString( "intervalCandle" );
			double tradingLimit = resultSet.getDouble( "tradingLimit" );
			
			UserBot userBot = new UserBot( botId, sId, username, botName, exchange, marketBase, marketQuote, locked, isRunning, isPublic, tradingLimit, interval );
			
			returnList.add( userBot );
		}
		
		return returnList;
	}

}
