package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Data Access Object class for encapsulated User Trading Strategy data access
 * @author danielanderson
 *
 */
public class SignalsLookupDAO extends UserDAOObject
{
	
	/**
	 * Explicit constructor to initialize with username
	 */
	public SignalsLookupDAO( )
	{
		this( "inhouse" );
	}
	
	/**
	 * Explicit constructor to initialize with username
	 * @param username
	 */
	public SignalsLookupDAO( String username )
	{
		super( username );
	}
	
	/**
	 * Returns a List of signal-name json spec pairs as an array where
	 * array[0] is the name and array[1] is the json string spec
	 */
	public List< String[] > getAllSignals( ) throws ClassNotFoundException, SQLException
	{

		String query = "SELECT * FROM TradingSignalsLookup";
		
		PreparedStatement preparedStatement = null;
		
		preparedStatement = this.connection.prepareStatement( query );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		List< String[] > allPairs = new ArrayList< String[] >( );
		
		while( resultSet.next( ) )
		{
			String name = resultSet.getString( "signalName" );
			String json = resultSet.getString( "parameters" );
			String[] pair = new String[] { name, json };
			allPairs.add( pair );
			
		}

		return allPairs;
	}
	
	/**
	 * Returns a List of signal-name json spec pairs as an array where
	 * array[0] is the name and array[1] is the json string spec
	 * @throws JSONException 
	 */
	public List< JSONObject > getPreconfiguredSignals( ) throws ClassNotFoundException, SQLException, JSONException
	{

		String query = "SELECT * FROM InHouseSignals";
		
		PreparedStatement preparedStatement = null;
		
		preparedStatement = this.connection.prepareStatement( query );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		List< JSONObject > returnList = new ArrayList< JSONObject >( );
		
		while( resultSet.next( ) )
		{
			returnList.add( new JSONObject( resultSet.getString( "parameterValues" ) ) );
			
		}

		return returnList;
	}
}
