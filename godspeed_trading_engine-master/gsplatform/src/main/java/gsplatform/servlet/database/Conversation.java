package gsplatform.servlet.database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Conversation implements Comparable< Conversation > {
	
	/**
	 *  Dynamic list of messages between users - will be sorted by timestamp
	 */
	private List< Message > messages;
	
	/**
	 * Most recent timestamp associated with conversation
	 */
	private Date timestamp;
	
	/**
	 * The username of the user one is having a conversation with
	 */
	private String username;
	
	/**
	 * The ID of the conversation, should also be retained in the front end as a hidden field
	 */
	private int conversationId;
	
	/**
	 * Create conversation by providing the username one is having the conversation with
	 * @param username	The username of the user one is having a conversation with
	 */
	public Conversation( String username, Date timestamp, int conversationId )
	{
		this.username       = username;
		this.timestamp      = timestamp;
		this.conversationId = conversationId;
		this.messages       = new ArrayList< Message >( );
	}
	
	/**
	 * Sorts messages by timestamp and updates conversations timestamp to the most recent message timestamp
	 */
	public void sortMessages( )
	{
		Collections.sort( this.messages, Collections.reverseOrder( ) );
		this.timestamp = this.messages.get( 0 ).getTimestamp( );
	}
	
	/**
	 * Add a message to the conversation, these should be added in sorted order by timestamp
	 * @param message
	 */
	public void addMessage( Message message )
	{
		messages.add( message );
	}
	
	/**
	 * Get this.messages at int index
	 * @param index	Index of message to grab
	 * @return
	 */
	public Message getMessage( int index )
	{
		return messages.get( index );
	}

	/**
	 * Return list of all messages in conversations - these should be pre-sorted by timestamp
	 * @return
	 */
	public List< Message > getMessages( )
	{
		return this.messages;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getConversationId() {
		return conversationId;
	}

	public void setConversationId(int conversationId) {
		this.conversationId = conversationId;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	/**
	 * Overidden comparison method for timstamps
	 * @param rhsObject
	 * @return	comparison result
	 */
	@Override
	public int compareTo( Conversation rhsObject ) 
	{
	    return this.timestamp.compareTo( rhsObject.timestamp );
	}
}
