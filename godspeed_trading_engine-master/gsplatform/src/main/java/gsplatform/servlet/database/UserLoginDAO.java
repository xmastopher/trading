package gsplatform.servlet.database;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import gsplatform.servlet.database.UserLoginDAO;
import gsplatform.servlet.database.UserLoginPackage;
import gsplatform.utilities.PasswordEncryptor;

/**
 * Data access object for logging in a user 
 * @author danielanderson
 *
 */
public class UserLoginDAO extends UserDAOObject
{
	
	/**
	 * The password for this user
	 */
	private String password;
	
	/**
	 * Empty
	 */
	public UserLoginDAO( )
	{;}
	
	/**
	 * Explicit constructor for logging in a user
	 * @param username
	 * @param password
	 */
	public UserLoginDAO( String username, String password )
	{
		super( username );
		this.password = password;
	}
	
	/**
	 * Authentication function returns UserLoginPackage object with information about login
	 * @return		UserLoginPackage with information about the login
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public UserLoginPackage authenticateUser( ) throws ClassNotFoundException, SQLException
	{
		PreparedStatement preparedStatement = null;
		String sql = "";
		
		if( username.contains("@") )
		{
			sql = "SELECT * FROM UserLogin WHERE email = ?";
		}
		else
		{
			sql = "SELECT * FROM UserLogin WHERE username = ?";
		}
		

		preparedStatement  = this.connection.prepareStatement( sql );
		preparedStatement.setString(1, this.username);
		
		ResultSet rs = preparedStatement.executeQuery( );
		
		//Username is found
		if( rs.next( ) )
		{
			
			//Grab all fields
			String actualUsername = rs.getString( "username" );
			String actualEmail = rs.getString( "email" );
			String hashedPassword = rs.getString( "password" );
			
			//Check that passwords match
			if( PasswordEncryptor.passwordsMatch( password, hashedPassword ) )
			{
				return new UserLoginPackage( actualUsername, actualEmail, true, "0000: User login successful" );
			}
			
			//Incorrect password
			else
			{
				return new UserLoginPackage( actualUsername, actualEmail, false, "0001: Incorrect password" ); 
			}
		}
		//Username/Email does not exist
		else
		{
			//Invalid email
			if( this.username.contains( "@" ) )
			{
				return new UserLoginPackage( this.username, this.username, false, "0002: Email does not exist in system" ); 
			}
			
			//Invalid username
			else
			{
				return new UserLoginPackage( this.username, this.username, false, "0003: Username does not exist in system" ); 
			}
		}
	}
	
	//Allow to do lookup for username from email - needs to be done for session token mapping
	public String getUsernameFromEmail( String email ) throws SQLException
	{
		
		PreparedStatement preparedStatement = null;
		String sql = "SELECT username FROM UserLogin WHERE email = ?";
		
		preparedStatement  = this.connection.prepareStatement( sql );
		preparedStatement.setString( 1, email );
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		if( resultSet.next( ) )
		{
			return resultSet.getString( "username" );
		}
		
		return email;
		
	}
	
	public boolean changePassword( String newPassword ) throws SQLException {
		String query = "UPDATE UserLogin SET password = ? WHERE username = ?";
		String newPasswordEncrypted = PasswordEncryptor.encryptPassword( newPassword );
		PreparedStatement preparedStatement = null;
		preparedStatement  = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, newPasswordEncrypted );
		preparedStatement.setString( 2, this.username );
		return preparedStatement.executeUpdate( ) > 0;
	}
	
	
}
