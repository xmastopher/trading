package gsplatform.servlet.database;

import java.util.Date;

/**
 * Abstraction for a message that is stored in DB used typically to define a conversation with a list of messages
 * @author danielanderson
 *
 */
public class Message implements Comparable< Message >
{
	/**
	 * The body of the message
	 */
	private String body;
	
	/**
	 * The body of the subject
	 */
	private String subject;
	
	/**
	 * Timestamp associated with message send
	 */
	private Date timestamp;
	
	/**
	 * The user who is recieving the message
	 */
	private String toUser;
	
	/**
	 * The user who is sending the message
	 */
	private String fromUser;
	
	/**
	 * Explicit constructor to create a message object w/ a subject and body
	 * @return
	 */
	public Message( String subject, String body, String fromUser, String toUser, Date timestamp )
	{
		this.subject   = subject;
		this.body      = body;
		this.fromUser  = fromUser;
		this.toUser    = toUser;
		this.timestamp = timestamp;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public Date getTimestamp( )
	{
		return this.timestamp;
	}
	
	public String getToUser( )
	{
		return this.toUser;
	}
	
	public String getFromUser( )
	{
		return this.fromUser;
	}
	
	/**
	 * Overidden comparison method for timstamps
	 * @param rhsObject
	 * @return	comparison result
	 */
	@Override
	public int compareTo( Message rhsObject ) 
	{
	    return this.timestamp.compareTo( rhsObject.timestamp );
	}
	
}
