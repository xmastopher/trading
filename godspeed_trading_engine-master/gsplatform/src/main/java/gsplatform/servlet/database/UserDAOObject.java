package gsplatform.servlet.database;

import java.sql.Connection;
import java.sql.SQLException;

import gsplatform.jobs.Constants;

public class UserDAOObject 
{
	/**
	 * The database to user - test or prod
	 */
	protected static final String DATABASE = Constants.instance( ).getPlatformDatabase( );
	
	/**
	 * The username of the user information is being grabbed for
	 */
	protected String username;
	
	/**
	 * The database connection - should be opened and closed manually
	 */
	protected Connection connection;
	
	/**
	 * Emptry constructor used in a small set of cases
	 */
	public UserDAOObject( ) {;}
	
	/**
	 * Explicit constructor for setting the user associated with the DB Query
	 * @param username
	 */
	public UserDAOObject( String username )
	{
		this.username = username;
	}
	
	/**
	 * Manually call before a query sequence to open a connection
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void openConnection( ) throws ClassNotFoundException, SQLException
	{
		this.connection = GodspeedDatabaseConnect.getConnection( Constants.instance( ).getPlatformDatabase( ) );
	}
	
	/**
	 * Manually call at the end of a query sequence to close the connection
	 * @throws SQLException 
	 */
	public void closeConnection( ) throws SQLException
	{
		this.connection.close( );
	}
	
}
