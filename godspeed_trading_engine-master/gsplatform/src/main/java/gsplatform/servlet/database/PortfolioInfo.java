package gsplatform.servlet.database;

import java.math.BigDecimal;

public class PortfolioInfo {

	/**
	 * The type of fiat user is set to - USD, EURO, etc
	 */
	protected String fiatType;
	
	/**
	 * The amount this user has thrown into the cryptoverse
	 */
	protected BigDecimal amountInvested;
	
	/**
	 * init
	 */
	public PortfolioInfo( String fiatType, BigDecimal amountInvested )
	{
		this.fiatType 		= fiatType;
		this.amountInvested  = amountInvested;
	}

	public String getFiatType() {
		return fiatType;
	}

	public void setFiatType(String fiatType) {
		this.fiatType = fiatType;
	}

	public BigDecimal getAmountInvested() {
		return amountInvested;
	}

	public void setAmountInvested(BigDecimal amountInvested) {
		this.amountInvested = amountInvested;
	}
}
