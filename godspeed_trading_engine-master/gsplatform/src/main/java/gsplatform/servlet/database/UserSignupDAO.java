package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import gsplatform.servlet.database.UserSignupDAO;
import gsplatform.servlet.database.UserSignupPackage;
import gsplatform.utilities.GS_Utils;
import gsplatform.utilities.PasswordEncryptor;

/**
 * Data access object for user signup
 * @author danielanderson
 *
 */
public class UserSignupDAO extends UserDAOObject {

	/**
	 * Constant for maximum salt value
	 */
	private static final int MAX_SALT = 2147483647;
	
	/**
	 * Constant for length of verificatino string
	 */
	private static final int VERIFICATION_STRING_LENGTH = 50;
	
	/**
	 * Email for signup
	 */
	private String email;

	/**
	 * Password for signup
	 */
	private String password;
	
	/**
	 * Explicit construct with just email - should only be used when checking verification token
	 * @param email
	 */
	public UserSignupDAO( String username, String email )
	{
		super( username );
		this.email = email;
	}
	
	/**
	 * Explicit constructor
	 * @param username
	 * @param email
	 * @param password
	 */
	public UserSignupDAO( String username, String email, String password )
	{
		super( username );
		this.email    = email;
		this.password = password;
	}
	
	/**
	 * Access database to search for a pre-existing username
	 * @param username
	 * @param email
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public UserSignupPackage profileIsAvailable( String username, String email ) throws SQLException, ClassNotFoundException
	{
		
		String emailQuery    = "SELECT * FROM UserLogin WHERE email = ?";
		String usernameQuery = "SELECT * FROM UserLogin WHERE username = ?";
		
		PreparedStatement pEmailStmt     = null;
		PreparedStatement pUsernameStmt  = null;
        
        pUsernameStmt = this.connection.prepareStatement( usernameQuery );
        pUsernameStmt.setString(1, username);
        ResultSet rs = pUsernameStmt.executeQuery();
        
        if ( rs.next( ) )
        {
        	 	return new UserSignupPackage( username, email, false, "0002: Username is already taken"  );
        }
        
        pEmailStmt    = this.connection.prepareStatement( emailQuery );
        pEmailStmt.setString( 1, email ); 
        
        rs = pEmailStmt.executeQuery();
        
        if ( rs.next( ) )
        {
        	 	return new UserSignupPackage( username, email, false, "0001: Email is already taken" );
        }
        
        
        return new UserSignupPackage( username, email, true, "Success!" );
        
        
	}
	
	/**
	 * Call to signup a user into the database
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public UserSignupPackage signupUser( ) throws ClassNotFoundException, SQLException
	{
		UserSignupPackage usp = profileIsAvailable( this.username, this.email );
		
		if( !usp.getIsSuccessful( ) )
		{
			return usp;
		}
		
		//Genereate salt and verification string
		String verificationString = GS_Utils.generateRandomHexString( 50 );
		
		//Setup query string
		String insertProfileQuery      = "INSERT INTO UserLogin (email, username, password) VALUES ( ?, ?, ? )";
		String insertUserSignupQuery   = "INSERT INTO UserSignup(username, email, verified, signupDate, verificationString) VALUES( ?, ?, ?, ?, ? )";

		//Add the user to userLogin
		PreparedStatement pAddUserStmt = null;
		pAddUserStmt                   = this.connection.prepareStatement( insertProfileQuery );
		pAddUserStmt.setString( 1, this.email ); 
		pAddUserStmt.setString( 2, this.username );
		pAddUserStmt.setString( 3, PasswordEncryptor.encryptPassword( this.password ) );
		pAddUserStmt.executeUpdate( );
		
		//Add the user to user signup
		PreparedStatement pSignupUser = null;
		pSignupUser                   = this.connection.prepareStatement( insertUserSignupQuery );
		pSignupUser.setString( 1, this.username );
		pSignupUser.setString( 2, this.email );
		pSignupUser.setBoolean( 3, false );
		pSignupUser.setString( 4, new Date( ).toString( ) );
		pSignupUser.setString( 5, verificationString );
		pSignupUser.executeUpdate( );
		
		UserSignupPackage userSignupPackage = new UserSignupPackage( username, email, true, "Success!" );
		userSignupPackage.setVerificationToken( verificationString );
		return userSignupPackage;
		
	}
	
	/**
	 * Public method to verify a user - sets the verified flag to true
	 * @param email
	 * @param verificationToken
	 * @return
	 * @throws SQLException
	 */
	public boolean verifyUser( String email, String verificationToken ) throws SQLException
	{
		String verifyQuery                      = "SELECT * FROM UserSignup WHERE email = ? AND verificationString = ?";
		PreparedStatement verificationStatement = null;
		verificationStatement                   = this.connection.prepareStatement( verifyQuery );
		
		verificationStatement.setString( 1, this.email );
		verificationStatement.setString( 2, verificationToken );
		ResultSet resultSet = verificationStatement.executeQuery( );
		
		//Token matched - update verified column set to true
		if( resultSet.next( ) )
		{
			String updateQuery 				  = "UPDATE UserSignup SET verified = ? WHERE email = ?";
			PreparedStatement updateStatement  = null;
			updateStatement					  = this.connection.prepareStatement( updateQuery );
			updateStatement.setBoolean( 1, true );
			updateStatement.setString( 2, this.email );
			updateStatement.executeUpdate( );
			return true;
		}
		
		return false;
	}
}
