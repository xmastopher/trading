package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Data access for grabbing messaging information with respect to a user
 * @author danielanderson
 *
 */
public class MessagesDAO extends UserDAOObject
{
	
	/**
	 * Explicit constructor to initialize with username
	 * @param username
	 */
	public MessagesDAO( String username )
	{
		super( username );
	}
	
	/**
	 * Call to grab all active conversations for a user
	 * @return	A List of all Conversations sorted by timestamp 
	 * @throws SQLException 
	 */
	public List< Conversation > getConversations( ) throws SQLException
	{
		//Grab all messages grouped by conversation and sorted by timestamp for this.user that are not deleted
 		String query = "SELECT DISTINCT * FROM messages WHERE username = ? or recipientUsername = ? and deleted != true ORDER BY conversationID DESC";
		
		PreparedStatement preparedStatement = null;
		preparedStatement  = this.connection.prepareStatement( query );
		preparedStatement.setString( 1, this.username );
		preparedStatement.setString( 2, this.username );
		
		ResultSet resultSet = preparedStatement.executeQuery( );
		
		List< Conversation > conversations = new ArrayList< Conversation >( );
		Boolean hasConversation = resultSet.next( ) ;
		
		while( hasConversation )
		{
			
			//Get the conversation and associated recipient
			int conversationId = resultSet.getInt( "conversationID" );
			String toUser    = resultSet.getString( "recipientUsername" );
			String fromUser  = resultSet.getString( "username" );
			
			//Grab the username that does not equal this user
			String otherUser = toUser.equals( this.username ) ? fromUser : toUser;
			
			//Create the conversation with most recent timestamp and convo ID
			Conversation conversation = new Conversation( otherUser, resultSet.getTimestamp( "messageTime" ), conversationId );
			
			//Iterate over all messages in grouped conversation
			while( hasConversation && resultSet.getInt( "conversationID" ) == conversationId )
			{
				
				toUser           = resultSet.getString( "recipientUsername" );
				fromUser         = resultSet.getString( "username" );
				String subject   = resultSet.getString( "subject" );
				String body      = resultSet.getString( "message" );
				Date timestamp   = resultSet.getTimestamp( "messageTime" );
				
				
				Message message  = new Message( subject, body, fromUser, toUser, timestamp );
				conversation.addMessage( message );
				
				hasConversation  = resultSet.next( );
			}
			
			conversation.sortMessages( );
			//Add conversation to return list
			conversations.add( conversation );
			
		}
		
		//Sort the list - sorted by most recent to least recent convo and return list
		Collections.sort( conversations, Collections.reverseOrder( ) );
		return conversations;
	}
	
	/**
	 * Public method called that writes a message to the DB
	 * based on associated conversation
	 * @throws SQLException 
	 */
	public void sendMessage( int messageId, int conversationId, String toUser, String message, String subject, String timestamp ) throws SQLException
	{
		PreparedStatement ps = null;
		
		String sql = "INSERT INTO Messages VALUES(?,?,?,?,?,?,?,?)";
		
		ps = this.connection.prepareStatement( sql );
		ps.setInt( 1, messageId );
		ps.setInt( 2, conversationId );
		ps.setString( 3, this.username );
		ps.setString( 4, toUser );
		ps.setString( 5, message );
		ps.setString( 6, subject );
		ps.setString( 7, timestamp );
		ps.setBoolean(8, false);
		
		ps.executeUpdate( );
		
	}
	
	/**
	 * Public method to delete a message
	 * @param messageId
	 * @param conversationId
	 * @throws SQLException 
	 */
	public void deleteMessage( int messageId, int conversationId ) throws SQLException
	{
		String sql = "UPDATE Messages SET deleted = true WHERE ID = ? and conversationId = ? and username = ?";
		PreparedStatement ps = this.connection.prepareStatement( sql );
		ps.setInt( 1, messageId );
		ps.setInt( 2, conversationId );
		ps.setString( 3, this.username );
		ps.executeUpdate( );
		
	}
}
