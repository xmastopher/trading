package gsplatform.servlet.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserSignalsDAO extends UserDAOObject
{
	
	/**
	 * Explicit constructor which builds the rest of the primary key
	 */
	public UserSignalsDAO( String username ) 
	{
		super( username );
	}
	
	
	/**
	* DAO function to add an additional signal to a user's dashboard
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	*
	**/
	public void addSignal( int botId ) throws ClassNotFoundException, SQLException
	{
		String query         = "INSERT INTO UserSignals( username, botId ) VALUES( ?, ? )";
		PreparedStatement ps = null;
		ps = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		ps.setInt( 2, botId );
		ps.executeQuery( );
	}
	
	/**
	* DAO function to add an additional signal to a user's dashboard
	* @return false if signal does not exist or failure
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	*
	**/
	public boolean removeSignal( int botId ) throws ClassNotFoundException, SQLException
	{
		String query         = "DELETE FROM UserSignals WHERE botId = ? AND username = ?";
		PreparedStatement ps = null;
		ps                   = this.connection.prepareStatement( query );
		ps.setInt( 1, botId );
		ps.setString( 2, this.username );
		return ps.executeUpdate( ) > 0;
	}
	
	public List< Integer > getSignals( ) throws SQLException {
		String query         = "DELETE id FROM UserSignals WHERE username = ?";
		PreparedStatement ps = null;
		ps                   = this.connection.prepareStatement( query );
		ps.setString( 1, this.username );
		
		ResultSet resultSet = ps.executeQuery( );
		
		List< Integer > ids = new ArrayList< Integer >( );
		
		while( resultSet.next( ) ){
			ids.add( resultSet.getInt( "botId") );
		}
		
		return ids;
	}
	
}