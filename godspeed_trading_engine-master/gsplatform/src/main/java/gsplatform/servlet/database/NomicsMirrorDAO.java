package gsplatform.servlet.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class NomicsMirrorDAO 
{
	/**
	 * The database to user - test or prod
	 */
	protected static final String DATABASE = "nomicsmirror";
	
	/**
	 * The database connection - should be opened and closed manually
	 */
	protected Connection connection;
	
	/**
	 * Emptry constructor used in a small set of cases
	 */
	public NomicsMirrorDAO( ) { ; }
	
	/**
	 * Manually call before a query sequence to open a connection
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void openConnection( ) throws ClassNotFoundException, SQLException
	{
		this.connection = GodspeedDatabaseConnect.getConnection( DATABASE );
	}
	
	/**
	 * Manually call at the end of a query sequence to close the connection
	 * @throws SQLException 
	 */
	public void closeConnection( ) throws SQLException
	{
		this.connection.close( );
	}
	
	/**
	 * Returns daily candle from exchange, base, quote
	 * @return
	 * @throws SQLException 
	 */
	public String getDailyCandles( String exchange, String base, String quote ) throws SQLException {
		
		String query = "SELECT * from NomicsDailyCandles WHERE exchange = ? AND base = ? AND quote = ?";
		PreparedStatement ps = this.connection.prepareStatement( query );
		ps.setString( 1, exchange );
		ps.setString( 2, base );
		ps.setString( 3, quote );
		
		ResultSet rs = ps.executeQuery( );
		
		if( rs.next( ) ) {
			return rs.getString( "candles" );
		}
		
		return "[]";
	}
	
	/**
	 * Returns all markets by exchange where exchange is the key and a json object
	 * is the value
	 * @return
	 * @throws SQLException 
	 * @throws JSONException 
	 */
	public Map< String, ArrayList< String > > getAllMarkets( ) throws SQLException, JSONException {
		
		String query = "SELECT * from NomicsMarkets";
		PreparedStatement ps = this.connection.prepareStatement( query );
		
		Map< String, ArrayList< String > > returnMap = new HashMap< String, ArrayList< String > >( );
		
		ResultSet rs = ps.executeQuery( );
		
		while( rs.next( ) ) {
			
			String exchange = rs.getString( "exchange" );
			String base     = rs.getString( "base" );
			String quote    = rs.getString( "quote" );
			String market   = rs.getString( "market" );
			
			if( !returnMap.containsKey( exchange) ) {
				returnMap.put( exchange, new ArrayList< String >( ) );
			}
			
			JSONObject marketJSON = new JSONObject( );
			marketJSON.put( "base", base );
			marketJSON.put( "quote", quote );
			marketJSON.put( "exchange", exchange );
			marketJSON.put( "market", market );
			
			returnMap.get( exchange ).add( marketJSON.toString( ) );
		}
		
		return returnMap;
		
	}
	
	/**
	 * Returns all the markets currently available at an exchange
	 * @throws SQLException 
	 * @throws JSONException 
	 * 
	 */
	public List< String > getMarketsFromExchange( String exchange ) throws SQLException, JSONException {
		
		String query = "SELECT base,quote,market from NomicsMarkets WHERE exchange = ?";
		PreparedStatement ps = this.connection.prepareStatement( query );
		
		ps.setString( 1, exchange );
		
		ResultSet rs = ps.executeQuery( );
		
		List< String > markets = new ArrayList< String >( );
		
		while( rs.next( ) ) {
			String base     = rs.getString( "base" );
			String quote    = rs.getString( "quote" );
			String market   = rs.getString( "market" );
			
			JSONObject marketJSON = new JSONObject( );
			marketJSON.put( "base", base );
			marketJSON.put( "quote", quote );
			marketJSON.put( "exchange", exchange );
			marketJSON.put( "market", market );
			
			markets.add( marketJSON.toString( ) );
		}
		
		return markets;
	}
	
	/**
	 * Returns the market which maps to provided exchange,base and quote
	 * @throws SQLException 
	 * @throws JSONException 
	 * 
	 */
	public String getMarketsFromExchangeBaseQuote( String exchange, String base, String quote ) throws SQLException, JSONException {
		
		String query = "SELECT market FROM NomicsMarkets WHERE exchange = ? AND base = ? AND quote = ?";
		PreparedStatement ps = this.connection.prepareStatement( query );
		
		ps.setString( 1, exchange );
		ps.setString( 2, base );
		ps.setString( 3, quote );
		
		ResultSet rs = ps.executeQuery( );
		
		if( rs.next( ) ) {

			return rs.getString( "market" );
		}
		
		return null;
	}
	
	/**
	 * Returns all intersected markets as json strings given two exchanges
	 * @param exchangeOne
	 * @param exchangeTwo
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	public List< String > getMarketsIntersections( String exchangeOne, String exchangeTwo ) throws SQLException, JSONException {
		
		String query = "SELECT base,quote,exchangeOneMarket,exchangeTwoMarket from NomicsMarketsIntersections WHERE exchangeOne = ? AND exchangeTwo = ?";
		PreparedStatement ps = this.connection.prepareStatement( query );
		
		ps.setString( 1, exchangeOne );
		ps.setString( 2, exchangeTwo );
		
		ResultSet rs = ps.executeQuery( );
		
		
		List< String > markets = new ArrayList< String >( );
		
		while( rs.next( ) ) {
			String base      = rs.getString( "base" );
			String quote     = rs.getString( "quote" );
			String marketOne = rs.getString( "exchangeOneMarket" );
			String marketTwo = rs.getString( "exchangeTwoMarket" );
			
			JSONObject market = new JSONObject( );
			market.put( "base", base );
			market.put( "quote", quote );
			market.put( "exchangeOne", exchangeOne );
			market.put( "exchangeTwo", exchangeTwo );
			market.put( "exchangeOneMarket", marketOne );
			market.put( "exchangeTwoMarket", marketTwo );
			
			markets.add( market.toString( ) );
		}
		
		return markets;
	}
	
}
