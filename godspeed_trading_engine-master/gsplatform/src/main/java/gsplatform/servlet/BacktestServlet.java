package gsplatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.api.BacktestAPI;
import gsplatform.servlet.database.AuthenticationManager;
import gsplatform.utilities.GS_Utils;


@WebServlet(description = "Perform a strategy backtest", urlPatterns = { "/backtestservlet" , "/backtest" } )
public class BacktestServlet extends HttpServlet {

	private static final long serialVersionUID = 2L;
	public static final String HTML_START="<html><body>";
	public static final String HTML_END="</body></html>";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BacktestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		String params      = GS_Utils.getJsonFromRequest( request );

		try
		{
			JSONObject verifiedParams     = AuthenticationManager.verifySession( new JSONObject( params ) );
			
			if( !verifiedParams.getBoolean( "success") ) 
			{
				writer.println( verifiedParams.toString( ) );
			}
			else {
				BacktestAPI backtestAPI = new BacktestAPI( new JSONObject( params ) );
				JSONObject results      = backtestAPI.createJSON( );
				writer.println( results.toString( ) );
			}
		} 
		catch (JSONException e) 
		{
			System.out.println( params );
			System.out.println( e.getMessage( ) );
			throw new ServletException();
		} 
		catch (Exception e) 
		{
			System.out.println( params );
			System.out.println( e.getMessage( ) );
			System.out.println( e.getStackTrace( ) );
			throw new ServletException();
		}
	}
    
    
}
