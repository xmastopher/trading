package gsplatform.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.account.Balance;
import org.knowm.xchange.dto.account.Wallet;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.service.account.AccountService;
import org.knowm.xchange.service.marketdata.MarketDataService;

import gsplatform.servlet.database.ExchangeAPIDAO;

public class Portfolio 
{
	private static MarketDataService coinMarketCap = null;
	private Exchange exchange;
	private String exchangeName;
	private List< CoinAsset > wallet;
	private String fiatType;
	private double totalFiat;
	
	public class CoinAsset implements Comparable< CoinAsset >
	{
		private String coin;
		private BigDecimal amount;
		private String location;
		private BigDecimal percentOfPortfolio;
		private BigDecimal fiatAmount;
		private BigDecimal fiatValue;
		
		public CoinAsset( String coin, BigDecimal amount, String location )
		{
			this.coin     = coin;
			this.amount   = amount;
			this.location = location;
			this.fiatAmount = null;
			this.fiatValue  = null;
		}
		
		public String getCoin( )
		{
			return this.coin;
		}
		
		public BigDecimal getAmount( )
		{
			return this.amount;
		}
		
		public void setPercentOfPortfolio( BigDecimal percent )
		{
			this.percentOfPortfolio = percent;
		}

		public BigDecimal toFiat( String fiatType ) throws IOException
		{
			if( this.fiatAmount == null )
			{
				CurrencyPair currencyPair = new CurrencyPair( this.coin, fiatType );
				Ticker ticker = Portfolio.coinMarketCap.getTicker( currencyPair );
				this.fiatAmount = ticker.getLast( ).multiply( this.amount );
				this.fiatValue  = ticker.getLast( );
				return this.fiatAmount;
			}
			
			return this.fiatAmount;
		}
		
		public BigDecimal getPercentOfPortfolio()
		{
			return this.percentOfPortfolio;
		}
		
		public BigDecimal getFiatAmount() {
			return fiatAmount;
		}

		public void setFiatAmount(BigDecimal fiatAmount) {
			this.fiatAmount = fiatAmount;
		}

		public BigDecimal getFiatValue() {
			return fiatValue;
		}

		public void setFiatValue(BigDecimal fiatValue) {
			this.fiatValue = fiatValue;
		}

		public void setCoin(String coin) {
			this.coin = coin;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getLocation() 
		{
			return this.location;
		}
		
		@Override
		public int compareTo( CoinAsset rhsObject ) 
		{
			return this.percentOfPortfolio.compareTo( rhsObject.percentOfPortfolio );
		}
		
	}
	
	public Portfolio( String exchangeString, String fiatType, String username ) throws IOException, ClassNotFoundException, SQLException
	{
		
		if ( Portfolio.coinMarketCap == null )
			Portfolio.coinMarketCap = ExchangeHandler.getExchangePublic( "COIN_MARKET_CAP" ).getMarketDataService( );
		
		this.fiatType     = fiatType;
		this.totalFiat    = 0;
		this.exchangeName = exchangeString;
		this.wallet       = new ArrayList< CoinAsset >( );
		
		ExchangeAPIDAO exchangeApiDao = new ExchangeAPIDAO( username, exchangeString );
		this.exchange                 = exchangeApiDao.getExchangeObject( );
		
		fillWallet( );
		updateTotalFiat( );
		
	}
	
	public Portfolio( String exchangeString, String fiatType, JSONObject jsonWallet ) throws JSONException, IOException
	{
		if ( Portfolio.coinMarketCap == null )
			Portfolio.coinMarketCap = ExchangeHandler.getExchangePublic( "COIN_MARKET_CAP" ).getMarketDataService( );
		
		this.exchangeName = exchangeString;
		this.fiatType     = fiatType;
		this.wallet       = new ArrayList< CoinAsset >( );
		this.totalFiat    = 0;
		
		JSONObject ethPortion = jsonWallet.getJSONObject("ETH");
		BigDecimal balance = new BigDecimal( ethPortion.getDouble( "balance" ) );
		this.wallet.add( new CoinAsset( "ETH", balance, this.exchangeName ) );
		
		JSONArray tokens      = jsonWallet.getJSONArray( "tokens" );
		
		for (int i = 0; i < tokens.length(); i++) 
		{
			  JSONObject tokenInfo  = tokens.getJSONObject( i ).getJSONObject( "tokenInfo" );
			  String symbol  	   = tokenInfo.getString( "symbol" );
			  
			  String balanceText	   = String.valueOf( tokens.getJSONObject( i ).getDouble( "balance" ) );
			  int decimals         = Integer.valueOf( tokenInfo.getString( "decimals" ) );
			  
			  if ( balanceText.contains("E") )
				  balance = reformatValue( balanceText, decimals );
			  else
				  balance = new BigDecimal( tokens.getJSONObject( i ).getDouble( "balance" ) );
			  this.wallet.add( new CoinAsset( symbol, balance, this.exchangeName ) );
		}
		
		updateTotalFiat( );
	}
	
	private BigDecimal reformatValue( String balance, int decimals )
	{
		String[] split   = balance.split("E");
		Integer exponent = Integer.valueOf( split[ 1 ] );
		exponent -= decimals;
		BigDecimal bd = new BigDecimal( split[0] + "E" + String.valueOf( exponent ) );
		return bd;
	}
	public void fillWallet( ) throws IOException
	{
		Set< String > checkedCoins         = new HashSet< String >( );
		List< CurrencyPair > currencyPairs = this.exchange.getExchangeSymbols( );
        AccountService accountService      = this.exchange.getAccountService();
        AccountInfo accountInfo 		      = accountService.getAccountInfo();
        Wallet wallet 				      = accountInfo.getWallet();
        
        //Get available balance
        
        for( CurrencyPair currencyPair : currencyPairs )
        {
        		if( !checkedCoins.contains( currencyPair.base.toString( ) ) )
        		{
        			Balance balance 			     = wallet.getBalance( new Currency( currencyPair.base.toString( ) ) );
        			BigDecimal totalBalance       = balance.getTotal();
        			
        			if( totalBalance.doubleValue( ) > 0 )
        			{
        				checkedCoins.add( currencyPair.base.toString( ) );
        				this.wallet.add( new CoinAsset( currencyPair.base.toString( ), totalBalance, this.exchangeName ) );
        			}
        			
        		}
        }
	}
	
	public void updateTotalFiat( ) throws IOException
	{
		
		for( CoinAsset coinAsset : wallet )
		{
			BigDecimal fiatAmount = coinAsset.toFiat( this.fiatType );
			this.totalFiat += fiatAmount.doubleValue( );
			System.out.println( coinAsset.getCoin() + ": " + fiatAmount );
		}
	}
	
	public void addCoinAmountPair( String coin, BigDecimal amount)
	{
		this.wallet.add( new CoinAsset( coin, amount, this.exchangeName ) ); 
	}
	
	public CoinAsset findCoin( String coinSymbol ) 
	{
		for( CoinAsset coinAsset : this.wallet )
		{
			if( coinAsset.getCoin( ).equalsIgnoreCase( coinSymbol ) )
			{
				return coinAsset;
			}
		}
		
		return null;
	}
	
	public double getTotalFiat()
	{
		return this.totalFiat;
	}
	
	public List< CoinAsset > getWallet()
	{
		return this.wallet;
	}
	
	public static void main( String args[ ] ) throws IOException
	{
		
	}
	
	
}
