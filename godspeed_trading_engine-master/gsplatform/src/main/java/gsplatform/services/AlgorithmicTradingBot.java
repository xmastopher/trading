package gsplatform.services;

import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.algorithms.TradingAlgorithm.SIGNAL;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.ExchangeAPIDAO;
import gsplatform.utilities.TradingParameters;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.account.Wallet;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.dto.trade.MarketOrder;
import org.knowm.xchange.service.account.AccountService;
import org.knowm.xchange.service.trade.TradeService;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParamCurrencyPair;

public class AlgorithmicTradingBot extends AutomatedTrader 
{
	/**
	 * Reference to logger instance
	 */
	private static final Logger LOGGER = Logger.getLogger( AlgorithmicTradingBot.class.getName( ) );
	
	/**
	 * Used to set scale for big decimals
	 */
	private static final int DECIMAL_PRECISION = 8;
	
	/**
	 *  The trading algorithm to deploy
	 */
	private TradingAlgorithm tradingAlgorithm;
	
	/**
	 *  The base currency in the pair
	 */
	private String baseCurrency;
	
	/**
	 *  The counter currency in the pair
	 */
	private String counterCurrency;
	
	/**
	 *  The exchange object used to perform the trades
	 */
	private Exchange exchange;
	
	/**
	 * Reference to the user's wallet
	 */
	private Wallet userWallet;
	
	/**
	 * The amount of the user's stack he/she is willing to trade
	 */
	private double tradingPortion;
	
	/**
	 * Is this bot currently waiting for an order to be filled?
	 */
	private boolean waitingForfill;
	
	/**
	 * Keeps track of last non-zero price
	 */
	private BigDecimal lastNonZeroPrice;
	
	/**
	 * Small internal class to keep track of the last trade order, we will
	 * need to constantly check that this is filled after making an order
	 * before proceeding
	 * @author danielanderson
	 *
	 */
	private class TradeOrder
	{
		protected String id;
		protected CurrencyPair currencyPair;
		
		public TradeOrder( String id, CurrencyPair currencyPair )
		{
			this.id           = id;
			this.currencyPair = currencyPair;
		}
	}
	
	/**
	 * Reference to the last trade order
	 */
	private TradeOrder lastTradeOrder;
	
	/**
	 * Constructor for an Algorithmic based trading bot, this differs from other bots in that it specifically focuses
	 * on trading a BASE/COUNTER pair with a specified trading algorithm.  The exchange must also be specified
	 * 
	 * @param tradingAlgorithm		The trading algorithm object the bot will use for signals
	 * @param baseCurrency			The base currency symbol for the trading pair - BASE/COUNTER
	 * @param counterCurrency		The counter currency symbol for the trading pair - BASE/COUNTER
	 * @param username				Username of the pre-authenticated user creating the bot
	 * @param exchangeName			
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public AlgorithmicTradingBot( TradingAlgorithm tradingAlgorithm, String baseCurrency, String counterCurrency, 
								 String username, int botId, String exchangeName, String mode ) throws IOException, ClassNotFoundException, SQLException
	{
		super( mode, username, botId );
		this.tradingAlgorithm        = tradingAlgorithm;
		this.baseCurrency 			= baseCurrency;
		this.counterCurrency 		= counterCurrency;
		
		ExchangeAPIDAO exchangeApiDao = new ExchangeAPIDAO( username, exchangeName );
		exchangeApiDao.openConnection( );
		this.exchange       = exchangeApiDao.getExchangeObject();
		this.lastTradeOrder = null;
		exchangeApiDao.closeConnection( );
		updateWallet();
	}
	
	/**
	 * setup method used for testing
	 * @param exchange
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 */
	public AlgorithmicTradingBot( String username, int botId, String exchange, String baseCurrency, String counterCurrency ) throws ClassNotFoundException, SQLException, IOException
	{
		super( null, username, botId );
		ExchangeAPIDAO exchangeApiDao = new ExchangeAPIDAO( username, exchange );
		exchangeApiDao.openConnection( );
		this.baseCurrency    = baseCurrency;
		this.counterCurrency = counterCurrency;
		this.exchange        = exchangeApiDao.getExchangeObject();
		this.lastTradeOrder  = null;
		exchangeApiDao.closeConnection( );
		updateWallet( );
	}
	
	/**
	 * Internal method called to retrieve the AccountService and wallet from user
	 * to update internal class wallet
	 * @throws IOException
	 */
	private void updateWallet( ) throws IOException
	{
        //Grab wallet information
        AccountService accountService = this.exchange.getAccountService();
        AccountInfo accountInfo 		 = accountService.getAccountInfo();
        this.userWallet 				 = accountInfo.getWallet();
	}
	
	public AccountService getUserAccountService( )
	{
		return this.exchange.getAccountService( );
	}
	
	public TradeService getTradeService( )
	{
		return this.exchange.getTradeService( );
	}
	
	public void setLastTrade( String tradeId, CurrencyPair currencyPair )
	{
		this.lastTradeOrder = new TradeOrder( tradeId, currencyPair );
	}
	
	/**
	 * Called every time a new candle arrives
	 * @throws Exception 
	 */
	@Override
	public BotLog doIteration( TradingParameters tradingParameters ) throws Exception
	{
		
		//Check if bot is waiting for fill, if the fill was
		//not satisifed we do nothing - of course if the mode
		//is market we do not need to do this
		if( this.mode == MODE.ORDER && this.waitingForfill )
		{
			this.waitingForfill = checkFill( );
			
			if( this.waitingForfill )
			{
				return null;
			}
		}
		
		TradingAlgorithm.SIGNAL signal = tradingAlgorithm.doIteration( tradingParameters );
		BigDecimal lastPrice           = tradingParameters.getAsBigDecimal( "last_price", DECIMAL_PRECISION );
		
		//Set last price to the last non-zero price in situations where there is no trading
		lastPrice = lastPrice != BigDecimal.ZERO ? lastPrice : this.lastNonZeroPrice;
		
		//Signal is buy, calculate buy amount and perform a buy
		if( signal == SIGNAL.BUY )
		{
			
			//Grab the amount to buy
			updateWallet( );
	        BigDecimal amountToBuy = calculateTradingAmount( lastPrice, new Currency( this.counterCurrency ), 
	        												    this.userWallet.getBalance( new Currency( this.counterCurrency ) ).getAvailable( ) );
			
	        //Check for market-buy mode - buy at the last price - regardless of slippage
			if( this.mode == AutomatedTrader.MODE.MARKET )
			{
				doMarketBuy( amountToBuy, new CurrencyPair( this.baseCurrency, this.counterCurrency ) );
			}
			
			//Assume order mode - sets a buy order at last price
			else
			{
				doBuyOrder( amountToBuy, lastPrice, new CurrencyPair( this.baseCurrency, this.counterCurrency ) );
			}
			
		}
		
		//Signal is sell, calculate the sell amount and perform a sell
		else if( signal == SIGNAL.SELL )
		{
			//Grab the amount to buy
			updateWallet( );
	        BigDecimal amountToSell = calculateTradingAmount( lastPrice, new Currency( this.counterCurrency ), 
				    											   this.userWallet.getBalance( new Currency( this.baseCurrency ) ).getAvailable( ) );
	        
	        //Buy at market price regardless of slippage
			if( this.mode == AutomatedTrader.MODE.MARKET )
			{
				this.doMarketSell( amountToSell,  new CurrencyPair( this.baseCurrency, this.counterCurrency )  );
			}
			
			//Assume order mode - sets a sell order at last price
			else
			{
				this.doMarketSell( amountToSell, new CurrencyPair( this.baseCurrency, this.counterCurrency ) );
			}	
		}
		
		if( lastPrice != BigDecimal.ZERO )
			this.lastNonZeroPrice = lastPrice;
			
		LOGGER.info( this.createLogMessage( ) );
		
		return null;
	}
	
	/**
	 * Called at the start of each iteration as long as the bot is waiting
	 * for an order to be filled
	 * @return TRUE if filled, false, otherwise
	 * @throws IOException 
	 */
	public boolean checkFill( ) throws IOException
	{
		
		if( this.lastTradeOrder == null )
			return false;
		
		TradeService tradeService     = exchange.getTradeService();
		DefaultOpenOrdersParamCurrencyPair params = new DefaultOpenOrdersParamCurrencyPair( new CurrencyPair( this.baseCurrency, this.counterCurrency ) );
		List< LimitOrder > openOrders = tradeService.getOpenOrders( params ).getOpenOrders();
		
		String id				 = this.lastTradeOrder.id;
		
		//Iterate over all open orders and search for id - if not exists order was filled
		for( LimitOrder limitOrder : openOrders )
		{
			String orderId = limitOrder.getId( );
			
			if( orderId.equals( id ) )
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Internal method for calculating the amount of the base or counter currency
	 * to either buy or sell based on the trading portion this bot is set to
	 * @param lastPrice
	 * @param currency
	 * @return
	 * @throws IOException
	 */
	public BigDecimal calculateTradingAmount( BigDecimal lastPrice, Currency currency, BigDecimal availableAmount ) throws IOException
	{
		//Do not portion the sell
		if( currency.toString( ).equals( this.baseCurrency ) )
		{
			return  availableAmount.setScale( DECIMAL_PRECISION, BigDecimal.ROUND_DOWN ).stripTrailingZeros( );
		}
		
		BigDecimal portionOfAvailable = availableAmount.multiply( new BigDecimal( this.tradingPortion ) );
		
		double a = portionOfAvailable.doubleValue( ) / lastPrice.doubleValue( );
		BigDecimal amount = new BigDecimal( a );
		return amount.setScale( DECIMAL_PRECISION, BigDecimal.ROUND_DOWN ).stripTrailingZeros( );
	}
	
	/**
	 * Performs a market buy
	 */
	@Override
	public void doMarketBuy( BigDecimal amount, CurrencyPair currencyPair ) throws Exception {
		
		TradeService tradeService = exchange.getTradeService();
		MarketOrder  marketOrder  = new MarketOrder( OrderType.BID, amount, currencyPair ); 
		tradeService.placeMarketOrder( marketOrder );
		
	}

	/**
	 * Performs a market sell
	 */
	@Override
	public void doMarketSell( BigDecimal amount, CurrencyPair currencyPair ) throws Exception {
		
		TradeService tradeService = exchange.getTradeService();
		MarketOrder  marketOrder  = new MarketOrder( OrderType.ASK, amount, currencyPair ); 
		tradeService.placeMarketOrder( marketOrder );
		
	}

	/**
	 * Performs a buy order and stores it in last order
	 */
	@Override
	public void doBuyOrder( BigDecimal amount, BigDecimal rate, CurrencyPair currencyPair ) throws Exception {
		
		TradeService tradeService   = exchange.getTradeService();
        LimitOrder   limitOrder     = new LimitOrder( OrderType.BID, amount, amount.multiply( rate ), 
        							     currencyPair, "", new Date(), rate );
        tradeService.placeLimitOrder( limitOrder );
        
        String id = getLastOrderId( tradeService );
        
        if( !id.equals("") )
        {
        		this.lastTradeOrder = new TradeOrder( id, limitOrder.getCurrencyPair( ) );
			this.waitingForfill = true;
        }
	}
	
	@Override
	public String createLogMessage() {
		String userString = this.owner + "-" + this.botId;
		return "[BOTLOG:" + userString + "] " + "WALLET: " + this.userWallet.toString( ) + ", MESSAGE: " + tradingAlgorithm.getLogMessage( );
	}

	/**
	 * Performs a sell order and stores it in last order
	 */
	@Override
	public void doSellOrder( BigDecimal amount, BigDecimal rate, CurrencyPair currencyPair ) throws Exception {
		
		TradeService tradeService   = exchange.getTradeService();
        LimitOrder   limitOrder     = new LimitOrder( OrderType.ASK, amount, amount, 
        							     currencyPair, "", new Date(), rate );
   		tradeService.placeLimitOrder( limitOrder );
        String id = getLastOrderId( tradeService );
        
        if( !id.equals("") )
        {
        		this.lastTradeOrder = new TradeOrder( id, limitOrder.getCurrencyPair( ) );
			this.waitingForfill = true;
        }
	}
	
	public String getLastOrderId( TradeService tradeService ) throws IOException
	{
		DefaultOpenOrdersParamCurrencyPair params = new DefaultOpenOrdersParamCurrencyPair( new CurrencyPair( "ETH", "BTC" ) );
		List< LimitOrder > limitOrders = tradeService.getOpenOrders( params ).getOpenOrders( );
		
		if( limitOrders.size( ) == 0 )
			return "";
		
		return limitOrders.get( 0 ).getId( );
	}
	
	public void setTradingPortion( double tradingPortion )
	{
		this.tradingPortion = tradingPortion;
	}

	@Override
	public int getPreprossingValue() {
		return this.tradingAlgorithm.getPreprocessingValue( );
	}

}
