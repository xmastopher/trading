package gsplatform.services;

import java.math.BigDecimal;
import java.util.HashMap;
import gsplatform.algorithms.SignalStrategy;
import gsplatform.jobs.GS_Logger;
import gsplatform.servlet.database.UserBot;
import gsplatform.signals.BollingerBandsBuySignal;
import gsplatform.signals.BollingerBandsSellSignal;
import gsplatform.signals.SMACrossBuySignal;
import gsplatform.signals.SMACrossSellSignal;
import gsplatform.signals.Signal;

/**
 * Singleton class that keeps track of all bot threads organized in two different hash tables:
 * 1. tradingBots   => all bots actually trading on exchanges
 * 2. simulatedBots => all bots that are 'fake' trading on exchanges
 * 
 * All keys are the same and are constructed as follows:
 * exchange-kline-market ie. binance-1h-ENJ/BTC
 * @author danielanderson
 *
 */
public class BotThreadHandler 
{
	/**
	 * Reference to logger instance
	 */
	public static GS_Logger LOGGER = GS_Logger.getLogger( );
	
    /**
     * Static member for singleton access
     */
    private static BotThreadHandler singletonInstance;
    
	/**
	 * Lookup for real bots
	 */
	private HashMap< String, BotThread > tradingBots;
	
	/**
	 * Lookup for real bots - the actual Thread object
	 */
	private HashMap< String, Thread > tradingBotsThreads;
	
	/**
	 * Lookup for simulate bots
	 */
	private HashMap< String, BotThread > simulatedBots;
	
	/**
	 * Lookup for simulated bots - the actual thread
	 */
	private HashMap< String, Thread > simulatedBotsThreads;
    
    /**
     * Provides a global point of access for the singleton delegator
     * @return	The singleton instance
     */
    public static BotThreadHandler getSingletonInstance( ) 
    {
        if ( null == singletonInstance ) 
        {
            singletonInstance = new BotThreadHandler( );
        }
    
        return singletonInstance;
    }
    
    /**
     * Called in unit tests to reset instance
     * after each test. Stops all threads
     * and makes singleton null
     */
    public static void resetSingleton( ) {
    		if( singletonInstance == null ) {
    			return;
    		}
    		else {
    			singletonInstance.stopAlBotThreads( );
    			singletonInstance = null;
    		}
    }
    
    /**
     * Initialized the hash tables
     */
    public BotThreadHandler( )
    {
    		this.tradingBots          = new HashMap< String, BotThread >( );
    		this.simulatedBots        = new HashMap< String, BotThread >( );
    		this.tradingBotsThreads   = new HashMap< String, Thread >( );
    		this.simulatedBotsThreads = new HashMap< String, Thread >( );
    }
    
    /**
     * Call on system stop to stop all 
     * bots that are running
     */
    public void stopAlBotThreads( ) {
	    	//iterating over keys only
	    	for ( String key : this.simulatedBotsThreads.keySet( ) ) {
	    	    this.simulatedBots.get( key ).removeAllBots( );
	    	}
	    	for ( String key : this.tradingBotsThreads.keySet( ) ) {
	    	    this.tradingBots.get( key ).removeAllBots( );
	    	}
    }
    
    /**
     * Add bot to corresponding runnable
     * @param userBot
     * @param automatedTrader
     * @throws Exception 
     */
    public void addBot( UserBot userBot, AutomatedTrader automatedTrader ) throws Exception
    {
    		addBot( userBot.getExchange( ), userBot.getInterval( ), userBot.getMarketBase( ) + "-" + userBot.getMarketQuote( ), automatedTrader, true );
    }
    
    /**
     * Adds a new bot to its corresponding thread 
     * @param exchange
     * @param kline
     * @param automatedTrader
     * @param isSimulated
     * @throws Exception 
     */
    public void addBot( String exchange, String kline, String market, AutomatedTrader automatedTrader, boolean isSimulated ) throws Exception
    {
    		String key    = generateKey( exchange, kline, market ); 
    		String logKey = key + "-" + automatedTrader.getOwner( ) + "-" + automatedTrader.getBotId( );
    		
    		if( isSimulated )
    		{

    			LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "ADDING SIMULATED BOT: " + logKey );
    			
    			if( this.simulatedBots.containsKey( key ) )
    			{
    				this.simulatedBots.get( key ).addBot( automatedTrader );
    			}
    			else
    			{
    				BotThread botThread = new BotThread( kline, exchange, market );
    				botThread.setLog( "SIMULATIONS" );
    				botThread.addBot( automatedTrader );
    				Thread thread = new Thread( botThread );
    				LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "ADDING SIMULATED BOT THREAD: " + key + thread.toString( ) );
    				this.simulatedBotsThreads.put( key, thread );
    				thread.start( );
    				this.simulatedBots.put( key, botThread );
    			}
    			
    		}
    			
    		else
    		{
    			LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "ADDING TRADING BOT: " + logKey );
    			
    			if( this.tradingBots.containsKey( key ) )
    			{
    				this.tradingBots.get( key ).addBot( automatedTrader );
    			}
    			else
    			{
    				BotThread botThread = new BotThread( kline, exchange, market );
    				botThread.setLog( "BOTS" );
    				botThread.addBot( automatedTrader );
    				Thread thread = new Thread( botThread );
    				LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "ADDING TRADING BOT THREAD: " + key + thread.toString( ) );
    				this.tradingBotsThreads.put( key, thread );
    				thread.start( );
    				this.tradingBots.put( key, botThread );
    			}
    		}
    }
    
    /**
     * Remove bot from thread handler
     * @param username
     * @param botId
     * @param exchange
     * @param kline
     * @param market
     * @param isSimulated
     */
    public void removeBot( String username, int botId, String exchange, String kline, String market, boolean isSimulated )
    {
    		String key = generateKey( exchange, kline, market ); 
    		String logKey = key + "-" + username + "-" + botId;

		if( isSimulated )
		{
			LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "REMOVING SIMULATED BOT: " + logKey );
			this.simulatedBots.get( key ).removeBot( username, botId );
			
			if( this.simulatedBots.get( key ).getBots( ).size( ) == 0 )
			{
				LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "KILLING SIMULATED BOT THREAD: " + key + this.simulatedBotsThreads.get( key ).toString( ) );
				this.simulatedBots.remove( key );
				this.simulatedBotsThreads.remove( key );
			}
		}
		else
		{
			LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "REMOVING TRADING BOT: " + logKey );
			this.tradingBots.get( key ).removeBot( username, botId );
			
			if( this.tradingBots.get( key ).getBots( ).size( ) == 0 )
			{
				LOGGER.addMessageEntry( "BOT-THREADS", "BotThreadHandler", "KILLING TRADING BOT THREAD: " + key + this.tradingBotsThreads.get( key ).toString( ) );
				this.tradingBots.remove( key );
				this.tradingBotsThreads.remove( key );
			}
		}
    }
    
    /**
     * Given parameters - return the thread containing all the trainers
     * @param exchange
     * @param kline
     * @param market
     * @param isSimulated
     * @return
     */
    public BotThread getThread( String exchange, String kline, String market, boolean isSimulated )
    {
    		String key = generateKey( exchange, kline, market );
    		
    		if( isSimulated )
    		{
    			return this.simulatedBots.get( key );
    		}
    		
    		return this.tradingBots.get( key );
    }
    
    /**
     * Generates a key that contains the concatenation of provided params delimitted by a '-'
     * @param exchange
     * @param kline
     * @param market
     * @return
     */
    public String generateKey( String exchange, String kline, String market  )
    {
    		return exchange + "-" + kline + "-" + market; 
    }

}
