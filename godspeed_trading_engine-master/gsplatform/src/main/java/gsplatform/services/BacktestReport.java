package gsplatform.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.utilities.GS_Utils;

/**
 * 
 * @author danielanderson
 */
public class BacktestReport {
	
	/**
	 * Make this false to activate the cap for storing array in resulting JSON.  This is specifically for generating examples
	 */
	final static Boolean TEST_MODE = false;
	
	/**
	 * The cap for the result arrays for test mode
	 */
	final static int TEST_MODE_CAP = 20;
	
	/**
	 * Name of the strategy
	 */
	private String strategyName;
	
	/**
	 * Base currency for market tested
	 */
	private String baseCurrency;
	
	/**
	 * Counter currency for market tested
	 */
	private String counterCurrency;
	
	/**
	 * The count of wins associated with the backtesting period
	 */
	private int numberOfWins;
	
	/**
	 * The count of losses associated with the backtesting period
	 */
	private int numberOfLosses;
	
	/**
	 * The initial investment for the backtesting period
	 */
	private double initialInvestment;
	
	/**
	 * The calculated buy and hold values
	 */
	private double buyAndHold;
	
	/**
	 * Current networth for the backtesting period
	 */
	private double currentNetworth;

	/**
	 * The value of the last buy
	 */
	private Double valueOfLastBuy;
	
	/**
	 * The value of the last sell
	 */
	private Double valueOfLastSell;
	
	/**
	 * The mean of the trades
	 */
	private double mean;
	
	/**
	 * Total iterations
	 */
	private int iterations;
	
	/**
	 * ROI
	 */
	private double returnOnInvestment;
	
	/**
	 * The total number of trades
	 */
	private int numberOfTrades;
	
	/**
	 * The win rate for the period
	 */
	private double winRatio;
	
	/**
	 * The loss rate for the period
	 */
	private double lossRatio;
	
	/**
	 * Expectancy at the end of the period
	 */
	private double expectancy;
	
	/**
	 * Total amount on winning trades
	 */
	private double totalWon;
	
	/**
	 * Total amount on losing trades
	 */
	private double totalLost;
	
	/**
	 * The average win in the quote currency amount
	 */
	private double averageWin;
	
	/**
	 * The average loss in the quote currency
	 */
	private double averageLoss;
	
	/**
	 * Volatility measure for the period
	 */
	private double volatilityMeasures;
	
	/**
	 * Max drawdown for the end of the trading period
	 */
	private double maxDrawdown;
	
	/**
	 * The all time high of the portfolio
	 */
	private double allTimeHigh;
	
	/**
	 * The all time low 
	 */
	private double allTimeLow;
	
	/**
	 * All the actions for the entire period
	 */
	private List< String > loggedActions;
	
	/**
	 * The logged networth over the entire period
	 */
	private List< Double > loggedNetworth;
	
	/**
	 * The logged buy and hold values over the entire period
	 */
	private List< Double > loggedBuyAndHold;
	
	/**
	 * The percent gains at each sell
	 */
	private List< Double > percentGains;
	
	/**
	 * All the closing prices
	 */
	private List< Double > priceDataClose;
	
	/**
	 * All the open prices
	 */
	private List< Double > priceDataOpen;
	
	/**
	 * All the indicators
	 */
	private List< JSONObject > indicators;
	
	/**
	 * Was it stopped out at a sell?
	 */
	private List< Boolean > stoppedOut;
	
	/**
	 * Timstamp associated with each iteration
	 */
	private List<String> timestamps;
	
	/**
	 * Are we adding trades
	 */
	private Boolean addTrades;
	
	/**
	 * Names of all parameters
	 */
	private String[] parameterNames;
	
	/**
	 * Names of all parameter values
	 */
	private String[] parameterValues;
	
	/**
	 * The last networth
	 */
	private double lastNetworth;
	
	/**
	 * Explicit constructor to create BacktestReport object
	 * @param strategyName		The name of the strategy
	 * @param baseCurrency		The base currency
	 * @param tradedCurrency		The counter currency
	 * @param initialInvestment  The initial investment 
	 */
	public BacktestReport( String strategyName, String baseCurrency, String counterCurrency, double initialInvestment )
	{
		this.loggedNetworth     = new ArrayList< Double >( );
		this.loggedBuyAndHold   = new ArrayList< Double >( );
		this.priceDataClose     = new ArrayList< Double >( );
		this.priceDataOpen      = new ArrayList< Double >( );
		this.percentGains       = new ArrayList< Double >( );
		this.loggedActions      = new ArrayList< String >( );
		this.timestamps         = new ArrayList< String >( );
		this.stoppedOut         = new ArrayList< Boolean >( );
		this.indicators         = new ArrayList< JSONObject >( );
		this.strategyName       = strategyName;
		this.baseCurrency       = baseCurrency;
		this.counterCurrency    = counterCurrency;
		this.allTimeHigh        = Double.MIN_VALUE;
		this.allTimeLow         = Double.MAX_VALUE;
		this.numberOfTrades     = 0;
		this.numberOfLosses     = 0;
		this.numberOfWins       = 0;
		this.winRatio           = 0;
		this.lossRatio          = 0;
		this.maxDrawdown        = 0;
		this.valueOfLastBuy     = 0.0;
		this.valueOfLastSell    = 0.0;
		this.initialInvestment  = initialInvestment;
		this.currentNetworth    = initialInvestment;
		this.volatilityMeasures = 0;
		this.mean  			    = 0;
		this.iterations         = 0;
		this.buyAndHold         = 0;
		this.addTrades          = false;
		this.averageWin         = 0;
		this.averageLoss        = 0;
		this.lastNetworth       = initialInvestment;
	}
	
	/**
	 * Set param names and values for the tested algorithms
	 * @param parameterNames		List of strings for the param names
	 * @param parameterValues	List of strings for the corresponding values
	 */
	public void setParameters( String [] parameterNames, String[] parameterValues )
	{
		this.parameterNames  = parameterNames;
		this.parameterValues = parameterValues;
	}
	
	/**
	 * Called every iteration to update the trading report
	 * @param tradeType
	 * @param assetValue
	 * @param currentNetworth
	 * @param stopLossHit
	 * @param timestamp
	 */
	public void update( TradingAlgorithm.SIGNAL tradeType, double closePrice, double openPrice, 
						double currentNetworth, boolean stopLossHit, String timestamp, JSONObject signals )
	{
		if( this.buyAndHold == 0 )
		{
			this.buyAndHold = currentNetworth / closePrice;
		}
		
		//Update Logs
		if( tradeType == TradingAlgorithm.SIGNAL.BUY )
		{
			this.valueOfLastBuy = openPrice;
			this.loggedActions.add( "BUY" );
		}
		
		//Update logs
		else if( tradeType == TradingAlgorithm.SIGNAL.SELL ) 
		{
			this.valueOfLastSell = openPrice;
			this.loggedActions.add( "SELL" );
		}
		
		//Holding & Assess for win/loss stats
		else
		{
			this.loggedActions.add( "HOLD" );
		}
		
		//The trade successfully completed check if success or fail
		if( tradeComplete( stopLossHit ) )
		{
			this.stoppedOut.add( stopLossHit );
			this.numberOfTrades++;
			
			if( this.valueOfLastBuy < this.valueOfLastSell ) {
				this.numberOfWins++;
				this.totalWon += ( currentNetworth - this.lastNetworth);
				System.out.println( "WIN: " + (currentNetworth - this.lastNetworth) );
				double percentGainOnTrade = ( this.valueOfLastSell / this.valueOfLastBuy ) - 1.00;
				System.out.println( "GAIN: " + percentGainOnTrade );
				this.percentGains.add( percentGainOnTrade );
			}
			else {
				this.numberOfLosses++;
				this.totalLost += ( currentNetworth - this.lastNetworth );
				System.out.println( "LOSS: " + (currentNetworth - this.lastNetworth) );
				double percentGainOnTrade = ( this.valueOfLastSell / this.valueOfLastBuy ) - 1.00;
				System.out.println( "GAIN: " + percentGainOnTrade );
				this.percentGains.add( percentGainOnTrade );
			}
			
			this.lastNetworth = currentNetworth;
			this.valueOfLastBuy = 0.0;
			this.valueOfLastSell = 0.0;
		}
		
		//Add the open price
		this.priceDataClose.add( closePrice );
		this.priceDataOpen.add( openPrice );
		
		//Add the timestamp
		this.timestamps.add( timestamp );
		this.currentNetworth     = currentNetworth; 
		
		//Update ATH and ATL
		this.allTimeHigh = Math.max( currentNetworth, this.allTimeHigh );
		this.allTimeLow  = Math.min( currentNetworth, this.allTimeHigh );
		
		//Add the networth
		this.loggedNetworth.add( this.currentNetworth );
		
		//Add buy and hold value
		this.loggedBuyAndHold.add( this.buyAndHold * closePrice );
		
		//Add signal json string
		this.indicators.add( signals );
		//Increment iterations
		this.iterations++;
		
	}
	
	/**
	 * Define what a trade completion means here
	 * @param stopLossHit
	 * @return
	 */
	public boolean tradeComplete( Boolean stopLossHit ){
		return ( this.valueOfLastBuy > 0 && this.valueOfLastSell > 0 ) || stopLossHit;
	}
	
	/**
	 * Called at the end of the trading period to generate 
	 * a report with the backtest results.
	 * @param addTrades
	 * @return
	 */
	public String generateReport( Boolean addTrades ){
		this.addTrades = addTrades;
		updateFinalValues( );
		return this.toString( );
	}
	
	/**
	 * Returns the backtest report as a JSON object
	 * @param addTrades
	 * @return
	 * @throws JSONException
	 */
	public JSONObject generateReportJson( Boolean addTrades ) throws JSONException{
		this.addTrades = addTrades;
		updateFinalValues( );
		
		return _generateReportJson( );
	}

	/**
	 * Internal method which updates all final values for the
	 * backtest.
	 */
	private void updateFinalValues( ){
		this.returnOnInvestment = ( this.currentNetworth / this.initialInvestment ) - 1.00 ;
		if( this.numberOfTrades == 0 ) {
			this.winRatio    = this.currentNetworth > this.initialInvestment ? 1 : 0;
			this.lossRatio   = this.currentNetworth <= this.initialInvestment ? 1 : 0;
			this.averageWin  = this.currentNetworth - this.initialInvestment;
			this.averageLoss = this.currentNetworth - this.initialInvestment;
			this.expectancy  = this.currentNetworth > this.initialInvestment ? this.averageWin : this.averageLoss;
			this.mean        	   = 0;
			this.volatilityMeasures = 0;
			return;
		}
		
		calculateMean( );
		calculateVolatility( );
		
		this.winRatio           = new Double( this.numberOfWins ) / ( this.numberOfTrades );
		this.lossRatio          = new Double( this.numberOfLosses ) / ( this.numberOfTrades );
		
		//(Win % * Average Winner) - (Loss % * Average Loser)
		Double numWins  = (double)this.numberOfWins;
		Double numLosses = (double)this.numberOfLosses;
		
		this.averageWin	         = this.numberOfWins != 0 ? this.totalWon / numWins : 0;
		this.averageLoss 		 = this.numberOfLosses != 0 ? this.totalLost / numLosses : 0;
		
		double expectedWin = this.averageWin*winRatio;
		double expectedLoss  = Math.abs( this.averageLoss )*lossRatio;
		//double riskRewardRatio  =  averageLoss != 0.0 ? averageWin / averageLoss : averageWin;
		this.expectancy         = expectedWin - expectedLoss;
		
		this.maxDrawdown = ( this.allTimeLow / this.allTimeHigh ) - 1.00;
	}
	
	/**
	 * Calculates mean over the period
	 */
	private void calculateMean( ){
		for( Double price : this.percentGains )
		{
			this.mean += price;
		}
		
		this.mean /= Double.valueOf( this.percentGains.size( ) );
	}
	
	/**
	 * Calculates volatility over the period
	 */
	private void calculateVolatility( ){
		
		Double sumOfDifferences = 0.0;
		
		for( Double price : this.percentGains )
		{
			sumOfDifferences += Math.pow( (price - this.mean), 2 );
		}
		
		double variance = sumOfDifferences / this.percentGains.size( );
		this.volatilityMeasures = Math.sqrt( variance );
	}
	
	/**
	 * Internal json report generation function 
	 * @return
	 * @throws JSONException
	 */
	private JSONObject _generateReportJson( ) throws JSONException{
				
		JSONObject reportAsJson = new JSONObject();
		
		reportAsJson.put("pair", this.baseCurrency + "-" + this.counterCurrency );
		reportAsJson.put("base", this.baseCurrency);
		reportAsJson.put("quote", this.counterCurrency );
		reportAsJson.put( "strategy", this.strategyName );
		reportAsJson.put( "initialInvestment", this.initialInvestment );
		reportAsJson.put( "finalNetworth", this.currentNetworth );
		reportAsJson.put( "returnOnInvestment", GS_Utils.convertToPercent( this.returnOnInvestment ) );
		reportAsJson.put( "numTrades", this.numberOfTrades );
		reportAsJson.put( "winRatio", GS_Utils.convertToPercent( this.winRatio ) );
		reportAsJson.put( "lossRatio", GS_Utils.convertToPercent( this.lossRatio ) );
		reportAsJson.put( "expectancy", this.expectancy );
		reportAsJson.put( "volatilityForPeriod", GS_Utils.convertToPercent( this.volatilityMeasures ) );
		
		if( this.totalLost != 0 ) {
			double profitFactor = this.totalLost != 0 ? this.totalWon / Math.abs( this.totalLost ) : this.totalWon;
			reportAsJson.put( "profitFactor", profitFactor );
		}
		else {
			reportAsJson.put( "profitFactor", this.totalWon );
		}
		
		reportAsJson.put( "maxDrawdown", GS_Utils.convertToPercent( this.maxDrawdown ) );
		
		if( TEST_MODE )
		{
			reportAsJson.put( "loggedActions", new JSONArray( this.loggedActions.subList( 0, TEST_MODE_CAP-1 ) ) );
			reportAsJson.put("loggedReturns", new JSONArray( this.loggedNetworth.subList( 0, TEST_MODE_CAP-1 ) )  );
			reportAsJson.put("loggedBuyAndHold", new JSONArray( this.loggedBuyAndHold.subList( 0, TEST_MODE_CAP-1 ) ) );
		}
		else
		{
			reportAsJson.put( "loggedActions", new JSONArray( this.loggedActions ) );
			reportAsJson.put("loggedReturns", new JSONArray( this.loggedNetworth ) );
			reportAsJson.put("loggedBuyAndHold", new JSONArray( this.loggedBuyAndHold ) );
			reportAsJson.put("priceDataCloses", new JSONArray( this.priceDataClose ) );
			reportAsJson.put("priceDataOpens", new JSONArray( this.priceDataOpen ) );
			reportAsJson.put("stopLosses", new JSONArray( this.stoppedOut ) );
			reportAsJson.put("timestamps", new JSONArray( this.timestamps ) );
			reportAsJson.put("indicators", new JSONArray( this.indicators ) );

			//Add all percent gains - to be matched with actions
			JSONArray percentGainsArray = new JSONArray( );
			for( int i = 0; i < this.percentGains.size(); i++ ) {
				percentGainsArray.put(  GS_Utils.convertToPercent( this.percentGains.get( i ) ) );
			}
			reportAsJson.put( "percentGains", percentGainsArray );

		}
		
		System.out.println( reportAsJson.toString( ) );
		return reportAsJson;
		
	}
	
	/**
	 * Debugging method to output light report as string
	 */
	@Override 
	public String toString( ){
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append( "Backtesting Report for " +strategyName + " on pair: " + this.baseCurrency + "/" + this.counterCurrency + "\n");
		stringBuilder.append( "ROI       :" +  GS_Utils.convertToPercent( this.returnOnInvestment ) + "\n" );
		stringBuilder.append( "NUM TRADES:" +  this.numberOfTrades + "\n" );
		stringBuilder.append( "WIN/LOSS  :" +  GS_Utils.convertToPercent( this.winRatio ) + "\n" );
		stringBuilder.append( "VOLATILITY:" +  GS_Utils.convertToPercent( this.volatilityMeasures ) + "\n\n" );
		return stringBuilder.toString( );
	}
	
	public String getStrategyName() {
		return strategyName;
	}

	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getCounterCurrency() {
		return this.counterCurrency;
	}

	public void setCounterCurrency(String counterCurrency) {
		this.counterCurrency = counterCurrency;
	}

	public int getNumberOfWins() {
		return numberOfWins;
	}

	public void setNumberOfWins(int numberOfWins) {
		this.numberOfWins = numberOfWins;
	}

	public int getNumberOfLosses() {
		return numberOfLosses;
	}

	public void setNumberOfLosses(int numberOfLosses) {
		this.numberOfLosses = numberOfLosses;
	}

	public double getInitialInvestment() {
		return initialInvestment;
	}

	public void setInitialInvestment(double initialInvestment) {
		this.initialInvestment = initialInvestment;
	}

	public double getCurrentNetworth() {
		return currentNetworth;
	}

	public void setCurrentNetworth(double currentNetworth) {
		this.currentNetworth = currentNetworth;
	}

	public double getValueOfLastBuy() {
		return this.valueOfLastBuy;
	}
	
	public double getValueOfLastSell() {
		return this.valueOfLastSell;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public double getReturnOnInvestment() {
		return returnOnInvestment;
	}

	public void setReturnOnInvestment(double returnOnInvestment) {
		this.returnOnInvestment = returnOnInvestment;
	}

	public int getNumberOfTrades() {
		return numberOfTrades;
	}

	public void setNumberOfTrades(int numberOfTrades) {
		this.numberOfTrades = numberOfTrades;
	}

	public double getRatio() {
		return this.winRatio;
	}
	
	public double getLossRatio( )
	{
		return this.lossRatio;
	}
	
	public double getExpectancy( )
	{
		return new BigDecimal( this.expectancy ).setScale( 2, BigDecimal.ROUND_DOWN ).doubleValue( );
	}

	public double getVolatilityMeasures() {
		return volatilityMeasures;
	}

	public void setVolatilityMeasures(double volatilityMeasures) {
		this.volatilityMeasures = volatilityMeasures;
	}

	public List<String> getLoggedActions() {
		return loggedActions;
	}

	public void setLoggedActions(List<String> loggedActions) {
		this.loggedActions = loggedActions;
	}

	public List<Double> getLoggedNetworth() {
		return loggedNetworth;
	}

	public void setLoggedNetworth(List<Double> loggedNetworth) {
		this.loggedNetworth = loggedNetworth;
	}

	public Boolean getAddTrades() {
		return addTrades;
	}

	public void setAddTrades(Boolean addTrades) {
		this.addTrades = addTrades;
	}

	public String[] getParameterNames() {
		return parameterNames;
	}

	public void setParameterNames(String[] parameterNames) {
		this.parameterNames = parameterNames;
	}

	public String[] getParameterValues() {
		return parameterValues;
	}

	public void setParameterValues(String[] parameterValues) {
		this.parameterValues = parameterValues;
	}
	
	public double getTotalWins( )
	{
		return this.totalWon;
	}
	
	public double getTotalLosses( )
	{
		return this.totalLost;
	}
	
	public double getAverageWin( )
	{
		return this.averageWin;
	}
	
	public double getAverageLoss( )
	{
		return this.averageLoss;
	}
}
