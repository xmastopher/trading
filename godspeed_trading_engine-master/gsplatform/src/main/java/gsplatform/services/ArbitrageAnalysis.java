package gsplatform.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.jobs.Constants;
import gsplatform.servlet.database.NomicsMirrorDAO;
import gsplatform.utilities.GS_Utils;
import nomics.core.NomicsExchangeCandles;

/**
 * Arbitrage Analysis class performs analysis at the exchange level - gives min prices, max prices
 * and best potential arbitrage opportunities
 * @author danielanderson
 *
 */
public class ArbitrageAnalysis 
{
	private static final int BATCH_SIZE = 32;
	private static final long TIMEOUT = -1;
	/**
	 * Private key for performing Nomics API calls
	 */
	private static final String nomicsAPIKey   = Constants.instance( ).getNomicsApiKey( );
	
	/**
	 * List of the exchanges to run an arbitrage analysis over
	 */
	private List< String > exchanges;
	
	/**
	 * Keeps track of how many threads were completed
	 */
	private List<Thread> runningThreads;
	
	/**
	 * List of all current arbitrage pairs
	 */
	List< ArbPair > arbitragePairs;
	
	/**
	 * Nomics Exchange candles object
	 */
	NomicsExchangeCandles nomicsCandles;
	
	//Used to cache API calls if certain market has already been called
	Map< String, JSONObject > dailyCache;
	Map< String, JSONObject > lastPriceCache;
	
	/**
	 * Class to organize prices by exchange
	 */
	public class ExchangePricePair implements Comparable< ExchangePricePair >
	{
		/**
		 * The exchange associated with this price
		 */
		private String exchange;
		
		/**
		 * The price associated with this exchange
		 */
		private BigDecimal price;
		
		/**
		 * The daily volume of this market
		 */
		private BigDecimal dailyVolume;
		
		/**
		 * Set internal state
		 * @param exchange
		 * @param price
		 */
		public ExchangePricePair( String exchange, BigDecimal price, BigDecimal dailyVolume )
		{
			this.exchange    = exchange;
			this.price       = price;
			this.dailyVolume = dailyVolume;
		}
		
		@Override
		public int compareTo( ExchangePricePair rhsObject ) 
		{
		    return this.price.compareTo( rhsObject.price );
		}
		
		public String getExchange( )
		{
			return this.exchange;
		}
		
		public BigDecimal getPrice( )
		{
			return this.price;
		}
		
		public BigDecimal getDailyVolume( )
		{
			return this.dailyVolume;
		}
	}
	
	/**
	 * Represent spread between exchange pairs
	 * @author danielanderson
	 *
	 */
	public class ArbPair implements Comparable< ArbPair >
	{
		private ExchangePricePair minPair;
		
		private ExchangePricePair maxPair;
		
		private BigDecimal percentSpread;
		
		private String market;

		public ArbPair( ExchangePricePair minPair, ExchangePricePair maxPair, BigDecimal percentSpread, String market )
		{
			this.minPair = minPair;
			this.maxPair = maxPair;
			this.percentSpread = percentSpread;
			this.market = market;
		}
		
		@Override
		public int compareTo( ArbPair o ) 
		{
			return percentSpread.compareTo( o.percentSpread );
		}
		
		public String getMarket( )
		{
			return this.market;
		}
		
		public BigDecimal getPercentSpread( )
		{
			return this.percentSpread;
		}
		
		public BigDecimal getDailyVolumeMin( )
		{
			return this.minPair.getDailyVolume( ).setScale( 4, BigDecimal.ROUND_DOWN ); 
		}
		
		public BigDecimal getDailyVolumeMax( )
		{
			return this.maxPair.getDailyVolume( ).setScale( 4, BigDecimal.ROUND_DOWN ); 
		}
		
		public String getExchangeOneName( )
		{
			return this.minPair.getExchange( );
		}
		
		public String getExchangeTwoName( )
		{
			return this.maxPair.getExchange( );
		}
		
		public String getBase( )
		{
			return this.market.split( "-" )[0];
		}
		
		public String getQuote( )
		{
			return this.market.split( "-" )[1];
		}
	}
	
	public class ArbitrageThread implements Runnable {
		
		private String exchangeOne;
		private String exchangeTwo;
		private String exchangeOneMarket;
		private String exchangeTwoMarket;
		private String base;
		private String quote;
		
		public ArbitrageThread( String exchangeOne, String exchangeTwo, String exchangeOneMarket,
							    String exchangeTwoMarket, String base, String quote) {
			super();
			this.exchangeOne = exchangeOne;
			this.exchangeTwo = exchangeTwo;
			this.exchangeOneMarket = exchangeOneMarket;
			this.exchangeTwoMarket = exchangeTwoMarket;
			this.base = base;
			this.quote = quote;
		}

		@Override
		public void run() {
			
			String key = base + "-" + quote;
			String cacheKeyOne = exchangeOneMarket + exchangeOne;
			String cacheKeyTwo = exchangeTwoMarket + exchangeTwo;
		
			try {
				JSONObject dailyCandleOne = null;
				JSONObject dailyCandleTwo = null;
				JSONObject candleOne      = null;
				JSONObject candleTwo      = null;

				if( !dailyCache.containsKey( cacheKeyOne ) ) {
					dailyCandleOne = new JSONObject( nomicsCandles.getMostRecentCandle(nomicsAPIKey, "1d", 
						   						  	this.exchangeOne, this.exchangeOneMarket ) );
					candleOne 	   = new JSONObject( nomicsCandles.getMostRecentCandle(nomicsAPIKey, "1m", 
													this.exchangeOne, this.exchangeOneMarket ) );
					dailyCache.put( cacheKeyOne, dailyCandleOne );
					lastPriceCache.put( cacheKeyOne, candleOne );
				}
				else {
					dailyCandleOne = dailyCache.get( cacheKeyOne );
					candleOne 	   = lastPriceCache.get( cacheKeyOne );
				}
				
				if( !dailyCache.containsKey( cacheKeyTwo ) ) {
					dailyCandleTwo = new JSONObject( nomicsCandles.getMostRecentCandle(nomicsAPIKey, "1d", 
						   						 	this.exchangeTwo, this.exchangeTwoMarket ) );
					candleTwo      = new JSONObject( nomicsCandles.getMostRecentCandle(nomicsAPIKey, "1m", 
													this.exchangeTwo, this.exchangeTwoMarket ) );
					dailyCache.put( cacheKeyTwo, dailyCandleTwo );
					lastPriceCache.put( cacheKeyTwo, candleTwo );
				}
				else {
					System.out.println("cached");
					dailyCandleTwo = dailyCache.get( cacheKeyTwo );
					candleTwo 	   = lastPriceCache.get( cacheKeyTwo );
				}
				
				//Grab 24 HR volumes
				BigDecimal dailyVolumeOne = new BigDecimal( dailyCandleOne.getString( "volume" ) ).setScale( 4, BigDecimal.ROUND_DOWN );
				BigDecimal dailyVolumeTwo = new BigDecimal( dailyCandleTwo.getString( "volume" ) ).setScale( 4, BigDecimal.ROUND_DOWN );

				//Create first EPP
				ExchangePricePair first  = new ExchangePricePair( this.exchangeOne, 
																 new BigDecimal( candleOne.getString( "close" ) )
																 .setScale( 8, BigDecimal.ROUND_DOWN ), 
																 dailyVolumeOne );
				
				//Create second EPP
				ExchangePricePair second = new ExchangePricePair( this.exchangeTwo, 
						  				   						 new BigDecimal( candleTwo.getString( "close" ) )
						  				   						 .setScale( 8, BigDecimal.ROUND_DOWN ), 
						  				   						 dailyVolumeTwo );
				
				//Calculate spread and add to arbitrage pairs
				BigDecimal spread = new BigDecimal( calculatePercentDifference( first, second ) ).setScale( 4, BigDecimal.ROUND_DOWN );
				
				if( new Double( candleOne.getString( "close" ) ) < new Double( candleTwo.getString( "close" ) ) ) {
					arbitragePairs.add( new ArbPair( first, second,  spread, key ) );
				}
				else {
					arbitragePairs.add( new ArbPair( second, first,  spread, key ) );
				}
			} 
			catch (JSONException | IOException e) { 
				System.out.println( e.getMessage( ) );
				return; 
			}
		}
		
	}
	/**
	 * 
	 * @param exchanges
	 */
	public ArbitrageAnalysis( List< String > exchanges )
	{
		this.exchanges = exchanges;
		this.arbitragePairs = new ArrayList< ArbPair >( );
		this.nomicsCandles = new NomicsExchangeCandles( );
		this.dailyCache 	 = new HashMap< String, JSONObject >( );
		this.lastPriceCache  = new HashMap< String, JSONObject >( );
	}
	
	/**
	 * Grabs all market data from requested exchanges and organizes prices based on the 1 min candle last prices.
	 * A hash map will be returned keyed on the market in the form BASE-QUOTE, where the values will be all exchange
	 * price pairs for that market
	 * @return HashMap
	 * @throws IOException 
	 * @throws JSONException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 */
	public List< ArbPair > doAnalysis( ) throws JSONException, IOException, SQLException, ClassNotFoundException, InterruptedException
	{
		NomicsMirrorDAO nomicsMirror = new NomicsMirrorDAO( );
		nomicsMirror.openConnection( );
		int index = 0;
		this.runningThreads = new ArrayList< Thread >( );
		for( int i = 0; i< this.exchanges.size( ); i++ )
		{
			for( int j = i+1; j < this.exchanges.size( ); j++ )
			{
				List< String > marketIntersections = nomicsMirror.getMarketsIntersections( this.exchanges.get( i ), 
																						  this.exchanges.get( j ) );
				
				for( String marketIntersection : marketIntersections ) {
					
					JSONObject marketIntersectionJSON = new JSONObject( marketIntersection );

					ArbitrageThread arbitrage = new ArbitrageThread( this.exchanges.get( i ), this.exchanges.get( j ),
																    marketIntersectionJSON.getString( "exchangeOneMarket" ),
																    	marketIntersectionJSON.getString( "exchangeTwoMarket" ),
																    	marketIntersectionJSON.getString( "base" ),
																    	marketIntersectionJSON.getString( "quote" ) );
					Thread arbitrageJob = new Thread( arbitrage );
					arbitrageJob.start( );
					this.runningThreads.add( arbitrageJob );
					index++;
				}
			}
		}
		
		for(int i = 0; i < this.runningThreads.size( ); i++) {
			  this.runningThreads.get( i ).join( );
		}
		
		nomicsMirror.closeConnection( );
		Collections.sort( this.arbitragePairs, Collections.reverseOrder( ) );
		return this.arbitragePairs;
	}
	
	public double calculatePercentDifference( Map.Entry< String, List< ArbitrageAnalysis.ExchangePricePair > > pair )
	{
		ExchangePricePair one = pair.getValue( ).get( pair.getValue( ).size( ) - 1 );
		ExchangePricePair two = pair.getValue( ).get( 0 );
		return calculatePercentDifference( one, two );

	}
	
	public double calculatePercentDifference( ExchangePricePair one, ExchangePricePair two ) {
		double maxPrice = one.getPrice( ).doubleValue( );
		double minPrice = two.getPrice( ).doubleValue( );
		double diff     = maxPrice - minPrice;
		
		if( diff == 0 )
			return 0;
		
		double avg	   = ( maxPrice + minPrice ) / 2.00;
		double pDiff   = diff / avg;
		return Math.abs( pDiff );
	}
	
	/**
	 * Generates a CSV of all arbitrage pairs sorted by percent-spread
	 * @param analysis
	 * @param csv
	 * @throws IOException
	 * @throws JSONException 
	 */
	public void generateCSV( List< ArbPair > arbPairs, String csv ) throws IOException, JSONException
	{ 
	    
		File fout = new File( csv );
		FileOutputStream fos = new FileOutputStream( fout );
	 
		BufferedWriter bw = new BufferedWriter( new OutputStreamWriter( fos ) );
	 
		//Write the header
		bw.write( "MARKET, EXCHANGE ONE (cheaper), EXCHANGE TWO (premium), SPREAD, EXCHANGE ONE VOLUME (USD), EXCHANGE TWO VOLUME (USD)" );
		bw.newLine();
		
		//Grab all USD prices
		CoinPriceLookup cpl = new CoinPriceLookup ( nomicsAPIKey );
		cpl.update( );
		
		for ( ArbPair arbPair : arbPairs ) 
		{
			String quote     = arbPair.getQuote( );
			BigDecimal price = BigDecimal.ZERO;
			
			try
			{
				price = cpl.getPrice( quote ).setScale( 4, BigDecimal.ROUND_DOWN );
			}
			catch( Exception e )
			{ ; }
			bw.write( arbPair.getMarket( ) );
			bw.write( "," + arbPair.getExchangeOneName( ) );
			bw.write( "," + arbPair.getExchangeTwoName( ) );
			bw.write( "," + GS_Utils.convertToPercent( arbPair.getPercentSpread( ) ) );
			bw.write( "," + GS_Utils.convertToMonetary( arbPair.getDailyVolumeMin( ).multiply( price ) ) );
			bw.write( "," + GS_Utils.convertToMonetary( arbPair.getDailyVolumeMax( ).multiply( price ) ) );
			bw.newLine( );
		}
	 
		bw.close();
	}

	
	public static void main( String[ ] args )
	{
		List< String > exchanges = new ArrayList< String >( );
		exchanges.add("bitfinex");
		exchanges.add("bittrex");
		exchanges.add("hitbtc");
		/**exchanges.add("kraken");
		exchanges.add("poloniex");
		exchanges.add("binance");
		exchanges.add("bitflyer");
		exchanges.add("bithumb");
		exchanges.add("gdax");
		exchanges.add("gemini");**/
		
		ArbitrageAnalysis aa = new ArbitrageAnalysis( exchanges );
		
		/**try 
		{
			List< ArbPair > arbPairs = aa.doAnalysis( );
			for( ArbPair arbPair : arbPairs ) {
				System.out.println( "MARKET: " + arbPair.getMarket( ) );
				System.out.println( "EXCHANGES: " + arbPair.getExchangeOneName( ) + "-" + arbPair.getExchangeTwoName( ) );
				System.out.println( "SPREAD: " + GS_Utils.convertToPercent( arbPair.getPercentSpread( ) ) );
			}
			System.out.println( "done" );
		    
		} 
		catch (JSONException | IOException | SQLException | ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace( );
		}**/
	}
}