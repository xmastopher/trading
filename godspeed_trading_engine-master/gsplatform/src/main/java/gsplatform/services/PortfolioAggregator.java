package gsplatform.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

import org.json.JSONException;
import gsplatform.servlet.database.PortfolioLookupDAO;
import gsplatform.utilities.GS_Utils;


public class PortfolioAggregator
{
	private String username;
	private Map< String, Portfolio > portfoliosByExchange;
	private Map< String, List< Portfolio.CoinAsset > > combinedPortfolio;
	private List< PortfolioLookupDAO.Location > linkedLocations;
	private String fiatType;
	private double totalCombinedFiat;
	
	public PortfolioAggregator( String fiatType, String username ) throws ClassNotFoundException, 
																	 SQLException, IOException, JSONException
	{
		this.username = username;
		
		PortfolioLookupDAO portfolioDAO = new PortfolioLookupDAO( username );
		this.linkedLocations      = portfolioDAO.getLocations( );
		this.fiatType             = fiatType;
		this.totalCombinedFiat    = 0;
		this.combinedPortfolio    = new HashMap< String, List< Portfolio.CoinAsset > > ();
		this.portfoliosByExchange = new HashMap< String, Portfolio >( );
		init( );
	}
	
	/**
	 * Internal function that uses the PortfolioLookupDAO to grab all linked locations for user
	 * @throws IOException
	 * @throws JSONException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void init( ) throws IOException, JSONException, ClassNotFoundException, SQLException
	{
		for( PortfolioLookupDAO.Location location : this.linkedLocations )
		{
			Portfolio portfolio = null;
			
			if( location.getType( ).equalsIgnoreCase("Ethereum Wallet") )
			{
				portfolio = new Portfolio( location.getType( ), this.fiatType, GS_Utils.getTokenBalancesEtherIO( location.getLocation( ) ) );
			}
			else
			{
				portfolio = new Portfolio( location.getLocation( ), this.fiatType, this.username );
			}
			
			this.portfoliosByExchange.put( location.getLocation( ), portfolio );
		}
	}
	
	/**
	 * Public level function which runs all internal functions for aggregating portfolios accross different exchanges
	 * @return				A sorted list of CoinAsset objects
	 * @throws IOException
	 * @throws JSONException
	 */
	public List< Portfolio.CoinAsset > getPortfolio() throws IOException, JSONException
	{
		combinePortfolios( );
		return calculatePercentages( );
	}
	
	/**
	 * Internal function that calculates the percentage breakdown of specific coins both at the exchange level and
	 * the global level
	 * @return	A list of CoinAsset objects sorted by percentage of entire portfolio in descending order
	 * @throws IOException
	 * @throws JSONException
	 */
	private List< Portfolio.CoinAsset > calculatePercentages() throws IOException, JSONException
	{
		List< Portfolio.CoinAsset > coinsSortedByOwnership = new ArrayList< Portfolio.CoinAsset >( );
		
		for ( Map.Entry< String, List< Portfolio.CoinAsset > > entry : this.combinedPortfolio.entrySet( ) ) 
		{
		    List< Portfolio.CoinAsset > coinAssets = entry.getValue();
		    
		    BigDecimal fiatSum = new BigDecimal( 0 );
		    
		    for( Portfolio.CoinAsset coinAsset : coinAssets )
		    {
		    		fiatSum = fiatSum.add( coinAsset.toFiat( this.fiatType ) );
		    }
		    
		    BigDecimal percentOfPortfolio = fiatSum.divide( new BigDecimal( this.totalCombinedFiat ), 2, RoundingMode.HALF_UP ); 
		    
		    for( Portfolio.CoinAsset coinAsset : coinAssets )
		    {
		    		coinAsset.setPercentOfPortfolio( percentOfPortfolio );
		    		coinsSortedByOwnership.add( coinAsset );
		    }
		}
		
		Collections.sort( coinsSortedByOwnership, Collections.reverseOrder( ) );
		return coinsSortedByOwnership;
	}
	
	/**
	 * Function for debugging purposes that outputs the aggregated portfolio
	 * @param coinAssets	List of all CoinAssets in portfolio
	 */
	public void show( List< Portfolio.CoinAsset > coinAssets )
	{
		for( Portfolio.CoinAsset ca :coinAssets )
		{
			System.out.print( ca.getCoin( ) + " %");
			System.out.println( ca.getPercentOfPortfolio( ) );
		}
	}
	
	/**
	 * Call to combine list of internal portfolios
	 */
	public void combinePortfolios( )
	{
		for( PortfolioLookupDAO.Location location : this.linkedLocations )
		{
			Portfolio portfolio     = portfoliosByExchange.get( location.getLocation( ) );
			this.totalCombinedFiat += portfolio.getTotalFiat( );
			addCoinAssets( portfolio );
		}
	}
	
	/**
	 * Adds all coin assets from internal portfolio to aggregates
	 * @param portfolio
	 */
	private void addCoinAssets( Portfolio portfolio )
	{
		List< Portfolio.CoinAsset > coinAssets = portfolio.getWallet( );
		
		for( Portfolio.CoinAsset coinAsset : coinAssets )
		{
			String symbol = coinAsset.getCoin( );
			
			if( !this.combinedPortfolio.containsKey( symbol ) )
			{
				this.combinedPortfolio.put( symbol, new ArrayList< Portfolio.CoinAsset >( ) );
			}
			
			this.combinedPortfolio.get( symbol ).add( coinAsset );
		}
	}
	
	/**
	 * Gets the total combined fiat for the aggregated portfolios
	 * @return
	 */
	public double getTotalCombinedFiat( )
	{
		return this.totalCombinedFiat;
	}
	
	public static void main( String[] args ) throws IOException, JSONException, ClassNotFoundException, SQLException
	{
		PortfolioAggregator pm = new PortfolioAggregator( "USD", "danimal" );
		System.out.println( pm.getPortfolio( ) );
	}
	
}
