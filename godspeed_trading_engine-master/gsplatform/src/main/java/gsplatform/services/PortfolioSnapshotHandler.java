package gsplatform.services;

import java.sql.SQLException;

import gsplatform.servlet.database.PortfolioStateDAO;

/**
 * Service provider that wraps PortfolioStateDAO for grabbing historical snapshots for a user
 * @author danielanderson
 *
 */
public class PortfolioSnapshotHandler 
{
	
	/**
	 * The database used to grab the snapshot - test or prod
	 */
	private static final String DATABASE = "godspeedTest";
	
	/**
	 * The snapshot the user is interested in grabbing the portfolio state from
	 */
	private String snapshot;
	
	/**
	 * Data access member for querying for portfolio state - snapshot
	 */
	private PortfolioStateDAO portfolioStateDAO;
	
	/**
	 * User associated with particular snapshot of interest
	 */
	private String username;
	
	/**
	 * Explicit constructor for initializing the snapshot string
	 * @param snapshot
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public PortfolioSnapshotHandler( String username, String snapshot ) throws ClassNotFoundException, SQLException
	{
		this.snapshot = snapshot;
		this.username = username;
		this.portfolioStateDAO = new PortfolioStateDAO( this.username, this.snapshot, DATABASE );
	}
	
	/**
	 * Returns JSON string for portfolio
	 * @return	String representation of JSON object for historical snapshot of portfolio.
	 */
	public String getPortfolioSnapshot( )
	{
		return portfolioStateDAO.getPortfolio( );
	}
	
	
}
