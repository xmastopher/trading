package gsplatform.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import nomics.core.NomicsPrices;

/**
 * Helper class for grabbing USD price of a given token - assumes aggregation
 * @author danielanderson
 *
 */
public class CoinPriceLookup 
{
	private NomicsPrices nomicsPrices;
	
	private String nomicsPrivateKey;
	
	private HashMap< String, BigDecimal > prices;
	
	public CoinPriceLookup( String key )
	{
		this.nomicsPrivateKey = key;
	}
	
	/**
	 * call before grabbing prices to get the most recent state
	 * @throws JSONException
	 * @throws IOException
	 */
	public void update( ) throws JSONException, IOException
	{
		prices       = new HashMap< String, BigDecimal >( );
		nomicsPrices = new NomicsPrices( );
		
		JSONArray pricesArray = new JSONArray( nomicsPrices.getAllPrices( nomicsPrivateKey ) );
		
		for( int i = 0; i < pricesArray.length( ); i++ )
		{
			JSONObject jsonObject = pricesArray.getJSONObject( i );
			String symbol 	      = jsonObject.getString( "currency" );
			BigDecimal price 	  = new BigDecimal( jsonObject.getString( "price") ).setScale( 8, BigDecimal.ROUND_DOWN );
			prices.put( symbol, price );
		}
		
		prices.put( "USD", BigDecimal.ONE );
	}
	
	/**
	 * Returns the price for a given symbol (in USD) ie. BTC
	 * @param symbol
	 * @return
	 */
	public BigDecimal getPrice( String symbol )
	{
		return this.prices.get( symbol );
	}
}
