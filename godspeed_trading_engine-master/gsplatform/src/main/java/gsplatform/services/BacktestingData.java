package gsplatform.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BacktestingData 
{
	protected Map<String, ArrayList<String> > data;
	protected List< String > fieldNames;
	protected double movingSum;
	protected int numberOfExamples;
	protected int numberOfPricePoints;
	
	public BacktestingData( String[] fieldNames )
	{
		this.data      = new HashMap< String, ArrayList< String > >( );
		this.movingSum = 0;
		this.fieldNames = new ArrayList< String >();
		for( String field : Arrays.asList( fieldNames ) )
		{
			this.fieldNames.add( field );
			this.data.put( field, new ArrayList<String>( ) );
		}
		this.data.put( "LAST_CANDLE", new ArrayList<String>( ) );
	}
	
	public Map<String, ArrayList<String> > getData( )
	{
		return data;
	}
	
	public void put( String fieldName, String value )
	{
		updateMovingSum( fieldName, value );
		this.data.get(fieldName ).add( value );
	}
	public List<String> get( String fieldName )
	{
		return this.data.get( fieldName );
	}
	
	public void incrementExamples( )
	{
		this.numberOfExamples++;
		this.numberOfPricePoints += 4;
	}
	
	public int getNumberOfExamples( )
	{
		return this.numberOfExamples;
	}
	
	public double getMean( )
	{
		return new Double( this.movingSum ) / new Double( this.numberOfPricePoints );
	}
	
	private void updateMovingSum( String fieldName, String value )
	{
		if( isPriceData( fieldName ) )
		{
			this.movingSum += Double.valueOf( value );
		}
	}
	
	private Boolean isPriceData( String fieldName )
	{
		return fieldName.equalsIgnoreCase( "OPEN" ) || fieldName.equalsIgnoreCase( "CLOSE" ) || 
			   fieldName.equalsIgnoreCase( "LOW" ) || fieldName.equalsIgnoreCase( "HIGH" );
	}
}
