package gsplatform.services;

import java.util.List;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import nomics.core.NomicsExchangeCandles;
import nomics.core.NomicsMarkets;

public class ArbitrageMaster 
{
	private static final String nomicsAPIKey   = "x4HS8DgzP/hmM/M1C1rbWem0TgC8Ab8W";
	private static final String[] allExchanges = { "BINANCE", "BITTREX" };
	private boolean useNomics = false;
	
	public class ArbitragePair implements Comparable< ArbitragePair >
	{
		
		private String pair;
		private String exchangeOne;
		private String exchangeTwo;
		private BigDecimal exchangeOnePrice;
		private BigDecimal exchangeTwoPrice;
		private BigDecimal priceDifference;
		
		public ArbitragePair( String pair, String exchangeLeft, String exchangeRight, BigDecimal exchangeOnePrice, BigDecimal exchangeTwoPrice )
		{
			this.pair = pair;
			this.exchangeOne = exchangeLeft;
			this.exchangeTwo = exchangeRight;
			this.exchangeOnePrice = exchangeOnePrice;
			this.exchangeTwoPrice = exchangeTwoPrice;
			double difference     = exchangeOnePrice.doubleValue( ) - exchangeTwoPrice.doubleValue( );
			this.priceDifference  = new BigDecimal( ( Math.abs( difference ) / this.exchangeOnePrice.doubleValue( ) ) * 100 ).setScale(1, BigDecimal.ROUND_HALF_UP);
		}
		
		@Override
		public int compareTo( ArbitragePair rhsObject ) 
		{
		    return this.priceDifference.compareTo( rhsObject.priceDifference );
		}
		
		/**
		 * Returns the exchange with the cheaper premium
		 * @return
		 */
		public String getSmallerExchange( )
		{
			return exchangeOnePrice.compareTo( exchangeTwoPrice ) == -1 ? exchangeOne : exchangeTwo;
		}
		
		/**
		 * Returns the smaller price of the two
		 * @return
		 */
		public BigDecimal getSmallerPrice( )
		{
			return this.exchangeOnePrice.compareTo( this.exchangeTwoPrice ) == -1 ? this.exchangeOnePrice : this.exchangeTwoPrice;
		}
		
		/**
		 * Returns the exchange with the cheaper premium
		 * @return
		 */
		public String getLargerExchange( )
		{
			return exchangeOnePrice.compareTo( exchangeTwoPrice ) == 1 ? exchangeOne : exchangeTwo;
		}
		
		/**
		 * Gets the larger price of the two
		 * @return
		 */
		public BigDecimal getLargerPrice( )
		{
			return this.exchangeOnePrice.compareTo( this.exchangeTwoPrice ) == 1 ? this.exchangeOnePrice : this.exchangeTwoPrice;
		}
		
		/**
		 * Grab the spread for the specified market
		 * @return
		 */
		public BigDecimal getSpread( )
		{
			return this.priceDifference;
		}
		
		/**
		 * Grab the market for arbing
		 * @return
		 */
		public String getMarket( )
		{
			return this.pair;
		}
	}
	
	public List< ArbitragePair > getAllArbitragePairsInOrder( ) throws IOException
	{
		List< ArbitragePair > arbitragePairs = new ArrayList< ArbitragePair >( );
		
		for( int i = 0; i < allExchanges.length; i++ )
		{
			for( int j = 0; j < allExchanges.length; j++ )
			{
				arbitragePairs.addAll( findArbitragesFromExchanges( allExchanges[ i ], allExchanges[ j ] ) );
			}
		}
		
		Collections.sort( arbitragePairs, Collections.reverseOrder( ) );
		return arbitragePairs;
	}
	
	
	/**
	 * Using XCHANGE library find arbitrage pairs
	 * @param exchangeOne
	 * @param exchangeTwo
	 * @return
	 * @throws IOException
	 */
	public List< ArbitragePair > findArbitragesFromExchanges( String exchangeOne, String exchangeTwo ) throws IOException
	{	
		Exchange firstExchange  = ExchangeHandler.getExchangePublic( exchangeOne );
		Exchange secondExchange = ExchangeHandler.getExchangePublic( exchangeTwo );
		
		List< CurrencyPair > firstPairs      = firstExchange.getExchangeSymbols( );
		List< ArbitragePair > arbitragePairs = new ArrayList< ArbitragePair >( );
		
		for( CurrencyPair currencyPair : firstPairs )
		{
			
			try 
			{
				ArbitragePair arbitragePair = createArbitragePair( firstExchange, secondExchange, exchangeOne, exchangeTwo, currencyPair );
				arbitragePairs.add( arbitragePair );
			}
			catch( Exception e )
			{
				continue;
			}
			
		}
		
		return arbitragePairs;
	}
	
	/**
	 * Find all arbitrage pairs using the nomics API
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public List< ArbitragePair > findArbitragesFromExchangesNomics( String[] exchanges ) throws IOException, JSONException
	{
		NomicsMarkets nomicsMarkets		 = new NomicsMarkets( );
		List< String > intersectedMarkets = nomicsMarkets.getMarketIntersections( exchanges, nomicsAPIKey );
		
		List< ArbitragePair > arbitragePairs = new ArrayList< ArbitragePair >( );
		
		//Iterate over all exchanges and perform pairwise arbitrage matching
		for( int i = 0; i < exchanges.length-1; i++ )
		{
			
			//Left hand side exchange
			JSONArray exchangeLeft = new JSONArray( intersectedMarkets.get( i ) );
		
			//Iterate over all markets in left exchange
			for( int left = 0; left < exchangeLeft.length( ); left++ )
			{
				//LHS Kline
				JSONObject leftKline    = exchangeLeft.getJSONObject( left );
				
				//Right hand side exchange
				JSONArray exchangeRight = new JSONArray( intersectedMarkets.get( i + 1 ) );
				
				//Iterate over all markets in right exchange
				for( int right = 0; right < exchangeRight.length( ); right++ )
				{
					//RHS Kline
					JSONObject rightKline    = exchangeRight.getJSONObject( right );
					
					if( leftKline.getString( "base" ).equals( rightKline.getString( "base" ) ) && 
						leftKline.getString( "quote" ).equals( rightKline.getString( "quote" ) ) )
					{
						if( leftKline.getString("base").equals( "BTS" ) )
						{
							System.out.println("here");
						}
						
						ArbitragePair arbitragePair = createArbitragePair( leftKline.getString( "market" ),
													  rightKline.getString( "market" ), 
													  exchanges[ i ], 
													  exchanges[ i + 1 ], 
													  rightKline.getString( "base" ) + "-" + rightKline.getString( "quote" ) );
						
						if( arbitragePair != null )
						{
							arbitragePairs.add( arbitragePair );
						}
						break;
					}
				}
				
			}
		}
		
		
		return arbitragePairs;
		
	}
	
	/**
	 * Given a pair find all potential matches or arbitrage opportunities
	 * @param base
	 * @param counter
	 * @throws IOException
	 */
	public void findArbitragesFromPair( String base, String counter ) throws IOException
	{	
		Set< String > exchangeSet      = new HashSet< String >( );
		List< ArbitragePair > arbPairs = new ArrayList< ArbitragePair >( );
		
		for( int i = 0; i < allExchanges.length; i++ )
		{
			Exchange exchangeOne = ExchangeHandler.getExchangePublic( allExchanges[ i ] );
			
			for( int j = i+1; j < allExchanges.length; j++ )	
			{
				Exchange exchangeTwo = ExchangeHandler.getExchangePublic( allExchanges[ j ] );
				String exchangePair  = allExchanges[ i ] + "/" + allExchanges[ j ];
				
				if( !exchangeSet.contains( exchangePair ) )
				{
					exchangeSet.add( exchangePair );
					ArbitragePair arbitragePair = createArbitragePair( exchangeOne, exchangeTwo, allExchanges[ i ], allExchanges[ j ], new CurrencyPair( base, counter ) );
					
					if( arbitragePair != null )
					{
						arbPairs.add( arbitragePair );
					}
				}
			}
		}
		
	}
	
	/**
	 * Create an arbitrage pair using XCHANGE library
	 * @param leftExchange
	 * @param rightExchange
	 * @param leftExchangeString
	 * @param rightExchangeString
	 * @param currencyPair
	 * @return
	 * @throws IOException
	 */
	private ArbitragePair createArbitragePair( Exchange leftExchange, Exchange rightExchange, 
			                                   String leftExchangeString, String rightExchangeString, 
			                                   CurrencyPair currencyPair ) throws IOException
	{
		Ticker leftTicker  = leftExchange.getMarketDataService( ).getTicker( currencyPair );
		Ticker rightTicker = rightExchange.getMarketDataService( ).getTicker( currencyPair );
		
		BigDecimal leftLast  = leftTicker.getLast( );
		BigDecimal rightLast = rightTicker.getLast( );
		
		return new ArbitragePair( currencyPair.toString( ), leftExchangeString, rightExchangeString, leftLast, rightLast );
	}
	
	/**
	 * Create an arbitrage pair from JSON kline data
	 * @param leftKline
	 * @param rightKline
	 * @param leftExchangeString
	 * @param rightExchangeString
	 * @param currencyPair
	 * @return
	 * @throws JSONException 
	 * @throws IOException 
	 */
	private ArbitragePair createArbitragePair( String leftMarket, String rightMarket, 
											  String leftExchangeString, String rightExchangeString, 
											  String currencyPair ) throws JSONException, IOException
	{
		//Grab the left kline
		NomicsExchangeCandles nomicsExchangeLeft = new NomicsExchangeCandles( );
		JSONObject leftKline  = new JSONObject( nomicsExchangeLeft.getMostRecentCandle( nomicsAPIKey, "1m", leftExchangeString, leftMarket ) );
		
		//Grab the left kline
		NomicsExchangeCandles nomicsExchangeRight = new NomicsExchangeCandles( );
		JSONObject rightKline  = new JSONObject( nomicsExchangeRight.getMostRecentCandle( nomicsAPIKey, "1m", rightExchangeString, rightMarket ) );
		
		if( leftKline.length( ) == 0 || rightKline.length( ) == 0 ) return null;
		
		BigDecimal leftClose  = new BigDecimal( leftKline.getString( "close" ) ).setScale( 8, BigDecimal.ROUND_DOWN );
		BigDecimal rightClose = new BigDecimal( rightKline.getString( "close" ) ).setScale( 8, BigDecimal.ROUND_DOWN );
		
		if( leftKline.length( ) == 0 ) return null;
		
		return new ArbitragePair( currencyPair, leftExchangeString, rightExchangeString, leftClose, rightClose );
	}
	
	public static void main( String args[] ) throws IOException, JSONException
	{
		//Constants.setup();
		//CryptoCurrencyLookup.buildAllMarkets();
		//ExchangeHandler.initExchangeMap();
		
		ArbitrageMaster am = new ArbitrageMaster( );
		List< ArbitragePair > arbitragePairs = am.findTopKArbitragesNomics( new String[] {"poloniex","bittrex"} );
		
		int count = 0;
		
		for( ArbitragePair ap : arbitragePairs )
		{
			if( count == 100 )
				break;
			
			String exchangePair = ap.exchangeOne + "-" + ap.exchangeTwo;
			System.out.println(count+1 + " " + exchangePair + " | PAIR: " + ap.pair + ", HAS DIFFERENCE: " + ap.priceDifference.setScale(2, BigDecimal.ROUND_HALF_UP) );
			count++;
		}
		
		System.out.println( "" );
		
		//findTopKArbitrages( "BITSTAMP","BINANCE", 10 );
		//findTopKArbitrages( "BITSTAMP","CRYPTOPIA", 10 );
		//findTopKArbitrages( "BITSTAMP","BITTREX", 10 );
		//findTopKArbitrages( "BITSTAMP","POLONIEX", 10 );
		
		//findTopKArbitrages( "BINANCE","POLONIEX", 10 );
		//findTopKArbitrages( "BINANCE", "CRYPTOPIA", 10 );
	    //findTopKArbitrages( "BINANCE","BITTREX", 10 );
		
		//findTopKArbitrages( "BITTREX","POLONIEX", 10 );
		//findTopKArbitrages( "BITTREX","CRYPTOPIA", 10 );
		//findTopKArbitrages( "CRYPTOPIA","POLONIEX", 10 );

	}
	
	public void findTopKArbitrages( String exchangeOne, String exchangeTwo, int topK ) throws IOException
	{
		ArbitrageMaster am = new ArbitrageMaster( );
		List< ArbitragePair > arbitragePairs = am.findArbitragesFromExchanges( exchangeOne, exchangeTwo );
		Collections.sort( arbitragePairs, Collections.reverseOrder( ) );
		int count = 0;
		System.out.println( exchangeTwo + "/" + exchangeOne ) ;
		
		for( ArbitragePair ap : arbitragePairs )
		{
			if( count == topK )
				break;
			
			System.out.println(count+1 + " | PAIR: " + ap.pair + ", HAS DIFFERENCE: " + ap.priceDifference.setScale(2, BigDecimal.ROUND_HALF_UP) );
			count++;
		}
		
		System.out.println( "" );
	}
	
	public List< ArbitragePair > findTopKArbitragesNomics( String[] exchanges ) throws JSONException, IOException
	{
		ArbitrageMaster am = new ArbitrageMaster( );
		List< ArbitragePair > arbitragePairs = am.findArbitragesFromExchangesNomics( exchanges );
		Collections.sort( arbitragePairs, Collections.reverseOrder( ) );
		return arbitragePairs;
	}
	
}
