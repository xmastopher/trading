package gsplatform.services;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.utilities.TradingParameters;

public class NomicsParametersAdapter 
{
	/**
	 * Takes in a JSON object with the most previous exchange candle
	 * and returns a Trading Parameters object
	 * @throws JSONException 
	 */
	public TradingParameters exchangeCandleToTradingParams( JSONObject candle ) throws JSONException
	{
		TradingParameters tradingParameters = new TradingParameters( );
		
		tradingParameters.updateValue( "open", candle.getString( "open") );
		tradingParameters.updateValue( "close", candle.getString( "close") );
		tradingParameters.updateValue( "last_price", candle.getString( "close") );
		tradingParameters.updateValue( "high", candle.getString( "high") );
		tradingParameters.updateValue( "low", candle.getString( "low") );
		tradingParameters.updateValue( "volume", candle.getString( "volume") );
		tradingParameters.updateValue( "timestamp", candle.getString( "timestamp") );
		tradingParameters.updateValue( "last_candle", "No" );
		return tradingParameters;
	}
	
	/**
	 * Takes in a JSON object with the most previous exchange candle
	 * and returns a Trading Parameters object
	 * @throws JSONException 
	 */
	public List< TradingParameters > exchangeCandlesToTradingParams( JSONArray candles ) throws JSONException
	{
		List< TradingParameters > tradingParameters = new ArrayList< TradingParameters >( );
		
		for( int i = 0; i < candles.length( ); i++ )
		{
			tradingParameters.add( exchangeCandleToTradingParams( candles.getJSONObject( i ) ) );
		}
		
		return tradingParameters;
	}
	
}
