package gsplatform.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.BotLogsDAO;
import gsplatform.jobs.Constants;
import gsplatform.jobs.GS_Logger;
import gsplatform.servlet.database.BotLeaderboardsDAO;
import gsplatform.utilities.ISO8601;
import gsplatform.utilities.TradingParameters;
import nomics.core.NomicsExchangeCandles;
import nomics.core.NomicsMarkets;

public class BotThread implements Runnable
{
	/**
	 * Reference to logger instance
	 */
	private static final GS_Logger LOGGER = GS_Logger.getLogger( );
	
	/**
	 * Slight delay to ensure new candle is grabbed
	 */
	public static int intervalDelay = 5000;
	
	/**
	 * Static variable for keeping track of unique threads
	 */
	public static int threadCount = 0;
	
	/**
	 * Single instance all bot threads share to grab data
	 */
	public static NomicsExchangeCandles nomicsExchangeCandles;
	
	/**
	 * The name of the exchange
	 */
	private String exchangeName;
	
	/**
	 * String value of interval
	 */
	private String intervalString;
	
	/**
	 * How long do we wait until the next candle?
	 */
	private int interval;
	
	/**
	 * The market to trade in
	 */
	private String symbol;
	
	/**
	 * Reference to the automated traders list - contains all traders for this exchange and candle
	 */
	private List< AutomatedTrader > automatedTraders;
	
	/**
	 * Nomics adapter
	 */
	private NomicsParametersAdapter nomicsAdapter;
	
	/**
	 * Reference to bot log DAO to write logs
	 */
	private BotLogsDAO botLogsDAO;
	
	/**
	 * Keeps track of bot performance
	 */
	private BotLeaderboardsDAO botLeaderboardsDAO;
	
	/**
	 * Log - used for simulations vs non simulations
	 */
	private String log;
	
	/**
	 * Reference to last timestamp - we will need to check this on iteration we should add slight delay if
	 * the last timestamp == current
	 */
	private String lastTimestamp;
	
	/**
	 * Reference to the last Trading Parameters instance - if the nomics API returns 0s this indicates no trading
	 * for the period, we want to use the last candle in this case
	 */
	private TradingParameters lastNonZeroTradingParams;
	
	/**
	 * Set with automated trader
	 * @param automatedTrader
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public BotThread( String interval, String exchangeName, String symbol ) throws ClassNotFoundException, SQLException, JSONException, IOException
	{
		this.botLogsDAO        = new BotLogsDAO( );
		this.botLeaderboardsDAO = new BotLeaderboardsDAO( );
		this.lastTimestamp = "";
		
		if( nomicsExchangeCandles == null )
		{
			nomicsExchangeCandles = new NomicsExchangeCandles( );
		}
		
		this.automatedTraders = new ArrayList< AutomatedTrader >( );
		this.exchangeName     = exchangeName;
		this.intervalString   = interval;
		this.interval         = mapIntervalFromString( interval );
		this.symbol           = symbol;
		this.nomicsAdapter    = new NomicsParametersAdapter( );
		
		NomicsMarkets nomicsMarkets = new NomicsMarkets( );
		String base    = this.symbol.split( "-" )[ 0 ];
		String counter = this.symbol.split( "-" )[ 1 ];
		this.symbol    = nomicsMarkets.getMarketFromPair( Constants.instance().getNomicsApiKey( ), this.exchangeName.toLowerCase( ), base, counter ); 
	}
	
	public void setLog( String log )
	{
		this.log = log;
	}
	
	/**
	 * Add an auto trader to the list, also preprocesses trader by running N iterations on it
	 * @param automatedTrader
	 * @throws Exception 
	 */
	public void addBot( AutomatedTrader automatedTrader ) throws Exception
	{
		
		String candles = nomicsExchangeCandles.getLastNCandles( Constants.instance().getNomicsApiKey( ), this.intervalString, 
															  this.exchangeName.toLowerCase( ), this.symbol, automatedTrader.getPreprossingValue( ), true );
		List< TradingParameters > tradingParameters = nomicsAdapter.exchangeCandlesToTradingParams( new JSONArray( candles ) );
		
		//Init last non zero trading params
		if( this.lastNonZeroTradingParams == null ) {
			this.lastNonZeroTradingParams = tradingParameters.get( tradingParameters.size( ) - 1 );
		}
		
		for( TradingParameters tradingParameterSet : tradingParameters )
		{
			automatedTrader.doIteration( tradingParameterSet );
		}
		
		this.automatedTraders.add( automatedTrader );
	}
	
	/**
	 * Remove a bot from this thread
	 * @param username
	 * @param botId
	 */
	public void removeBot( String username, int botId )
	{
		int indexOf = -1;
		
		for( int i = 0; i < this.automatedTraders.size( ); i++ )
		{
			AutomatedTrader bot = this.automatedTraders.get( i );
			
			if( bot.getBotId( ) == botId && bot.getOwner( ).equals( username ) )
			{
				indexOf = i;
				break;
			}
		}
		
		this.automatedTraders.remove( indexOf );
	}
	
	/**
	 * Remove all bots from this thread
	 */
	public void removeAllBots( ) {
	   this.automatedTraders = new ArrayList< AutomatedTrader >( );
	}
	
	public List< AutomatedTrader > getBots( )
	{
		return this.automatedTraders;
	}
	
	@Override
	/**
	 * Override run - will iterate every hour in sync with provided exchange and send a message
	 * when corresponding candle closes to update bots
	 */
	public void run( ) 
	{
		
        try 
        {
        	
        		//Grab most recent candle to synchronize
	    		TradingParameters tp = this.nomicsAdapter.exchangeCandleToTradingParams( new JSONObject ( 
						nomicsExchangeCandles.getMostRecentCandle( Constants.instance().getNomicsApiKey( ), this.intervalString, this.exchangeName.toLowerCase( ), this.symbol )
						) );
	    		
	    		//Synchronize with the specific exchange candle
	    		Date startTime = calculateStartTime( tp.get( "timestamp" ) );
	    		this.synchronize( startTime );

	    		//Perform signal checking
	    		while( this.automatedTraders.size( ) > 0 )
        		{
        			//Returns true if net timestamp found
	    			if( assessTrades( ) )
	    			{
	    				//Sleep based on interval + delay
	    				Thread.sleep( this.interval );
	    			}
	    			
	    			//Same timestamp - do a smaller delay
	    			else
	    			{
	    				Thread.sleep( intervalDelay );
	    			}
        		}
	    		
	    		this.botLogsDAO.closeConnection( );
        }
        
        catch( Exception e )
        {
        		LOGGER.addMessageEntry( log, "BotThread", this.toString( ) + " EXCEPTION: " + e.getMessage( ) );
        }
	}
	
	/**
	 * Helper function that calculates the start time for the thread - we want it
	 * to synchonize with the exchange so the close of the last candle should
	 * always be passed in
	 * @param lastCandleCloseTime
	 * @return
	 * @throws ParseException
	 */
	public Date calculateStartTime( String lastCandleCloseTime ) throws ParseException
	{
		Calendar calendar = ISO8601.toCalendar( lastCandleCloseTime );
		calendar.add( Calendar.MILLISECOND, this.interval );
		return calendar.getTime( );
	}
	
	/**
	 * Endlessly loop until timestamp is synchronized 
	 * @param startTime
	 */
	public void synchronize( Date startTime )
	{
		Boolean synchronizing = true;
		
		while( synchronizing )
		{
			Date now = new Date( );
			synchronizing = now.before( startTime );
		}
	}
	
	/**
	 * Method called every single iteration to assess all bot signals
	 * @throws JSONException
	 * @throws IOException
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws ParseException 
	 */
	public Boolean assessTrades( ) throws JSONException, IOException, ClassNotFoundException, SQLException, ParseException
	{
		//Grab most recent candle from nomics
 		JSONObject nomicsCandle = new JSONObject( nomicsExchangeCandles.getMostRecentCandle( Constants.instance().getNomicsApiKey( ), intervalString, exchangeName.toLowerCase(), symbol ) );
		
 		//Check that the candle is not the same as the last one
 		if( nomicsCandle.getString( "timestamp" ).equalsIgnoreCase( lastTimestamp ) )
 			return false;
 		
 		
		LOGGER.addMessageEntry( log, "BotThread", this.toString( ) + " NEW CANDLE: " + nomicsCandle );
		
		//Convert using adapter
		TradingParameters tradingParameters = nomicsAdapter.exchangeCandleToTradingParams( nomicsCandle );
		
		//Timestamp as DATE
		Date tradeTime = ISO8601.toCalendar( tradingParameters.get( "timestamp" ) ).getTime( );
		
		
		//Use non zero value in the case of no trading activity
		if( tradingParameters.getAsDouble("close") == 0 ) {
			if( this.lastNonZeroTradingParams != null ) {
				String timestamp  = tradingParameters.get("timestamp");
				tradingParameters = this.lastNonZeroTradingParams;
				tradingParameters.updateValue("timestamp", timestamp);
			}
		}
		
		
		//Used to store  bot logs for post processing into DB
		List< BotLog > botLogsList = new ArrayList< BotLog >( );
		List< BigDecimal > trades  = new ArrayList< BigDecimal >( );
		
		//Grab data
		for( AutomatedTrader automatedTrader : automatedTraders )
		{
			try
			{
				//Add bot results to bot log
				BotLog botLog = automatedTrader.doIteration( tradingParameters );
				botLogsList.add( botLog );
				
				if( automatedTrader.tradeComplete( ) ) {
					LOGGER.addMessageEntry("BOT-THREADS","BotThread", automatedTrader.getOwner( ) + "-" + automatedTrader.getBotId( ) + " has completed a trade");
					trades.add( automatedTrader.getTradeROI( ) );
					automatedTrader.resetTrade( );
				}
				else {
					trades.add( null );
				}
			}
			catch( Exception e )
			{
				LOGGER.addMessageEntry( "BOT-THREADS", "BotThread", this.toString( ) + " EXCEPTION: " + e.getMessage( ) );
				continue;
			}
		}
		
		//Write logs and bot trades
		writeDB( botLogsList, trades );

		//Shuffle list to elimate bot priority bias
		Collections.shuffle( automatedTraders );
		this.lastTimestamp = nomicsCandle.getString( "timestamp" );
		return true;
	}
	
	/**
	 * Call to write all bot logs results from this iteration
	 * @param botLogs
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void writeDB( List< BotLog > botLogs, List< BigDecimal > tradeROIs ) throws ClassNotFoundException, SQLException {
		
		this.botLogsDAO.openConnection( );
		this.botLeaderboardsDAO.openConnection( );
		
		for( int i = 0; i < botLogs.size( ); i++ ) {
			
			BotLog botLog = botLogs.get( i );
			
			try {
				this.botLogsDAO.setUsernameAndId( botLog.getUsername(), botLog.getBotId( ) );
				this.botLeaderboardsDAO.setUsernameAndId( botLog.getUsername( ), botLog.getBotId( ) );
				this.botLogsDAO.writeLog( botLog.getAction( ), botLog.getNetworth( ), botLog.getQuoteOwned( ), 
								      	 botLog.getAsssetsOwned( ), botLog.getLastPrice( ), botLog.getSignalStates( ), botLog.getDated( ) );
				if( tradeROIs.get( i ) != null ){
					this.botLeaderboardsDAO.addTrade( tradeROIs.get( i ) );
				}
			}
			catch( Exception e ) {
				LOGGER.addMessageEntry( log, "BotThread", this.toString( ) + " EXCEPTION: " + e.getMessage( ) );
				this.botLogsDAO.closeConnection();
				this.botLeaderboardsDAO.closeConnection( );
				continue;
			}
		}
		this.botLogsDAO.closeConnection();
		this.botLeaderboardsDAO.closeConnection( );
		
	}
	
	/**
	 * Static function which maps a string interval to the corresponding integer value in milliseconds
	 * @param interval
	 * @return the millisecond conversation of provided interval, -1 if said interval DNE
	 */
	public static int mapIntervalFromString( String interval )
	{
		switch( interval )
		{
		
		case "1m":
			return 60000;
		
		case "5m":
			return 300000;
			
		case "15m":
			return 9000000;
				
		case "30m":
			return 1800000;				
				
		case "1h":
			return 3600000;		
		
		case "2h":
			return 7200000;		
			
		case "4h":
			return 14400000;		
			
		case "6h":
			return 21600000;		
			
		case "12h":
			return 43200000;	
			
		case "1d":
			return 86400000;	
		}
		
		return 60000;
	}
	
	@Override
	public String toString() {
		String key = exchangeName + "-" + intervalString + "-" + symbol;
		return "[BOTTHREAD:" + key + "]";
	}
			
}
