package gsplatform.services;

import java.util.HashMap;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.binance.BinanceExchange;
//import org.knowm.xchange.bitstamp.BitstampExchange;
import org.knowm.xchange.bittrex.BittrexExchange;

public class ExchangeHandler {

	/**
	 * 
	 */
	private static HashMap< String, Exchange > exchanges = new HashMap< String, Exchange >();
	
	/**
	 * 
	 */
	private static Boolean hasBeenInitialized = false;
	
	/**
	 * 
	 */
	public static void initExchangeMap( )
	{
		exchanges.put( "BINANCE", ExchangeFactory.INSTANCE.createExchange( BinanceExchange.class.getName()) );
		exchanges.put( "BITTREX", ExchangeFactory.INSTANCE.createExchange( BittrexExchange.class.getName()) );
		/*
		exchanges.put("COIN_MARKET_CAP", ExchangeFactory.INSTANCE.createExchange( CoinMarketCapExchange.class.getName( ) ) );
		exchanges.put( "GDAX", ExchangeFactory.INSTANCE.createExchange( GDAXExchange.class.getName( ) ) );
		exchanges.put( "BITSTAMP", ExchangeFactory.INSTANCE.createExchange( BitstampExchange.class.getName( ) ) );
		exchanges.put( "CRYPTOPIA", ExchangeFactory.INSTANCE.createExchange( CryptopiaExchange.class.getName( ) ) );
		exchanges.put( "POLONIEX", ExchangeFactory.INSTANCE.createExchange( PoloniexExchange.class.getName( ) ) );
		exchanges.put( "KRAKEN", ExchangeFactory.INSTANCE.createExchange( KrakenExchange.class.getName( ) ) );
		*/
	}
	
	/**
	 * 
	 */
	public static Exchange getExchangePublic( String exchange )
	{
		if( !hasBeenInitialized )
		{
			initExchangeMap();
			hasBeenInitialized = true;
		}
		return exchanges.get( exchange.toUpperCase() );
	}
	
	/**
	 * 
	 */
	public static Exchange getExchangePrivate( String exchangeName, String privateKey, String publicKey )
	{
		ExchangeSpecification exchange = null;
		
		switch( exchangeName )
		{
		case "binance":
			exchange = new BinanceExchange().getDefaultExchangeSpecification();
			break;
		case "bittrex":
			exchange = new BittrexExchange().getDefaultExchangeSpecification();
			break;
		default:
			return null;	
		}
		
		exchange.setApiKey( publicKey );
		exchange.setSecretKey( privateKey );
		return ExchangeFactory.INSTANCE.createExchange( exchange );
	}
}
