package gsplatform.services;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import org.json.JSONException;
import gsplatform.algorithms.*;
import gsplatform.algorithms.TradingAlgorithm.SIGNAL;
import gsplatform.jobs.Constants;
import gsplatform.servlet.database.HistoricalDataDAO;
import gsplatform.servlet.database.NomicsMirrorDAO;
import gsplatform.utilities.TradingParameters;
import gsplatform.services.BacktestReport;

/**
 * Backtest class which provides the functionality for
 * the Backtesting service.  Called to perform backtests when 
 * the backtest endoint is hit.  The backtest process is iterative,
 * in that it iterates over all kline candles for a provided
 * period.  All information is passed to the BacktestReport
 * object on completion and returned to the caller at the end
 * of the cycle.
 * @author danielanderson
 *
 */
public class Backtest
{
	/**
	 * Which hose are we running on? 'OVH' or 'LOCAL'
	 */
	private static final String HOST = Constants.instance( ).getHost( );

	/**
	 * Precision used for writing all trading values
	 */
	final static int PRECISION = 8;
	
	/**
	 * Internal member to hold all trading data 
	 * for the period
	 */
	private HistoricalDataDAO historicalData;
	
	/**
	 * Reference to the specific strategy
	 */
	private TradingAlgorithm tradingAlgorithm;

	/**
	 * Internal reference to the report updated during
	 * the backtesting period
	 */
	private BacktestReport backtestReport;
	
	/**
	 * The quote (counter) currency for the trading period
	 */
	private String quoteCurrency;
	
	/**
	 * The base currency for the trading period
	 */
	private String baseCurrency;

	/**
	 * Passed initial investment
	 */
	private double initialInvestment;
	
	/**
	 * Current networth updated throughout the backtest
	 */
	private double currentNetworth;
	
	/**
	 * How many assets are owned at a specific point in time
	 */
	private double assetsOwned;
	
	/**
	 * What was the last price asset was purchased at
	 */
	private double lastBuyPrice;
	
	/**
	 * What was the last price asset was sold at
	 */
	private double lastSellPrice;
	
	/**
	 * Was the last action a stop loss triggering action?
	 */
	private boolean stopLoss;
	
	/**
	 * Is nomics data being used?
	 */
	private boolean usingNomics;
	
	/**
	 * What was the last signal that was generated
	 */
	private SIGNAL lastAction;
	
	/**
	 * Explicit constructor
	 * @param algorithm					The strategy to use
	 * @param exchange					The exchange associated with the backtests
	 * @param candle						The candle interval ie. 1h
	 * @param baseCurrency				Base currency for the market
	 * @param quoteCurrency				The 	quote currency for the market
	 * @param initialInvestment			Initial investment provided
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws JSONException
	 * @throws ParseException
	 */
	public Backtest( TradingAlgorithm algorithm, 
					String exchange,
					String candle, 
					String baseCurrency, 
					String quoteCurrency, 
					double initialInvestment ) throws IOException, 
													 ClassNotFoundException, 
													 SQLException,
													 JSONException, 
													 ParseException {
		
		this.quoteCurrency      = quoteCurrency;
		this.baseCurrency       = baseCurrency;
		this.tradingAlgorithm   = algorithm;
		this.assetsOwned        = 0;
		this.lastBuyPrice       = Double.MIN_VALUE;
		this.lastSellPrice      = Double.MAX_VALUE;
		this.stopLoss           = false;
		this.currentNetworth    = initialInvestment;
		this.initialInvestment  = initialInvestment;
		this.backtestReport     = new BacktestReport( tradingAlgorithm.getName( ), baseCurrency, 
													quoteCurrency, initialInvestment );
		this.usingNomics        = !candle.equals( "1d" ) || HOST.equalsIgnoreCase( "local" );
		this.historicalData     = new HistoricalDataDAO( );

		if( !this.usingNomics ){
			this.historicalData.setCounter( quoteCurrency );
			this.historicalData.makeNomicsDataMirror( candle, exchange, baseCurrency, quoteCurrency );
		}
		else if( this.usingNomics ){
			NomicsMirrorDAO nomicsMirror = new NomicsMirrorDAO();
			nomicsMirror.openConnection();
			String pair         = nomicsMirror.getMarketsFromExchangeBaseQuote(exchange, baseCurrency, quoteCurrency);
			this.historicalData = new HistoricalDataDAO( Constants.instance().getNomicsApiKey( ), false );
			this.historicalData.setCounter( quoteCurrency );
			this.historicalData.makeNomicsData( candle, exchange, pair );
			nomicsMirror.closeConnection();
		}
	
	}

	/**
	 * Iterates over doIteration with all tradingParameters
	 * @return			Backtesting report with all information
	 * @throws Exception
	 */
	public BacktestReport run( ) throws Exception {

		TradingParameters tradingParameters =  historicalData.doIteration( );
		
		while( tradingParameters != null )
		{
			doIteration( tradingParameters );
			tradingParameters =  historicalData.doIteration( );
		}

		return this.backtestReport;
		
	}
	
	/**
	 * Called each iteration of the backtest with new candle data (trading parameters)
	 * @param currentPrice			Current price associated with candle
	 * @param tradingParameters		Trading params for this iteration
	 * @throws Exception
	 */
	public void doIteration( TradingParameters tradingParameters ) throws Exception
	{
		tradingParameters.updateValue( "last_price", tradingParameters.get( "close" ) );
		tradingParameters.updateValue( "last_buy_price", String.valueOf( this.lastBuyPrice ) );
		tradingParameters.updateValue( "last_sell_price", String.valueOf( this.lastSellPrice ) );
		TradingAlgorithm.SIGNAL signal = tradingAlgorithm.doIteration( tradingParameters );
		
		boolean stopLoss = false;
		
		if( this.lastAction != null ){

			if( this.lastAction == SIGNAL.BUY ){
				onBuy( tradingParameters.getAsDouble( "open" ) );
				this.lastBuyPrice = tradingParameters.getAsDouble( "open" );
			}
			else if( this.lastAction == SIGNAL.SELL || this.tradingAlgorithm.getExternalStopLossFlag( ) ){
				
				double priceToSellAt = tradingParameters.getAsDouble( "open" );
				
				if( this.tradingAlgorithm.getExternalStopLossFlag( ) ) {
					priceToSellAt = this.tradingAlgorithm.getStopLossPrice( ).doubleValue( );
					this.lastAction = SIGNAL.SELL;
					stopLoss = true;
				}
				
				onSell( priceToSellAt );
				this.lastSellPrice = tradingParameters.getAsDouble( "open" );
			}

			this.backtestReport.update( this.lastAction, 
									    tradingParameters.getAsDouble( "close" ), 
									    tradingParameters.getAsDouble( "open" ), 
									    calculateCurrentActualNetworth( tradingParameters.getAsDouble( "open" ) ),
									    this.tradingAlgorithm.getExternalStopLossFlag( ), 
									    tradingParameters.get("timestamp"), 
									    tradingAlgorithm.getSignalsAsJson( ) );
			
			
		}
		
		if( !stopLoss ) {
			this.lastAction = signal;
		}
		else {
			this.lastAction = SIGNAL.HOLD;
		}
	}
	
	/**
	 * Called in run to check signal and perform logic for a BUY
	 * @param signal			The SignalPortionPair object for this iteration
	 * @param currentPrice	The current price for this iteration
	 * @throws Exception
	 */
	protected double onBuy( double currentPrice ) throws Exception {
		double amount = 0;
		amount = this.currentNetworth / currentPrice;
		doBuy( currentPrice, amount );
		return amount;
	}
	
	/**
	 * Called in run to check signal and perform logic for a SELL
	 * @param signal			The SignalPortionPair object for this iteration
	 * @param currentPrice	The current price for this iteration
	 * @throws Exception
	 */
	protected double onSell( double currentPrice ) throws Exception {

		double amount = 0;
		amount        = this.assetsOwned;
		doSell( currentPrice, amount );
		return amount;
	}

	/**
	 * Perform a buy provided the current price and amount	
	 * @param price			The price to purchase at
	 * @param amount			The amount to purchase
	 * @throws Exception
	 */
	public void doBuy( double price, double amount ) throws Exception {
		this.assetsOwned       += amount;
		double amountToSubtract = amount * price;
		this.currentNetworth   -= amountToSubtract;
	}

	/**
	 * Perform a sell given a price and amount
	 * @param price			The price to sell at
	 * @param amount			The amount to sell
	 * @throws Exception
	 */
	public void doSell( double price, double amount ) throws Exception {
		this.currentNetworth += price * amount;
		this.assetsOwned     -= amount;
	}
	
	/**
	 * Calculates the networth relative to everything the user owns, which is exactly the current networth added
	 * to the user's current assets multiplied buy their value
	 * @param currentPrice	The current price of this iteration
	 * @return
	 */
	public double calculateCurrentActualNetworth( double currentPrice ) {
		return this.currentNetworth + ( currentPrice * this.assetsOwned );
	}
	
	/**
	 * Get the base currency
	 * @return String symbol
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * Set the base currency
	 * @param baseCurrency String symbol
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	
	/**
	 * Get the counter currency
	 * @return String sym counter
	 */
	public String getCounterCurrency( )
	{
		return this.quoteCurrency;
	}
	
	/**
	 * Set the quote currency
	 * @param quoteCurrency String symbol
	 */
	public void setCounterCurrency(String quoteCurrency) {
		this.quoteCurrency = quoteCurrency;
	}

	/**
	 * Return the intial for the backtest
	 * @return double 
	 */
	public double getInitialInvestment() {
		return initialInvestment;
	}

	/**
	 * Set the initial for the backtest
	 * @param initialInvestment double
	 */
	public void setInitialInvestment(double initialInvestment) {
		this.initialInvestment = initialInvestment;
	}

	/**
	 * Return the current trading strategy
	 * @return TradingAlgorithm
	 */
	public TradingAlgorithm getTradingAlgorithm() {
		return tradingAlgorithm;
	}

	/**
	 * Set the current trading strategy
	 * @param tradingAlgorithm TradingAlgorithm object
	 */
	public void setTradingAlgorithm(TradingAlgorithm tradingAlgorithm) {
		this.tradingAlgorithm = tradingAlgorithm;
	}

	/**
	 * Return current backtesting report with updated state
	 * @return
	 */
	public BacktestReport getBacktestReport() {
		return backtestReport;
	}

	/**
	 * Set the backtesting report
	 * @param backtestReport
	 */
	public void setBacktestReport(BacktestReport backtestReport) {
		this.backtestReport = backtestReport;
	}

	/**
	 * Get the current networth
	 * @return
	 */
	public double getCurrentNetworth() {
		return currentNetworth;
	}

	/**
	 * Set the current networth
	 * @param currentNetworth
	 */
	public void setCurrentNetworth(double currentNetworth) {
		this.currentNetworth = currentNetworth;
	}

	/**
	 * Get assets owned at current point in time
	 * @return
	 */
	public double getAssetsOwned() {
		return assetsOwned;
	}

	/**
	 * Set assets owned at current point in time
	 * @param assetsOwned
	 */
	public void setAssetsOwned(double assetsOwned) {
		this.assetsOwned = assetsOwned;
	}

	/**
	 * Grab the last signal triggered
	 * @return
	 */
	public SIGNAL getLastAction() {
		return lastAction;
	}

	/**
	 * Set the last action (signal)
	 * @param lastAction
	 */
	public void setLastAction( SIGNAL lastAction ) {
		this.lastAction = lastAction;
	}
	
}
