package gsplatform.services;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import gsplatform.signals.Signal;
import gsplatform.utilities.GS_Utils;

public class BacktestCSVWriter 
{
	/**
	 * The csv file to be written to
	 */
	private FileWriter fileWriter;
	
	/**
	 * The filename to be references
	 */
	private String filename;
	
	/**
	 * List of headers for trading data
	 */
	private String[] headers;
	
	/**
	 * Creates the filewriter and adds the header via CSV format.
	 * Typical headers provided will be:
	 * 0. open ( The opening price of the asset )
	 * 1. close ( The closing price of the asset )
	 * 2. signal param0 ( The first parameter value of the signal(s) being used )
	 * 3. signal paramN
	 * 4. action ( buy, sell, hold )
	 * 5. stop loss triggered ( TRUE, FALSE )
	 * 6. amount bought ( double for the amount of the asset the was purchased / 0 if HOLD )
	 * 7. amount sold ( double for the amount of the asset that was sold / 0 if HOLD)
	 * 8. networth
	 * 9. assets
	 * 10. timestamp
	 * 11. message
	 * 
	 * @param headers
	 * @throws IOException 
	 */
	public BacktestCSVWriter( String headers[], String filename ) throws IOException
	{
		this.fileWriter = new FileWriter( "./testFiles/" + filename + ".csv" );
		this.filename   = filename;
		this.headers    = headers;
	}
	
	/**
	 * Called after each iteration in backtest to produce a csv file with all
	 * logs.
	 * @param signals	The signals to be logged - only add
	 * @throws IOException 
	 */
	public void writeLine( BigDecimal openPrice, BigDecimal closePrice, List< Signal > signals, String action, 
						   List< BigDecimal > targetValues, String stopLossTriggered, 
						   BigDecimal amountBought, BigDecimal amountSold, BigDecimal currencyHeld, 
						   BigDecimal assetsHeld, BigDecimal networth, String timestamp ) throws IOException
	{
		StringBuilder stringBuilder = new StringBuilder( );
		
		stringBuilder.append( openPrice.toString( ) );
		stringBuilder.append( "," );
		
		stringBuilder.append( closePrice.toString( ) );
		stringBuilder.append( "," );
		
		for( Signal signal : signals )
		{
			stringBuilder.append( signal.getIndicatorsAsString( ) ); 
			stringBuilder.append( "," );
		}
		
		if( targetValues != null )
		{
			for( BigDecimal target : targetValues )
			{
				stringBuilder.append( target.toString( ) ); 
				stringBuilder.append( "," );
			}
		}
		
		stringBuilder.append( action );
		stringBuilder.append( "," );
		
		stringBuilder.append( stopLossTriggered );
		stringBuilder.append( "," );
		
		stringBuilder.append( amountBought.toString( ) );
		stringBuilder.append( "," );
		
		stringBuilder.append( amountSold.toString( ) );
		stringBuilder.append( "," );
		
		stringBuilder.append( currencyHeld.toString( ) );
		stringBuilder.append( "," );
		
		stringBuilder.append( assetsHeld.toString( ) );
		stringBuilder.append( "," );
		
		stringBuilder.append( networth.toString( ) );
		stringBuilder.append( "," );
		
		stringBuilder.append( timestamp );
		stringBuilder.append( "," );
		
		this.fileWriter.write( stringBuilder.toString( ) );
		this.fileWriter.write( "\n" );
		
	}
	
	/**
	 * Called to write all parameters and their respective values in a parallel csv file
	 * @param params
	 * @param values
	 * @throws IOException 
	 */
	public void writeParameters( String[] params, String[] values ) throws IOException
	{
		this.fileWriter.write( "\nPARAMETERS\n" );
		
		for( int i = 0; i < values.length; i++ )
		{
			this.fileWriter.write( params[i] + "," + values[i] + "\n" );
		}
		
		this.fileWriter.write( "\nTRADES\n" );
		this.fileWriter.write( Arrays.toString( headers ).replace( "[", "" ).replace( "]", "" ) );
		this.fileWriter.write( "\n" );
	}
	
	/**
	 * Called to write all results with their values
	 * @param params
	 * @param values
	 * @throws IOException 
	 */
	public void writeResults( BacktestReport backtestReport ) throws IOException
	{
		this.fileWriter.write( "\nRESULTS\n" );
		this.fileWriter.write("initial_investment" + "," + backtestReport.getInitialInvestment( ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("final_networth" + "," + backtestReport.getCurrentNetworth( ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("roi" + "," + GS_Utils.convertToPercent( backtestReport.getReturnOnInvestment( ) ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("mean" + "," + GS_Utils.convertToPercent( backtestReport.getMean( ) ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("volatility" + "," + GS_Utils.convertToPercent( backtestReport.getVolatilityMeasures( ) ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("num_trades" + "," + backtestReport.getNumberOfTrades( ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("win ratio" + "," + GS_Utils.convertToPercent( backtestReport.getRatio( ) ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("loss ratio" + "," + GS_Utils.convertToPercent( backtestReport.getLossRatio( ) ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("average win" + "," + new BigDecimal( backtestReport.getAverageWin( ) ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("average loss" + "," +  new BigDecimal( backtestReport.getAverageLoss( ) ).setScale( 2, BigDecimal.ROUND_DOWN ) );
		this.fileWriter.write( "\n" );
		this.fileWriter.write("expectancy" + "," + backtestReport.getExpectancy( ) );
		this.fileWriter.write( "\n" );
	}
	
	public void close( ) throws IOException
	{
		this.fileWriter.close( );
	}
}
