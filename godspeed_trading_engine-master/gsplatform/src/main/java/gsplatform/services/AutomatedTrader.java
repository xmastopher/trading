package gsplatform.services;

import java.math.BigDecimal;
import java.sql.SQLException;
import org.knowm.xchange.currency.CurrencyPair;

import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.TradingStrategiesDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.servlet.database.UserBotsDAO;
import gsplatform.servlet.database.UserStrategiesPackage;
import gsplatform.utilities.TradingParameters;

public abstract class AutomatedTrader {

	/**
	 * Will this bot be performing market orders
	 * or limit orders
	 * @author danielanderson
	 *
	 */
	public enum MODE { MARKET, ORDER };
	
	/**
	 * Is the bot turned on?
	 */
	protected Boolean on;
	
	/**
	 * The mode of the bot - market or order
	 */
	protected MODE mode;
	
	/**
	 * The username of the owner for this bot
	 */
	protected String owner;
	
	/**
	 * The ID associated with this bot
	 */
	protected int botId;
	
	/**
	 * The kline the bot is trading on
	 */
	protected String kline;
	
	/**
	 * Keeps track of buy/sell pairs to calculate trade performance
	 */
	protected class BuySellPair{
		
		private BigDecimal buyPrice;
		private BigDecimal sellPrice;
		
		public BuySellPair( )
		{
			reset( );
		}
		
		public boolean isComplete( )
		{
			return !buyPrice.equals( BigDecimal.ZERO ) && !sellPrice.equals( BigDecimal.ZERO );
		}
		
		public void reset( )
		{
			this.buyPrice  = BigDecimal.ZERO;
			this.sellPrice = BigDecimal.ZERO;
		}
		
		public BigDecimal calculateROI( )
		{
			double gain = ( ( this.sellPrice.doubleValue( ) - this.buyPrice.doubleValue( ) ) / this.buyPrice.doubleValue( ) );
			return new BigDecimal( gain ).setScale( 8, BigDecimal.ROUND_DOWN );
		}
		
		public void setBuyPrice( BigDecimal buyPrice )
		{
			this.buyPrice = buyPrice;
		}
		
		public void setSellPrice( BigDecimal sellPrice )
		{
			this.sellPrice = sellPrice;
		}
	}
	
	/**
	 * Keep track of recent trade
	 */
	protected BuySellPair buySellPair;
	
	/**
	 * last sell price
	 */
	protected Double lastBuy;
	
	/**
	 * last buy price
	 */
	protected Double lastSell;
	
	/**
	 * 	Base constructor for abstract class, derive trading bots from this class.
	 *  Set to off by default
	 */
	AutomatedTrader( )
	{
		this.on   = false;
		this.mode = MODE.ORDER;
		this.lastBuy  = Double.MAX_VALUE;
		this.lastSell = Double.MIN_VALUE;
	}
	
	/**
	 * 	Explicit constructor when mode is provided
	 * 
	 */
	AutomatedTrader( String mode, String owner, int botId )
	{
		this();
		this.botId = botId;
		this.owner = owner;
		this.lastBuy  = Double.MIN_VALUE;
		this.lastSell = Double.MAX_VALUE;
		setMode( mode );
	}
	
	/**
	 * Must be implemented for all children classes
	 */
	public abstract BotLog doIteration( TradingParameters tradingParameters )  throws Exception;
	
	/**
	 * Sets the internal enum mode
	 * @param mode string representation of the mode, is checked and sets the appropriate enum
	 */
	public void setMode( String mode )
	{
		if( mode == null )
		{
			this.mode = MODE.MARKET;
			return;
		}
		
		if( mode.equalsIgnoreCase("MARKET") )
		{
			this.mode = MODE.MARKET;
		}
		
		this.mode = MODE.ORDER;
	}
	
	
	public void recoverState( BotLog botLog, UserBot userBot ) throws ClassNotFoundException, SQLException { ; }
	
	/**
	 * Override in a trading bot to define how to handle a market buy
	 * 
	 * @param amount			The amount the buy
	 * @param currencyPair	The trading pair to perform the buy ie. ETH/USD
	 * @throws Exception		
	 */
	public abstract void doMarketBuy( BigDecimal amount, CurrencyPair currencyPair ) throws Exception;
	
	/**
	 * Override in a trading bot to define how a market sell is handled
	 * 
	 * @param amount			The amount to sell
	 * @param currencyPair	The pair to perform the sell ie. ETH/USD
	 * @throws Exception
	 */
	public abstract void doMarketSell( BigDecimal amount, CurrencyPair currencyPair ) throws Exception;
	
	/**
	 * Override in a trading bot to define how a buy order is handled
	 * 
	 * @param amount			The amount to set a buy order for
	 * @param rate			The bid
	 * @param currencyPair	The pair to set the order for ie. ETH/USD
	 * @throws Exception
	 */
	public abstract void doBuyOrder( BigDecimal amount, BigDecimal rate, CurrencyPair currencyPair ) throws Exception;
	
	/**
	 * Override in a trading bot to define how a sell order is handled
	 * 
	 * @param amount			The amount to set the sell order for
	 * @param rate			The asking price
	 * @param currencyPair	The pair to set the order for ie. ETH/USD
	 * @throws Exception
	 */
	public abstract void doSellOrder( BigDecimal amount, BigDecimal rate, CurrencyPair currencyPair ) throws Exception;
	
	public abstract String createLogMessage( );
	
	public abstract int getPreprossingValue( );
	
	public String getOwner( )
	{
		return this.owner;
	}
	
	public int getBotId( )
	{
		return this.botId;
	}
	
	public String getKline( )
	{
		return this.kline;
	}
	
	public boolean tradeComplete( )
	{
		return this.buySellPair.isComplete( );
	}
	
	public void resetTrade() {
		this.buySellPair.reset( );
	}
	
	public BigDecimal getTradeROI( )
	{
		return this.buySellPair.calculateROI( );
	}
	
}
