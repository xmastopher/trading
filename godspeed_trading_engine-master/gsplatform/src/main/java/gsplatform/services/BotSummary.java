package gsplatform.services;

import java.math.BigDecimal;

import gsplatform.servlet.database.UserBot;

public class BotSummary 
{
	protected UserBot userBot;

	protected BigDecimal averageROIPerTrade;
	
	protected int totalWins;
	
	protected int totalLosses;
	
	protected BigDecimal winLossRate;
	
	
	public BotSummary( UserBot userBot )
	{
		this.userBot = userBot;
	}
	
	public UserBot getUserBot() {
		return userBot;
	}

	public void setUserBot(UserBot userBot) {
		this.userBot = userBot;
	}

	public BigDecimal getAverageROIPerTrade() {
		return averageROIPerTrade;
	}

	public void setAverageROIPerTrade(BigDecimal averageROIPerTrade) {
		this.averageROIPerTrade = averageROIPerTrade;
	}

	public int getTotalWins() {
		return totalWins;
	}

	public void setTotalWins(int totalWins) {
		this.totalWins = totalWins;
	}

	public int getTotalLosses() {
		return totalLosses;
	}

	public void setTotalLosses(int totalLosses) {
		this.totalLosses = totalLosses;
	}

	public BigDecimal getWinLossRate() {
		return winLossRate;
	}

	public void setWinLossRate(BigDecimal winLossRate) {
		this.winLossRate = winLossRate;
	}
	
}
