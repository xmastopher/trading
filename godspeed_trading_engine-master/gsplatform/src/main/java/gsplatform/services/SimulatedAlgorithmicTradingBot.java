package gsplatform.services;

import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.algorithms.TradingAlgorithm.SIGNAL;
import gsplatform.jobs.GS_Logger;
import gsplatform.servlet.database.BotLog;
import gsplatform.servlet.database.ExchangeAPIDAO;
import gsplatform.servlet.database.UserBot;
import gsplatform.utilities.ISO8601;
import gsplatform.utilities.TradingParameters;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.knowm.xchange.currency.CurrencyPair;

/**
 * The Simulated Algorithmic traders provide the base functionality
 * for all algorithmic trading minus the actual API calls to the 
 * exchange.  We run sims to prove strategies in real time or
 * to set up for a real trading bot.
 * @author danielanderson
 */
public class SimulatedAlgorithmicTradingBot extends AutomatedTrader 
{
	/**
	 * Reference to logger instance
	 */
	GS_Logger LOGGER = GS_Logger.getLogger( );
	
	/**
	 * Will always be one when using a simlulated bot
	 */
	private static final double DEFAULT_NETWORTH = 1.00;
	/**
	 * Used to set scale for big decimals
	 */
	private static final int DECIMAL_PRECISION = 8;
	
	/**
	 *  The trading algorithm to deploy
	 */
	private TradingAlgorithm tradingAlgorithm;
	
	/**
	 *  The base currency in the pair
	 */
	private String baseCurrency;
	
	/**
	 *  The counter currency in the pair
	 */
	private String counterCurrency;
	
	/**
	 *  The exchange object used to perform the trades
	 */
	private String interval;
	
	/**
	 * Amount to trade - just keep at 100 for now
	 */
	private double amountToTrade;
	
	/**
	 * Fake wallet
	 * @author danielanderson
	 *
	 */
	public class Wallet
	{
		private BigDecimal quote;
		private BigDecimal assets;
		
		public Wallet( BigDecimal quote, BigDecimal assets )
		{
			this.quote  = quote;
			this.assets = assets;
		}
		
		public BigDecimal getBalance( String value )
		{
			if( value.equals( "quote" ) )
			{
				return this.quote;
			}
			
			return this.assets;
		}
		
		@Override
		public String toString( ) 
		{
			return " ASSETS: " + assets.setScale( 8, BigDecimal.ROUND_DOWN ).toPlainString( ) + ", QUOTE: " + quote.setScale( 8, BigDecimal.ROUND_DOWN ).toPlainString( );
		}
	}
	
	/**
	 * Keeps track of the last price that was non-zero, this way we can avoid any zero prices
	 */
	private BigDecimal lastNonZeroPrice;
	
	/**
	 * Internal fake wallet
	 */
	private Wallet userWallet;
	
	/**
	 * Constructor for an Algorithmic based trading bot, this differs from other bots in that it specifically focuses
	 * on trading a BASE/COUNTER pair with a specified trading algorithm.  The exchange must also be specified
	 * 
	 * @param tradingAlgorithm		The trading algorithm object the bot will use for signals
	 * @param baseCurrency			The base currency symbol for the trading pair - BASE/COUNTER
	 * @param counterCurrency		The counter currency symbol for the trading pair - BASE/COUNTER
	 * @param username				Username of the pre-authenticated user creating the bot
	 * @param exchangeName			
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public SimulatedAlgorithmicTradingBot( TradingAlgorithm tradingAlgorithm, String baseCurrency, String counterCurrency, 
											String username, int botId, String exchangeName, String mode ) throws IOException, ClassNotFoundException, SQLException
	{
		super( mode, username, botId );
		this.tradingAlgorithm        = tradingAlgorithm;
		this.baseCurrency 			= baseCurrency;
		this.counterCurrency 		= counterCurrency;
		this.amountToTrade           = 100;
		this.userWallet              = new Wallet( new BigDecimal( DEFAULT_NETWORTH ), BigDecimal.ZERO );
		this.lastNonZeroPrice        = BigDecimal.ZERO;
		this.buySellPair             = new BuySellPair( );
	}
	
	/**
	 * setup method used for testing
	 * @param exchange
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 */
	public SimulatedAlgorithmicTradingBot( String username, int botId, String exchange, String baseCurrency, String counterCurrency ) throws ClassNotFoundException, SQLException, IOException
	{
		super( null, username, botId );
		ExchangeAPIDAO exchangeApiDao = new ExchangeAPIDAO( username, exchange );
		exchangeApiDao.openConnection( );
		this.baseCurrency    = baseCurrency;
		this.counterCurrency = counterCurrency;
		exchangeApiDao.closeConnection( );
	}
	
	public void setOwner( String username )
	{
		this.owner = username;
	}
	
	/**
	 * Called every time a new candle arrives
	 * @throws Exception 
	 */
	@Override
	public BotLog doIteration( TradingParameters tradingParameters ) throws Exception 
	{
		tradingParameters.updateValue("last_sell_price", String.valueOf( this.lastSell ) );
		tradingParameters.updateValue("last_buy_price", String.valueOf( this.lastBuy ) );
		TradingAlgorithm.SIGNAL signal = tradingAlgorithm.doIteration( tradingParameters );
		BigDecimal lastPrice           = tradingParameters.getAsBigDecimal( "last_price", DECIMAL_PRECISION );

		if( signal == SIGNAL.BUY ) {
	        BigDecimal amountToBuy = new BigDecimal( this.userWallet.getBalance( "quote" ).doubleValue( ) / lastPrice.doubleValue( ) )
	        												.setScale( 8, BigDecimal.ROUND_DOWN );
	        
			doMarketBuy( amountToBuy, new CurrencyPair( this.baseCurrency, this.counterCurrency ) );
			this.buySellPair.setBuyPrice( lastPrice );
			this.lastBuy = lastPrice.doubleValue( );
			
		}
		else if( signal == SIGNAL.SELL ) {
	        BigDecimal amountToSell = new BigDecimal( this.userWallet.getBalance( "asssets" ).doubleValue( ) * lastPrice.doubleValue( ) )
	        												.setScale( 8, BigDecimal.ROUND_DOWN ); 
	        
			doMarketSell( amountToSell,  new CurrencyPair( this.baseCurrency, this.counterCurrency )  );
			this.buySellPair.setSellPrice( lastPrice );
			this.lastSell = lastPrice.doubleValue( );
		}
			
		LOGGER.addMessageEntry("SIMULATIONS", "SimulatedAlgorithmicTrader", createLogMessage( ) );
		
		BotLog botLog = new BotLog( this.botId, this.owner, signal.toString( ), calculateNetworth( lastPrice ), 
								    this.userWallet.quote, this.userWallet.assets, lastPrice, this.tradingAlgorithm.getSignalsAsJson( ).toString( ),
								    new Timestamp( ISO8601.toCalendar( tradingParameters.get( "timestamp" ) ).getTimeInMillis( ) ) );
		
		return botLog;
	}
	
	/**
	 * Performs a market buy
	 */
	@Override
	public void doMarketBuy( BigDecimal amount, CurrencyPair currencyPair ) throws Exception {
		this.userWallet.assets = amount;
		this.userWallet.quote  = BigDecimal.ZERO;
	}

	/**
	 * Performs a market sell
	 */
	@Override
	public void doMarketSell( BigDecimal amount, CurrencyPair currencyPair ) throws Exception {
		this.userWallet.assets = BigDecimal.ZERO;
		this.userWallet.quote  = amount;
	}

	/**
	 * Performs a buy order and stores it in last order
	 */
	@Override
	public void doBuyOrder( BigDecimal amount, BigDecimal rate, CurrencyPair currencyPair ) throws Exception {
	}

	/**
	 * Performs a sell order and stores it in last order
	 */
	@Override
	public void doSellOrder( BigDecimal amount, BigDecimal rate, CurrencyPair currencyPair ) throws Exception {
	}
	
	protected BigDecimal calculateNetworth( BigDecimal lastPrice )
	{
		return new BigDecimal( this.userWallet.assets.doubleValue( ) * lastPrice.doubleValue( ) + this.userWallet.quote.doubleValue( ) )
				.setScale( 8, BigDecimal.ROUND_DOWN );
	}
	
	@Override
	public String createLogMessage() {
		String userString = this.owner + "-" + this.botId;
		return "[BOTLOG:" + userString + "] " + "WALLET: " + this.userWallet.toString( ) + ", MESSAGE: " + tradingAlgorithm.getLogMessage( );
	}

	@Override
	public int getPreprossingValue() 
	{
		return this.tradingAlgorithm.getPreprocessingValue( )-1;
	}
	
	/**
	 * Called to recover state from bot log
	 * @param lastAction
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	@Override
	public void recoverState( BotLog botLog, UserBot userBot ) throws ClassNotFoundException, 
																	 SQLException {
		
		super.recoverState( botLog, userBot );
		
		if( botLog == null ) {
			return;
		}
		
		this.userWallet = new Wallet( botLog.getQuoteOwned( ), botLog.getAsssetsOwned( ) );
		String lastAction = botLog.getAction( );
		double lastPrice  = botLog.getLastPrice( ).doubleValue( );
		
		if( botLog.getAction( ).equalsIgnoreCase( "buy") ) {
			this.buySellPair = new BuySellPair( );
			this.buySellPair.setBuyPrice( botLog.getLastPrice( ) );
			this.tradingAlgorithm.setWaitingToBuy( false );
			this.tradingAlgorithm.setWaitingToSell( true );
			this.lastBuy = lastPrice;
		} 
		else if( lastAction.equalsIgnoreCase( "sell" ) ) {
			this.tradingAlgorithm.setWaitingToSell( false );
			this.tradingAlgorithm.setWaitingToBuy( true );
			this.lastSell = lastPrice;
		}
	}

	public static double getDefaultNetworth() {
		return DEFAULT_NETWORTH;
	}

	public static int getDecimalPrecision() {
		return DECIMAL_PRECISION;
	}

	public TradingAlgorithm getTradingAlgorithm() {
		return tradingAlgorithm;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public String getCounterCurrency() {
		return counterCurrency;
	}

	public String getInterval() {
		return interval;
	}

	public double getAmountToTrade() {
		return amountToTrade;
	}

	public BigDecimal getLastNonZeroPrice() {
		return lastNonZeroPrice;
	}

	public Wallet getUserWallet() {
		return userWallet;
	}

	public void setLOGGER(GS_Logger lOGGER) {
		LOGGER = lOGGER;
	}

	public void setTradingAlgorithm(TradingAlgorithm tradingAlgorithm) {
		this.tradingAlgorithm = tradingAlgorithm;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public void setCounterCurrency(String counterCurrency) {
		this.counterCurrency = counterCurrency;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public void setAmountToTrade(double amountToTrade) {
		this.amountToTrade = amountToTrade;
	}

	public void setLastNonZeroPrice(BigDecimal lastNonZeroPrice) {
		this.lastNonZeroPrice = lastNonZeroPrice;
	}

	public void setUserWallet(Wallet userWallet) {
		this.userWallet = userWallet;
	}

}
