package gsplatform.factories;

import org.json.JSONException;
import org.json.JSONObject;

public interface FactoryImpl<T> {
	
	public T createObject( JSONObject specification ) throws JSONException;

}
