package gsplatform.factories;

import java.math.BigDecimal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.algorithms.SignalStrategy;
import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.servlet.database.UserStrategiesPackage;
import gsplatform.signals.Signal;

public class TradingStrategyFactory implements FactoryImpl< TradingAlgorithm > {

	@Override
	public TradingAlgorithm createObject(JSONObject specification) {
		
		try 
		{
			
			TradingSignalFactory tradingSignalFactory = new TradingSignalFactory( );
			
			//For loop over buy signals - create signals and add to strategy
			JSONArray sellSignals = specification.getJSONArray( "sellSignals" );
			Signal sellSignal     = tradingSignalFactory.createObject( sellSignals.getJSONObject( 0 ) );
			
			//For loop over sell signals - create signals and add to strategy
			JSONArray buySignals = specification.getJSONArray( "buySignals" );
			Signal buySignal     = tradingSignalFactory.createObject( buySignals.getJSONObject( 0 ) );
			
			BigDecimal stopLoss = new BigDecimal( specification.getDouble( "stopLoss" ) ).setScale( 2, BigDecimal.ROUND_DOWN );
			SignalStrategy ss = new SignalStrategy( stopLoss, buySignal, sellSignal );
			ss.setWaitingToBuy( true );
			ss.setName( specification.getString( "name") );
			return ss;
		}
		catch( Exception e )
		{
			System.out.println( e.getMessage( ) );
			return null;
		}
	}
	
	public TradingAlgorithm createObject( UserStrategiesPackage usp, int index ) throws JSONException {
		JSONArray buySignals    = new JSONArray( usp.getBuySignal( index ) );
		
		//For loop over buy signals - create signals and add to strategy
		JSONArray sellSignals    = new JSONArray( usp.getSellSignal( index ) );
		
		//Signal based strategy - for now assume single signal
		if( buySignals.length( ) > 0 && sellSignals.length( ) > 0 )
		{
			Signal buySignal  = new TradingSignalFactory().createObject( buySignals.getJSONObject( 0 ) );
			Signal sellSignal = new TradingSignalFactory().createObject( sellSignals.getJSONObject( 0 ) );
			SignalStrategy ss = new SignalStrategy( usp.getStopLoss( 0 ), buySignal, sellSignal );
			ss.setWaitingToBuy( true );
			return ss;
		}
		
		//Buy strat w/ targets 
		if( buySignals.length( ) > 0 && sellSignals.length( ) == 0 )
		{
			return null;
		}
		
		//Sell strat w/ targets
		if( buySignals.length( ) == 0 && sellSignals.length( ) > 0 )
		{
			return null;
		}
		
		return null;
	}

}
