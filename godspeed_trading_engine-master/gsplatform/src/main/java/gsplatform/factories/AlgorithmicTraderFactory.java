package gsplatform.factories;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONObject;

import gsplatform.algorithms.TradingAlgorithm;
import gsplatform.services.SimulatedAlgorithmicTradingBot;
import gsplatform.servlet.database.UserBot;

public class AlgorithmicTraderFactory implements FactoryImpl< SimulatedAlgorithmicTradingBot > {

	@Override
	public SimulatedAlgorithmicTradingBot createObject(JSONObject specification) 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	public SimulatedAlgorithmicTradingBot createObject( UserBot userBot, TradingAlgorithm tradingStrategy ) throws ClassNotFoundException, IOException, SQLException 
	{
		SimulatedAlgorithmicTradingBot algorithmicTradingBot = new SimulatedAlgorithmicTradingBot( tradingStrategy, userBot.getMarketBase(), userBot.getMarketQuote( ), userBot.getUsername( ), userBot.getBotId( ), userBot.getExchange( ), "BUY" );
		algorithmicTradingBot.setOwner( userBot.getUsername( ) );
		return algorithmicTradingBot;
	}

}
