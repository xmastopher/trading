package gsplatform.factories;

import org.json.JSONObject;

import gsplatform.algorithms.*;
import gsplatform.services.Backtest;

public class BacktestFactory 
{
	/**
	 * Enums to represent the strategy chosen by the user
	 * @author danielanderson
	 *
	 */
	public enum ALGORITHM  { BUY_AND_HOLD, SMA, SMA_CROSS };
	
	/**
	 * Enum to represent the mode the user has chosen to use the signals for 
	 * @author danielanderson
	 *
	 */
	public enum MODE { BUY, SELL, SIGNALS, NONE };
    
	/**
	 * Internal function that returns the enum from a string
	 * @param strategyName
	 * @return
	 */
	public static ALGORITHM stringToEnum( String strategyName )
	{
		if( strategyName.equalsIgnoreCase("SMA Strategy") || 
				strategyName.equalsIgnoreCase( "Simple Moving Average Strategy" ) ||
					strategyName.equalsIgnoreCase( "SMA" ) ) 
		{
			return ALGORITHM.SMA;
		}
		
		if( strategyName.equalsIgnoreCase("SMA Cross Strategy") || strategyName.equalsIgnoreCase( "SMA Cross" ) )
		{
			return ALGORITHM.SMA_CROSS;
		}
		
		return ALGORITHM.BUY_AND_HOLD;
		
	}
	
	/**
	 * Method that transforms string mode to enum base
	 * @param mode
	 * @return
	 */
	public static MODE getModeFromString( String mode )
	{
		
		switch( mode )
		{
			case "BUY":
			case "buy":
				return MODE.BUY;
			case "SELL":
			case "sell":
				return MODE.SELL;
			case "SIGNALS":
			case "signals":
				return MODE.SIGNALS;
		}
		
		return MODE.NONE;
	}
	
	/**
	 * Internal function that converts provided params to AlgorithmParmeter object list
	 * @param parameters
	 * @param parameterValues
	 * @return
	 */
	public static AlgorithmParameter[] arraysToParameters( String[] parameters, String[] parameterValues )
	{
		AlgorithmParameter[] algorithmParameters = new AlgorithmParameter[ parameters.length ];
		
		for( int i = 0; i < parameters.length; i++ )
		{
			algorithmParameters[ i ] = new AlgorithmParameter( parameters[ i ], parameterValues[ i ] );
		}
		
		return algorithmParameters;
	
	}
	
	/**
	 * Given a JSON description of a backtest, parse and create the backtest object
	 * @param backtestJSON
	 * @return
	 * @throws Exception
	 */
	public static Backtest createBacktest( JSONObject backtestJSON, int backtestIndex ) throws Exception
	{
		TradingAlgorithm tradingStrategy = new TradingStrategyFactory().createObject( backtestJSON.getJSONObject( "strategy") );
		String exchange          = backtestJSON.getString( "exchange" );
		String currencyPair      = backtestJSON.getJSONArray("markets" ).getString( backtestIndex );
		String candleSize        = backtestJSON.getString( "candleSize" );
		Double initialInvestment = backtestJSON.getDouble( "initialInvestment" );

		String baseCurrency    = currencyPair.split( "-" )[ 0 ];

		String counterCurrency = currencyPair.split( "-" )[ 1 ];
		
		return new Backtest( tradingStrategy, exchange, candleSize, baseCurrency, counterCurrency, initialInvestment );
	}
	
	
}
