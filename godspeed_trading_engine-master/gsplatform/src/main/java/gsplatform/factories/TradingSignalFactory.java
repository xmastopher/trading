package gsplatform.factories;

import org.json.JSONException;
import org.json.JSONObject;

import gsplatform.signals.*;

public class TradingSignalFactory implements FactoryImpl< Signal > {

	@Override
	public Signal createObject(JSONObject specification) throws JSONException 
	{
	
		String signalName = specification.getString( "signal" );
		
		switch( signalName )
		{
			case "TARGET-BUY":
				double target = specification.getInt( "target" );
				return new TargetBuySignal( target );
				
			case "TARGET-SELL":
				target = specification.getDouble( "target" );
				return new TargetSellSignal( target );
			
			case "SMA-BUY":
				int sma = specification.getInt( "sma" );
				return new SMABuySignal( sma );
				
			case "SMA-SELL":
				sma = specification.getInt( "sma" );
				return new SMASellSignal( sma );
				
			case "EMA-BUY":
				int ema = specification.getInt( "ema" );
				return new EMABuySignal( ema );
				
			case "EMA-SELL":
				sma = specification.getInt( "ema" );
				return new EMASellSignal( sma );
				
			case "SMA-CROSS-BUY":
				int smaSmaller = specification.getInt( "smaSmaller" );
				int smaLarger  = specification.getInt( "smaLarger" );
				return new SMACrossBuySignal( smaSmaller, smaLarger );
				
			case "SMA-CROSS-SELL":
				smaSmaller = specification.getInt( "smaSmaller" );
				smaLarger  = specification.getInt( "smaLarger" );
				return new SMACrossSellSignal( smaSmaller, smaLarger );
				
			case "EMA-CROSS-BUY":
				int emaSmaller = specification.getInt( "emaSmaller" );
				int emaLarger  = specification.getInt( "emaLarger" );
				return new EMACrossBuySignal( emaSmaller, emaLarger );
				
			case "EMA-CROSS-SELL":
				emaSmaller = specification.getInt( "emaSmaller" );
				emaLarger  = specification.getInt( "emaLarger" );
				return new EMACrossSellSignal( emaSmaller, emaLarger );
				
			case "BOLLINGER-BANDS-BUY":
				sma = specification.getInt( "sma" );
				double std = specification.getDouble( "std" );
				return new BollingerBandsBuySignal( sma, std );
				
			case "BOLLINGER-BANDS-SELL":
				sma = specification.getInt( "sma" );
				std = specification.getDouble( "std" );
				return new BollingerBandsSellSignal( sma, std );
				
			case "LOCAL-MINIMA-BUY":
				int windowSize = specification.getInt( "windowSize" );
				return new LocalMinimaBuySignal( windowSize );
				
			case "LOCAL-MAXIMA-SELL":
				windowSize = specification.getInt( "windowSize" );
				return new LocalMinimaSellSignal( windowSize );
				
			case "RSI-BUY":
				int period 		= specification.getInt( "period" );
				double threshold = specification.getDouble( "threshold" );
				return new RSIBuySignal( period, threshold );
				
			case "RSI-SELL":
				period 		= specification.getInt( "period" );
				threshold   = specification.getDouble( "threshold" );
				return new RSISellSignal( period, threshold );
				
			case "MACD-BUY":
				emaSmaller 	  = specification.getInt( "emaSmaller" );
				emaLarger 	  = specification.getInt( "emaLarger" );
				int emaSignal = specification.getInt( "emaSignal" );
				return new MACDBuySignal( emaSmaller, emaLarger, emaSignal );
				
			case "MACD-SELL":
				emaSmaller 	 = specification.getInt( "emaSmaller" );
				emaLarger 	 = specification.getInt( "emaLarger" );
				emaSignal    = specification.getInt( "emaSignal" );
				return new MACDSellSignal( emaSmaller, emaLarger, emaSignal );
		}
		
		return null;
	}

}
