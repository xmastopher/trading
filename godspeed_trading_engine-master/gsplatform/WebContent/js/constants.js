//Prefix for the server URL
var serverURL = "http://localhost:8080/gsplatform";

//Terrible crummy session implementation -TODO: Find a real auth solution
var Constants = (function () {
    
	//Reference to session instance
	var instance;
 
	//Creates a session instance and returns it
    function createInstance( ) {
        var constants = {
        		
        		//GENERAL
        		version: "TRAVAS ALPHA v1.0.5",
        		
        		//PAGE URLS
        		dashboard: serverURL+"/dashboard.html",
        		login: serverURL+"/login.html",
        		
        		//API URLS
        		arbitrageURL: serverURL+"/arbitrage",
        		backtestURL: serverURL+"/backtest",
        		botLogsURL: serverURL+"/botlogs",
        		botTradesURL: serverURL+"/bottrades",
        		botLeaderboardsURL: serverURL+"/botleaderboards",
        		botSummaryURL: serverURL+"/botsummary",
        		changeLockURL: serverURL+"/changelock",
        		deleteBotURL: serverURL+"/deletebot",
        		deleteStrategyURL: serverURL+"/deletestrategy",
        		exchangeCandlesURL: serverURL+"/exchangecandles",
        		getSignalsURL: serverURL+"/getsignals",
        		getStrategiesURL: serverURL+"/getstrategies",
        		inboxURL: serverURL+"/inbox",
        		loginURL: serverURL+"/login",
        		marketLookupURL: serverURL+"/marketlookup",
        		messagingURL: serverURL+"/messaging",
        		portfolioURL: serverURL+"/portfolio",
        		portfolioSnapshotURL: serverURL+"/portfoliosnapshot",
        		saveBotURL: serverURL+"/savebot",
        		saveStrategyURL: serverURL+"/savestrategy",
        		signupURL: serverURL+"/signup",
        		startStopBotURL: serverURL+"/startstopbot",
        		tradingLimitURL: serverURL+"/tradinglimit",
        		userProfileURL: serverURL+"/userprofile",
        		dashboardURL: serverURL+"/userdashboard",
        		changePasswordURL: serverURL+"/updatepassword",
        		updateTickersURL: serverURL+"/updatetickers",
        }
        return constants;
    }
    
    //Updates token with new session token
    function updateToken( pToken ){
    		if(!instance){
    			 return;
    		}
    		instance.token = pToken;
    }
 
    //Defines what is returned from getInstance
    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
    
    
})();
