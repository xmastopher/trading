/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if( document.getElementById("username") !== undefined ){
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}

var cache = {
	userTopBot: undefined,
	currentLeaderboard: "alltime"
};

/***
 * Called on load
 * @returns
 */
function onLoad( ){
	'use strict';
	let firstRowId = getBotLeaderboards( );
	
	if( cache.userTopBot != undefined ){
		var id         =  cache.userTopBot.rank + "-" + globalSessionInit.userProfileResponse.username + "-" + cache.userTopBot.botId;
		selectRow( $('#'+id), cache.userTopBot.rank, globalSessionInit.userProfileResponse.username, cache.userTopBot.botId );
	}
	else{
		selectRow( $('#'+firstRowId), firstRowId.split("-")[0], firstRowId.split("-")[1], firstRowId.split("-")[2] );
	}
	
}

$('#sendData').on('click', function () {
	getBotLeaderboards( );
});

$('#alltimeButton').on('click', function () {
	cache.currentLeaderboard = "alltime";
	getBotLeaderboards( );
	document.getElementById("timespan").innerHTML = $(this)[0].innerHTML;
    selectRow( cache.userTopBot.rank, 
			   globalSessionInit.userProfileResponse.username, 
			   cache.userTopBot.botId );
});


$('#weeklyButton').on('click', function () {
	cache.currentLeaderboard = "weekly";
	getBotLeaderboards( );
	document.getElementById("timespan").innerHTML = $(this)[0].innerHTML;
    selectRow( cache.userTopBot.rank, 
			   globalSessionInit.userProfileResponse.username, 
			   cache.userTopBot.botId );
});


$('#dailyButton').on('click', function () {
	cache.currentLeaderboard = "daily";
	getBotLeaderboards( );
	document.getElementById("timespan").innerHTML = $(this)[0].innerHTML;
    selectRow( cache.userTopBot.rank, 
			   globalSessionInit.userProfileResponse.username, 
			   cache.userTopBot.botId );
});

$(document).on('click', '#dataTable > tbody > tr', function( ) {
	var id = $(this)[0].id;	//ROW-USER-BOT_ID
	selectRow( $(this), id.split("-")[0], id.split("-")[1], id.split("-")[2] );
});

/**
 * Called to select a specific row from the leaderboards table
 * @param element
 * @param row
 * @param username
 * @param botId
 * @returns
 */
function selectRow( element, row, username, botId ){
	element.addClass('bg-info').siblings().removeClass('bg-info');
	var response = getBotLogs( username, botId );
	var data     = response.botLogs;
	//document.getElementById("botOwnerSpan").innerHTML  = username.toUpperCase( );
	//document.getElementById("botNameSpan").innerHTML   = data[0].botName.toUpperCase( );
	//document.getElementById("botMarketSpan").innerHTML = (data[0].market + " (" + data[0].exchange + ")").toUpperCase( );
	if( data.length > 0 ){ 
		createGraphBotLeaderboards( username, row, data );
	}
	else{
		
	}
}

/**
 * Provided a username and botId, returns a list of a specific user's bot's
 * performance - this is always daily performance data points
 * @param username
 * @param botId
 * @returns
 */
function getBotLogs( username, botId ){
	
	var request = {
			"botOwner": username,
			"botId": botId
	};
	
    var botLogsEndpoint = new AuthenticatedEndpoint( );
    var response		    = botLogsEndpoint.doRequest( request, Constants.getInstance( ).botLogsURL ); 
	
    //Response is good - reset selections and load page
    if( response.success ){
		return response;
    }
    
    else {
    		alert( response.message );
    		return false;
    }
}

/**
 * Returns up to 100 botleaderboards
 * @returns
 */
function getBotLeaderboards(){
	
	var request = {
			"numberToFetch": 100,
			"leaderboard": cache.currentLeaderboard
	}
	
    var botLeaderboardsEndpoint = new AuthenticatedEndpoint( );
    var response			        = botLeaderboardsEndpoint.doRequest( request, Constants.getInstance( ).botLeaderboardsURL ); 
	
    //Response is good - reset selections and load page
    if( response.success ){
    		if( response.bots.length === 0 )
    			return;
		loadLeaderboards( response.bots );
    }
    
    else {
    		alert( response.message );
    		return false;
    }
	
}

/**
 * Loads the bot leaderboards from the top N fetched rows
 * @param bots
 * @returns
 */
function loadLeaderboards( bots ){
	
	if( bots == undefined )
		return;
	
	cache.userTopBot = undefined;
	
	let username = document.getElementById("username").innerHTML;
	let html		 = '';
	let firstRowId = 0;
	
	for (i = 0; i < bots.length; i++)
    {
		let bot  = bots[i];
		let trId = (i+1) + "-" + bot.username + "-" + bot.botId;
		
		if( bot.username === username.toLowerCase() && cache.userTopBot === undefined ){
			setUserTopBot( bot.botName, i+1, bot.botId );
		}
		if( i === 0 ){
			firstRowId = trId;
		}
		
		//Set classes
		var roiClass    = Number(bot.roi.replace("%","")) > 0 		? "text-success" : "text-danger";
		var avgRoiClass = Number(bot.avgRoi.replace("%","")) > 0 		? "text-success" : "text-danger";
		var winLoss     = bot.winLoss > 1 							? "text-success" : "text-danger";
		var bestTrade   = Number(bot.bestTrade.replace("%","")) > 0 	? "text-success" : "text-danger";
		var td          = bot.username === username.toLowerCase()  	? '<td class="font-weight-bold">' : '<td>';
		var th          = bot.username === username.toLowerCase() 	? '<th scope="row" class="text-primary font-weight-bold">' : 
																	  '<th scope="row">';
        html+= '<tr id="' + trId + '">' +
		  th  + (i+1) +'</th>' +
		  td + bot.username.toUpperCase() +'</td>'  +
		  '<td>'+ bot.botName.toUpperCase() +'</td>' 	+
		  '<td>'+ bot.exchange.toUpperCase() +'</td>' 	+
		  '<td>'+ bot.market.toUpperCase() +'</td>' 	+
		  '<td class="'+ roiClass +'">'+ bot.roi +'</td>' 		+
		  '<td class="'+ avgRoiClass +'">'+ bot.avgRoi +'</td>' 	+
		  '<td class="'+ winLoss +'">'+ bot.winLoss +'</td>' 	+
		  '<td class="'+ bestTrade +'">'+ bot.bestTrade +'</td>'
		  '</tr>';
    }
	
	//User is not in top N - do API call for bot's rank
	if( cache.userTopBot == undefined ){
		var botRank = getBotRank( username );
		if( botRank != undefined ){
			setUserTopBot( botRank.botName, botRank.rank, botRank.botId );
		}
	}
	
	$("#innerBotTable")[0].innerHTML = "";
    $("#innerBotTable").append(html);
    return firstRowId;
}

/**
 * Call to set the user's top ranked bot
 * @param botName	The name of the bot that is ranked
 * @param rank		The rank of the user's top bot
 * @returns
 */
function setUserTopBot( botName, rank, id ){
	cache.userTopBot = {
		"botName": botName,
		"rank": rank,
		"botId": id
	};
	document.getElementById("botRank").innerHTML   = rank;
	document.getElementById("rankedBot").innerHTML = botName;
}

/**
 * Return a bots rank give the bot owner's
 * username.  Typically called if bot does not
 * fall within current leaderboard
 * @param username
 * @returns
 */
function getBotRank( username ){
	
	var request = {
			"numberToFetch": 100,
			"grabBotRank": true,
			"username": username,
			"leaderboard": cache.currentLeaderboard
	}
	
    var botLeaderboardsEndpoint = new AuthenticatedEndpoint( );
    var response			        = botLeaderboardsEndpoint.doRequest( request, Constants.getInstance( ).botLeaderboardsURL ); 
    
    //Response is good - reset selections and load page
    if( response.success  && response.rank != 0){
		return { 
			"rank": response.rank, 
			"botName": response.botName 
		};
    }
    else if( !response.success) {
    		alert( response.message );
    		return false;
    }
    
    return undefined;
	
}

