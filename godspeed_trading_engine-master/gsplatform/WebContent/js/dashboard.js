/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if (document.getElementById("username") !== undefined) {
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}

/**
* Constant list for ticker colors
*/
const tickerColors = [ 'rgb(21, 121, 246)', 'rgb(148, 1, 150)', 'rgb(251, 163, 88)', 'rgb(1, 224, 39)' ];

/**
 * Page level cache and globals
 */
let cache = {
	map : {
		binance : new Object(),
		bittrex : new Object(),
		bitfinex : new Object(),
		hitbtc : new Object(),
		bitflyer : new Object(),
		gdax : new Object(),
		poloniex : new Object(),
		kraken : new Object(),
		gemini : new Object(),
		bithumb : new Object()
	},
	selectedExchange : "",
	selectedBase : "",
	selectedQuote : "",
	selectedTickerbox : -1,
	updateInterval : 60000,
	tickers: [],
	selectedCandles: [ "1d", "1d", "1d", "1d" ]
}

/**
 * Endpoints for dashboard
 */
let endpoints = {
	marketLookupEndpoint : Constants.getInstance().marketLookupURL,
	dashboardEndpoint : Constants.getInstance().dashboardURL,
	exchangeCandlesEndpoint : Constants.getInstance().exchangeCandlesURL,
	updateTickersEndpoint : Constants.getInstance().updateTickersURL,
}

/**
 * Initial load function
 * 
 * @returns
 */
function onLoad() {
	loadPage();
}

/**
 * Called on page load
 * @returns
 */
async function loadPage() {
	await updateMap();
	await updateDashboard();
	setInterval(updateDashboard, cache.updateInterval);
}

/**
 * Saves the strategy on based on which item on the table was clicked and then
 * turns the saved item blue.
 */
$(document).on( 'click', '#marketTable > tbody > tr', function() {
	$(this).addClass('bg-info').siblings().removeClass(
			'bg-info');
	document.getElementById("selectedMarket").innerHTML = $(this)[0].innerHTML;
	cache.selectedBase = $(this)[0].innerHTML.split("-")[0]
			.replace("<td>", "");
	cache.selectedQuote = $(this)[0].innerHTML.split("-")[1]
			.replace("</td>", "");
});

/**
 * Selects a market from market selection drop down, and sets cached global
 */
$(document).on('click', '#dropdown1 > a', function() {
	cache.selectedExchange = $(this)[0].innerHTML.toLowerCase();
	cache.selectedTickerBox = 1;
	updateMarketsModal( );
});

/**
 * Selects a market from market selection drop down, and sets cached global
 */
$(document).on('click', '#dropdown2 > a', function() {
	cache.selectedExchange = $(this)[0].innerHTML.toLowerCase();
	cache.selectedTickerBox = 2;
	updateMarketsModal( );
});

/**
 * Selects a market from market selection drop down, and sets cached global
 */
$(document).on('click', '#dropdown3 > a', function() {
	cache.selectedExchange = $(this)[0].innerHTML.toLowerCase();
	cache.selectedTickerBox = 3;
	updateMarketsModal( );
});

/**
 * Selects a market from market selection drop down, and sets cached global
 */
$(document).on('click', '#dropdown4 > a', function() {
	cache.selectedExchange = $(this)[0].innerHTML.toLowerCase();
	cache.selectedTickerBox = 4;
	updateMarketsModal( );
});

/**
 * When a check box is changed, we should regenerate table to reflect filtered
 * results
 */
$(document).on('change', '#quoteButtonGroup > input', function() {
	updateMarketsTable( );
});

/**
 * Performs a ticker update for user information to be saved
 * @returns
 */
$(document).on('click', '#submitTickerUpdate', function() {
	callUpdateTickersEndpoint( cache.selectedTickerBox );
});

/**
 * Used to update candle stick states 1h,4h,1d
 */
function changeCandleForPanel( panel, candle ){
	cache.selectedCandles[ panel-1 ] = candle;
	updateTickers( [cache.tickers[panel-1]], [panel] );
}

/**
 * Called when user selects new ticker to update user information and update the
 * actual ticker
 * 
 * @param tickerBox
 * @returns
 */
function callUpdateTickersEndpoint(tickerBox) {

	let ticker = {
		"base" : cache.selectedBase,
		"quote" : cache.selectedQuote,
		"exchange" : cache.selectedExchange
	};

	let request = {
		"ticker" : ticker,
		"tickerBox" : tickerBox
	};

	let marketLookupEndpoint = new AuthenticatedEndpoint();
	let response 			 = marketLookupEndpoint.doRequest(request,
							   endpoints.updateTickersEndpoint);

	if (response.success) {
		updateTickers([ ticker ], [ tickerBox ]);
		cache.tickers[ tickerBox-1 ] = ticker;
	} else {
		alert(response.message);
		return false;
	}
}

/**
 * Updates exchange-markets map
 */
async function updateMap(markets) {

	for (key in cache.map) {

		let request = {
			exchange : key
		};

		let marketLookupEndpoint = new AuthenticatedEndpoint();
		await marketLookupEndpoint.doRequest(request, endpoints.marketLookupEndpoint, true, updateMapSuccess);
	}
}

/**
* Called after successfull POST request 
* to grab exchange markets
*/
function updateMapSuccess(response){
	if (response.success) {
		updateMapMarkets(response.exchange, response.markets);
	} else {
		alert(response.message);
		return false;
	}
}

/**
 * Updates markets at the exchange level in the cache
 */
function updateMapMarkets(exchange, markets) {

	let i;

	for (i = 0; i < markets.length; i++) {
		let market = markets[i];
		base = market.base;
		quote = market.quote;
		if (!(quote in cache.map[exchange])) {
			cache.map[exchange][quote] = [];
		}
		cache.map[exchange][quote].push(base);
	}
}
/**
 * Always called after selecting a market
 * @returns
 */
function updateMarketsModal() {
	generateQuoteButtonGroup();
	updateMarketsTable();
}

/**
 * Updates markets table with markets from specific exchange
 * 
 * @returns
 */
function updateMarketsTable() {
	let html = "";
	for (key in cache.map[cache.selectedExchange]) {
		let id = key + "Check";
		if (document.getElementById(id).checked) {
			html += generateMarketsByQuote(key);
		}
	}
	document.getElementById("marketTableBody").innerHTML = html;
}

/**
 * Called inside update markets table to regenerate table only with respective
 * selected quotes
 * 
 * @param quote
 * @returns
 */
function generateMarketsByQuote(quote) {

	let markets = cache.map[cache.selectedExchange]
	let html = "";
	for (let i = 0; i < markets[quote].length; i++) {
		let base = markets[quote][i];
		html += "<tr align='center'><td>" + (base + '-' + key) + "</td></tr>\n";
	}
	return html;
}

/**
 * Appends a button for each unique quote coin from selected exchange
 * @returns
 */
function generateQuoteButtonGroup() {

	let quoteButtonGroup = document.getElementById("quoteButtonGroup");
	let html = "";
	for (key in cache.map[cache.selectedExchange]) {
		html += generateQuoteButton(key);
	}
	quoteButtonGroup.innerHTML = html;
}

/**
 * Helper function which generates HTML for a quote checkmark button
 * @param quote
 * @returns
 */
function generateQuoteButton(quote) {
	return "<input id='" + quote + "Check' type='checkbox' checked='true'>"
			+ quote + "&nbsp;&nbsp;\n";
}

/**
* Called to recieve most recent dashboard information
* for the user
*/
async function updateDashboard() {

	if( cache.tickers.length === 0 ){

		let request = {
			numSignals : 25
		};

		const dashboardEndpoint = new AuthenticatedEndpoint();
		await dashboardEndpoint.doRequest(request,endpoints.dashboardEndpoint,true,updateDashboardSuccess);
	}
	else { 
		updateTickers( cache.tickers, [ 1, 2, 3, 4 ] );
	}
}

/**
* Called as callback on POST success to dashboard endpoint
*/
async function updateDashboardSuccess(response){
	if (response.success) {
		await updateTickers(response.tickers, [ 1, 2, 3, 4 ]);
		await updateBotRanksTable(response.botMetrics);
		await updateBotSignalFeed(response.botSignals);
	} else {
		alert(response.message);
		return false;
	}
}

/**
* Called on market selection or dashboard load to
* update graphs
*/
async function updateTickers(tickers, locations) {

	const storeTickers = cache.tickers.length === 0;
	
	for (let i = 0; i < tickers.length; i++) {
		const ticker = tickers[i];
		if( storeTickers ){
			cache.tickers.push( ticker );
		}
		updateTicker(ticker,locations[i]);
	}
}

/**
* Called to update a ticker box with the new graph, performs
* two asynchron api calls to grab exchange candles
*/
async function updateTicker(ticker, location){
		const panel = "panel" + location;

		let request = {
			exchange : ticker.exchange,
			interval : cache.selectedCandles[ location-1 ],
			base : ticker.base,
			quote : ticker.quote
		};

		let request1m = {
			exchange : ticker.exchange,
			interval : "1m",
			base : ticker.base,
			quote : ticker.quote,
		};

		const callbackParams = {
			"ticker": ticker,
			"panel": panel,
			"location": location
		}

		const exchangeCandles = new AuthenticatedEndpoint();
		exchangeCandles.doRequest(request, endpoints.exchangeCandlesEndpoint, true, updateTickerSuccessDaily, callbackParams);
		exchangeCandles.doRequest(request1m, endpoints.exchangeCandlesEndpoint, true, updateTickerSuccessMinute, callbackParams);
}

/**
* When daily candles are successfully grabbed this function
* is called to generate the graph
*/
function updateTickerSuccessDaily(response, params){

	const lastPrice 	= response.priceData[response.priceData.length - 1].close;
	const lastLastPrice = response.priceData[response.priceData.length - 2].close;

	document.getElementById(params.panel + "Market").innerHTML = params.ticker.base + "-" + params.ticker.quote;
	document.getElementById(params.panel + "Exchange").innerHTML = "(" + params.ticker.exchange.toUpperCase() + ")";

	let price       = document.getElementById(params.panel + "Price");
	price.className = '';
	let arrow       = "";

	if (lastPrice > lastLastPrice) {
		price.classList.add('text-success');
		arrow = "&uarr;";
	} else if (lastPrice < lastLastPrice) {
		price.classList.add('text-danger');
		arrow = "&darr;";
	} else {
		price.classList.add('text-secondary');
		arrow = "";
	}
	price.classList.add('text-center');
	price.innerHTML    = arrow + lastPrice + " " + cache.selectedCandles[ params.location-1 ].toUpperCase( );
	let selectedChart  = '#dashboardChart' + ( params.location ) + ' svg';
	let selectedMarket = params.ticker.base + "-" + params.ticker.quote;
	createGraphDashboard(response, selectedChart, selectedMarket, tickerColors[params.location-1]);
}

/**
* Called on success to 1 minute candle api calls to update
* the last price field
*/
function updateTickerSuccessMinute(response, params){

	const last1mPrice 	  = response.priceData[response.priceData.length - 1].close;
	const lastLast1mPrice = response.priceData[response.priceData.length - 2].close;
	let price1m 		  = document.getElementById(params.panel + "PriceLast");
	price1m.className 	  = '';
	let arrow1m  	      = "";

	if (last1mPrice > lastLast1mPrice) {
		price1m.classList.add('text-success');
		arrow1m = "&uarr;";
	} else if (last1mPrice < lastLast1mPrice) {
		price1m.classList.add('text-danger');
		arrow1m = "&darr;";
	} else {
		price1m.classList.add('text-secondary');
		arrow1m = "";
	}
	price1m.classList.add('text-center');
	price1m.innerHTML = arrow1m + last1mPrice + " LAST";
}

/**
 * Generates table body html with top n bots
 * @param botMetrics
 * @returns
 */
function updateBotRanksTable(botMetrics) {
	let html = "";
	let tableBody = document.getElementById("botRanks");
	let rank = 1;
	for (let i = 0; i < botMetrics.length; i++) {
		html += generateTableRow(rank, botMetrics[i]);
		rank++;
	}
	tableBody.innerHTML = html;
}

/**
 * 
 * @param rank
 * @param metrics
 * @returns
 */
function generateTableRow(rank, metrics) {

	let classifier = isPositiveRoi(metrics.roi) ? "text-success"
			: "text-danger";
	return "<tr>" + "<td>" + rank + "</td>" + "<td>" + metrics.botName
			+ "</td>" + "<td class='" + classifier + "'>" + metrics.roi
			+ "</td>" + "<td>" + metrics.exchange.toUpperCase() + "</td>"
			+ "</tr>";
}
/**
 * Helper method which parses % based string and checks if value greater than 0
 * @param roiPercent
 * @returns
 */
function isPositiveRoi(roiPercent) {
	let roi = Number(roiPercent.replace("%", ""));
	return roi > 0;
}

/**
 * Updates the bot signal feed with notifications of recent trade/signals
 * 
 * @param botSignals
 * @returns
 */
function updateBotSignalFeed(botSignals) {
	let html = "";
	for (let i = 0; i < botSignals.length; i++) {
		let botSignal = botSignals[i];
		html += generateAlert(i + 1, botSignal);
	}
	document.getElementById("botSignalFeed").innerHTML = html;
}

/**
 * Generates a notification for the bot signal feed
 * 
 * @param botSignal
 * @returns
 */
function generateAlert(num, botSignal) {

	let actionClass = botSignal.action === "BUY" ? 'alert-success'
			: 'alert-danger';
	return " <div class='alert "
			+ actionClass
			+ " alert-dismissable text-left'>"
			+ "<p>"
			+ "("
			+ num
			+ ") "
			+ botSignal.timestamp
			+ "</p>"
			+ "<h2 class='text-center'>"
			+ botSignal.action
			+ ": "
			+ botSignal.base
			+ "-"
			+ botSignal.quote
			+ " (" + botSignal.exchange.toUpperCase( ) + ")"
			+ "</h2>" 
			+ "<p class='text-center'><strong>INDICATORS: </strong><br> "
			+ signalsToStringList( botSignal.signalStates )
			+ "</p></div>";
}

/**
 * Returns a list of all indicators at the signal level as a single string
 * @param signals
 * @returns
 */
function signalsToStringList( signals ){
	let returnString = "";
	return indicatorsToStringList( signals.buyIndicators ) + indicatorsToStringList( signals.sellIndicators );
}

/**
 * Returns a list of all sell or buy indicators
 * @param indicators
 * @returns
 */
function indicatorsToStringList( indicators ){
	let returnString = "";
	let map = new Map( );
	for( let key in indicators ){
		if( key != "class" && !map.has( key ) ){
			map.set( key, true );
			returnString += ( key + "=" + indicators[key] + " " );
		}
	}
	return returnString;
}