/*jslint browser: true*/
/*global $, jQuery, alert*/

/* 
 *  cache for the returned backtest 
 */
let backtestedStrategies = Object();
/* 
 *  cache for the created backtest 
 */
let chachedBacktest = {};

$(function() {
    $( "#exchangesButton" ).click(function() {
    	$( "#exchangesExpandRight" ).toggle("slide");
        $( "#algorithmsExpandRight" ).hide("slide");
        $( "#tokensExpandRight" ).hide("slide");
        $( "#queuedExpandRight" ).hide("slide");
        $( "#submitTestExpandRight" ).hide();
        $( "#reportsExpandRight" ).hide();
    });
    $( "#algorithmsButton" ).click(function() {
    	$( "#algorithmsExpandRight" ).toggle("slide");
        $( "#exchangesExpandRight" ).hide("slide");
        $( "#tokensExpandRight" ).hide("slide");
        $( "#queuedExpandRight" ).hide("slide");
        $( "#submitTestExpandRight" ).hide();
        $( "#reportsExpandRight" ).hide();
    });
    $( "#tokensButton" ).click(function() {
    	$( "#tokensExpandRight" ).toggle("slide");
        $( "#exchangesExpandRight" ).hide("slide");
        $( "#algorithmsExpandRight" ).hide("slide");
        $( "#queuedExpandRight" ).hide("slide");
        $( "#submitTestExpandRight" ).hide();
        $( "#reportsExpandRight" ).hide();
    });
    $( "#submitTestButton" ).click(function() {
        $( "#submitTestExpandRight" ).toggle("slide");
    	$( "#exchangesExpandRight" ).hide("slide");
        $( "#algorithmsExpandRight" ).hide("slide");
        $( "#tokensExpandRight" ).hide("slide");
        $( "#queuedExpandRight" ).hide("slide");
        $( "#reportsExpandRight" ).hide();
    });
    $( "#queuedButton" ).click(function() {
    	$( "#queuedExpandRight" ).toggle("slide");
        $( "#submitTestExpandRight" ).hide();
        $( "#exchangesExpandRight" ).hide("slide");
        $( "#algorithmsExpandRight" ).hide("slide");
        $( "#tokensExpandRight" ).hide("slide");
        $( "#reportsExpandRight" ).hide();
        window.dispatchEvent(new Event('resize'));

    });
    $( "#reportsButton" ).click(function() {
    	$( "#reportsExpandRight" ).show();
        $( "#queuedExpandRight" ).hide();
        $( "#submitTestExpandRight" ).hide();
        $( "#exchangesExpandRight" ).hide();
        $( "#algorithmsExpandRight" ).hide();
        $( "#tokensExpandRight" ).hide();
        
    });
    
    $('#graphToggleButton').click(function() {
    	if($(this).val() == "graph +") {
    		$( "#chart" ).show();
    		$(this).val("graph -");
    	}
    	else {
    		$( "#chart" ).hide();
    		$(this).val("graph +");
    	}
    });
    
    $('#tableToggleButton').click(function() {
    	if($(this).val() == "table +") {
    		$( "#innerTable" ).show();
    		$(this).val("table -");
    	}
    	else {
    		$( "#innerTable" ).hide();
    		$(this).val("table +");
    	}
    });  
});


$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});

$('body').on("click", "input[name='algoButton']" , function() {
    $("[value='detailsDiv']").hide();
    $('[name="' + this.value + '"]').toggle();
});

$(document).on('click', 'input[name="exchangeButton"]' , function( ) {
	$("[value='exchangeDiv']").hide();
    $('[name="' + this.value.toLowerCase() + 'Div"]').toggle();
});

$(document).on('click', 'input[name="coinButton"]' , function( ) {
	$("[value='coinDiv']").hide();
    $('[name="' + this.value.toLowerCase() + 'Div"]').toggle();
});

$('body').on("click", "input[name='modeCheck']" , function() {
    $("[value='modeDiv']").hide();
    $('[name="' + this.id + '"]').toggle();
});

$(document).on('mouseover', '#backtestTooltip' , function( ) {
	$('#backtestTooltip').tooltip('hide').attr('data-original-title', getCurrentBacktest()).tooltip({
    	html: true, placement: "left"}).tooltip('show');
});

function updateGraph(identifier) {
	
} // end updateGraph

function cacheBacktest(backtest) {
	chachedBacktest = backtest;
}

function getCurrentBacktest() {
	
	var exchange 	= getExchange().toLowerCase();;
	var markets 	= multiCoinCheck();
	var strategy 	= getStrategy();
	var candleSize 	= getTimeFrame();
	var timeFrame	= getTimeFrame();
	var investment	= getInvestment();
	var marketSize = markets.length;
	
	var exchangeColor 		= ( exchange 	== "empty" ) ? "red" : "green";
	var strategyColor		= ( strategy 	== "empty" ) ? "red" : "green";
	var candleSizeColor		= ( candleSize	== "empty" ) ? "red" : "green";
	var investmentColor		= ( investment 	== "empty" ) ? "red" : "green";
	var marketsColor 		= ( markets 	== "empty" || marketSize > 10 ) ? "red" : "green";
	
	if ( markets == "empty" ) marketSize = 0;
	var timeFrameColor = candleSizeColor;
	
	if (exchangeColor == "green" && strategyColor == "green" && candleSizeColor == "green" 
		&& investmentColor == "green" && marketsColor == "green") {
		$("#backtestSuccess").removeClass().addClass("icon-check").addClass("color-success");
	}
	else {
		$("#backtestSuccess").removeClass().addClass("icon-exclamation").addClass("color-fail");
	}
	
	return  "<font color = " + exchangeColor 	+ ">Exchange: " 	+ exchange 			+ "</font><br>" +
			"<font color = " + marketsColor 	+ ">Markets (" + marketSize + "/10): " 	+ markets 		+ "</font><br>" +
			"<font color = " + strategyColor 	+ ">Strategy: "		+ strategy.name 	+ "</font><br>" +
			"<font color = " + candleSizeColor 	+ ">Candle Size: "	+ candleSize 		+ "</font><br>" +
			"<font color = " + timeFrameColor 	+ ">Time Frame: " 	+ timeFrame 		+ "</font><br>" +
			"<font color = " + investmentColor 	+ ">Investment: " 	+ investment; 
}

function multiCoinCheck() {
    'use strict';
    
    var base = document.getElementsByName("tradedCoinCheck");
    var baseLength = base.length;
    var quote = document.getElementsByName("baseCheck");
    var quoteLength = quote.length;
    var markets = [];
    var i, j;
    
    for (i = 0; i < quoteLength; i++) {
    	
        if (quote[i].checked) {
        	
        	for (j = 0; j < baseLength; j++) {
        		
                if (base[j].checked) {
                	
                	markets.push(base[j].value + "-" + quote[i].value)
                	                  
                } // end inner if
            } // end inner for       
        } // end outer if
    } // end outer for
    
    return (markets === 'undefined' || markets.length == 0) ? "empty" : markets;
} // end multiCoinCheck

function getStrategy() {
    'use strict';
    
    return (cache.selectedStrategy == -1) ? "empty" :
	    {
			name: strategiesMap[cache.selectedStrategy].name,
			buySignals: strategiesMap[cache.selectedStrategy].buySignals,
			sellSignals: strategiesMap[cache.selectedStrategy].sellSignals,
			stopLoss: strategiesMap[cache.selectedStrategy].stopLoss
	    };
} // end getStrategy


function getInvestment() {
    'use strict';
    var investment = document.getElementsByName("investment");
    return (investment[0].value === "") ? "empty" : investment[0].value;
}

function getTimeFrame() {
    'use strict';
    var time = document.getElementsByName("rb");
    var i;
    for (i = 0; i < time.length; i++) {
        if (time[i].checked) {
            return time[i].value;
        }
    }
    return "empty";
}

// gets value of exchange selected

function getExchange() {
    'use strict';
    var exchange = document.getElementsByName("rbExchange");
    var i;
    
    for (i = 0; i < exchange.length; i++) {
        if (exchange[i].checked) {
            return exchange[i].value;
        }
    }
    return "empty";
} // end getExchange

function getUserBacktestData() {
    'use strict';
    var exchange = getExchange().toLowerCase();
    var time = getTimeFrame();                          // string for time time frame to get candle data
    var investment = getInvestment();                   // string for investment amount
    var coins = multiCoinCheck();                       // array of array coin pairs ex: ["quote-base"]
    var strategy = getStrategy();   					// user picked strategy from arsenal
    backtestName = exchange + "-" + strategy.name;
    return {
		    	strategy: strategy,
		    	exchange: exchange,
		    	markets: coins,
		    	initialInvestment: investment,
		    	candleSize: time
    		};
} // end getUserBacktestData

$(document).ready(function () {
    'use strict';
});

$('#sendData').on('click', function () {
    'use strict';
    
   let request = getUserBacktestData();     // holds the backtest object
   if (request.strategy == "empty" || request.exchange == "empty" || request.markets == "empty" 
	   || request.initialInvestment == "empty" || request.candleSize == "empty") {
		   $('#backtestTooltip').effect("shake");
   }
   else {
	   
	   cacheBacktest(request);					// caches pre backtest
	   
	   //updateBacktestedStrategies(request); 
	    
	    var backtestEndpoint = new AuthenticatedEndpoint( );
	    var response			= backtestEndpoint.doRequest( request, endpoints.backtestEndpoint );
	   
	    if( response.success ){
	    		cachedBacktestResults = response.backtestResults;
			updateBacktestTable(response.backtestResults);
			createGraph(response.backtestResults);
	    }
	    else{
	    		alert( response.message );
	    		return false;
	    }
   }
});

//fills dynamic table with results from backtest.

function updateBacktestTable( obj )
{
	
    var html 			= '';
    var className 		= '';
    
    html += '<div class="table"><table class="table table-hover" id="dataTable" cellspacing="0">' +
    		    '<thead><tr class="backtestRow"><th>Market&nbsp;&nbsp;&nbsp;</th><th>Initial' + 
    		    	 createDenomination( obj[0].quote ) + 
    		    	'</th><th>Networth' +
    		    	 createDenomination( obj[0].quote ) + 
    		    	'</th><th>ROI</th>' +
    		    '<th>Num Trades</th><th>Win Rate</th><th>Expectancy</th><th>Profit Factor</th><th>Max Drawdown</th>' +
    		    '<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr></thead><tbody>';
 
    for (var i = 0; i < obj.length; i++)
    {
    		var roiTrimmed		   = parseFloat( obj[i].returnOnInvestment.slice(0, -1) );
        var maxDrawdownTrimmed  = parseFloat( obj[i].returnOnInvestment.slice(0, -1) );
        var winRateTrimmed	   = parseFloat( obj[i].winRatio.slice(0, -1) );
        var expectancy   	   = obj[i].expectancy.toFixed(2); 
        var profitFactor        = obj[i].profitFactor.toFixed(2); 
        var finalNet            = obj[i].finalNetworth.toFixed(2); 
        
        html+= '<tr class="backtestRow">' +
      //'<th class=""' +  
      //' scope="row">'+ obj[i].strategy + '</th>' +
      '<th class="text-primary">' + obj[i].pair + '</th>' +
      '<th>' + obj[i].initialInvestment + '</th>' +
      '<th>' + finalNet + '</th>' +
      '<th><font color="' + getColorFromValue( roiTrimmed, 0, 'green', 'red', 'grey' ) + '">' + obj[i].returnOnInvestment + '</font></th>' +
      '<th>' + obj[i].numTrades + '</th>' +
      '<th><font color="' + getColorFromValue( winRateTrimmed, 1, 'green', 'red', 'grey' ) + '">' + obj[i].winRatio + '</th>' +
      '<th><font color="' + getColorFromValue( expectancy, 0, 'green', 'red', 'grey' ) + '">' + expectancy + '</th>' +
      '<th><font color="' + getColorFromValue( profitFactor, 1, 'green', 'red', 'grey' ) + '">' + profitFactor + '</th>' +
      '<th>' + obj[i].maxDrawdown + '</th>' +
      '<td>' + getActionButton( i ) + '</td>' + 
      '</tr>';
    }
    /**<input type="image"src="img/link.png" onclick="loadBotModal(' + i +');return false;"class="save-bot-btn"data-toggle="modal"' +
        'data-target="#botModalCenter" width="24" heght="24"></input></td>' + 
        '</tr>';**/
    html += '</tbody></table></div>';
    
    $("#innerTable").html(html);
} // end fillTable

function createDenomination( quote ){
	return "(" + quote + ")";
}

function getActionButton( index ){
	return  '<div class="dropdown float-right text-center">'+
			'<button class="dropbtn">'+
			'<b>+</b>'+
			'</button>'+
			'<div id="dropdown1" class="dropdown-content">'+
			'<a onclick="exportBacktestResults(' + index + ')" href="#">EXPORT TRADES</a>'+
			'<a onclick="loadBotModal(' + index +');return false;" href="#" data-toggle="modal" data-target="#botModalCenter">SAVE BOT</a>'+
			'</div>'+
			'<a href="#">'+
			'</a>'+
			'</div>';
}

function exportBacktestResults( index ){
	const filename = new Date( ) + "-travas-backtest";
	exportBacktestCsv( filename, cachedBacktestResults[ index ] );
}
function updateBacktestedStrategies(tests) {
	
	//Reset map
   backtestedStrategies = tests;
    
} // end updateBacktestMap

function loadBotModal( num ) {
	document.getElementById("selectedExchange").innerHTML = chachedBacktest.exchange.toUpperCase();
	document.getElementById("selectedMarket").innerHTML   = chachedBacktest.markets[num].toUpperCase();
	document.getElementById("selectedStrategy").innerHTML = chachedBacktest.strategy.name.toUpperCase();
	$("#" + chachedBacktest.candleSize )[0].checked = true;
}
