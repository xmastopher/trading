
/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if( document.getElementById("username") !== undefined ){
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}

/**
 * Page level cache and globals
 */
var cache = {
	map: new Object(),
	buySignals: [],
	sellSignals: [],
	preconfiguredBuySignals: [],
	preconfiguredSellSignals: [],
	userStrategyMap: new Object( ),
	validationObject : { buySignalParamKeys: [], sellSignalParamKeys: [] },
	confirmedStrategy: new Object(),
	strategyToDelete: -1,
	defaultBuySignal: "SMA-BUY",
	defaultSellSignal: "SMA-SELL"
}

/**
 * Immutable values
 */
const pageConstants = {
		minInterval: 1,
		maxInterval: 200
}

/**
 * Endpoints defined at the page level
 */
var endpoints = {
		getSignalsEndpoint: Constants.getInstance().getSignalsURL,
		getStrategiesEndpoint: Constants.getInstance( ).getStrategiesURL,
		saveStrategyEndpoint: Constants.getInstance( ).saveStrategyURL,
		deleteStrategyEndpoint: Constants.getInstance( ).deleteStrategyURL
}

/**
 * Initial load function
 * @returns
 */
function onLoad( ){
	loadPage( );
}

/**
 * Used to update entire page
 * @returns
 */
function loadPage( ){
    
	//Grab supported signals and populate dropdowns
	getSupportedSignals( );
	
	//Grab and update page with all user strategies
	getStrategies( );
}

/**
 * Called to grab all travas supported signals and populate them
 * in the drop down
 * @returns
 */
function getSupportedSignals( ){
	
	//Grab unauthenticated endpoint
	var getSignalsEndpoint = new UnauthenticatedEndpoint( );
	var response 		   = getSignalsEndpoint.doRequest( "GET", {}, endpoints.getSignalsEndpoint );
	
	//Generate dropdown on success
	if( response.success ){
		generateSignalsDropdown( response.signals );
		cache.preconfiguredBuySignals = response.preconfiguredBuySignals;
		cache.preconfiguredSellSignals = response.preconfiguredSellSignals;
	}
	else{
		alert( response.message );
		return false;
	}
}

//Calls get strategies API for user 
function getStrategies( ){
	
	// Perfrom Get Strategies API Call
    var getStrategiesEndpoint = new AuthenticatedEndpoint( );
    var response = getStrategiesEndpoint.doRequest( {}, endpoints.getStrategiesEndpoint );
    
    //Update strategies table
    if( response.success ){
		generateStrategiesTable( response.strategies );
    }
    else{
    		alert( response.message );
    		return false;
    }
    
    return true;
	
}

/**
 * Populates the signals drop down given a list of signals
 * @param signals
 * @returns
 */
function generateSignalsDropdown( signals ){
	
	var buySignalsHtml  = "";
	var sellSignalsHtml = "";
	
	for( var i = 0; i < signals.length; i++ ){
		
		var name      = signals[ i ].name;
		var params    = signals[ i ].parameters;
		var splitName = getSignalNameFromAPIString(name);
		
		//Store parameters in cache
		cache.map[name] = params;
		
		//<a class="dropdown-item" href="#" id="exchange-binance">Binance</a>
		//This is a buy signal
		if( name.includes( "BUY" ) )
		{
			cache.buySignals.push( name );
			buySignalsHtml += '<a class="dropdown-item" href="#" id="signal-'+ name +'">' + splitName + '</a>\n';
		}
		
		//Assume sell signal
		else
		{
			cache.sellSignals.push( name );
			sellSignalsHtml += '<a class="dropdown-item" href="#" id="signal-'+ name +'">' + splitName + '</a>\n';
		}
	}
	
	//Update HTML
	document.getElementById("buySignalList").innerHTML = buySignalsHtml;
	document.getElementById("sellSignalList").innerHTML = sellSignalsHtml;
	initSignalSelection( );
}

/**
 * Sets the selected strategy to SMA-BUY-CROSS and SMA-SELL-CROSS
 * @returns
 */
function initSignalSelection(){
	var buySignalKey = "signal-" + cache.defaultBuySignal;
	var sellSignalKey = "signal-" + cache.defaultSellSignal;
	updateSignalDescriptionAndName( document.getElementById( buySignalKey ).innerHTML, buySignalKey );
	updateSignalDescriptionAndName( document.getElementById( sellSignalKey ).innerHTML, sellSignalKey );
	document.getElementById("buySignalParams").innerHTML  = generateParamsHTML( cache.map[cache.defaultBuySignal], "buy" );
	document.getElementById("sellSignalParams").innerHTML = generateParamsHTML( cache.map[cache.defaultSellSignal], "sell" );
}

/**
 * Generates strategy table with list of user strats
 * @param strategies
 * @returns
 */
function generateStrategiesTable( strategies ){
	
	var html = "";
	
	for( var i = 0; i < strategies.length; i++ ){
		
		var strategy = strategies[ i ];
		html += generateTableRow( strategy, i+1 );
		
		cache.userStrategyMap[ i+1 ] = strategy;
	}
	
	document.getElementById("userStrategiesTableBody").innerHTML =  html;
}

/**
 * Generates a table row for strategy
 * @param strategy
 * @param row
 * @returns
 */
function generateTableRow( strategy, row ){

	return  '<tr>' + 
			'<th scope="row">' + row + '</th>' +
			'<td>' + strategy.name + '</td>'   +
			'<td id="' + 'buySignals-' + row + '">' + signalsToString( strategy.buySignals ) + '</td>'     +
			'<td id="' + 'sellSignals-' + row + '">' + signalsToString( strategy.sellSignals ) + '</td>'    +
			'<td><button id="' + 'link-' + row + '"class="btn btn-link" onclick="generateParamsHTMLExplicit('+row+')"><img src="img/link.png" width="24" heght="24"></button></td>' +
			'<td><button id="' + 'link-' + row + '"class="btn btn-link" onclick="deleteStrategy('+row+')" data-toggle="modal" data-target="#deleteStrategyModal"><img src="img/delete_icon.png" width="24" heght="24"></button></td>' +
			'</tr>';
}

/**
 * Converts a signal to a readable string
 * @param signals
 * @returns
 */
function signalsToString( signals ){
	
	var str = "";
	var keys = [];
	
	for( var i = 0; i < signals.length; i++ ){
		
		var signal = signals[ i ];
		keys.push( signal.signal );
	}
	
	return keys.join(", ");
}

/**
 * Updates the signal's description and its name
 * @param selText
 * @param id
 * @returns
 */
function updateSignalDescriptionAndName( selText, id ){
	  
	  //Check for correct tag prefix
	  if( id.includes( "signal" ) )
	  { 
		  var key = selText;
		  
		  //Buy strategies
		  if( id.includes( "BUY" ) )
		  {
			  key += "-BUY";
			  document.getElementById("buySignalName").innerHTML = selText;
			  document.getElementById("buySignalLabel").innerHTML = key;
			  document.getElementById("buySignalDescription").innerHTML = getDescriptionFromName( key );
			  document.getElementById("buySignalParams").innerHTML = generateParamsHTML( cache.map[key], "buy" );
		  }
		  
		  //Sell strategies
		  else
		  {
			  key += "-SELL";
			  document.getElementById("sellSignalName").innerHTML = selText;
			  document.getElementById("sellSignalLabel").innerHTML = key;
			  document.getElementById("sellSignalDescription").innerHTML = getDescriptionFromName( key );
			  document.getElementById("sellSignalParams").innerHTML = generateParamsHTML( cache.map[key], "sell" );
		  }
	  }
}

/**
 * Explicit call to pass in values to update values - used for copying strategies
 * @param id
 * @returns
 */
function generateParamsHTMLExplicit( id ){ 
	
	
	//Grab buy signals for row
	var buySignalsText = document.getElementById( "buySignals-" + id ).innerHTML;
	var buyArray       = buySignalsText.split(',');
	
	//Grab sell signals for row
	var sellSignalsText = document.getElementById( "sellSignals-" + id ).innerHTML;
	var sellArray       = sellSignalsText.split(',');
	
	//Get IDs for the signals
	var buySignalKey = "signal-" + buyArray[0];
	var sellSignalKey = "signal-" + sellArray[0];
	
	//Simulate button press for signals from dropdowns
	updateSignalDescriptionAndName( document.getElementById( buySignalKey ).innerHTML, buySignalKey );
	updateSignalDescriptionAndName( document.getElementById( sellSignalKey ).innerHTML, sellSignalKey );
	
	document.getElementById("buySignalParams").innerHTML  = generateParamsHTML( cache.map[buyArray[0]], "buy" );
	document.getElementById("sellSignalParams").innerHTML = generateParamsHTML( cache.map[sellArray[0]], "sell" );
	
	//Update all input boxes
	updateParameterBox( cache.userStrategyMap[id].buySignals, "buy", id )
	updateParameterBox( cache.userStrategyMap[id].sellSignals, "sell", id )
}

/**
 * Called to delete a strategy
 * @param row
 * @returns
 */
function deleteStrategy( row ){
	
	cache.strategyToDelete = cache.userStrategyMap[row].id;
	document.getElementById( "strategyToDelete" ).innerHTML = "Are you sure you would like to delete strategy " + cache.userStrategyMap[row].name + "? If you meant to perform this action, please click confirm below.";
	
}

/**
 * Called to update the box that holds strategy params
 * @param signals
 * @param signalType
 * @param row
 * @returns
 */
function updateParameterBox( signals, signalType, row ){
	
	var signal = signals[0];
	
	for( var key in signal ){
		
		if( key != "signal" )
		{
			var generatedId = "param-" + signalType + "-" + key;
			document.getElementById( generatedId ).value = signal[key];
		}
	}
}

/**
 * Generate params for display - can also use random
 * @param params
 * @param signalType
 * @param useRand
 * @returns
 */
function generateParamsHTML( params, signalType, providedParams ){
	
	var html = "";
	var last;
	var buySignalParamKeys  = [];
	var sellSignalParamKeys = [];
	
	for( var i = 0; i < params.length; i++ ) {
		
		var param = params[ i ];
		
		for( var key in param ){
			
			if( key != "name" ) continue;
			
			var value = param[key];
				
			//Update references keys
			if( signalType === "buy"){
				buySignalParamKeys.push( "param-" + signalType + "-" + value );
			}
			
			//Update references key
			else if( signalType === "sell"){
				sellSignalParamKeys.push( "param-" + signalType + "-" + value );
			}
			
			if( providedParams === undefined ){
				html += '<td><a href="#" class="tooltip-travas"><strong>' + camelCaseToNormal( value ).toLowerCase( ) + '</strong>' +
				'<span> <img class="callout" src="img/callout_black.gif"><h5><strong>' + camelCaseToNormal( value ).toUpperCase( ) + '</strong></h5>' +
				getParameterDefinition(value) + '</span></a>' +
				'</td><td><input  id="param-' + signalType + "-" + value + '" value = "0"></td></tr>\n';
			}
			else{
				last = getRandomFromParameter( value );
				html += '<td><a href="#" class="tooltip-travas"><strong>' + camelCaseToNormal( value ).toLowerCase( ) + '</strong>' +
				'<span> <img class="callout" src="img/callout_black.gif"><h5><strong>' + camelCaseToNormal( value ).toUpperCase( ) + '</strong></h5>' +
				getParameterDefinition(value) + '</span></a>' +
				'</td><td><input id="param-' + signalType + "-" + value + '" value = "'+ providedParams[value] +'"></td></tr>\n';
			}
		}
	}
	
	//Update validation object
	if( signalType === "buy"){
		cache.validationObject.buySignalParamKeys  = buySignalParamKeys;
	}
	
	else if( signalType === "sell"){
		cache.validationObject.sellSignalParamKeys = sellSignalParamKeys;
	}
	
	//Return generated html
	return html;
}

/**
 * Called to generate params when clicking on dice button
 * @returns
 */
function randomPick( ){
	
	//Grab random number to pick buySignal
	var randomNumberBuySignal  = Math.floor(Math.random() * cache.preconfiguredBuySignals.length);
	var randomNumberSellSignal = Math.floor(Math.random() * cache.preconfiguredSellSignals.length);
	
	var signalNameBuy  = cache.preconfiguredBuySignals[randomNumberBuySignal].signal;
	var signalNameSell = cache.preconfiguredSellSignals[randomNumberSellSignal].signal;
	
	//Get IDs for the signals
	var buySignalKey = "signal-" + signalNameBuy;
	var sellSignalKey = "signal-" + signalNameSell;
	
	//Simulate button press for signals from dropdowns
	updateSignalDescriptionAndName( document.getElementById( buySignalKey ).innerHTML, buySignalKey );
	updateSignalDescriptionAndName( document.getElementById( sellSignalKey ).innerHTML, sellSignalKey );
	
	document.getElementById("buySignalParams").innerHTML = generateParamsHTML( cache.map[signalNameBuy], "buy" );
	document.getElementById("sellSignalParams").innerHTML = generateParamsHTML( cache.map[signalNameSell], "sell" );
	
	//Update all input boxes
	updateParameterBox( [cache.preconfiguredBuySignals[randomNumberBuySignal]], "buy" )
	updateParameterBox( [cache.preconfiguredSellSignals[randomNumberSellSignal]], "sell" )
}

/**
 * Called before a strategy can be saved to validate all input
 * @returns
 */
function createAndValidateStrategy( ){
	
	cache.confirmedStrategy = {
			buySignals: [],
			sellSignals: []
	}
	
	var validatedStrategy = {
			buySignalName: document.getElementById("buySignalName").innerHTML,
			sellSignalName: document.getElementById("sellSignalName").innerHTML,
			buySignalParams: [],
			buySignalParamValues: [],
			sellSignalParams: [],
			sellSignalParamValues: []
	}
	
	document.getElementById( "saveStrategyButton" ).setAttribute( "data-target", "#saveStrategyModal" );
	var success = true;
	var results = [];
	
	var buySignal = Object();
	buySignal["signal"] = validatedStrategy.buySignalName + "-BUY";
	
	for( var i = 0; i < cache.validationObject.buySignalParamKeys.length; i++ )
	{
		var split = cache.validationObject.buySignalParamKeys[i].split("-");
		var param = split[ split.length-1 ];
		var value = document.getElementById( cache.validationObject.buySignalParamKeys[ i ] ).value;
		var result = checkSignalParameterConstraint( param, value, "buy" );
		validatedStrategy.buySignalParams.push( camelCaseToNormal( param ) );
		validatedStrategy.buySignalParamValues.push( value );
		buySignal[param] = value;
		cache.confirmedStrategy.buySignals.push()
		results.push( result );
	}
	
	var sellSignal = Object();
	sellSignal["signal"] = validatedStrategy.sellSignalName + "-SELL";
	
	for( var i = 0; i < cache.validationObject.sellSignalParamKeys.length; i++ )
	{
		var split  = cache.validationObject.sellSignalParamKeys[i].split("-");
		var param  = split[ split.length-1 ];
		var value  = document.getElementById( cache.validationObject.sellSignalParamKeys[ i ] ).value;
		var result = checkSignalParameterConstraint( param, value, "sell" );
		validatedStrategy.sellSignalParams.push( camelCaseToNormal( param ) );
		validatedStrategy.sellSignalParamValues.push( value );
		sellSignal[param] = value;
		cache.confirmedStrategy.sellSignals.push()
		results.push( result );
	}
	
	var message = "<p>";
	
	for( var i = 0; i < results.length; i++ )
	{
		success = success && results[ i ] === "success";
		
		if( results[ i ] != "success" )
		{
			message += results[ i ] + '<br>';
		}
	}
	
	//Not successfull
	if( !success ){
		message += "</p>"
		document.getElementById( "parameterErrorLog" ).innerHTML = message;
		document.getElementById( "saveStrategyButton" ).setAttribute( "data-target", "#errorModal" );
	}
	
	//Is successfull
	else{
		cache.confirmedStrategy.buySignals.push(buySignal);
		cache.confirmedStrategy.sellSignals.push(sellSignal);
		//Update modal with validated strategy
		document.getElementById( "strategyDefinition" ).innerHTML = updateChosenStrategyText( validatedStrategy );
	}
	
}

/**
 * Updates the text of the selected strategy - should already be verified
 * @param verifiedStrategy
 * @returns
 */
function updateChosenStrategyText( verifiedStrategy ){
	
	var html = "";
	
	//Update Buy Signals
	html += '<div class="row"><div class="col-md-12"><h5>BUY SIGNAL: ' + verifiedStrategy.buySignalName + '</h5>\n';
	html +=  generateParameterDefinition( verifiedStrategy.buySignalParams, verifiedStrategy.buySignalParamValues );
	html += '</div></div><br>';
	
	//Update Sell Signals
	html += '<div class="row"><div class="col-md-12"><h5>SELL SIGNAL: ' + verifiedStrategy.sellSignalName + '</h5>\n';
	html +=  generateParameterDefinition( verifiedStrategy.sellSignalParams, verifiedStrategy.sellSignalParamValues );
	html += "</div></div><br>";
	
	html += '<div class="row"><div class="col-md-12"><h5>Stop Loss Percent</h5>' +
			'<input id="stopLossSlider" type="range" data-slider-min="1" data-slider-max="99" data-slider-step="1" data-slider-value="50" style="width:100%;">' +
			'<h5 id="stopLossValue">50%</h5></div></div>';
	
	//With JQuery
	$(document).on('input', '#stopLossSlider', function( ) {
		$('#stopLossValue').html( $(this).val() + "%" );
	});
	
	return html;
}

/**
 * generates definition of each param
 * @param signalParams
 * @param signalParamValues
 * @returns
 */
function generateParameterDefinition( signalParams, signalParamValues ){
	
	var html = "";
	
	for( var i = 0; i < signalParams.length; i++ ){
		
		var paramName  = signalParams[ i ];
		var paramValue = signalParamValues[ i ];
		
		html += "<p>" + paramName + ": " + paramValue + "</p>";
	}
	
	return html;
}

/**
 * Returns a random value given a signal
 * @param name
 * @param dependancy
 * @returns
 */
function getRandomFromParameter( name, dependancy ){
	
	switch( name ){
		case "sma":
			return Math.floor(Math.random() * 101);
			
		case "ema":
			return Math.floor(Math.random() * 101);
			
		case "smaSmaller":
			return Math.floor(Math.random() * 51);
			
		case "smaLarger":
			var min = dependancy+1;
			var max = 101;
			return Math.floor(Math.random() * (max - min + 1)) + min;
			
		case "emaSmaller":
			return Math.floor(Math.random() * 51);
			
		case "emaLarger":
			var min = dependancy+1;
			var max = Math.floor(101);
			return Math.floor(Math.random() * (max - min + 1)) + min;
			
		case "std":
			var min = 1;
			var max = 300;
			return ( Math.floor(Math.random() * (max - min + 1)) + min ) / 100;
			
		case "useAbsolutes":
			var rand = Math.floor(Math.random() * 2);
			if( rand == 0 ) return "TRUE";
			else return "FALSE";
			
		case "windowSize":
			return Math.floor(Math.random() * 101);
	}

	return "INVALID PARAMETER";
}

/**
 * Returns input constraint for parameter
 * @param parameter
 * @param value
 * @param signalType
 * @returns
 */
function checkSignalParameterConstraint( parameter, value, signalType ){
	
	switch( parameter ){
		case "sma":
		case "smaSmaller":
		case "smaLarger":
		case "ema":
		case "emaSmaller":
		case "emaLarger":
		case "emaSignal":
		case "windowSize":
		case "period":
			
			if( isNaN( value ) ){
				return parameter + " is not a number, please provide an integer value";
			}
			
			//Check that decimal does not exist
			var isDecimal = value.includes(".");
			
			//ERROR decimal was provided
			if( isDecimal ){ 
				return parameter + " can not be a decimal value, it must be an integer";
			}
			
			value = Number(value);
			
			//ERROR parameter is out of range
			if( value < pageConstants.minInterval || value > pageConstants.maxInterval ){ 
				return parameter + " is outside of range 0-201";
			}
			
			//If parameter has dependancy
			if( parameter === "smaLarger" || parameter === "emaLarger" ){
				
				if(  parameter === "smaLarger" )
				{
					
					var dependencyId = "param-" + signalType + "-" + "smaSmaller";
					var val = document.getElementById( dependencyId ).value;
					
					//Dependancy invalid
					if( val >= value )
					{
						return parameter + " must be larger than sma smaller value";
					}

				}
				else
				{
					
					var dependencyId = "param-" + signalType + "-" + "emaSmaller";
					var val = document.getElementById( dependencyId ).value;
					
					//Dependancy invalid
					if( val >= value )
					{
						return parameter + " must be larger than ema smaller value";
					}
				}

			}
			
			//Checks passed
			return "success";
			
		case "std":
			
			if( isNaN( value ) ){
				return parameter + " is not a number, please provide a decimal value";
			}
			
			//Check that decimal does not exist
			var isDecimal = value.includes(".");
			
			value = Number(value);
			
			//ERROR decimal was provided
			if( !isDecimal && value != 1 && value != 2 && value != 3 ) { 
				return parameter + " must be a decimal point value or within the set {1,2,3}";
			}
			
			//ERROR for negative or large value
			else if( value < 0 || value > 3 ){
				return parameter + " must be a decimal point value or within the set {1,2,3}";
			}
			
			return "success";
			
			
		case "target":
			
			if( isNaN( value ) ){
				return parameter + " is not a number, please provide a decimal value";
			}
			
			//Check that decimal does not exist
			var isDecimal = value.includes(".");
			
			value = Number(value);
			
			//ERROR decimal was provided
			if( !isDecimal && value != 1 && value != 2 && value != 3 ) { 
				return parameter + " must be a decimal point value";
			}
			
			//ERROR for negative or large value
			else if( value < 0 ){
				return parameter + " must be greater than zero";
			}
			
			return "success";
			
		case "threshold":
			
			if( isNaN( value ) ){
				return parameter + " is not a number, please provide a decimal value";
			}
			
			value = Number(value);
			
			//ERROR decimal was provided
			if( value < .01 || value > 99.99 ) { 
				return parameter + " must be greater than .01 and less than 99.99";
			}
			
			return "success";
	}
	
	return "INVALID PARAMETER";
}

