/**
 * Set document to strict
 */
$(document).ready(function () {
    'use strict';
});

/**
 * Called when user actually wants to save the bot 
 * @returns
 */
$(document).on('click', '#finalSaveButton', function( ){
	
	'use strict';

	var strategyNameField = document.getElementById("strategyNameInput").value;
	var stopLossField     = document.getElementById("stopLossValue").innerHTML;
	
	
    var request = {
    		strategyName: strategyNameField,
    		buySignals: cache.confirmedStrategy.buySignals,
    		sellSignals: cache.confirmedStrategy.sellSignals,
    		targets: [],
    		stopLoss: stopLossField
    }
    
    var saveStrategyEndpoint = new AuthenticatedEndpoint( );
    var response 			= saveStrategyEndpoint.doRequest( request, endpoints.saveStrategyEndpoint );
    
    if( response.success ){
		getStrategies( );
		alert("Successfully saved strategy!");
    }
    else{
    		alert( response.message );
    		return false;
    }
	
} );

/**
 * Checks for clicks on drop downs
 * @returns
 */
$(".dropdown-menu").on("click", "a", function(){
	
	  'use strict';
	  
	  //Grab the selected text
	  var selText = $(this).text( );
	  
	  //Grab id to check later
	  var id = $(this)[0].id;
	  
	  updateSignalDescriptionAndName( selText, id );
});

/**
 * Called when user confirms to actually delete a strategy
 * @returns
 */
$(document).on('click', '#confirmDeleteStrategy', function( ) {

	'use strict';
	  
	var request = {
			strategyId: cache.strategyToDelete
	};
	
    var deleteStrategyEndpoint = new AuthenticatedEndpoint( );
    var response 			  = deleteStrategyEndpoint.doRequest( request, endpoints.deleteStrategyEndpoint );
	
    if( response.success ){
		alert( "successfully deleted strategy" );
		getStrategies( );
    }
    else{
    		alert( response.message );
    		return false;
    }
    
});
