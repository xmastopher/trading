/**
 * Object which encapsulates all shared elements for fronted pages.
 * To date this includes:
 * 	1. Navigation Panel
 * 	2. Logout Modal
 * 
 * To generate a page template include the following line of code at the top
 * of a pages respective js file:
 * 	TemplateManager.initPage( );
 */
var TemplateManager = function(){

	this.title					 = Constants.getInstance( ).version;
	this.dashboardURL 			 = "dashboard.html";
	this.strategyManagerURL 		 = "strategy_manager.html";
	this.botManagerURL	 		 = "bot_manager.html";
	this.botLeaderboardsURL 		 = "bot_leaderboards.html";
	this.backtestURL		 		 = "backtest.html";
	this.crossExchangeAnalysisURL = "cross_exchange_analysis.html";
	this.settingsURL			     = "settings.html";
	
	this.generateNavigationPanel = function( ){
		document.getElementById( "mainNav" ).innerHTML = 
			'<a class="navbar-brand" href="' + this.dashboardURL + '"><i><strong>' + this.title + '</strong></i></a>\n'+
			'<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"\n'+
				'aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">\n'+
	      		'<span class="navbar-toggler-icon"></span>\n'+
	      	'</button>\n'+
			'<div class="collapse navbar-collapse" id="navbarResponsive">\n'+
				'<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">\n'+
	        			'<li class="text-left"\n>'+
	        				'<img src="img/travas_logo_white.png" style="width:32px;height:32px;">\n'+
	        			'</li>\n'+
	        			'<li class="text-center">\n'+
	        				'<img id="avi" style="width:128px;height:128px;">\n'+
	        			'</li>\n'+
	        			'<li class="text-center">\n'+
	        				'<h3 id="username"></h3>\n'+
	        			'</li>\n'+
    			        '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">\n'+
	          			'<a class="nav-link" href="' + this.dashboardURL + '">\n'+
	            			'<i class="fa fa-fw fa-dashboard"></i>\n'+
	            			'<span class="nav-link-text">Dashboard</span>\n'+
	          			'</a>'+
	        			'</li>\n'+
    			        '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">\n'+
			          	'<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponentsStrategy" data-parent="#exampleAccordion">\n'+
			            '<i class="fa fa-fw fa-sliders"></i>\n'+
			            '<span class="nav-link-text">Strategies</span>\n'+
			          	'</a>\n'+
			          	'<ul class="sidenav-second-level collapse" id="collapseComponentsStrategy">\n'+
			            		'<li>\n'+
			              		'<a href="' + this.strategyManagerURL + '">Strategy Manager</a>\n'+
			            		'</li>\n'+
			          	'</ul>\n'+
			        '</li>\n'+
    			        '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">\n'+
			          	'<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponentsBots" data-parent="#exampleAccordion">\n'+
			            '<i class="fa fa-fw fa-gears"></i>\n'+
			            '<span class="nav-link-text">Bots</span>\n'+
			          	'</a>\n'+
			          	'<ul class="sidenav-second-level collapse" id="collapseComponentsBots">\n'+
			            		'<li>\n'+
			              		'<a href="' + this.botManagerURL + '">Bot Manager</a>\n'+
			            		'</li>\n'+
			            		'<li>\n'+
			              		'<a href="' + this.botLeaderboardsURL + '">Bot Leaderboards</a>\n'+
			            		'</li>\n'+
			          	'</ul>\n'+
			        '</li>\n'+
    			        '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">\n'+
			          	'<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponentsAnalytics" data-parent="#exampleAccordion">\n'+
			            '<i class="fa fa-fw fa-area-chart"></i>\n'+
			            '<span class="nav-link-text">Analytics</span>\n'+
			          	'</a>\n'+
			          	'<ul class="sidenav-second-level collapse" id="collapseComponentsAnalytics">\n'+
			            		'<li>\n'+
			              		'<a href="' + this.backtestURL + '">Backtest</a>\n'+
			            		'</li>\n'+
			            		'<li>\n'+
			              		'<a href="' + this.crossExchangeAnalysisURL + '">Cross Exchange Analysis</a>\n'+
			            		'</li>\n'+
			          	'</ul>\n'+
			        '</li>\n'+
			        '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Components">\n'+
			          	'<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponentsSettings" data-parent="#exampleAccordion">\n'+
			            '<i class="fa fa-fw fa-wrench"></i>\n'+
			            '<span class="nav-link-text">Settings</span>\n'+
			          	'</a>\n'+
			          	'<ul class="sidenav-second-level collapse" id="collapseComponentsSettings">\n'+
			            		'<li>\n'+
			              		'<a href="' + this.settingsURL + '">Account/Security</a>\n'+
			            		'</li>\n'+
			          	'</ul>\n'+
			        '</li>\n'+
			    '</ul>\n'+
		        '<ul class="navbar-nav sidenav-toggler">\n'+
			        '<li class="nav-item">\n'+
			        		'<a class="nav-link text-center" id="sidenavToggler">\n'+
		            		'<i class="fa fa-fw fa-angle-left"></i>\n'+
			        		'</a>\n'+
			        '</li>\n'+
			    '</ul>\n'+
			    '<ul class="navbar-nav ml-auto">\n'+
			    		'<li class="nav-item">\n'+
	          			'<a class="nav-link" data-toggle="modal" data-target="#logoutModal">\n'+
	          			'<i class="fa fa-fw fa-sign-out"></i>Logout</a>\n'+
	          		'</li>\n'+
	          	'</ul>\n'+
	        '</div>\n';
	};
	
	this.generateLogoutModal = function( ){
		var logout = document.getElementById( "logoutModal" );
		logout.innerHTML = '<div class="modal-dialog" role="document">\n'+
        '<div class="modal-content">\n'+
            '<div class="modal-header">\n'+
                '<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>\n'+
                '<button class="close" type="button" data-dismiss="modal" aria-label="Close">\n'+
                    '<span aria-hidden="true">×</span>\n'+
                '</button>\n'+
            '</div>\n'+
            '<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>\n'+
            '<div class="modal-footer">\n'+
                '<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>\n'+
                '<a class="btn btn-primary" onclick="endSession()" href="login.html" style="color: rgba(255,255,255,1);">Logout</a>\n'+
            '</div>\n'+
        '</div>\n'+
		'</div>\n';
	};
	
	this.initPage = function( ) {
		this.generateNavigationPanel( );
		this.generateLogoutModal( );
		document.getElementsByName("title").innerHTML = "Travas Platform";
	}
};

/**
 * Global function to wrap template generator object and call init
 * @returns
 */
function generateTemplate( ){
	new TemplateManager( ).initPage( );
}