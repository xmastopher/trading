/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if( document.getElementById("username") !== undefined ){
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}

/**
 * When user clicks submit for change password
 */
$(document).on('click', '#changePasswordSubmit', function() {
	callChangePasswordEndpoint( );
});

/**
 * Called at start of page
 * @returns
 */
function onLoad( ) {
	'use strict';
	updatePage( );
}

/**
 * Called to update entire page
 * @returns
 */
function updatePage( )
{
    var userProfileEndpoint = new AuthenticatedEndpoint()
    var response			    = userProfileEndpoint.doRequest( {}, Constants.getInstance( ).userProfileURL ); 
	loadProfileView( response );
	loadExchangeApiKeyTable( response.linkedExchanges );
}

/**
 * Loads the exchange table w/ respective API keys
 * @param exchanges
 * @returns
 */
function loadExchangeApiKeyTable( exchanges )
{
	var tableHtml = "";
	
	//Iterate over all exchanges
	//populate each row
	//make rows red that are inactive, green otherwise
	//buttons should be dynamic, 'activate' , 'deactivate'
	for( var i = 0; i < exchanges.length; i++ )
	{
		var column = generateColumn( i+1, exchanges[ i ] );
		tableHtml += column;
	}
	
	document.getElementById("exchangeTableBody").innerHTML = tableHtml;
}

/**
 * Generates column for the exchange API keys table
 * @param index
 * @param exchange
 * @returns
 */
function generateColumn( index, exchange )
{
	var name     = exchange.name;
	var isActive = exchange.isActive;
	
	if( exchange.isActive ) {
	
	    return "<tr class='table-success'>" 		   +
	    		   "<td>" + index + "</td>"     		   +
	    		   "<td>" + name + "</td>" +
	    		   "<td><button id=deactivate-" + index + "> deactivate </button></td></tr>"
	}
	
	//Non-active
	return "<tr class='table-danger'>" 	   +
	   "<td>" + index + "</td>"     		   +
	   "<td>" + name + "</td>" +
	   "<td><button id=activate-" + index + "> activate </button></td></tr>"
}

/**
 * Used to load user information into dynamic elements
 * @param obj
 * @returns
 */
function loadProfileView( obj )
{
	//Grab view object
	//Set internal for credibility, followers, email, username, bio
	//populate left column
	//document.getElementById( "userCredibility" ).innerHTML = "<h5>Credibility: " + obj.credibility + "</h5>";
	//document.getElementById( "userFollowers" ).innerHTML   = "<h5>Followers: " + obj.numFollowers + "</h5>"
	//document.getElementById("userBio").innerHTML           = "<p><i>" + obj.bio + " <a href='#'>edit</a></p>"
	document.getElementById("userEmail").innerHTML         = "<h5> Email: " + obj.email + "</h5></div>"
	document.getElementById("userUsername").innerHTML      = "<h5> Username: " + obj.username + "</h5></div>"
	
	//Call gen followers modal
}

/**
 * Called when user request to change password
 * @returns
 */
function callChangePasswordEndpoint( ){
	var currentPassword 			= document.getElementById( "currentPassword" ).value;
	var currentPasswordConfirmed = document.getElementById( "currentPasswordConfirmed" ).value;
	var newPassword 				= document.getElementById( "newPassword" ).value;
	var newPasswordConfirmed 	= document.getElementById( "newPasswordConfirmed" ).value;
	var confirmedInput			= confirmInputsForPassword( currentPassword, currentPasswordConfirmed, newPassword, newPasswordConfirmed );
	
	if( !confirmedInput[ 0 ] ){
		alert( confirmedInput[ 1 ] );
		return false;
	}
	
	var request = {
			password: currentPassword,
			passwordConfirmed: currentPasswordConfirmed,
			newPassword: newPassword,
			newPasswordConfirmed: newPasswordConfirmed
	}
	
    var changePasswordEndpoint = new AuthenticatedEndpoint()
    var response			      = changePasswordEndpoint.doRequest( request, Constants.getInstance( ).changePasswordURL ); 
	
	if( response.success ){
		alert( "Password succesfully changed!" );
		return true;
	}
	else{
		alert( response.message );
		return false;
	}
}

/**
 * Called at the start of a change password request
 * @returns string array of size two w/ [ true/false, message ]
 */
function confirmInputsForPassword( currentPassword, confirmCurrentPassword, newPassword, confirmNewPassword ){
	
	
	if( currentPassword === "" || confirmCurrentPassword === "" || newPassword === "" || confirmNewPassword === "" ){
		return [ false, "one or more passwords have no value, please enter a value and try again." ]
	}
	
	else if( currentPassword != confirmCurrentPassword ){
		return[ false, "current passwords do not match, please enter matching fields for current passwords." ]
	}
	
	else if( newPassword != confirmNewPassword ){
		return [ false, "new passwords do not match, please enter matching fields for new passwords." ]
	}
	
	else if( currentPassword === newPassword ){
		return [ false, "new password cannot be the same as current password, please enter a different password." ]
	}
	
	else if( newPassword.length < 8 ){
		return [ false, "new password must be larger than eight characters... Come on! Get some better security!"]
	}
	
	return [ true, "success" ];
}






