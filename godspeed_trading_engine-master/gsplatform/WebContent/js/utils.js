
//Returns the signal name without the prefix 'buy' or 'sell' - stripping them
function getSignalNameFromAPIString( apiString ){
	
	var list = apiString.split( "-" );
	list.splice(-1,1)
	return list.join("-");
}

//Returns a description given a signal - TODO: embed tooltips into strings for each 
//unique signal which dives into more detail about the functions used
function getDescriptionFromName( name ){
	
	switch( name ){
	
		case "TARGET-BUY":
			return "Buys on the next open price when the current candle closes below ( X * last sell price ) + last sell price, where X is a percentage (target buy price)";
			
		case "TARGET-SELL":
			return "Sells on the next open price when the current candle closes above ( X * last buy price ) + last buy price, where X is a percentage (target sell price)";
		
		case "SMA-BUY":
			return "Buys on the next open price when the current candle closes above SMA(X)";
			
		case "SMA-SELL":
			return "Sells on the next open price when the current candle closes below SMA(X)";
			
		case "SMA-CROSS-BUY":
			return "Buys on the next open price when SMA(X) crosses above SMA(Y) where Y > X";
			
		case "SMA-CROSS-SELL":
			return "Sells on the next open price when SMA(X) crosses below SMA(Y) where Y < X"
			
		case "EMA-BUY":
			return "Buys on the next open price when the current candle closes above EMA(X)";
			
		case "EMA-SELL":
			return "Sells on the next open price when the current candle closes below EMA(X)";
			
		case "EMA-CROSS-BUY":
			return "Buys on the next open price when EMA(X) crosses above EMA(Y) where Y > X";
			
		case "EMA-CROSS-SELL":
			return "Sells on the next open price when EMA(X) crosses below EMA(Y) where Y < X"
			
		case "BOLLINGER-BANDS-BUY":
			return "Buys on the next open price when the current candle closes above or matches the upper bollinger band.";
			
		case "BOLLINGER-BANDS-SELL":
			return "Sells on the next open price when the current candle closes below or matches the lower bollinger band.";
			
		case "LOCAL-MINIMA-BUY":
			return "Buys on the next open price when the current candle closes below the local minima of the last X candles";
			
		case "LOCAL-MAXIMA-SELL":
			return "Sells on the next open price when the current candle closes above the local maxima of the last X candles";
			
		case "BOLLINGER-BANDS-SELL":
			return "Sells on the next open price when the current candle closes above the local maxima of the last X candles";
			
		case "RSI-BUY":
			return "Buys on the next open price when the current RSI (relative strength index) > threshold (a value between 0 and 100)"
			
		case "RSI-SELL":
			return "Sells on the next open price when the current RSI (relative strength index) < threshold (a value between 0 and 100)"
			
		case "MACD-BUY":
			return "Buys on the next open price when the MACD value crosses above the SIGNAL line (EMA line over the last X MAC values)"
			
		case "MACD-SELL":
			return "Buys on the next open price when the MACD value crosses below the SIGNAL line (EMA line over the last X MAC values)"
	}
	
	return "INVALID SIGNAL";
}

//Implement smart function that takes in a list of buy signals and a list of sell signals and
//generates a unique description for the function ie. if the user has a function with two buy
//signals: SMA-CROSS && LOCAL MINIMA and one sell signal BOLLINGER-BAND description might be:
//'Buys when SMA(X) crosses above SMA(Y) AND current close price is <= the local minima.  Sells
//when the current close <= the upper bollinger band :) - should just be a concatenation function
function generateAlgorithmDescription( buySignals, sellSignals ){
	return "";
}

function getParameterDefinition( name ){
	
	switch( name ){
		case "sma":
			return "sma(x): simple moving average, takes the last x candles and averages over their close prices.";
			
		case "smaSmaller":
			return "sma-smaller(x): simple moving average [shorter], takes the last x candles and averages over their close prices. Sma shorter always uses less candles than sma longer.";
			
		case "smaLarger":
			return "sma-larger(x): simple moving average [longer], takes the last x candles and averages over their close prices. Sma longer always uses more candles than sma shorter.";
			
		case "ema":
			return "ema(x): exponential moving average, takes the last x candles and returns the exponential moving average of their close prices";
			
		case "emaSmaller":
			return "ema-smaller(x): exponential moving average [shorter], takes the last x candles and returns the exponential moving average of their close prices. EMA shorter always uses less candles than EMA longer.";
			
		case "emaLarger":
			return "ema-larger(x): exponential moving average [longer], takes the last x candles and returns the exponential moving average of their close prices. EMA longer always uses more candles than EMA shorter.";
		
		case "emaSignal":
			return "ema-signal(x): exponential moving average, takes the last x candles and returns the exponential moving average of their close prices. EMA signal uses the output from MACD instead of the candle's close prices";
			
		case "period":
			return "period: the number of candles to include in the signals calculation";
			
		case "target":
			return "target(x): a target value to buy or sell at, returns a value that is either X% above or below the previous buy or sell price";
		
		case "windowSize":
			return "window-size: defines the number of candles to use within a set of numbers";
			
		case "threshold":
			return "threshold: parameter that can be choosed to define a cut off point where buy and sells will be generated"
	}

	return "INVALID PARAMETER";
}

function camelCaseToNormal( camel ){
	return camel.replace( /([A-Z])/g, " $1" );
}

function generateAVIPath( username ){
	return "userContent/" + username + ".png"
}

function getColorFromValue( value, threshold, goodColor, badColor, neutralColor ){
	if( value < threshold ){
		return badColor;
	}
	else if( value > threshold ){
		return goodColor;
	}
	return neutralColor;
}
