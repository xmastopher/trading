

$(function() {
});

$('#loginForm').on('submit', function () {
    'use strict';
    var redirect = null;
    var username = document.getElementById('emailInput').value;
    var password = document.getElementById('passwordInput').value;
    var loginEndpoint  = new AuthenticatedEndpoint( );
    loginEndpoint.login = true;
    var response = loginEndpoint.doRequest( {username: username,password: password}, Constants.getInstance().loginURL );
    if( response.success ) {
    		updateSession( response.sessionToken );
    		document.location.href = Constants.getInstance( ).dashboard;
    		return false;
    }
    else {
    		alert( response.message );
    		return false;
    }
});

        