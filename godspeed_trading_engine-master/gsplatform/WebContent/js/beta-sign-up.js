$(function() {
});

$('#betaForm').on('submit', function (e) {
    'use strict';
    e.preventDefault();
    var email = document.getElementById('singUpEmailInput').value;
    var name = document.getElementById('nameSignUp').value;
    var description = document.getElementById('alphaComment').value;
    var signUpData = 
    {
        name : name,
        email: email,
        description: description
    };

    $.ajax(
        {
            type: 'POST',
            url: "php/contact_me.php",
            data: signUpData,
            cache: false,
            timeout: 30000,
            success: function(){
                $('#betaForm').trigger("reset");
                $('#message').empty();
                $('#message').html("<div class='alert alert-success alphaMessage'>");
                    $('#message > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#message > .alert-success')
                        .append("<p>Your request has been sent! </p>");
                    $('#message > .alert-success')
                        .append('</div>');
    			},
            error: function () {
                $('#betaForm').trigger("reset");
                $('#message').empty();
                $('#message').html("<div class='alert alert-success alphaMessage'>");
                    $('#message > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#message > .alert-success')
                        .append("<p>Sorry " + name + ", it seems that our mail server is not responding. Please try again later! </p>");
                    $('#message > .alert-success')
                        .append('</div>');
            }
        }
    );
});

        

