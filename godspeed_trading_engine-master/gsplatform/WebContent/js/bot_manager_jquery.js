/**
 * Set document to strict
 */
$(document).ready(function () {
    'use strict';
});

/**
 * Links the close bot modal button to clear the current interval
 */
$(document).on('click', '#botModalLiveClose', function( ){
	
	if( cache.intervalId != null )
	{
		clearInterval(cache.intervalId);
	}
});

/**
 * Triggered when a dropdown menu button is clicked, this either be an 
 * exchange, market or strategy
 */
$(".dropdown-menu").on("click", "a", function(){
      
	  'use strict';
	  
	  //Grab the selected text and parses key from id
	  var selText = $(this).text( );
	  var id      = $(this)[0].id;
	  
	  //If the selection is an exchange
	  if( id.includes( "exchange" ) )
	  { 
		  //Grab the exchange
		  document.getElementById("selectedExchange").innerHTML = selText;
		  
		  //Build the request
		  var request = {
				  exchange:selText.toLowerCase( ) 
		  }
		  
		  //Build endpoint and grab markets
		  var marketLookupEndpoint = new AuthenticatedEndpoint( );
		  var response             = marketLookupEndpoint.doRequest( request, endpoints.marketLookupEndpoint );
		  
		  if( response.success ){
			  document.getElementById("selectedMarket").innerHTML = "";
			  document.getElementById( "marketList" ).innerHTML   = generateMarketList( response.markets );
		  }
		  else{
			  alert( response.message );
		  }
	  }
	  
	  //Set the selected market
	  else if( id.includes( "market" ) )
	  { 
		  
		  document.getElementById("selectedMarket").innerHTML = selText;
	  }
	  
	  //Set the selected strategy
	  if( id.includes( "strategy" ) )
	  { 
		  document.getElementById("selectedStrategy").innerHTML = selText;
		  var split = $(this)[0].id.split("-");
		  cache.selectedStrategy = split[split.length-1];
	  }
});


/**
 * Link click button to save current bot config
 * @returns
 */
$(document).on('click', '#saveBot', function( ){
	
	'use strict';
	
	//Exctract all fields for request
	const nameField     = document.getElementById( "botNameInput" ).value;
	const exchangeField = document.getElementById( "selectedExchange" ).innerHTML;
	const strategyField = document.getElementById( "selectedStrategy" ).innerHTML;
	const marketField   = document.getElementById( "selectedMarket" ).innerHTML.split( "-" );
	const tradingAmount = parseFloat( document.getElementById( "sliderValue" ).innerHTML.replace('%','') ) / 100;
	const privacyField  = false;
	let intervalField = false;
	const baseField     = marketField[ 0 ];
	const quoteField    = marketField[ 1 ];
	
	//Verify that a candle was chosen
	if($("#1m")[0].checked)
	{
	   intervalField = '1m';
	}
	if($("#5m")[0].checked)
	{
	   intervalField = '5m';
	}
	if($("#15m")[0].checked)
	{
	   intervalField = '15m';
	}
	if($("#30m")[0].checked)
	{
	   intervalField = '30m';
	}
	if($("#1h")[0].checked)
	{
	   intervalField = '1h';
	}
	if($("#2h")[0].checked)
	{
	   intervalField = '2h';
	}
	if($("#4h")[0].checked)
	{
	   intervalField = '4h';
	}
	if($("#6h")[0].checked)
	{
	   intervalField = '6h';
	}
	if($("#12h")[0].checked)
	{
	   intervalField = '12h';
	}
	if($("#1d")[0].checked)
	{
	   intervalField = '1d';
	}
	
	//Verify that an exchange was chosen
	if( !exchangeField )
	{
		alert( "Please select an exchange." )
		return;
	}
	
	//Verify that a market was chosen
	if( !marketField )
	{
		alert( "Please select a market." )
		return;
	}
	
	//Verify that a strategy was chosen
	if( !strategyField )
	{
		alert( "Please select a strategy." )
		return;
	}
	
	//Verify that a candle was chosen
	if( !intervalField )
	{
		alert( "Please select an interal ie. 1m,5m,15m,30m,1h,2h,4h,6h,12h,1d" )
		return;
	}
	
	//Verify that a trading amount was chosen
	if( isNaN( tradingAmount ) ){
		alert("Please choose a trading amount.");
		return;
	}
	
	//Construct the request
    var request = {
    		botName: nameField,
    		exchange: exchangeField,
    		baseCurrency: baseField,
    		counterCurrency: quoteField,
    		strategyId: window.location.href.includes("backtest.html") ? strategiesMap[Number(cache.selectedStrategy)].id
    																  : cache.selectedStrategy,
    		interval: intervalField,
    		tradingLimit: tradingAmount,
    		"private": privacyField
    }
    
    var saveBotEndpoint = new AuthenticatedEndpoint( );
    var response		   = saveBotEndpoint.doRequest( request, endpoints.saveBotEndpoint );
    
    //Response is good - reset selections and load page
    if( response.success ){
		document.getElementById("selectedMarket").innerHTML = "";
		document.getElementById("selectedExchange").innerHTML = "";
		document.getElementById("selectedStrategy").innerHTML = "";
		alert("Successfully saved bot!")
		loadPage( );
    }
    
    else {
    		alert( response.message );
    		return false;
    }
	
} );

/**
 * Buy signal filter checked
 * @returns
 */
$('#buyCheckBox').change(function(){
	
	if ( cache.cachedLogObject === null )
		return;

	var buyIsChecked = document.getElementById( "buyCheckBox" ).checked;
	var sellIsChecked = document.getElementById( "sellCheckBox" ).checked;
	var holdIsChecked = document.getElementById( "holdCheckBox" ).checked;
	
	flags = [ true, true, true ]
	flags[ 0 ] = document.getElementById( "buyCheckBox" ).checked;
	flags[ 1 ] = document.getElementById( "sellCheckBox" ).checked;
	flags[ 2 ] = document.getElementById( "holdCheckBox" ).checked;
	
	document.getElementById( "logTable" ).innerHTML = generateLogScroller( cache.cachedLogObject, flags );
	
});

/**
 * Sell signal filer checked
 * @returns
 */
$('#sellCheckBox').change(function(){
	
	if ( cache.cachedLogObject === null )
		return;
	
	var buyIsChecked = document.getElementById( "buyCheckBox" ).checked;
	var sellIsChecked = document.getElementById( "sellCheckBox" ).checked;
	var holdIsChecked = document.getElementById( "holdCheckBox" ).checked;
	
	flags = [ true, true, true ]
	flags[ 0 ] = document.getElementById( "buyCheckBox" ).checked;
	flags[ 1 ] = document.getElementById( "sellCheckBox" ).checked;
	flags[ 2 ] = document.getElementById( "holdCheckBox" ).checked;
	
	document.getElementById( "logTable" ).innerHTML = generateLogScroller( cache.cachedLogObject, flags );
	
});

/**
 * Hold signal filter checked
 * @returns
 */
$('#holdCheckBox').change(function(){
	
	if ( cache.cachedLogObject === null )
		return;
	
	flags = [ true, true, true ]
	flags[ 0 ] = document.getElementById( "buyCheckBox" ).checked;
	flags[ 1 ] = document.getElementById( "sellCheckBox" ).checked;
	flags[ 2 ] = document.getElementById( "holdCheckBox" ).checked;
	
	document.getElementById( "logTable" ).innerHTML = generateLogScroller( cache.cachedLogObject, flags );
	
});

/**
 * Delete strategy button was pressed - calls delete bot api
 * @returns
 */
$(document).on('click', '#confirmDeleteStrategy', function( ) {
	
	//Check that the bot is not running
	if( cache.botMap[cache.botToDelete].running )
	{
		alert( "Cannot delete a bot that is running, please turn off the bot then delete it." );
		return;
	}
	
	//Construct the request
	var request = {
			botId: cache.botToDelete
	};
	
	var deleteBotEndpoint = new AuthenticatedEndpoint( );
	var response          = deleteBotEndpoint.doRequest( request, endpoints.deleteBotEndpoint );
	
	//Response is good - reload page and alert user
	if( response.success ){
		loadPage( );
		alert( "successfully deleted bot" );
	}
	
	else {
		alert(response.message);
		return false;
	}
});