$(document).ready(function () {
    'use strict';
});


/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if( document.getElementById("username") !== undefined ){
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}

/**
 * Global cache
 */
var cache = {
		currentInterval: "1m",
		selectedRow: "",
		isLoading: false,
		currentMarket: ""
}

/**
 * Immutable values
 */
const pageConstants = {
		arbLimit: 2
}

/**
 * Endpoints wrapper
 */
var endpoints ={
		exchangeCandlesEndpoint : Constants.getInstance().exchangeCandlesURL,
		arbitrageEndpoint: Constants.getInstance().arbitrageURL
}

/**
* Link all table row clicks to select row function
*/
$(document).on('click', '#dataTable > tbody > tr', function( ) {
	var id = $(this)[0].id;
	selectRow( $(this), id );
});

function onLoad( ){
    document.getElementById("currentMarkets").innerHTML = "GDAX" + "-" + "GEMINI";
	runCrossExchangeAnalysis( );
}

/**
 * Called to select a specific row from the cross exchange analysis table
 * @param element
 * @param row
 * @param username
 * @param botId
 * @returns
 */
function selectRow( element, id ){
	if( id === 0 ){
		return;
	}
	cache.selectedRow = id;
	showButtons( id );
	element.addClass('bg-info').siblings().removeClass('bg-info');
	generateGraph( id );
}

/**
 * Called to make the candle buttons visable
 * @returns
 */
function showButtons( id ) {
	var base        = id.split("-")[0];
	var quote       = id.split("-")[1];
	cache.currentMarket = base + "-" + quote;
	document.getElementById("marketTag").innerHTML = base + "-" + quote + " (" + cache.currentInterval.toUpperCase() + ")";
    document.getElementById("candleStickButtons").style.visibility = "visible";
}

/**
 * Called to make the candle buttons invisible
 * @returns
 */
function hideButtons( ) {
	document.getElementById("marketTag").innerHTML = "";
    document.getElementById("candleStickButtons").style.visibility = "hidden";
}

/**
 * Called to update current candle stick
 */
function changeCandle( candle ){
	cache.currentInterval = candle;
	document.getElementById("marketTag").innerHTML = cache.currentMarket + " (" + cache.currentInterval.toUpperCase() + ")";
	generateGraph( cache.selectedRow );
}

/**
 * Clear graph and buttons
 */
function clearGraphView(){
	hideButtons();
	$("#crossExchangeChart > svg").html("");
	$("#crossExchangeChart > svg").removeClass( );
	$("#innerDataTable").html("");
}

/**
 * Call to generate graph with two lines for both 
 * markets of differed exchanges
 */
async function generateGraph( id ){
	
	if( id === 0 ){
		return;
	}
	var base        = id.split("-")[0];
	var quote       = id.split("-")[1];
	var exchangeOne = id.split("-")[2];
	var exchangeTwo = id.split("-")[3];
	
	var request = {
			exchange : exchangeOne,
			interval : cache.currentInterval,
			base : base,
			quote : quote
	};
	
	var requestTwo = {
			exchange : exchangeTwo,
			interval : cache.currentInterval,
			base : base,
			quote : quote
	};
	
	var exchangeCandles 	   = new AuthenticatedEndpoint();
	const responseExchangeOne = await exchangeCandles.doRequest(request, endpoints.exchangeCandlesEndpoint, false );
	const responseExchangeTwo = await exchangeCandles.doRequest(requestTwo, endpoints.exchangeCandlesEndpoint, false );

	if ( responseExchangeOne.success && responseExchangeTwo.success ) {
		var allCandles = [ responseExchangeOne, responseExchangeTwo ];
		createGraphCrossExchangeAnalysis( allCandles, [ exchangeOne, exchangeTwo ] );
	}
	else{
		return false;
	}
	
}

// fills portfolio table

function loadArbTable( obj ) {
	
	$("#graphSection").html("<svg><svg>")
	if( obj == null )
		return;
	
	var html = '';
	
	var arbitragePairs = obj.arbitrages;
	var selectedRow    = 0;
	var exchangeOne    = arbitragePairs[0].min;
	var exchangeTwo    = arbitragePairs[0].max;
	
	for (i=0; i<arbitragePairs.length; i++)
    {
		arbitrage = arbitragePairs[i];
		var generatedId = arbitrage.market + "-" + arbitrage.min + "-" + arbitrage.max;
		if( i === 0 ){
			selectedRow = generatedId;
		}
        html+= '<tr id="' + generatedId +'">' +
		  '<th scope="row">'+ arbitrage.min.toUpperCase( ) +'</th>' +
		  '<th scope="row">'+ arbitrage.max.toUpperCase( ) +'</th>' +
		  '<td>'+ arbitrage.market.toUpperCase( ) +'</td>' +
		  '<td>'+ arbitrage.spread +'</td>' +
		  '<td>'+ arbitrage.minVolume +'</td>' +
		  '<td>'+ arbitrage.maxVolume +'</td>' +
		'</tr>';
    }
	$("#innerDataTable")[0].innerHTML = "";
    $("#innerDataTable").append(html);
    $("#currentMarkets").html( exchangeOne.toUpperCase( ) + "-" + exchangeTwo.toUpperCase( ) );
    selectRow( $("#"+selectedRow), selectedRow );
}

/**
 * Grabs selected exchanges from toggles
 * @returns
 */
function getExchanges(){
	
	var exchanges = [];
	var count = 0;
	if( document.getElementById("binance").checked ){
		exchanges.push( "binance" );
		count++
	}
	
	if( document.getElementById("kraken").checked ){
		exchanges.push( "kraken" );
		count++;
	}
	
	if( document.getElementById("gdax").checked ){
		exchanges.push( "gdax" );
		count++
	}
	
	if( document.getElementById("bittrex").checked ){
		exchanges.push( "bittrex" );
		count++
	}
	
	if( document.getElementById("hitbtc").checked ){
		exchanges.push( "hitbtc" );
		count++
	}
	
	if( document.getElementById("bithumb").checked ){
		exchanges.push( "bithumb" );
		count++
	}
	
	if( document.getElementById("bitflyer").checked ){
		exchanges.push( "bitflyer" );
		count++
	}
	
	if( document.getElementById("gemini").checked ){
		exchanges.push( "gemini" );
		count++
	}
	
	if( document.getElementById("poloniex").checked ){
		exchanges.push( "poloniex" );
		count++
	}
	
	if( document.getElementById("bitfinex").checked ){
		exchanges.push( "bitfinex" );
		count++
	}
	
	if( count > pageConstants.arbLimit ){
		alert( "You ca only select at most two exchanges at a time" );
		return [];
	}
	
	return exchanges;
	
}
$('#sendData').on('click', function () {
    runCrossExchangeAnalysis( );
});

/**
 * Asynchroneous method for running arb analysis
 * @returns
 */
async function runCrossExchangeAnalysis( ){
    var exchangeList = [];
    exchangeList = getExchanges();
    if( exchangeList.length === 0 ){
    		return false;
    }
	//setGraphSectionToLoad( );
	clearGraphView();
    
    var request = {
    		exchanges: exchangeList 
    }
    
    var arbEndpoint   = new AuthenticatedEndpoint( );
    await arbEndpoint.doRequest( request, endpoints.arbitrageEndpoint, true, loadArbTable );
}

/**
 * Set graph section to loading icon while waiting
 * @returns
 */
function setGraphSectionToLoad(){
	$("#crossExchangeChart").html( "<div class='item text-center'>"+
    "<div class='item-inner'>"+
    "<div class='item-loader-container'>"+
    "<div class='la-ball-spin la-2x'>"+
    "<div></div>"+
    "<div></div>"+
    "<div></div>"+
    "<div></div>"+
    "<div></div>"+
    "<div></div>"+
    "<div></div>"+
    "<div></div>"+
	"</div>"+
    "</div>"+
    "</div>"+
    "</div>"	);
}