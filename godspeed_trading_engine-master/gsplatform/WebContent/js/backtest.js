/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if( document.getElementById("username") !== undefined ){
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}

/*
 *  Globally created cache map for coins
 */

let cache = {
		selectedStrategy: -1
}

let map = Object();
let strategiesMap = Object();
let backtestName = "";
let cachedBacktestResults = Object();

/**
 * Endpoints defined at the page level
 */
var endpoints = {
		marketLookupEndpoint: Constants.getInstance( ).marketLookupURL,
		getStrategiesEndpoint: Constants.getInstance( ).getStrategiesURL,
		backtestEndpoint: Constants.getInstance( ).backtestURL,
		saveBotEndpoint: Constants.getInstance( ).saveBotURL,
}

/**
 * Wrapper to auto download backtest as CSV
 * @returns
 */
function downloadBacktestCSV(){
	let filename = new Date( ) + "-" + backtestName;
	exportTable( filename, "backtestRow", "Save Bot" );
}

/* 
 * come back and work on this. not completed.
 * clears map "cache and loads in newly selected 
 * market from given exchange.
 */
function updateMap(markets) {
	
	var i;
	
	//Reset map
    map = Object();
    
    //Iterate over all markets
    for (i = 0; i < markets.length; i++) {
    	
    		var market = markets[ i ];
    		
	    	//Grab base and quote symbols
	    	base = market.base;
	    quote = market.quote;
	    
	    //Check if key does not exist - if not init w/ empty array
	    if( !(quote in map) ) {
	        map[quote] = [ ];
	    }
	    
	    //Add base currency to quote's currency list
	    map[quote].push(base);
    
    }
} // end updateMap

function updateStrategiesMap(strategies) {
	//Reset map
	for( var i = 0; i < strategies.length; i++ ){
		var strategy = strategies[ i ];
		strategiesMap[ strategy.id ] = strategy;
		if( i === 0 ){
			$( "#" + strategy.id ).addClass('bg-info').siblings().removeClass('bg-info');
			cache.selectedStrategy = $( "#" + strategy.id ).attr("id");
		    createStrategyDescription( strategiesMap[ strategy.id ], "innerStrategyDiv1", "INSPECTED STRATEGY" );
		    createStrategyDescription( strategiesMap[ strategy.id ], "innerStrategyDiv2", "INSPECTED STRATEGY" );
		}
	}
} // end updateStrategiesMap

function fillBacktestPage()
{
    fillExchange();
    fillExchangeDiv();
    fillFirstMarket();
    fillCoinDiv();
    fillCandlestick();
    fillStrategy();
}

function fillFirstMarket()
{
	var request = {
			  exchange: "gdax" 
   };
   
   var marketLookupEndpoint = new AuthenticatedEndpoint( );
   var response			    = marketLookupEndpoint.doRequest( request, endpoints.marketLookupEndpoint );
   
   if( response.success ){
	   $("#innerTradedCoin").html("");
	   $("#innerBaseCoin").html("");
	   updateMap( response.markets );
	   fillMarketsQuote();
	   //fillMarketsBase();
   }
   else{
	   alert( response.message );
	   return false;
   }
}


/* Dynamically creates quote coin toggles based on
 * the exchange that was selected
 */

function fillMarketsQuote( )
{
    
    let html = ''; 
    
    for ( var quote in map )
    {
        html += '<div class="flex-content toggle-btn-content">' +
        '<label class="switch">' +
        '<input class="coinCheck hidden" ' + 'name="baseCheck" type="radio" ' +
        'id="'+quote+'" autocomplete="off" ' + 'value="'+quote+'">' +
        '<span class="slider round cc '+quote+'"' + 'title="'+quote+'"> ' + 
        '</span> '+
        '</label><input class="toggle-btn toggle-btn-small" ' + 'name="coinButton" ' + 'id="'+quote+'Button" ' + 'type="button" ' + 'value="'+quote+'" >' +
         '</div>';
    }
    
    $("#innerBaseCoin").append(html);
} // end fillTradedDropdown

/* 
 * Dynamically creates base coin toggles based on
 * the exchange that was selected
 */
$(document).on('click', '[name="baseCheck"]', function( ) {
	
    let selQuote = $(this).attr("id");
    let quoteMap = map[selQuote];
    let html = '<h4>Base</h4>'; 
    
    for ( var i = 0; i < quoteMap.length; i++ )
    {
        html += '<div class="flex-content toggle-btn-content">' +
        '<label class="switch">' +
        '<input class="coinCheck hidden" ' + 'name="tradedCoinCheck" type="checkbox" ' +
        'id="'+quoteMap[i]+'" autocomplete="off" ' + 'value="'+quoteMap[i]+'">' +
        '<span class="slider round cc '+quoteMap[i]+'" ' + 'title="'+quoteMap[i]+'"> ' +
        '</span> '+
        '</label><input class="toggle-btn toggle-btn-small" ' + 'name="coinButton" ' + 'id="'+quoteMap[i]+'Button" ' + 'type="button" ' + 'value="'+quoteMap[i]+'" >' +
         '</div>';
    }
    
    $("#quoteCurrencySpan").html( selQuote.toUpperCase( ) );
    $("#innerTradedCoin").html(html);
});

/*
 * Saves the strategy on based on which item on the table was clicked
 * and then turns the saved item blue.
 */
$(document).on('click', '#strategyTable > tbody > tr', function( ) {
	$(this).addClass('bg-info').siblings().removeClass('bg-info');
	cache.selectedStrategy = $(this).attr("id");
	createStrategyDescription( strategiesMap[ cache.selectedStrategy ], "innerStrategyDiv2", "SELECTED STRATEGY" );
});

/*
 * Updates information of the strategy based on which
 * strategy the mouse has last hovered over.
 */
$(document).on('mouseover', '#strategyTable > tbody > tr', function( ) {
	let strategyNum = $(this).attr("id");
	$("[value='strategyDiv']").hide();
    $('[name="' + strategyNum + 'Div"]').toggle();
	createStrategyDescription( strategiesMap[ strategyNum ], "innerStrategyDiv1", "INSPECTED STRATEGY" );
    
});

// fills backtest submit section with candlestick toggles

function fillCandlestick() {
	
    let array = ["1m","5m","15m","30m", "1h", "2h", "4h", "6h", "1d"];
    let html = ''; 
    
    for (var i=0; i<array.length; i++)
    {
        var btnName = "rb" + i;
        html += '<div class="flex-content toggle-btn-content">' +
        '<label class="switch">' +
        '<input class="candleRadio hidden" ' + 'name="rb" ' + ' type="radio" '+
        'id="'+btnName+'" autocomplete="off" ' + 'value="'+array[i]+'">' +
        '<span class="slider round cc '+array[i]+'" ' + 'title="'+array[i]+'"> ' +
        '</span> '+
        '</label><input class="toggle-btn toggle-btn-small" ' + 'name="coinButton" ' + 'id="'+array[i]+'Button" ' + 'type="button" ' + 'value="'+array[i]+'" >' +
         '</div>';
    }
    
    $("#innerCandlestick").append(html);
} // end fillCandlestick 

function generateStrategiesTable(strategies) {
	
	var html =  "";
		
	for ( var i = 0; i < strategies.length; i++ ) {
		
		html += '<tr id='+ strategies[i].id + '><td>'+ strategies[i].name + '</td></tr>';
	}
	$("#innerStrategy").append(html);
} // end generateStrategiesTable

/**
 * Called after grabbing user strategies to populate the
 * strategies drop down
 * @param strategies	A list of json strategy objects
 * @returns generated HTML
 */
function generateStrategyList( strategies ){
	
	html = ""
	
	for( var i = 0; i < strategies.length; i++ )
	{
		html +=  '<a class="dropdown-item" href="#" id="strategy-' + strategies[i].id + '">' + strategies[ i ].name + '</a>\n';
	}
		
	return html;
}

// fills backtest algorithm section with algorithm/parameter toggles

function fillStrategy() {
    
    let request = {};
    
    var getStrategiesEndpoint = new AuthenticatedEndpoint( );
    var response				 = getStrategiesEndpoint.doRequest( request, endpoints.getStrategiesEndpoint );
    
    if( response.success ){
		updateStrategiesMap(response.strategies);
		//fillStrategyDiv(response.strategies);
		generateStrategiesTable(response.strategies);
		generateStrategyList(  response.strategies );
    }
    else{
    		alert( response.message );
    		return false;
    }

} // end fillStrategy

function createStrategyDescription( strategy, id, title ){
	
	//Grab internal strategy breakdown
	var buySignals   = strategy.buySignals;
	var sellSignals  = strategy.sellSignals;
	var paddedName   = strategy.name;
	var length       = paddedName.length;
	
	html = '<br><h4>' + title + ': ' + '<br>' + paddedName + '</h4><br>' +
        	   generateSignalsHtml( buySignals )  + '<br>' +
        	   generateSignalsHtml( sellSignals ) + 
        	   '</span></a><br><br>';
	
	$("#"+id).html( html );
}

/**
 * Generates the internal text inside the tooltip
 * @param signalList
 * @returns
 */
function generateSignalsHtml( signalList ){
	
	var html = '';
	
	for ( var i = 0; i < signalList.length; i++ ) 
	{
		var signal  = signalList[ i ];
		var title   = '';
		var params  = '';
		
		for( var key in signal )
		{
		    if ( signal.hasOwnProperty( key ) ) 
		    {
		    	
		    		//Grab the name
		    		if( key === 'signal' )
		    		{
		    			title += signal[key] + ": ";
		    		}
		    		
		    		else
		    		{
		    			params += key + ' = ' + signal[key] + '  ';
		    		}
		    }
		}
		
		html += title + params;
	}	
	
	return html;
}

function fillExchange() {
    var array = ["Bitfinex", "Bittrex", "Hitbtc", "Kraken", "Poloniex", "Binance", "Bitflyer", "Bithumb", "Gdax", "Gemini"];
    var arrayLength = array.length;
    var html = ''; 
    var checked = '';
    for (var i=0; i<arrayLength; i++)
    {
    	if (i === 8) checked = "checked";
    	else checked = '';
    	
        var btnName = "rb" + i;
        html += '<div class="flex-content toggle-btn-content">' +
        '<label class="switch">' +
        '<input class="candleRadio hidden" ' + 'name="rbExchange" ' + ' type="radio" '+
        'id="exchange-'+array[i].toLowerCase()+'" autocomplete="off" ' + 'value="'+array[i]+'"' + checked + '>' +
        '<span class="slider round cc '+array[i]+'" ' + 'title="'+array[i]+'"> ' +
        '</span> '+
        '</label><input class="toggle-btn toggle-btn-small" ' + 'name="exchangeButton" ' + 'id="'+array[i]+'Button" ' + 'type="button" ' + 'value="'+array[i]+'" >' +
         '</div>';
    }
    
    $("#innerExchange").append(html);
    
} // end fillCandlestick 

function fillExchangeDiv() {

	let exchangeInfo = [
			 {
				heading: "Bitfinex",
				users: 0,
				volume: 0,
				generalDescription: "something",
				specialDescription: "something special"
			},
			{
					heading: "Bittrex",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Hitbtc",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Kraken",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Poloniex",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Binance",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Bitflyer",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Bithumb",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Gdax",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			},
			{
					heading: "Gemini",
					users: 0,
					volume: 0,
					generalDescription: "something",
					specialDescription: "something special"
			}
			
	];
	
    let html = ''; 
    for (var i=0; i<exchangeInfo.length; i++)
    {    
        html += '<div class="details-container hidden" name="'+ exchangeInfo[i].heading.toLowerCase() +'Div" value="exchangeDiv">' +
        '<h2>'+ exchangeInfo[i].heading +'</h2><p>' + exchangeInfo[i].generalDescription + '</p><p>' + exchangeInfo[i].specialDescription + '</p>' +
        '<p>Users: ' + exchangeInfo[i].users + '</p><p>Volume:' + exchangeInfo[i].volume + '</p></div>';
    }
    
    $("#innerExchangeDiv").append(html);
} // end fillStrategyDiv

function fillCoinDiv() {

	coinInfo = getCoinInformation();
	
    let html = ''; 
    for (var i=0; i<coinInfo.length; i++)
    {    
        html += '<div class="details-container hidden" name="'+ coinInfo[i].heading.toLowerCase() +'Div" value="coinDiv">' +
        '<h2>'+ coinInfo[i].heading +'</h2><p>' + coinInfo[i].generalDescription + '</p><p>' + coinInfo[i].specialDescription + '</p>' +
        '<p>Market Cap: ' + coinInfo[i].marketCap + '</p><p>Volume:' + coinInfo[i].volume + '</p></div>';
    }
    
    $("#innerQuoteDiv").append(html);
    $("#innerBaseDiv").append(html);
    
} // end fillStrategyDiv

function generateMarketList( markets ){
	
	html = ""
	
	for( var i = 0; i < markets.length; i++ )
	{
		html +=  '<a class="dropdown-item" href="#" id="market-' + i + '">' + markets[ i ].base + "-" + markets[ i ].quote + '</a>\n';
	}
		
	return html;
}

$(document).on('click', '[name="rbExchange"]', function( ){
    
	var selExchange = $(this).attr("id");
    selExchange = selExchange.split("-")[1];
    
    var request = {
			  exchange: selExchange.toLowerCase() 
    };
    
    var marketLookupEndpoint = new AuthenticatedEndpoint( );
    var response			    = marketLookupEndpoint.doRequest( request, endpoints.marketLookupEndpoint );
    
    if( response.success ){
        $("#innerTradedCoin").html("");
        $("#innerBaseCoin").html("");
        updateMap( response.markets );
        fillMarketsQuote();
        //fillMarketsBase();
    }
    else{
    		alert( response.message );
    		return false;
    }
});

function saveBotBacktest( index ){
	
}
