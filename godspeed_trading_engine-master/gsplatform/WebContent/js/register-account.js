$(function() {
});

$(document).ready(function () {
   $("#passwordInput, #passwordInputConfirm").keyup(checkPasswordMatch);
});

function checkPasswordMatch() {
    var password = $("#passwordInput").val();
    var confirmPassword = $("#passwordInputConfirm").val();
    var myInput = document.getElementById("passwordInput");
    
    if (confirmPassword.length < 1)
        $("#passwordInputConfirm").css("box-shadow", "0 0 0px white");
    else if (password != confirmPassword)
        $("#passwordInputConfirm").css("box-shadow", "0 0 12px red");
    else
        $("#passwordInputConfirm").css("box-shadow", "0 0 12px green");
    
    myInput.onkeyup = function() {
      // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        var upperCaseLetters = /[A-Z]/g;
        var numbers = /[0-9]/g;
        if(myInput.value.match(lowerCaseLetters) && 
           myInput.value.match(upperCaseLetters) && 
           myInput.value.match(numbers) && 
           myInput.value.length >= 8 && myInput.value.length <=15) {  
            $("#passwordInput").css("box-shadow", "0 0 12px green");
        }
        else {
            $("#passwordInput").css("box-shadow", "0 0 12px red");
        }
    }
} // end checkPasswordMatch

$('#registerForm').on('submit', function () {
    'use strict';
    var first = document.getElementById('firstNameInput').value;
    var last = document.getElementById('lastNameInput').value;
    var username = document.getElementById('emailInput').value;
    var password = document.getElementById('passwordInput').value;
    var loginData = 
        {
            FirstName: first,
            LastName: last,
            Username: username,
            Password: password,
        };
   
    var returnData = {
    		data : loginData
    }
 
    alert( JSON.stringify( returnData ) )
    var params = "data=" + JSON.stringify( returnData )
    
    $.ajax(
        {
            type: 'POST',
            url: 'http://localhost:8080/gsplatform/BacktestServlet',
            data: params,
            contentType: "application/json",
            success: function (json) {
                alert('Successful Post');
                
                if (json.data.Success)
                    window.location.href = "home.html";
                else
                    alert(json.data.Message);
            },
            error: function () {
                alert('ERROR, data not sent');
            }
        }
    );
});