function loadTest() {
	$.getJSON("json-examples/portfolio.json", function(json) { 
	    loadPieChart(json);
	    loadTokenDropdowns(json.data);
	    loadPortfolioTable(json.data);
	});
}

// shows/hides dust tokens in portfolio
 
$(function() {
    $( "#hideDustTokens" ).click(function() {
        $( "#dustTokenDropdown" ).slideToggle("fast");
    });
    $( "#hideTopTokens" ).click(function() {
        $( "#topTokenDropdown" ).slideToggle("fast");
    });
    $( "#toggleDailyPortfolio" ).click(function() {
        $( "#dailyPortfolio" ).show();
        $( "#weelyPortfolio" ).hide();
        $( "#monthlyPortfolio" ).hide();
        $( "#yearlyPortfolio" ).hide();
        $( "#overallPortfolio" ).hide();
    });
    $( "#toggleWeeklyPortfolio" ).click(function() {
    	$( "#dailyPortfolio" ).hide();
    	$( "#weelyPortfolio" ).show();
    	$( "#monthlyPortfolio" ).hide();
    	$( "#yearlyPortfolio" ).hide();
    	$( "#overallPortfolio" ).hide();
    });
    $( "#toggleMonthlyPortfolio" ).click(function() {
    	$( "#dailyPortfolio" ).hide();
    	$( "#weelyPortfolio" ).hide();
        $( "#monthlyPortfolio" ).show();
        $( "#yearlyPortfolio" ).hide();
        $( "#overallPortfolio" ).hide();
    });
    $( "#toggleYearlyPortfolio" ).click(function() {
    	$( "#dailyPortfolio" ).hide();
    	$( "#weelyPortfolio" ).hide();
    	$( "#monthlyPortfolio" ).hide();
        $( "#yearlyPortfolio" ).show();
        $( "#overallPortfolio" ).hide();
    });
    $( "#toggleOverallPortfolio" ).click(function() {
    	$( "#dailyPortfolio" ).hide();
    	$( "#weelyPortfolio" ).hide();
    	$( "#monthlyPortfolio" ).hide();
    	$( "#yearlyPortfolio" ).hide();
        $( "#overallPortfolio" ).show();
    });
});

// loads token dropdowns

function loadTokenDropdowns(tokenData) {
	var tophtml = '';
	var dusthtml = '';
	for (i=0; i<tokenData.length; i++) {
	
		var portion = tokenData[i].portionOfPortfolio;
		portion = portion.slice(0, -1);
		portion = parseInt(portion);
		
		if (portion != 0) {
			tophtml+= '<div class="btn-group">' +
		      '<button type="button" class="btn btn-secondary dropdown-toggle topToken"' +   
		      'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + 
		      tokenData[i].symbol +
		      '</button>' +
		      '<div class="dropdown-menu">' + 
		      '<a class="dropdown-item" href="#">Balance: ' + tokenData[i].totalBalance + '</a>' +
		      '<a class="dropdown-item" href="#">Proportion: ' + tokenData[i].tokenProportions[0] + '</a>' +
		      '<a class="dropdown-item" href="#">Portfolio Portion: ' + tokenData[i].portionOfPortfolio + '</a>' +
		      '<a class="dropdown-item" href="#">Location: ' + tokenData[i].tokenLocations[0] + '</a>' +
		      '<a class="dropdown-item" href="#">USD: ' + tokenData[i].USD + '</a>' +
		      '<div class="dropdown-divider"></div>' +
		      '<a class="dropdown-item" href="#">How is ' + tokenData[i].symbol + ' doing today?</a>' +
		      '</div></div>';
		}
		else {
			dusthtml+= '<div class="btn-group">' +
		      '<button type="button" class="btn btn-secondary dropdown-toggle dustToken"' +   
		      'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + 
		      tokenData[i].symbol +
		      '</button>' +
		      '<div class="dropdown-menu">' + 
		      '<a class="dropdown-item" href="#">Balance: ' + tokenData[i].totalBalance + '</a>' +
		      '<a class="dropdown-item" href="#">Proportion: ' + tokenData[i].tokenProportions[0] + '</a>' +
		      '<a class="dropdown-item" href="#">Portfolio Portion: ' + tokenData[i].portionOfPortfolio + '</a>' +
		      '<a class="dropdown-item" href="#">Location: ' + tokenData[i].tokenLocations[0] + '</a>' +
		      '<a class="dropdown-item" href="#">USD: ' + tokenData[i].USD + '</a>' +
		      '<div class="dropdown-divider"></div>' +
		      '<a class="dropdown-item" href="#">How is ' + tokenData[i].symbol + ' doing today?</a>' +
		      '</div></div>';
		}
		
		
	} // end for loop
	$("#topTokenDropdown").append(tophtml);
	$("#dustTokenDropdown").append(dusthtml);
} // end loadTokenDropdowns

// loads user specified portfolio information

function loadPortfolio() {   
    var dataRequest = new XMLHttpRequest();
    dataRequest.open('GET', 'json-examples/portfolio.json'); // look into this more
    
    dataRequest.onload = function()
    {
        var acceptedData = dataRequest.JSON.parse(dataRequest.responseText);
    }
    dataRequest.send();
    
    
    
    var numOfObjects = acceptedData.length;
    var portfolioInfo = [];
    
    for (i=0; i<numOfObjects; i++)
    {
        backtestArray.push([
            acceptedData[i].strategy,
            acceptedData[i].currencyPair,
            acceptedData[i].initialInvestment,
            acceptedData[i].finalNetWorth,
            acceptedData[i].returnOnInvestment,
            acceptedData[i].numTrades,
            acceptedData[i].winLossRatio,
            acceptedData[i].volitilityForPeriod,
            acceptedData[i].parameters,
            acceptedData[i].parameterValues,
            acceptedData[i].loggedActions,
            acceptedData[i].loggedReturns]);
    } // end for loop
    
    fillTable(acceptedData);
} // end loadPortfolio

// creates pie chart based on amount per token.

function loadPieChart(json) {
	var pieTokenData = [];		// holds token info for pie chart
	for (var i = 0; i < json.data.length; i++) {
		var portion = json.data[i].portionOfPortfolio;
		portion = portion.slice(0, -1);
		portion = parseInt(portion);
		var tokenTotal = json.data[i].totalBalance;
		tokenTotal = parseFloat(tokenTotal);
		var info = {y: portion, name: json.data[i].symbol, tokenAmount : tokenTotal};
		pieTokenData.push(info);
	}
	var options = {
			exportEnabled: true,
			animationEnabled: true,
			title:{
				text: "Tokens"
			},
			legend:{
				horizontalAlign: "right",
				verticalAlign: "center"
			},
			data: [{
				type: "pie",
				showInLegend: true,
				toolTipContent: "<b>{name}</b>: ${y} (#percent%) Balance:{tokenAmount}",
				indexLabel: "{name}",
				legendText: "{name} {tokenAmount}",
				indexLabelPlacement: "inside",
				dataPoints: pieTokenData
			}]
		};
		$("#chartContainer").CanvasJSChart(options);
		$("#chartContainer2").CanvasJSChart(options);
		$("#chartContainer3").CanvasJSChart(options);
}

// fills portfolio table

function loadPortfolioTable(tokenData) {
	var html = '';
	for (i=0; i<tokenData.length; i++)
    {
        html+= '<tr>' +
		  '<th scope="row">'+ tokenData[i].symbol + '</th>' +
		  '<td>' + tokenData[i].totalBalance + '</td>' +
		  '<td>' + tokenData[i].totalBalance + '</td>' +
		  '<td>' + "1jabillion" + '</td>' +
		  '<td>' + "us amount" + '</td>' +
		  '<td>' + "something" + '</td>' +
		'</tr>';
    }
    $("#innerPortfolioTable").append(html);
}