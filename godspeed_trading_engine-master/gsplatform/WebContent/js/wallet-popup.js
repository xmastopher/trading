$( document ).ready(function() {
  clipboard.on('success', function(e) {
    $(e.trigger).text("Copied!");
    e.clearSelection();
    setTimeout(function() {
      $(e.trigger).text("Copy");
    }, 2500);
  });

  clipboard.on('error', function(e) {
    $(e.trigger).text("Can't in Safari");
    setTimeout(function() {
      $(e.trigger).text("Copy");
    }, 2500);
  });
});

// When the user clicks on div, open the popup
function btcPopup() {
    var popup = document.getElementById("btcWallet");
    popup.classList.toggle("show");
    var popupetc = document.getElementById("ethWallet");
    popupetc.classList.remove("show");
    var popupeth = document.getElementById("etcWallet");
    popupeth.classList.remove("show");
    var popupbch = document.getElementById("bchWallet");
    popupbch.classList.remove("show");
    
}
function ethPopup() {
    var popup = document.getElementById("ethWallet");
    popup.classList.toggle("show");
    var popupbtc = document.getElementById("btcWallet");
    popupbtc.classList.remove("show");
    var popupetc = document.getElementById("etcWallet");
    popupetc.classList.remove("show");
    var popupbch = document.getElementById("bchWallet");
    popupbch.classList.remove("show");
}
function etcPopup() {
    var popup = document.getElementById("etcWallet");
    popup.classList.toggle("show");
    var popupbtc = document.getElementById("btcWallet");
    popupbtc.classList.remove("show");
    var popupeth = document.getElementById("ethWallet");
    popupeth.classList.remove("show");
    var popupbch = document.getElementById("bchWallet");
    popupbch.classList.remove("show");
}
function bchPopup() {
    var popup = document.getElementById("bchWallet");
    popup.classList.toggle("show");
    var popupbtc = document.getElementById("btcWallet");
    popupbtc.classList.remove("show");
    var popupeth = document.getElementById("ethWallet");
    popupeth.classList.remove("show");
    var popupetc = document.getElementById("etcWallet");
    popupetc.classList.remove("show");
}
function btcCopy() {
    var btcText = document.getElementById("btc").value;
    btcText.select();
    document.execCommand("Copy");
    alert(btcText);
    
}
function ethCopy() {
    var ethText = document.getElementById("eth").value;
    alert(ethText);
    ethText.select();
    document.execCommand("Copy");
}
function etcCopy() {
    var etcText = document.getElementById("etc").value;
    etcText.select();
    document.execCommand("Copy");
}
function bchCopy() {
    var bchText = document.getElementById("bch").value;
    bchText.select();
    document.execCommand("Copy");
}