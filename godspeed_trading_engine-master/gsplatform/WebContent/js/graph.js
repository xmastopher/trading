let currentCharts = [];

function createGraph(backtest) {
	/*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
	nv.addGraph(function() {
	
		var xAxisLength = 0;
		  
		  for (var j = 0; j < backtest.length; j++) {
				if (backtest[j].loggedReturns.length > xAxisLength) {
					xAxisLength = backtest[j].loggedReturns.length;
				}
			}
		
	  var chart = nv.models.lineWithFocusChart()
	                .margin({left: 60})  //Adjust chart margins to give the x-axis some breathing room.
	                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
	                //.transitionDuration(350)  //how fast do you want the lines to transition?
	                .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
	                .showYAxis(true)        //Show the y-axis
	                .showXAxis(true)        //Show the x-axis
	                
	  chart.brushExtent([0, xAxisLength]);
	  
	  var time = backtest[0].timestamps;
	  chart.xAxis     //Chart x-axis settings
	      .axisLabel('Timestamp')
	      .tickFormat(d3.format(',f'));
	      /*.tickFormat(function(index) {
	    	  	var value = time[index].substring(0,10);
	    	  	value += " ";
	    	  	value += time[index].substring(11,16);
	    	  	value += '\n';
	    	    return value;
	      });*/  
	  
	  chart.yAxis     //Chart y-axis settings
	      .axisLabel('Networth')
	  	  .tickFormat(d3.format(',.8f'));

	  /* Done setting the chart up? Time to render it!*/
	  var myData = otherDataFiler(backtest);   //You need data...

	  clearTooltip();
	  currentCharts.push(chart);
	  d3.select('#chart svg')    //Select the <svg> element you want to render the chart in.   
	      .datum(myData)         //Populate the <svg> element with chart data...
	      .call(chart);          //Finally, render the chart!

		  	  
	  //Update the chart when window resizes.
	  nv.utils.windowResize(chart.update);
	  return chart;
	});
	
	function otherDataFiler(backtest) {
		
		var tempObjectArray = [];
		var tempDataArray = [];
		var i = 0; 
		var b = backtest[0].timestamps;
		var longestLength = 0;
	
		
		for (var j = 0; j < backtest.length; j++) {
			if (backtest[j].loggedReturns.length > longestLength) {
				longestLength = backtest[j].loggedReturns.length;
			}
		}
				
		d3.range(backtest.length).map(function () {
			
			var loggedReturns = [longestLength-1];
			var loggedActions = [longestLength-1];
			var priceDataCloses = [longestLength-1];
			var indicators = [longestLength-1];
			var timestamps = [longestLength-1];
			
			var l = 0;
			for ( k=0; k<longestLength; k++ ) {
				if ( k < ( longestLength - backtest[i].loggedReturns.length ) ) {
					loggedReturns[k] 	= backtest[i].loggedReturns[0];
					loggedActions[k] 	= backtest[i].loggedActions[0];
					priceDataCloses[k] 	= backtest[i].priceDataCloses[0];
					indicators[k] 		= backtest[i].indicators[0];
					timestamps[k]		= backtest[i].timestamps[0];
				}
				else {
					loggedReturns[k] 	= backtest[i].loggedReturns[l];
					loggedActions[k] 	= backtest[i].loggedActions[l];
					priceDataCloses[k] 	= backtest[i].priceDataCloses[l];
					indicators[k] 		= backtest[i].indicators[l];
					timestamps[k]		= backtest[i].timestamps[l];
					
					l++;
				}				
			}
					
			var j = 0;
			d3.range(longestLength).map(function () {
				tempDataArray.push({
					x: j, //backtest[i].timestamps[j],
					y: Math.max(0, loggedReturns[j]),
					z: loggedActions[j],
					percent: backtest[i].percentGains,
					close: priceDataCloses[j],
					indicator: indicators[j],
					timestamp: timestamps[j],
					graphType: "backtestGraph"

				});			
				j++;
			});
			
			tempObjectArray.push({
				values: tempDataArray,
				key: backtest[i].pair
			});
			tempDataArray = [];
			i++;
		});
		
		return tempObjectArray;
	}
}


function createGraphBotManager(backtest) {
	/*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
	nv.addGraph(function() {
	
	  var xAxisLength = backtest.priceData.length;  
		
	  var chart = nv.models.lineWithFocusChart()
	                .margin({left: 40})  //Adjust chart margins to give the x-axis some breathing room.
	                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
	                //.transitionDuration(350)  //how fast do you want the lines to transition?
	                .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
	                .showYAxis(true)        //Show the y-axis
	                .showXAxis(true)        //Show the x-axis
	                
	  chart.brushExtent([0, xAxisLength]);
	  
	  chart.xAxis     //Chart x-axis settings
	      .tickFormat(d3.format(',f'));
	  chart.yAxis     //Chart y-axis settings
	      .axisLabel('Close Price')
	      .tickFormat(d3.format(',.8f'));

	  /* Done setting the chart up? Time to render it!*/
	  var myData = otherDataFiler(backtest);   //You need data...

	  clearTooltip();
	  currentCharts.push(chart);
	  d3.select('#botchart svg')    //Select the <svg> element you want to render the chart in.   
	      .datum(myData)         	//Populate the <svg> element with chart data...
	      .call(chart);          	//Finally, render the chart!
		  	  
	  //Update the chart when window resizes.
	  nv.utils.windowResize(chart.update);
	  return chart;
	});
	
	function otherDataFiler(backtest) {
		
		var tempObjectArray = [];
		var tempDataArray = [];
		
		var j = 0;
		d3.range(backtest.priceData.length).map(function () {
			tempDataArray.push({
				x: j, //backtest[i].timestamps[j],
				y: Math.max(0, backtest.priceData[j].close),
				volume: backtest.priceData[j].volume,
				open: backtest.priceData[j].open,
				high: backtest.priceData[j].high,
				low: backtest.priceData[j].low,
				timestamp: backtest.priceData[j].timestamp,
				graphType: "botManagerGraph"
				

			});			
			j++;
		});
		
		tempObjectArray.push({
			values: tempDataArray,
			key: "Bot"
		});
		tempDataArray = [];

		return tempObjectArray;
	}
}

function createGraphDashboard(backtest, selectedChart, selectedMarket,lineColor) {
	clearTooltips();
	/*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
	nv.addGraph(function() { 
	
	  var chart = nv.models.lineWithFocusChart()
	                .margin({left: 0,right: 0})  //Adjust chart margins to give the x-axis some breathing room.
	                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
	                //.transitionDuration(350)  //how fast do you want the lines to transition?
	                .showLegend(false)       //Show the legend, allowing users to turn on/off line series.
	                .showYAxis(false)        //Show the y-axis
	                .showXAxis(false)        //Show the x-axis
	  
	  chart.xAxis     //Chart x-axis settings
	  	.tickFormat(function(d) { return d3.time.format('%y/%m/%d')(new Date(d)); });
	  chart.x2Axis     //Chart x-axis settings
	  	.tickFormat(function(d) { return d3.time.format('%y/%m/%d')(new Date(d)); });
	  
	  /* Done setting the chart up? Time to render it!*/
	  var myData = otherDataFiler(backtest);   //You need data...
	  currentCharts.push(chart);
	  d3.select(selectedChart)    //Select the <svg> element you want to render the chart in.   
	      .datum(myData)          //Populate the <svg> element with chart data...
	      .call(chart);           //Finally, render the chart!
	  
	  //Update the chart when window resizes.
	  nv.utils.windowResize(chart.update);
	  return chart;
	});
	
	function otherDataFiler(backtest) {
		
		var tempObjectArray = [];
		var tempDataArray = [];
		
		var j = 0;
		
		d3.range(backtest.priceData.length).map(function () {
			tempDataArray.push({
				x: new Date(backtest.priceData[j].timestamp),
				y: Math.max(0, backtest.priceData[j].close),
				volume: backtest.priceData[j].volume,
				open: backtest.priceData[j].open,
				high: backtest.priceData[j].high,
				low: backtest.priceData[j].low,
				timestamp: backtest.priceData[j].timestamp,
				graphType: "dashboardGraph"
				

			});			
			j++;
		});
		
		tempObjectArray.push({
			values: tempDataArray,
			key: selectedMarket,
			color: lineColor,
			area: true
		});
		tempDataArray = [];

		return tempObjectArray;
	}
}

function createGraphBotLeaderboards(user, rank, backtest) {
	
	const lineColor = '#1579f6';
	
	/*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
	nv.addGraph(function() {
	
	  var xAxisLength = backtest.length;  
		
	  var chart = nv.models.lineChart()
	                .margin({left: 60})  			//Adjust chart margins to give the x-axis some breathing room.
	                .useInteractiveGuideline(true)   //We want nice looking tooltips and a guideline!
	                //.transitionDuration(350)  		//how fast do you want the lines to transition?
	                .showLegend(true)       			//Show the legend, allowing users to turn on/off line series.
	                .showYAxis(true)        			//Show the y-axis
	                .showXAxis(true)        			//Show the x-axis
	                
	  chart.brushExtent([0, xAxisLength]);
	  
	  //Chart x-axis settings - using timestamp dates
	  chart.xAxis     
	  	  .axisLabel('ACTIONS')
	      .tickFormat(d3.format(',f'));
	  
	//Chart y-axis settings - using ROI for day
	  chart.yAxis     
	      .axisLabel('NETWORTH')
	      .tickFormat(d3.format(',.4f'));

	  /* Done setting the chart up? Time to render it!*/
	  var myData = otherDataFiler(backtest);   //You need data...

	  currentCharts.push(chart);
	  d3.select('#botGraph svg')    //Select the <svg> element you want to render the chart in.   
	      .datum(myData)         	//Populate the <svg> element with chart data...
	      .call(chart);          	//Finally, render the chart!
		  	  
	  //Update the chart when window resizes.
	  nv.utils.windowResize(chart.update);
	  return chart;
	});
	
	function otherDataFiler(backtest) {
		
		var tempObjectArray = [];
		var tempDataArray = [];
		
		var j = 0;
		/**
		 * 		logObject.put( "closePrice", botLog.getLastPrice( ) );
				logObject.put( "networth", botLog.getNetworth( ) );
				logObject.put( "assets", botLog.getAsssetsOwned( ) );
				logObject.put("quotes", botLog.getQuoteOwned( ) );
		 */
		d3.range(backtest.length).map(function () {
			tempDataArray.push({
				x: j, //backtest[j].dated,
				y: Number( backtest[j].networth ),
				graphType: "dashboardGraph"
			});			
			j++;
		});
		
		var generatedKey = user.toUpperCase() + "/" + rank;
		
		tempObjectArray.push({
			values: tempDataArray,
			key: generatedKey,
			color: lineColor,
			area: true
		});
		
		tempDataArray = [];

		return tempObjectArray;
	}
}

function createGraphCrossExchangeAnalysis(backtests, markets) {
	/*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
	nv.addGraph(function() {
	
	  
	  var xAxisLength = Math.max( backtests[0], backtests[1] );  
		
	  var chart = nv.models.lineChart()
	                .margin({left: 60})  //Adjust chart margins to give the x-axis some breathing room.
	                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
	                //.transitionDuration(350)  //how fast do you want the lines to transition?
	                .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
	                .showYAxis(true)        //Show the y-axis
	                .showXAxis(true)        //Show the x-axis
	                
	  chart.brushExtent([0, xAxisLength]);
	  
	  chart.xAxis     //Chart x-axis settings
	      .tickFormat(d3.format(',f'));
	  chart.yAxis     //Chart y-axis settings
	      .axisLabel('Close Price')
	      .tickFormat(d3.format(',.8f'));

	  /* Done setting the chart up? Time to render it!*/
	  var myData = otherDataFiler(backtests);   //You need data...

	  currentCharts.push(chart);
	  d3.select('#crossExchangeChart svg')   //Select the <svg> element you want to render the chart in.   
	      .datum(myData)         			//Populate the <svg> element with chart data...
	      .call(chart);          			//Finally, render the chart!
		  	  
	  //Update the chart when window resizes.
	  nv.utils.windowResize(chart.update);
	  return chart;
	});
	
	function otherDataFiler(backtests) {
		
		var tempObjectArray = [];
		var tempDataArray   = [];
		var lengthOne       = backtests[0].priceData.length;
		var lengthTwo       = backtests[1].priceData.length;
		var maxLength		= Math.max( lengthOne, lengthTwo );
		var minLength		= Math.min( lengthOne, lengthTwo );
		var X			    = lengthOne > lengthTwo ? 0 : 1;
		var Y				= X == 0 ? 1 : 0;
		
		var j = 0;
		var shorterStartingPoint = 0;
		d3.range(maxLength).map(function () {
			tempDataArray.push({
				x: j, //backtest[i].timestamps[j],
				y: Math.max(0, backtests[X].priceData[j].close),
				volume: backtests[X].priceData[j].volume,
				open: backtests[X].priceData[j].open,
				high: backtests[X].priceData[j].high,
				low: backtests[X].priceData[j].low,
				timestamp: backtests[X].priceData[j].timestamp,
				graphType: "dashboardGraph"
			});
			if( new Date( backtests[X].priceData[j].timestamp ) === new Date( backtests[Y].priceData[0].timestamp ) ){
				shorterStartingPoint = j;
			}
			else if( j != 0 && new Date( backtests[X].priceData[j-1].timestamp ) < new Date( backtests[Y].priceData[0].timestamp ) && 
					 new Date( backtests[X].priceData[j].timestamp ) > new Date( backtests[Y].priceData[0].timestamp ) ){
				shorterStartingPoint = j;
			}
			j++;
		});
			
		tempObjectArray.push({
			values: tempDataArray,
			key: markets[X]
		});
			
		tempDataArray = [];	
		j = 0;
		if( minLength != maxLength && shorterStartingPoint == 0 ){
			shorterStartingPoint = maxLength - minLength;
		}
		d3.range(minLength).map(function () {
			tempDataArray.push({
				x: j + shorterStartingPoint, //backtest[i].timestamps[j],
				y: Math.max(0, backtests[Y].priceData[j].close),
				volume: backtests[Y].priceData[j].volume,
				open: backtests[Y].priceData[j].open,
				high: backtests[Y].priceData[j].high,
				low: backtests[Y].priceData[j].low,
				timestamp: backtests[Y].priceData[j].timestamp,
				graphType: "dashboardGraph"
			});	
			j++;
		});		
		tempObjectArray.push({
			values: tempDataArray,
			key: markets[Y]
		});
	
		return tempObjectArray;
		
	}
}

function clearTooltips(){
	var rem = document.querySelectorAll("[id*='nvtooltip']"), i = 0;
	for (; i < rem.length; i++)
	    rem[i].parentNode.removeChild(rem[i]);
}

