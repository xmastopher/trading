
/**
 * Download a CSV - internal for export
 * @param csv
 * @param filename
 * @returns
 */
function downloadCSV( csv,filename ){
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}

/**
 * Export current backtest to table
 * @returns
 */
function exportTable( filename, tableClass, ignore ) {
    var csv = [];
    var rows = document.querySelectorAll( "table tr." + tableClass );
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) {
        		if( cols[j].innerText.includes( ignore ) )
        			continue
            row.push(cols[j].innerText);
        }
        
        csv.push(row.join(","));        
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}

function exportBacktestCsv( filename, backtestResults ){
	filename += '.csv';
	var csv = [];
	const market    = backtestResults.pair;
	const base      = backtestResults.base;
	const quote     = backtestResults.quote;
	const initial   = backtestResults.initialInvestment;
	const final     = backtestResults.finalNetworth;
	const strategy  = backtestResults.strategy;
	const roi       = backtestResults.returnOnInvestment;
	const numTrades = backtestResults.numTrades;
	const winRate   = backtestResults.winRatio;
	const lossRate  = backtestResults.lossRatio;
	const expect    = backtestResults.expectancy;
	const vol		= backtestResults.volatilityForPeriod;
	
    csv.push( ["RESULTS"] );
    csv.push( ["market",market].join(",") );
    csv.push( ["base",base].join(",") );
    csv.push( ["quote",quote].join(",") );
    csv.push( ["initial",initial].join(",") );
    csv.push( ["final",final].join(",") );
    csv.push( ["strategy",strategy].join(",") );
    csv.push( ["roi",roi].join(",") );
    csv.push( ["num trades",numTrades].join(",") );
    csv.push( ["win rate",winRate].join(",") );
    csv.push( ["loss rate",lossRate].join(",") );
    csv.push( ["expectancy",expect].join(",") );
    csv.push( ["volatility",vol].join(",") );
    
    let headers = ["OPEN","CLOSE"];
    let buyIndicatorKeys  = [];
    let sellIndicatorKeys = [];
    for( var key in backtestResults.indicators[ 0 ].buyIndicators ){
    		if( key === "class" ){
    			continue;
    		}
    		headers.push( "BUY-SIGNAL-" + key );
    		buyIndicatorKeys.push( key );
    }
    for( var key in backtestResults.indicators[ 0 ].sellIndicators ){
		if( key === "class" ){
			continue;
		}
		headers.push( "SELL-SIGNAL-" + key );
		sellIndicatorKeys.push( key );
    }
    headers.push("ACTION");headers.push("NETWORTH");headers.push("% GAIN/LOSS");headers.push("STOP LOSS HIT");
    csv.push( headers.join(",") );
    
    let x = 0;
    for ( var i = 0; i < backtestResults.loggedReturns.length; i++ ) {
        var row = [ backtestResults.priceDataOpens[i], backtestResults.priceDataCloses[i] ];
        
        for( var k = 0; k < buyIndicatorKeys.length; k++ ){
        		let key = buyIndicatorKeys[ k ];
        		row.push( backtestResults.indicators[i].buyIndicators[key] );
        }
        for( var k = 0; k < sellIndicatorKeys.length; k++ ){
    			let key = sellIndicatorKeys[ k ];
    			row.push( backtestResults.indicators[i].sellIndicators[key] );
        }
        
        row.push( backtestResults.loggedActions[i] );
        row.push( backtestResults.loggedReturns[i] );
        if( backtestResults.loggedActions[i] === "SELL" ){
	        	row.push( backtestResults.percentGains[x] );
	        	row.push( backtestResults.stopLosses[x] );
        		x++;
        }
        else{
        		row.push( "0" );
        		row.push( "false" );
        }
        
        
        csv.push(row.join(","));        
    }
    
    downloadCSV(csv.join("\n"), filename);
}

function exportBotTradesCsv( filename, botLogs ){
	filename += '.csv';
	var csv = [];
    let headers = ["TIMESTAMP", "CLOSE PRICE", "NETWORTH", "ASSET HOLDINGS", "QUOTE HOLDINGS", "ACTION"];
    let buyIndicatorKeys  = [];
    let sellIndicatorKeys = [];
    
    for( var key in botLogs[ 0 ].indicators.buyIndicators ){
    		if( key === "class" ){
    			continue;
    		}
    		headers.push( "BUY-SIGNAL-" + key );
    		buyIndicatorKeys.push( key );
    }
    for( var key in botLogs[ 0 ].indicators.sellIndicators ){
		if( key === "class" ){
			continue;
		}
		headers.push( "SELL-SIGNAL-" + key );
		sellIndicatorKeys.push( key );
    }
    csv.push(headers.join(","));
    let x = 0;
    for ( var i = 0; i < botLogs.length; i++ ) {
        var row = [ botLogs[ i ].timestamp, 
        	 		    botLogs[ i ].closePrice,
				    botLogs[ i ].networth,
		 		    botLogs[ i ].assets, 
		 		    botLogs[ i ].quotes, 
		 		    botLogs[ i ].action ];
        for( var k = 0; k < buyIndicatorKeys.length; k++ ){
        		let key = buyIndicatorKeys[ k ];
        		row.push( botLogs[ i ].indicators.buyIndicators[key] );
        }
        for( var k = 0; k < sellIndicatorKeys.length; k++ ){
    			let key = sellIndicatorKeys[ k ];
    			row.push( botLogs[ i ].indicators.sellIndicators[key] );
        }
  
        csv.push(row.join(","));        
    }
    
    downloadCSV(csv.join("\n"), filename);
}