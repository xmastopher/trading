/**
 * Generate the templated page
 */
generateTemplate( ); 

/**
 * Call at very beginning to set username and avi
 */
if( document.getElementById("username") !== undefined ){
	document.getElementById("username").innerHTML = globalSessionInit.userProfileResponse.username.toUpperCase();
	document.getElementById("avi").src = "userContent/" + globalSessionInit.userProfileResponse.username + "/avi.png";
}
	
/**
 * Page cache which holds global information about
 * the different user services and cached data from API
 */
let cache = {
	strategyMap : new Object(),
	botMap: new Object(),
	selectedStrategy: 0,
	ids: [],
	intervalId: null,
	botToDelete: -1,
	evenColor: '#f7f4ff',
	evenOddColor: '#edeaf3',
	oddColor: '#e7e7f3',
	oddEvenColor: '#dbd9ef',
	cachedLogObject: null
}

/**
 * Wrapper object to reference service-level endpoints
 */
let endpoints = {
		botSummaryEndpoint: Constants.getInstance( ).botSummaryURL,
		getStrategiesEndpoint: Constants.getInstance( ).getStrategiesURL,
		marketLookupEndpoint: Constants.getInstance( ).marketLookupURL,
		startStopBotEndpoint: Constants.getInstance( ).startStopBotURL,
		botLogsEndpoint: Constants.getInstance( ).botLogsURL,
		exchangeCandlesEndpoint: Constants.getInstance( ).exchangeCandlesURL,
		deleteBotEndpoint: Constants.getInstance( ).deleteBotURL,
		saveBotEndpoint: Constants.getInstance( ).saveBotURL
}

/**
 * Function linked when page is loaded
 * @returns
 */
function onLoad(){
	loadPage( );
}

/**
 * Wrapper for initial calls to load page
 * @returns
 */
function loadPage( ){
	
	if( !botSummary() ) return false;
	
	if( !getStrategies() ) return false;
	
	return true;
}

/**
 * Wrapper for bot summary API
 * @returns false on fail, true otherwise
 */
function botSummary(){
	
	// Perform bot summary API call
    let botSummaryEndpoint = new AuthenticatedEndpoint( );
    let response = botSummaryEndpoint.doRequest( {}, endpoints.botSummaryEndpoint );
    
    //Check success flag and perform page generation
    if( response.success ){
    		loadBots( response );
    }
    
    //Handle all errors here elegantly
    else{
    		alert( response.message );
    		return false;
    }
    
    return true;
}

/**
 * Wrapper for get strategies API
 * @returns false on fail, true otherwise
 */
function getStrategies(){
	
	// Perfrom Get Strategies API Call
    let getStrategiesEndpoint = new AuthenticatedEndpoint( );
    let response = getStrategiesEndpoint.doRequest( {}, endpoints.getStrategiesEndpoint );
    
    //Update and map all strategies
    if( response.success ){
		//Map strategies on ID
		for( let i = 0; i < response.strategies.length; i++) {
			let strategy = response.strategies[ i ];
			cache.strategyMap[ strategy.id ] = strategy;  
			document.getElementById( "strategyList" ).innerHTML = generateStrategyList( response.strategies );
		}
    }
    
    //Handle all errors here elegantly
    else{
    		alert( response.message );
    		return false;
    }
    
    return true;
}

/**
 * Called after grabbing User Bot data to load page view with all 
 * user bot information
 * @param obj
 * @returns
 */
function loadBots( obj ) {
	
	//check for null
	if( obj == null )
		return false;
	
	//Empty the container
	$("#botContainer").empty();
	
	
	let botList          = obj.botSummaries;
	let strategyListId   = []
	let strategyObjects  = []
	
	for( let i = 0; i < botList.length; i++ )
	{
		cache.botMap[ botList[i].id ] = botList[ i ];
		
		let lastBot = i === botList.length-1;
		let html    = '';
		let stratId = botList[i].strategyId;
		
		if( !strategyListId.includes( stratId ) )
		{
			strategyListId.push( stratId );
			strategyObjects.push( botList[ i ].strategy );
		}
		
		if( i % 2 == 0 )
		{
			html = '<div class="row" style="background-color:' + cache.evenColor + ';">\n' +
			       generateNameColumn( botList[ i ], lastBot ) + 
				   generateSummaryTable( botList[ i ], cache.evenColor, cache.evenOddColor ) +
				   '</div>';
		}
		else
		{
			html = '<div class="row" style="background-color:' + cache.oddColor + ';">\n' +
			        generateNameColumn( botList[ i ], lastBot ) + 
			        generateSummaryTable( botList[ i ], cache.oddColor, cache.oddEvenColor ) +
			        '</div>';
		}
		
	    $("#botContainer").append( html );
	}
}

/**
 * Generates the name column in the row container:
 * NAME
 * isRunning
 * start/stop-button delete-button
 * @returns row html markup
 */
function generateNameColumn( botObject, lastBot ){

	let id      = botObject.id;
	let name    = botObject.name;
	let running = botObject.running;
	
	let runningStyle  = "success";
	let buttonStyle   = "danger";
	let runningString = "RUNNING";
	let buttonText    = "STOP";
	let buttonId      = "stopBot";
	let onclick       = "";
	cache.ids.push( id );
	
	if( !running )
	{
		runningStyle  = "danger";
		buttonStyle   = "success";
		runningString = "STOPPED";
		buttonText    = "START";
		buttonId      = "startBot";
	}
	
	let html = ''
		

	if( lastBot === false ) {
		let onclick = generateOnClickStartStop( id, buttonText );
		html = '<div class="col-md-4 text-left">\n' +
				'<br>' + getActionButton( id ) +
				   '<br><br><h1 class="text-center">\n' +
				   name  + '</h1>\n' +
				   '<br><br><h1 class="text-center text-' + runningStyle + '">\n' +
				   runningString + '</h><br><br><br>\n' +
				   '<div class="div class=" btn-toolbar="" text-center="">' +
				   '<button onclick="'+onclick+'" class="btn btn-' + buttonStyle + '" value=' + id + '>' + buttonText + '</button>\n' +
				   '<input onclick="deleteBot('+id+')" data-toggle="modal" data-target="#deleteBotModal" class="btn btn-danger" value="DELETE" type="button">\n' +
				   '</div></div>\n';
	}
	else {
		let onclick = generateOnClickStartStop( id, buttonText );
		html = '<div class="col-md-4 text-left">\n' +
		   '<br>'  + getActionButton( id ) +
		   '<br><br><h1 class="text-center" id="maxBot">\n' +
		   name  + '</h1>\n' +
		   '<br><br><h1 class="text-center text-' + runningStyle + '">\n' +
		   runningString + '</h><br><br><br>\n' +
		   '<div class="div class=" btn-toolbar="" text-center="">' +
		   '<button onclick="'+onclick+'" class="btn btn-' + buttonStyle + '" value=' + id + '>' + buttonText + '</button>\n' +
		   '<input onclick="deleteBot('+id+')" data-toggle="modal" data-target="#deleteBotModal" class="btn btn-danger" value="DELETE" type="button">\n' +
		   '</div></div>\n';	
		
	}
			
	return html;
}

/**
 * Generates the second column for the main scroll view, which is one table containing the bot summary
 * @param botObject
 * @param evenColor
 * @param oddColor
 * @returns
 */
function generateSummaryTable( botObject, evenColor, oddColor ){

	let exchange     = botObject.exchange;
	let market       = botObject.market;
	let strategy     = botObject.strategy;
	let averageRoi   = botObject.averageROI != 0 ? botObject.averageROI.slice( 0, botObject.averageROI.length-1 ) : botObject.averageROI;
	let totalWins    = botObject.totalWins;
	let totalLosses  = botObject.totalLosses;
	let winLoss      = botObject.winLoss;
	let tradingLimit = botObject.tradingLimit;
	let interval     = botObject.interval;
	
	let table = 	'<div class="col-md-8">\n' +
				'<table class="table" align="left">\n' +
				'<tbody align="left">\n' +
				createTableRow( evenColor, "Exchange", exchange )        +
				createTableRow( oddColor, "Market", market )              +
				createTableRow( evenColor, "Strategy", createStrategyTooltip( strategy ) )    +
				createTableRow( oddColor, "Average ROI", createRowWithColor( Number( averageRoi ), 0, '%' ) )     +
				createTableRow( evenColor, "Total Wins", createRowWithColor( totalWins, totalLosses, '')  )     +
				createTableRow( oddColor, "Total Losses", totalLosses )   +
				createTableRow( evenColor, "Win/Loss", createRowWithColor( winLoss, 1, '' ) ) 		   +
				createTableRow( oddColor, "Trading Limit", tradingLimit ) +
				createTableRow( evenColor, "Interval", interval ) +
				'</tbody>\n</table>\n</div>';
	return table;
	
}

/**
 * Helper method which generates a single column in the summary
 * @param color
 * @param columnOneText
 * @param columnTwoText
 * @returns
 */
function createTableRow( color, columnOneText, columnTwoText){
	
	let tableRow = '</tr><tr class="table" style="background-color:'  + color + ';" align="left">\n' +
				   '<td style="width: 100px;text-align: left;" align="left"><b>'  + columnOneText + '</b></td>\n'              +
				   '<td style="width: 100px;text-align: left;" align="left">'  + columnTwoText + '</td>\n';
	
	return tableRow;
} 

/**
 * Wrapper to create a row for the bot description from
 */
function createRowWithColor( value, threshold, char ){
	return '<font color="' + getColorFromValue( value, threshold, 'green', 'red', 'black' ) + '">' + value + char + '</font>';
} 

/**
 * Generates the internal tooltip html markup for the strategy column's 4th row
 * @param strategyObject
 * @returns
 */
function createStrategyTooltip( strategy ){
	
	//Grab internal strategy breakdown
	let buySignals   = strategy.buySignals;
	let sellSignals  = strategy.sellSignals;
	let paddedName   = strategy.name;
	let length       = paddedName.length;
	
	html = '<a href="#" class="tooltip-travas"><i>' + paddedName + '</i>' +
		   '<span> <img class="callout" src="img/callout_black.gif"><h5><strong>SIGNALS </strong></h5>' +
        	   generateSignalsHtml( buySignals )  + 
        	   generateSignalsHtml( sellSignals ) + '</span></a>';
	
	return html;
}

/**
 * Called after grabbing user strategies to populate the
 * strategies drop down
 * @param strategies	A list of json strategy objects
 * @returns generated HTML
 */
function generateStrategyList( strategies ){
	
	html = ""
	
	for( let i = 0; i < strategies.length; i++ )
	{
		html +=  '<a class="dropdown-item" href="#" id="strategy-' + strategies[i].id + '">' + strategies[ i ].name + '</a>\n';
	}
		
	return html;
}

/**
 * Generates dropdown button with multiple actions
 * on the Bot Manager
 * @param params
 * @returns
 */
function getActionButton( id ){
	return  '<div class="dropdown float-left text-center">'+
			'<button class="dropbtn">'+
			'<b>+</b>'+
			'</button>'+
			'<div id="dropdown1" class="dropdown-content">'+
			'<a onclick="requestBotLogs('+id+');" href="#" data-toggle="modal" data-target="#botModalLive">VIEW TRADES</a>'+
			'<a onclick="exportTrades('+ id + ')" href="#">EXPORT TRADES</a>'+
			'</div>'+
			'<a href="#">'+
			'</a>'+
			'</div>';
}

/**
 * 
 * @param params
 * @returns
 */
function exportTrades( id ){
	
	let botLogsEndpoint = new AuthenticatedEndpoint( );
	const response      = botLogsEndpoint.doRequest( { "botOwner": globalSessionInit.userProfileResponse.username, "botId": id }, endpoints.botLogsEndpoint );
	
	if( response.success && response.botLogs.length != 0 ){
		const filename = new Date( ) + "-bottrades";
		exportBotTradesCsv( filename, response.botLogs );
	}
	else{
		alert("Bot had no trades to export");
	}
}

/**
 * Sets the refresh rate for the bot logs viewed in the live bot view
 * @param botId		ID of the bot being viewed
 * @param interval	How long to wait before refreshing the modal
 * @returns	
 */
function setRefresh( botId, interval )
{
	let milli = decideIntervalInMilli( interval );
    cache.intervalId = setInterval( function( ) { requestBotLogs( botId ) }, milli );
}

/**
 * Generates button for start stop bot
 * @param id
 * @param action
 * @param username
 * @returns
 */
function generateOnClickStartStop( id, action )
{
	return "startStopBot("+id+",'"+action+"'" + ")";
}

/**
 * Generates the internal text inside the tooltip
 * @param signalList
 * @returns
 */
function generateSignalsHtml( signalList ){
	
	
	let html = '';
	
	for ( let i = 0; i < signalList.length; i++ ) 
	{
		let signal  = signalList[ i ];
		let title   = '';
		let params  = '';
		
		for( let key in signal )
		{
		    if ( signal.hasOwnProperty( key ) ) 
		    {
		    	
		    		//Grab the name
		    		if( key === 'signal' )
		    		{
		    			title += '<strong>' + signal[key] + '</strong><br>';
		    		}
		    		
		    		else
		    		{
		    			params += key + ' = ' + signal[key] + '<br>';
		    		}
		    }
		}
		
		html += title + params;
	}	
	
	return html;
}

/**
 * Generates html for list of markets called from Markets API
 * @param markets
 * @returns
 */
function generateMarketList( markets ){
	
	html = ""
	
	for( let i = 0; i < markets.length; i++ )
	{
		html +=  '<a class="dropdown-item" href="#" id="market-' + i + '">' + markets[ i ].base + "-" + markets[ i ].quote + '</a>\n';
	}
		
	return html;
}

/**
 * Called when user clicks start or stop for a bot
 * @param botIdField
 * @param commandField
 * @param usernameField
 * @returns
 */
function startStopBot( botIdField, commandField ){

	//Setup the request
    let request = {
		botId: botIdField,
    		command: commandField
    };
    
    let startStopBotEndpoint = new AuthenticatedEndpoint( );
    let response				= startStopBotEndpoint.doRequest( request, endpoints.startStopBotEndpoint );
    
    //Reset the page
    if( response.success ){
		document.getElementById("selectedMarket").innerHTML = "";
		document.getElementById("selectedExchange").innerHTML = "";
		document.getElementById("selectedStrategy").innerHTML = "";
		loadPage( );
    }
    else{
    		alert( response.message );
    		return false;
    }

}

/**
 * Calls Bot Logs API to grab all trades for a specfic bot
 * @param id
 * @returns
 */
function requestBotLogs( id ){
	
	//Setup the request
    let request = {
    		botId: id
    };
    
    //Grab the bot log endpoint
    let botLogsEndpoint = new AuthenticatedEndpoint( );
    let response        = botLogsEndpoint.doRequest( request, endpoints.botLogsEndpoint );
    
    //Reset filter flags, generate scroll view and modal title
    if( response.success ){
	    flags = [ true, true, true ]
		flags[ 0 ] = document.getElementById( "buyCheckBox" ).checked;
		flags[ 1 ] = document.getElementById( "sellCheckBox" ).checked;
		flags[ 2 ] = document.getElementById( "holdCheckBox" ).checked;
		
		document.getElementById( "marketTitle" ).innerHTML = createModalTitleFromUserBot( response.userBot );
		document.getElementById( "logTable" ).innerHTML    = generateLogScroller( response, flags );
		
		const lastTrade = getLastTrade( response.botLogs );
		if( lastTrade != undefined ){
			let color = getColorFromValue( lastTrade, 0, "green", "red", "black" );
			$('#liveBotSubtitle').html( 'LAST TRADE: <font color=' + color + '>' + ( ( lastTrade * 100 ).toFixed( 2 ) ) + "% </font>" );
		}
    }
    else{
    		alert( response.message );
    		return false;
    }
    
}

/**
 * Returns roi on last trade
 * @param botLogs
 * @returns
 */
function getLastTrade( botLogs ){
	let buy  = 0;
	let sell = 0;
	for( let i = botLogs.length-1; i > 0; i-- ){
		if( botLogs[ i ].action === "BUY"){
			buy = botLogs[ i ].closePrice;
		}
		else if( botLogs[ i ].action === "SELL" ){
			sell = botLogs[ i ].closePrice;
		}
		if( buy != 0 && sell != 0 ){
			break;
		}
	}
	if( buy === 0 || sell === 0 ){
		return undefined;
	}
	return ( ( sell - buy ) / buy );
}

/**
 * Generates a modal title given the selected bot
 * @param userBot
 * @returns
 */
function createModalTitleFromUserBot( userBot ){
	return userBot.botName + " " + userBot.market + " (" + userBot.exchange + " - " + userBot.interval + ")";
}

/**
 * Helper function to choose delay interval for bot logs API
 * @param interval
 * @returns
 */
function decideIntervalInMilli( interval )
{
	switch( interval )
	{
	
	case "1m":
		return 60000;
	
	case "5m":
		return 300000;
		
	case "15m":
		return 9000000;
			
	case "30m":
		return 1800000;				
			
	case "1h":
		return 3600000;		
	
	case "2h":
		return 7200000;		
		
	case "4h":
		return 14400000;		
		
	case "6h":
		return 21600000;		
		
	case "12h":
		return 43200000;	
		
	case "1d":
		return 86400000;	
	}
}

/**
 * Generates scroll view with all the bot logs
 * @param logObject
 * @param flags
 * @returns
 */
function generateLogScroller( logObject, flags )
{
	
	cache.cachedLogObject = logObject;
	
	//Colors
	let buyColor  = "rgb(40,167,69,.04)";
	let sellColor = "rgb(220,53,69,.04)";
	let hodlColor = "rgb(167,166,166,.04)";
	
	html =  '<tr align="left">\n'+
		    '<th>Timestamp</th>\n'+
		    '<th>Action</th>\n'+
		    '<th>Message</th>\n'+
		    '</tr>\n';
	
	let botLogs = logObject.botLogs;
	
	for( let i = 0; i < botLogs.length; i++ )
	{
		let log   = botLogs[ i ];
		let color = "";
		let classText = "";
		
		//Set the color accordingly
		if( log.action === "BUY" )
		{
			if( !flags[ 0 ] )
			{
				continue;
			}
			
			color = buyColor;
			classText = 'class="text-success"';
		}
		else if( log.action === "SELL" )
		{
			if( !flags[ 1 ] )
			{
				continue;
			}
			
			color = sellColor;
			classText = 'class="text-danger"';
		}
		else if( log.action === "HOLD" )
		{
			if( !flags[ 2 ] )
			{
				continue;
			}
			
			color = hodlColor;
			classText = "";
		}
		
		html += createLogRow( log, color, classText );
		
	}
	
	return html;
	
}

/**
 * Creates a row in the log scroll view
 * @param log
 * @param color
 * @param classText
 * @returns
 */
function createLogRow( log, color, classText )
{
	html = '<tr align="left" style="background-color:' + color + '">' +
    		   '<td><b>' + log.timestamp + '</b></td>\n' +
    		   '<td '+ classText + '><b>' + log.action + '</b></td>\n' +
    		   '<td>' + log.message + '</td></tr>\n';
	
	return html;
}

/**
 * Function called when deleting a bot - just sets the id of the selected bot
 * @param id		ID of the bot to delete
 * @returns
 */
function deleteBot( id ){
	cache.botToDelete = id;
	document.getElementById( "botToDelete" ).innerHTML = "Are you sure you would like to delete bot " + 
														 cache.botMap[id].name +
														 "? If you meant to perform this action, please click confirm below.";
}
