drop DATABASE godspeedTest;
CREATE DATABASE godspeedTest;
use godspeedTest;

CREATE TABLE ApiKeys (
    username VARCHAR(31) NOT NULL,
    exchange VARCHAR(31) NOT NULL,
    publicKey VARCHAR(255) NOT NULL,
    privateKey VARCHAR(255) NOT NULL,
    isActive Boolean NOT NULL,
    PRIMARY KEY( username, exchange )
);

insert into ApiKeys VALUES( 'daniel', 'binance', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'daniel', 'bittrex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'daniel', 'gdax', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'daniel', 'poloniex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'matt', 'binance', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'matt', 'bittrex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'matt', 'gdax', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'matt', 'poloniex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'christian', 'binance', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'christian', 'bittrex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'christian', 'gdax', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'christian', 'poloniex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'nick', 'binance', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'nick', 'bittrex', 'xxxx', 'yyyy', false );
insert into ApiKeys VALUES( 'nick', 'gdax', 'xxxx', 'yyyy', true );
insert into ApiKeys VALUES( 'nick', 'poloniex', 'xxxx', 'yyyy', false );

CREATE TABLE PortfolioLookup (
    username VARCHAR(31) NOT NULL,
    location VARCHAR(63) NOT NULL,
    locationtype VARCHAR(255),
    PRIMARY KEY( username, location )
);

insert into PortfolioLookup VALUES("daniel", "0xd37fc846d81229d94a91f9fd8309d889eb8c6f60", "Ethereum Wallet");
insert into PortfolioLookup VALUES("daniel", "Binance", "Exchange");
insert into PortfolioLookup VALUES("daniel", "Bittrex", "Exchange");
insert into PortfolioLookup VALUES("matt", "0xd37fc846d81229d94a91f9fd8309d889eb8c6f60", "Ethereum Wallet");
insert into PortfolioLookup VALUES("matt", "Binance", "Exchange");
insert into PortfolioLookup VALUES("matt", "Bittrex", "Exchange");
insert into PortfolioLookup VALUES("christian", "0xd37fc846d81229d94a91f9fd8309d889eb8c6f60", "Ethereum Wallet");
insert into PortfolioLookup VALUES("christian", "Binance", "Exchange");
insert into PortfolioLookup VALUES("christian", "Bittrex", "Exchange");
insert into PortfolioLookup VALUES("nick", "0xd37fc846d81229d94a91f9fd8309d889eb8c6f60", "Ethereum Wallet");
insert into PortfolioLookup VALUES("nick", "Binance", "Exchange");
insert into PortfolioLookup VALUES("nick", "Bittrex", "Exchange");

CREATE TABLE UserLogin (
    username VARCHAR(31) NOT NULL,
    email VARCHAR(63) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY( username, email )
);

insert into UserLogin VALUES("daniel", "daniel@travas.io", "$2a$12$WjrGp5NQcBp3YWX1ARd/SeNlogDA5lnS/x6H4Shz6loA0N3.hbQqK");
insert into UserLogin VALUES("matt", "matt@travas.io", "$2a$12$WjrGp5NQcBp3YWX1ARd/SeNlogDA5lnS/x6H4Shz6loA0N3.hbQqK");
insert into UserLogin VALUES("christian", "christian@travas.io", "$2a$12$WjrGp5NQcBp3YWX1ARd/SeNlogDA5lnS/x6H4Shz6loA0N3.hbQqK");
insert into UserLogin VALUES("nick", "nicholas@travas.io", "$2a$12$WjrGp5NQcBp3YWX1ARd/SeNlogDA5lnS/x6H4Shz6loA0N3.hbQqK");

CREATE TABLE SessionTokens (
    username VARCHAR(31) NOT NULL,
    token VARCHAR(64) NOT NULL,
    lastApiCall Timestamp NOT NULL,
    PRIMARY KEY( username )
);

insert into SessionTokens VALUES("daniel","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("matt","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("christian","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("nick","EMPTY",'2018-03-08 23:59:59');

CREATE TABLE UserSignup (
    username VARCHAR(31) NOT NULL,
    email VARCHAR(63) NOT NULL,
    verified BOOLEAN NOT NULL,
    signupDate timestamp NOT NULL,
    verificationString VARCHAR(255) NOT NULL,
    PRIMARY KEY( username, email )
);

insert into UserSignup VALUES("daniel", "daniel@travas.io", true, NOW( ), "1234" );
insert into UserSignup VALUES("daniel", "christian@travas.io", true, NOW( ), "1234" );
insert into UserSignup VALUES("daniel", "matt@travas.io", true, NOW( ), "1234" );
insert into UserSignup VALUES("daniel", "nicholas@travas.io", true, NOW( ), "1234" );

CREATE TABLE PortfolioState(
    snapshot TIMESTAMP NOT NULL,
    username VARCHAR(31) NOT NULL,
    portfolio VARCHAR(19999) NOT NULL,
    PRIMARY KEY( snapshot, username )
);

insert into PortfolioState VALUES('2018-02-25 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$433.5","tokenValue":"$17.00","portionOfPortfolio":"37.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$750.00","tokenValue":"$750.00","portionOfPortfolio":"63.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1183.50"}' );
insert into PortfolioState VALUES('2018-02-26 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$459.00","tokenValue":"$18.00","portionOfPortfolio":"38.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$750.00","tokenValue":"$750.00","portionOfPortfolio":"62.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1209.00"}' );
insert into PortfolioState VALUES('2018-02-27 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$484.5","tokenValue":"$19.00","portionOfPortfolio":"38.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$790.00","tokenValue":"$790.00","portionOfPortfolio":"62.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1274.50"}' );
insert into PortfolioState VALUES('2018-02-28 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$510.00","tokenValue":"$20.00","portionOfPortfolio":"39.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$810.00","tokenValue":"$810.00","portionOfPortfolio":"61.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1320.00"}');
insert into PortfolioState VALUES('2018-03-01 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$510.00","tokenValue":"$20.00","portionOfPortfolio":"38.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$840.00","tokenValue":"$840.00","portionOfPortfolio":"62.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1350.00"}' );
insert into PortfolioState VALUES('2018-03-02 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$535.50","tokenValue":"$21.00","portionOfPortfolio":"42.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$750.00","tokenValue":"$750.00","portionOfPortfolio":"58.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1285.50"}');
insert into PortfolioState VALUES('2018-03-03 23:59:59', 'daniel', '{"data":[{"symbol":"OMG","tokenProportions":["100.00%"],"tokenBalance":25.5,"totalBalance":"$408.00","tokenValue":"$16.00","portionOfPortfolio":"37.00%","tokenLocations":["Binance"],"tokenBalances":[25.5]},{"symbol":"ETH","tokenProportions":["100.00%"],"tokenBalance":1,"totalBalance":"$700.00","tokenValue":"$700.00","portionOfPortfolio":"63.00%","tokenLocations":["Binance"],"tokenBalances":[1]}],"amountInvested":"$1000.00","totalBalance":"$1108.00"}' );

CREATE TABLE PortfolioInfo(
    username VARCHAR(31) NOT NULL,
    fiatType VARCHAR(255) NOT NULL,
    amountInvested DOUBLE(64,8) NOT NULL,
    PRIMARY KEY( username )
);

insert into PortfolioInfo VALUES( 'daniel', 'USD', 1000.00 );

CREATE TABLE TradingStrategies(
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyName VARCHAR(255) NOT NULL,
    buySignals VARCHAR(1023),
    sellSignals VARCHAR(1023),
    targets VARCHAR(255),
    stopLoss DECIMAL(4,4) NOT NULL,
    PRIMARY KEY( ID, username )
);

insert into TradingStrategies VALUES( 1, 'daniel', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'daniel', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 1, 'matt', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'matt', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 1, 'christian', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'christian', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 1, 'nick', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'nick', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );

CREATE TABLE DeletedTradingStrategies(
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyName VARCHAR(255) NOT NULL,
    buySignals VARCHAR(1023),
    sellSignals VARCHAR(1023),
    targets VARCHAR(255),
    stopLoss DECIMAL(2,2) NOT NULL,
    PRIMARY KEY( ID, username )
);

CREATE TABLE TradingSignalsLookup(
    ID INT NOT NULL AUTO_INCREMENT,
    signalName VARCHAR(63),
    parameters VARCHAR(511),
    PRIMARY KEY( ID )
);

insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-BUY', '[{name:"sma",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-CROSS-BUY', '[{name:"smaSmaller",datatype:"int"},{name:"smaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-BUY', '[{name:"ema",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-CROSS-BUY', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'BOLLINGER-BANDS-BUY', '[{name:"sma",datatype:"int"},{name:"std",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'LOCAL-MINIMA-BUY', '[{name:"windowSize",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'LOCAL-MAXIMA-SELL', '[{name:"windowSize",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-SELL', '[{name:"sma",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-CROSS-SELL', '[{name:"smaSmaller",datatype:"int"},{name:"smaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-SELL', '[{name:"ema",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-CROSS-SELL', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'BOLLINGER-BANDS-SELL', '[{name:"sma",datatype:"int"},{name:"std",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'TARGET-BUY', '[{name:"target",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'TARGET-SELL', '[{name:"target",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'RSI-BUY', '[{name:"period",datatype:"int"},{name:"threshold",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'RSI-SELL', '[{name:"period",datatype:"int"},{name:"threshold",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'MACD-BUY', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"},{name:"emaSignal",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'MACD-SELL', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"},{name:"emaSignal",datatype:"int"}]' );

CREATE TABLE Access(
    username VARCHAR(64) NOT NULL,
    tier ENUM('free', 'premium', 'admin'),
    PRIMARY KEY( username )
);

INSERT INTO Access VALUES( "daniel", 'admin' );
INSERT INTO Access VALUES( "nick", 'admin' );
INSERT INTO Access VALUES( "christian", 'admin' );
INSERT INTO Access VALUES( "matt", 'admin' );
INSERT INTO Access VALUES( "garren", 'admin' );

CREATE TABLE Limits(
    service VARCHAR(64) NOT NULL,
    tier ENUM('free', 'premium', 'admin'),
    setLimit INT NOT NULL,
    PRIMARY KEY( service, tier, setLimit )
);

INSERT INTO Limits VALUES( "BotManager", 'free', 25 );
INSERT INTO Limits VALUES( "BotManager", 'premium', 100 );
INSERT INTO Limits VALUES("BotManager", 'admin', 1000 );
INSERT INTO Limits VALUES( "StrategyManager", 'free', 100 );
INSERT INTO Limits VALUES( "StrategyManager", 'premium', 500 );
INSERT INTO Limits VALUES( "StrategyManager", 'admin', 1000 );

CREATE TABLE UserBots(
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyID INT NOT NULL,
    botName VARCHAR(255) NOT NULL,
    exchange VARCHAR(127) NOT NULL,
    marketBase VARCHAR(15) NOT NULL,
    marketQuote VARCHAR(15) NOT NULL,
    locked BOOLEAN NOT NULL,
    running BOOLEAN NOT NULL,
    isPublic BOOLEAN NOT NULL,
    tradingLimit DOUBLE NOT NULL,
    intervalCandle VARCHAR(15) NOT NULL,
    PRIMARY KEY( ID, strategyID, username )   
);

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle) VALUES( "daniel", 1, "Bot One", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle) VALUES( "daniel", 2, "Bot Two", "gdax", "BTC", "USD",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle) VALUES( "daniel", 1, "Bot Three", "binance", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle) VALUES( "daniel", 2, "Bot Four", "binance", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "daniel", 1, "Bot Five", "bittrex", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "daniel", 2, "Bot Six", "bittrex", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "daniel", 1, "Bot Seven", "gemini", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "daniel", 2, "Bot Eight", "gemini", "BTC", "USD",  false, false, false, 0.5, '1d' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 1, "Bot One", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 2, "Bot Two", "gdax", "BTC", "USD",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 1, "Bot Three", "binance", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 2, "Bot Four", "binance", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 1, "Bot Five", "bittrex", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 2, "Bot Six", "bittrex", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 1, "Bot Seven", "gemini", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 2, "Bot Eight", "gemini", "BTC", "USD",  false, false, false, 0.5, '1d' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 1, "Bot One", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 2, "Bot Two", "gdax", "BTC", "USD",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 1, "Bot Three", "binance", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 2, "Bot Four", "binance", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 1, "Bot Five", "bittrex", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 2, "Bot Six", "bittrex", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 1, "Bot Seven", "gemini", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 2, "Bot Eight", "gemini", "BTC", "USD",  false, false, false, 0.5, '1d' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 1, "Bot One", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 2, "Bot Two", "gdax", "BTC", "USD",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 1, "Bot Three", "binance", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 2, "Bot Four", "binance", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 1, "Bot Five", "bittrex", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 2, "Bot Six", "bittrex", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 1, "Bot Seven", "gemini", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 2, "Bot Eight", "gemini", "BTC", "USD",  false, false, false, 0.5, '1d' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 1, "Bot One", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 2, "Bot Two", "gdax", "BTC", "USD",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 1, "Bot Three", "binance", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 2, "Bot Four", "binance", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 1, "Bot Five", "bittrex", "ETH", "USDT",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 2, "Bot Six", "bittrex", "BTC", "USDT",  false, false, false, 0.5, '1d' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 1, "Bot Seven", "gemini", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 2, "Bot Eight", "gemini", "BTC", "USD",  false, false, false, 0.5, '1d' );

CREATE TABLE UserDeletedBots( 
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyID INT NOT NULL,
    botName VARCHAR(255) NOT NULL,
    PRIMARY KEY( ID, strategyID, username )   
);


CREATE TABLE BotLeaderboards(
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( botID, username )
);

insert into BotLeaderboards VALUES( 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboards VALUES( 2,'daniel', 'Bot Two', 'gdax', 'BTC-USD', .4, 3, 11, .2, 7, 4 );
insert into BotLeaderboards VALUES( 3,'daniel', 'Bot Three', 'binance', 'ETH-USDT', -.1, -1, 7, .1, 2, 5 );
insert into BotLeaderboards VALUES( 4,'daniel', 'Bot Four', 'binance', 'BTC-USDT', .2, 1.5, 7, .1, 3, 4 );
insert into BotLeaderboards VALUES( 5,'daniel', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, 1.1, 21, .1, 1, 20 );
insert into BotLeaderboards VALUES( 6,'daniel', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .5, 21, .3, 20, 1 );
insert into BotLeaderboards VALUES( 7,'daniel', 'Bot Seven', 'gemini', 'ETH-USD', .09, .1, 4, .4, 2, 2 );
insert into BotLeaderboards VALUES( 8,'daniel', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2, 11, .75, 10, 1 );

insert into BotLeaderboards VALUES( 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboards VALUES( 2,'matt', 'Bot Two', 'gdax', 'BTC-USD', .4, 1, 11, .2, 7, 4 );
insert into BotLeaderboards VALUES( 3,'matt', 'Bot Three', 'binance', 'ETH-USDT', -.1, .1, 7, .1, 2, 5 );
insert into BotLeaderboards VALUES( 4,'matt', 'Bot Four', 'binance', 'BTC-USDT', .2, .2, 7, .1, 3, 4 );
insert into BotLeaderboards VALUES( 5,'matt', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -1, 21, .1, 1, 20 );
insert into BotLeaderboards VALUES( 6,'matt', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .1, 21, .3, 20, 1 );
insert into BotLeaderboards VALUES( 7,'matt', 'Bot Seven', 'gemini', 'ETH-USD', .09, .3, 4, .4, 2, 2 );
insert into BotLeaderboards VALUES( 8,'matt', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2.1, 11, .75, 10, 1 );

insert into BotLeaderboards VALUES( 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboards VALUES( 2,'christian', 'Bot Two', 'gdax', 'BTC-USD', .4, 1.1, 11, .2, 7, 4 );
insert into BotLeaderboards VALUES( 3,'christian', 'Bot Three', 'binance', 'ETH-USDT', -.1, -1, 7, .1, 2, 5 );
insert into BotLeaderboards VALUES( 4,'christian', 'Bot Four', 'binance', 'BTC-USDT', .2, .3, 7, .1, 3, 4 );
insert into BotLeaderboards VALUES( 5,'christian', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -1.1, 21, .1, 1, 20 );
insert into BotLeaderboards VALUES( 6,'christian', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .04, 21, .3, 20, 1 );
insert into BotLeaderboards VALUES( 7,'christian', 'Bot Seven', 'gemini', 'ETH-USD', .09, .1, 4, .4, 2, 2 );
insert into BotLeaderboards VALUES( 8,'christian', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 1.5, 11, .75, 10, 1 );

insert into BotLeaderboards VALUES( 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, -1, 10, .5, 5, 5 );
insert into BotLeaderboards VALUES( 2,'nick', 'Bot Two', 'gdax', 'BTC-USD', .4, -2, 11, .2, 7, 4 );
insert into BotLeaderboards VALUES( 3,'nick', 'Bot Three', 'binance', 'ETH-USDT', -.1, -3, 7, .1, 2, 5 );
insert into BotLeaderboards VALUES( 4,'nick', 'Bot Four', 'binance', 'BTC-USDT', .2, -4, 7, .1, 3, 4 );
insert into BotLeaderboards VALUES( 5,'nick', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -5, 21, .1, 1, 20 );
insert into BotLeaderboards VALUES( 6,'nick', 'Bot Six', 'bittrex', 'BTC-USDT', .05, -6, 21, .3, 20, 1 );
insert into BotLeaderboards VALUES( 7,'nick', 'Bot Seven', 'gemini', 'ETH-USD', .09, -7, 4, .4, 2, 2 );
insert into BotLeaderboards VALUES( 8,'nick', 'Bot Eight', 'gemini', 'BTC-USD', -10, -10000, 100, -10, 0, 100 );

insert into BotLeaderboards VALUES( 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboards VALUES( 2,'garren', 'Bot Two', 'gdax', 'BTC-USD', .4, 1.2, 11, .2, 7, 4 );
insert into BotLeaderboards VALUES( 3,'garren', 'Bot Three', 'binance', 'ETH-USDT', -.1, 2, 7, .1, 2, 5 );
insert into BotLeaderboards VALUES( 4,'garren', 'Bot Four', 'binance', 'BTC-USDT', .2, 1, 7, .1, 3, 4 );
insert into BotLeaderboards VALUES( 5,'garren', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, .5, 21, .1, 1, 20 );
insert into BotLeaderboards VALUES( 6,'garren', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .95, 21, .3, 20, 1 );
insert into BotLeaderboards VALUES( 7,'garren', 'Bot Seven', 'gemini', 'ETH-USD', .09, 1.05, 22, .4, 2, 2 );
insert into BotLeaderboards VALUES( 8,'garren', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2, 11, .75, 10, 1 );

insert into BotLeaderboards VALUES( 11 , "daniel"   , "TroNiX"                , "Binance"  , "TRX-USDT" , -0.03236516 , 0.96758748 ,         5 , 0.01599561 ,       1 ,         4);
insert into BotLeaderboards VALUES( 12 , "daniel"   , "ETC-CLASSIC-GOD"       , "Binance"  , "ETC-ETH"  , -0.00291950 , 0.99708050 ,         1 , 0.00000000 ,       0 ,         1);
insert into BotLeaderboards VALUES( 13 , "daniel"   , "Insert Name"           , "Binance"  , "ETC-ETH"  ,  0.04018598 , 1.04048375 ,         3 , 0.02860880 ,       2 ,         1);
insert into BotLeaderboards VALUES( 14 , "daniel"   , "SCALPman"              , "Binance"  , "OMG-BTC" , -0.00628308 , 0.99343044 ,         4 , 0.01626842 ,       1 ,         3);
insert into BotLeaderboards VALUES( 15 , "daniel"   , "HEAVY HITTER"          , "Binance"  , "EOS-ETH"  ,  0.00230121 , 1.00230121 ,         1 , 0.00230121 ,       1 ,         0);
insert into BotLeaderboards VALUES( 16 , "daniel"   , "BNB Will Make Me Rich" , "Binance"  , "BNB-ETH"  ,  0.00000000 , 1.00000000 ,         0 , 0.00000000 ,       0 ,         0);
insert into BotLeaderboards VALUES( 17 , "daniel"   , "Scalp Some ADA"        , "Binance"  , "ADA-BNB"  ,  0.00000000 , 1.00000000 ,         0 , 0.00000000 ,       0 ,         0);
insert into BotLeaderboards VALUES( 18 , "daniel"   , "Minute Man "           , "Binance"  , "TRX-USDT" ,  0.03373963 , 1.03388833 ,         2 , 0.02852723 ,       2 ,         0);

CREATE TABLE BotLeaderboardsDaily(
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( botID, username )
);

/**
insert into BotLeaderboardsDaily VALUES( 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsDaily VALUES( 2,'matt', 'Bot Two', 'gdax', 'BTC-USD', .4, 3, 11, .2, 7, 4 );
insert into BotLeaderboardsDaily VALUES( 3,'matt', 'Bot Three', 'binance', 'ETH-USDT', -.1, -1, 7, .1, 2, 5 );
insert into BotLeaderboardsDaily VALUES( 4,'matt', 'Bot Four', 'binance', 'BTC-USDT', .2, 1.5, 7, .1, 3, 4 );
insert into BotLeaderboardsDaily VALUES( 5,'matt', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, 1.1, 21, .1, 1, 20 );
insert into BotLeaderboardsDaily VALUES( 6,'matt', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .5, 21, .3, 20, 1 );
insert into BotLeaderboardsDaily VALUES( 7,'matt', 'Bot Seven', 'gemini', 'ETH-USD', .09, .1, 4, .4, 2, 2 );
insert into BotLeaderboardsDaily VALUES( 8,'matt', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2, 11, .75, 10, 1 );

insert into BotLeaderboardsDaily VALUES( 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsDaily VALUES( 2,'christian', 'Bot Two', 'gdax', 'BTC-USD', .4, 1, 11, .2, 7, 4 );
insert into BotLeaderboardsDaily VALUES( 3,'christian', 'Bot Three', 'binance', 'ETH-USDT', -.1, .1, 7, .1, 2, 5 );
insert into BotLeaderboardsDaily VALUES( 4,'christian', 'Bot Four', 'binance', 'BTC-USDT', .2, .2, 7, .1, 3, 4 );
insert into BotLeaderboardsDaily VALUES( 5,'christian', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -1, 21, .1, 1, 20 );
insert into BotLeaderboardsDaily VALUES( 6,'christian', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .1, 21, .3, 20, 1 );
insert into BotLeaderboardsDaily VALUES( 7,'christian', 'Bot Seven', 'gemini', 'ETH-USD', .09, .3, 4, .4, 2, 2 );
insert into BotLeaderboardsDaily VALUES( 8,'christian', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2.1, 11, .75, 10, 1 );

insert into BotLeaderboardsDaily VALUES( 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsDaily VALUES( 2,'nick', 'Bot Two', 'gdax', 'BTC-USD', .4, 1.1, 11, .2, 7, 4 );
insert into BotLeaderboardsDaily VALUES( 3,'nick', 'Bot Three', 'binance', 'ETH-USDT', -.1, -1, 7, .1, 2, 5 );
insert into BotLeaderboardsDaily VALUES( 4,'nick', 'Bot Four', 'binance', 'BTC-USDT', .2, .3, 7, .1, 3, 4 );
insert into BotLeaderboardsDaily VALUES( 5,'nick', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -1.1, 21, .1, 1, 20 );
insert into BotLeaderboardsDaily VALUES( 6,'nick', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .04, 21, .3, 20, 1 );
insert into BotLeaderboardsDaily VALUES( 7,'nick', 'Bot Seven', 'gemini', 'ETH-USD', .09, .1, 4, .4, 2, 2 );
insert into BotLeaderboardsDaily VALUES( 8,'nick', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 1.5, 11, .75, 10, 1 );

insert into BotLeaderboardsDaily VALUES( 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, -1, 10, .5, 5, 5 );
insert into BotLeaderboardsDaily VALUES( 2,'garren', 'Bot Two', 'gdax', 'BTC-USD', .4, -2, 11, .2, 7, 4 );
insert into BotLeaderboardsDaily VALUES( 3,'garren', 'Bot Three', 'binance', 'ETH-USDT', -.1, -3, 7, .1, 2, 5 );
insert into BotLeaderboardsDaily VALUES( 4,'garren', 'Bot Four', 'binance', 'BTC-USDT', .2, -4, 7, .1, 3, 4 );
insert into BotLeaderboardsDaily VALUES( 5,'garren', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -5, 21, .1, 1, 20 );
insert into BotLeaderboardsDaily VALUES( 6,'garren', 'Bot Six', 'bittrex', 'BTC-USDT', .05, -6, 21, .3, 20, 1 );
insert into BotLeaderboardsDaily VALUES( 7,'garren', 'Bot Seven', 'gemini', 'ETH-USD', .09, -7, 4, .4, 2, 2 );
insert into BotLeaderboardsDaily VALUES( 8,'garren', 'Bot Eight', 'gemini', 'BTC-USD', -10, -10000, 100, -10, 0, 100 );
**/

insert into BotLeaderboardsDaily VALUES( 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsDaily VALUES( 2,'daniel', 'Bot Two', 'gdax', 'BTC-USD', .4, 1.2, 11, .2, 7, 4 );
insert into BotLeaderboardsDaily VALUES( 3,'daniel', 'Bot Three', 'binance', 'ETH-USDT', -.1, 2, 7, .1, 2, 5 );
insert into BotLeaderboardsDaily VALUES( 4,'daniel', 'Bot Four', 'binance', 'BTC-USDT', .2, 1, 7, .1, 3, 4 );
insert into BotLeaderboardsDaily VALUES( 5,'daniel', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, .5, 21, .1, 1, 20 );
insert into BotLeaderboardsDaily VALUES( 6,'daniel', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .95, 21, .3, 20, 1 );
insert into BotLeaderboardsDaily VALUES( 7,'daniel', 'Bot Seven', 'gemini', 'ETH-USD', .09, 1.05, 22, .4, 2, 2 );
insert into BotLeaderboardsDaily VALUES( 8,'daniel', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2, 11, .75, 10, 1 );

CREATE TABLE BotLeaderboardsWeekly(
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( botID, username )
);

/**
insert into BotLeaderboardsWeekly VALUES( 1,'nicholas', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsWeekly VALUES( 2,'nicholas', 'Bot Two', 'gdax', 'BTC-USD', .4, 3, 11, .2, 7, 4 );
insert into BotLeaderboardsWeekly VALUES( 3,'nicholas', 'Bot Three', 'binance', 'ETH-USDT', -.1, -1, 7, .1, 2, 5 );
insert into BotLeaderboardsWeekly VALUES( 4,'nicholas', 'Bot Four', 'binance', 'BTC-USDT', .2, 1.5, 7, .1, 3, 4 );
insert into BotLeaderboardsWeekly VALUES( 5,'nicholas', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, 1.1, 21, .1, 1, 20 );
insert into BotLeaderboardsWeekly VALUES( 6,'nicholas', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .5, 21, .3, 20, 1 );
insert into BotLeaderboardsWeekly VALUES( 7,'nicholas', 'Bot Seven', 'gemini', 'ETH-USD', .09, .1, 4, .4, 2, 2 );
insert into BotLeaderboardsWeekly VALUES( 8,'nicholas', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2, 11, .75, 10, 1 );

insert into BotLeaderboardsWeekly VALUES( 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsWeekly VALUES( 2,'christian', 'Bot Two', 'gdax', 'BTC-USD', .4, 1, 11, .2, 7, 4 );
insert into BotLeaderboardsWeekly VALUES( 3,'christian', 'Bot Three', 'binance', 'ETH-USDT', -.1, .1, 7, .1, 2, 5 );
insert into BotLeaderboardsWeekly VALUES( 4,'christian', 'Bot Four', 'binance', 'BTC-USDT', .2, .2, 7, .1, 3, 4 );
insert into BotLeaderboardsWeekly VALUES( 5,'christian', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -1, 21, .1, 1, 20 );
insert into BotLeaderboardsWeekly VALUES( 6,'christian', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .1, 21, .3, 20, 1 );
insert into BotLeaderboardsWeekly VALUES( 7,'christian', 'Bot Seven', 'gemini', 'ETH-USD', .09, .3, 4, .4, 2, 2 );
insert into BotLeaderboardsWeekly VALUES( 8,'christian', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2.1, 11, .75, 10, 1 );

insert into BotLeaderboardsWeekly VALUES( 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsWeekly VALUES( 2,'garren', 'Bot Two', 'gdax', 'BTC-USD', .4, 1.1, 11, .2, 7, 4 );
insert into BotLeaderboardsWeekly VALUES( 3,'garren', 'Bot Three', 'binance', 'ETH-USDT', -.1, -1, 7, .1, 2, 5 );
insert into BotLeaderboardsWeekly VALUES( 4,'garren', 'Bot Four', 'binance', 'BTC-USDT', .2, .3, 7, .1, 3, 4 );
insert into BotLeaderboardsWeekly VALUES( 5,'garren', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -1.1, 21, .1, 1, 20 );
insert into BotLeaderboardsWeekly VALUES( 6,'garren', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .04, 21, .3, 20, 1 );
insert into BotLeaderboardsWeekly VALUES( 7,'garren', 'Bot Seven', 'gemini', 'ETH-USD', .09, .1, 4, .4, 2, 2 );
insert into BotLeaderboardsWeekly VALUES( 8,'garren', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 1.5, 11, .75, 10, 1 );

insert into BotLeaderboardsWeekly VALUES( 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, -1, 10, .5, 5, 5 );
insert into BotLeaderboardsWeekly VALUES( 2,'matt', 'Bot Two', 'gdax', 'BTC-USD', .4, -2, 11, .2, 7, 4 );
insert into BotLeaderboardsWeekly VALUES( 3,'matt', 'Bot Three', 'binance', 'ETH-USDT', -.1, -3, 7, .1, 2, 5 );
insert into BotLeaderboardsWeekly VALUES( 4,'matt', 'Bot Four', 'binance', 'BTC-USDT', .2, -4, 7, .1, 3, 4 );
insert into BotLeaderboardsWeekly VALUES( 5,'matt', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, -5, 21, .1, 1, 20 );
insert into BotLeaderboardsWeekly VALUES( 6,'matt', 'Bot Six', 'bittrex', 'BTC-USDT', .05, -6, 21, .3, 20, 1 );
insert into BotLeaderboardsWeekly VALUES( 7,'matt', 'Bot Seven', 'gemini', 'ETH-USD', .09, -7, 4, .4, 2, 2 );
insert into BotLeaderboardsWeekly VALUES( 8,'matt', 'Bot Eight', 'gemini', 'BTC-USD', -10, -10000, 100, -10, 0, 100 );
**/

insert into BotLeaderboardsWeekly VALUES( 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsWeekly VALUES( 2,'daniel', 'Bot Two', 'gdax', 'BTC-USD', .4, 1.2, 11, .2, 7, 4 );
insert into BotLeaderboardsWeekly VALUES( 3,'daniel', 'Bot Three', 'binance', 'ETH-USDT', -.1, 2, 7, .1, 2, 5 );
insert into BotLeaderboardsWeekly VALUES( 4,'daniel', 'Bot Four', 'binance', 'BTC-USDT', .2, 1, 7, .1, 3, 4 );
insert into BotLeaderboardsWeekly VALUES( 5,'daniel', 'Bot Five', 'bittrex', 'ETH-USDT', -.5, .5, 21, .1, 1, 20 );
insert into BotLeaderboardsWeekly VALUES( 6,'daniel', 'Bot Six', 'bittrex', 'BTC-USDT', .05, .95, 21, .3, 20, 1 );
insert into BotLeaderboardsWeekly VALUES( 7,'daniel', 'Bot Seven', 'gemini', 'ETH-USD', .09, 1.05, 22, .4, 2, 2 );
insert into BotLeaderboardsWeekly VALUES( 8,'daniel', 'Bot Eight', 'gemini', 'BTC-USD', 1.0, 2, 11, .75, 10, 1 );

CREATE TABLE BotLeaderboardsHistorical(
    dated timestamp,
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( dated, botID, username )
);

/**
insert into BotLeaderboardsHistorical VALUES( '2018-06-10 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-11 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 3, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-12 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-13 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.5, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-14 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.6, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-15 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.7, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-16 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.8, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-17 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.9, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-18 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2.5, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-19 23:59:00', 1,'daniel', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );

insert into BotLeaderboardsHistorical VALUES( '2018-06-10 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-11 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-12 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-13 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.3, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-14 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.3, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-15 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.3, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-16 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-17 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.15, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-18 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-19 23:59:00', 1,'matt', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );

insert into BotLeaderboardsHistorical VALUES( '2018-06-10 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-11 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-12 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1.9, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-13 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-14 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-15 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 2.5, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-16 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-17 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-18 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, .5, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-19 23:59:00', 1,'christian', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );

insert into BotLeaderboardsHistorical VALUES( '2018-06-10 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 8, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-11 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 7, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-12 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 6, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-13 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 5, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-14 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 4, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-15 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 3, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-16 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-17 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-18 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, 0, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-19 23:59:00', 1,'nick', 'Bot One', 'gdax', 'ETH-USD', .5, -1, 10, .5, 5, 5 );

insert into BotLeaderboardsHistorical VALUES( '2018-06-10 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-11 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-12 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-13 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.0, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-14 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.15, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-15 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.19, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-16 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.2, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-17 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-18 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.14, 10, .5, 5, 5 );
insert into BotLeaderboardsHistorical VALUES( '2018-06-19 23:59:00', 1,'garren', 'Bot One', 'gdax', 'ETH-USD', .5, 1.1, 10, .5, 5, 5 );
**/

CREATE TABLE BotLogs(
    botID INT NOT NULL,
    username VARCHAR(31) NOT NULL,
    action ENUM('BUY', 'SELL', 'HOLD'),
    networth DOUBLE(64,8) NOT NULL,
    quoteOwned DOUBLE(64,8) NOT NULL,
    assetsOwned DOUBLE(64,8) NOT NULL,
    lastPrice DOUBLE(64,8),
    signalStates VARCHAR(2055),
    dated timestamp NOT NULL,
    PRIMARY KEY( botID, username, dated )   
);

INSERT INTO BotLogs VALUES( 1 , 'daniel'   , 'SELL'   , 1.03179701 , 1.03179701 ,   0.00000000 , 0.03944999 , '{"buyIndicators":{"sma(25)":"0.03942440","class":"SMA"},"sellIndicators":{"sma(25)":"0.03942440","class":"SMA"}}' , '2018-07-04 12:07:00' );
INSERT INTO BotLogs VALUES( 1 , 'daniel'   , 'BUY'    , 1.03179700 , 0.00000000 ,  26.30793659 , 0.03921999 , '{"buyIndicators":{"sma(25)":"0.03919200","class":"SMA"},"sellIndicators":{"sma(25)":"0.03919200","class":"SMA"}}',  '2018-07-04 13:07:00' );           

CREATE TABLE UserTickers(
    username VARCHAR(31) NOT NULL,
    ticker VARCHAR(511) NOT NULL,
    tickerBox int NOT NULL,
    PRIMARY KEY( username, tickerBox )
);

insert into UserTickers VALUES( 'daniel', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'daniel', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'daniel', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'daniel', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

CREATE TABLE SignalFeedLookup(
    id int NOT NULL AUTO_INCREMENT NOT NULL,
    baseCurrency VARCHAR(255) NOT NULL,
    counterCurrency VARCHAR(255) NOT NULL,
    exchange VARCHAR(255) NOT NULL,
    inHouseSignal int NOT NULL,
    candle VARCHAR(15),
    PRIMARY KEY( id )
);

insert into SignalFeedLookup( baseCurrency, counterCurrency, exchange, inHouseSignal, candle ) VALUES( 'OMG', 'ETH', 'binance', 1, '1h' );
insert into SignalFeedLookup( baseCurrency, counterCurrency, exchange, inHouseSignal, candle ) VALUES( 'OMG', 'ETH', 'binance', 1, '1d' );
insert into SignalFeedLookup( baseCurrency, counterCurrency, exchange, inHouseSignal, candle ) VALUES( 'ETH', 'BTC', 'gdax', 1, '1h' );
insert into SignalFeedLookup( baseCurrency, counterCurrency, exchange, inHouseSignal, candle ) VALUES( 'ETH', 'USD', 'gdax', 1, '1d' );

CREATE TABLE UserSignals(
    username VARCHAR(31) NOT NULL,
    botId int NOT NULL,
    PRIMARY KEY( username, botId )
);

insert into UserSignals VALUES( 'daniel', 1 );
insert into UserSignals VALUES( 'daniel', 2 );
insert into UserSignals VALUES( 'daniel', 3 );
insert into UserSignals VALUES( 'daniel', 4 );

CREATE TABLE SignalFeed(
    signalId int,
    message VARCHAR(1023) NOT NULL,
    dated timestamp NOT NULL,
    PRIMARY KEY( signalId, dated )
);

CREATE TABLE InHouseSignals(
     id int NOT NULL AUTO_INCREMENT NOT NULL,
     signalName VARCHAR(255) NOT NULL,
     parameterValues VARCHAR(511) NOT NULL,
     PRIMARY KEY( id )
);

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:5,smaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:10,smaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:15,smaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:20,smaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:50,smaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:5,smaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:6,smaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:6,smaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:9,smaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:5,smaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:10,smaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:15,smaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:20,smaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:50,smaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:5,smaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:6,smaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:6,smaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:9,smaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:7,emaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:5,emaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:10,emaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:15,emaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:20,emaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:50,emaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:5,emaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:6,emaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:6,emaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:9,emaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:7,emaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:5,emaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:10,emaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:15,emaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:20,emaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:50,emaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:5,emaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:6,emaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:6,emaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:9,emaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:21,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:20,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:14,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:30,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:50,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:21,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:20,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:14,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:30,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:50,std:1.0}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:21,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:20,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:14,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:30,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:50,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:21,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:20,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:14,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:30,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:50,std:1.0}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.01}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.05}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.1}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.2}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.3}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.4}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:1.00}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.01}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.05}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.1}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.2}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.3}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.4}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:1.00}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:5,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:10,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:15,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:25,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:50,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:5,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:10,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:15,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:25,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:50,threshold:40}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:5,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:10,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:15,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:25,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:50,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:5,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:10,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:15,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:25,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:50,threshold:40}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:12,emaLarger:26,emaSignal:9}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:10,emaLarger:20,emaSignal:7}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:15,emaLarger:30,emaSignal:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:9,emaLarger:18,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:8,emaLarger:16,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:18,emaLarger:35,emaSignal:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:20,emaLarger:40,emaSignal:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:25,emaLarger:50,emaSignal:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:30,emaLarger:60,emaSignal:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:7,emaLarger:14,emaSignal:4}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:12,emaLarger:26,emaSignal:9}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:10,emaLarger:20,emaSignal:7}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:15,emaLarger:30,emaSignal:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:9,emaLarger:18,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:8,emaLarger:16,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:18,emaLarger:35,emaSignal:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:20,emaLarger:40,emaSignal:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:25,emaLarger:50,emaSignal:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:30,emaLarger:60,emaSignal:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:7,emaLarger:14,emaSignal:4}' );

CREATE TABLE UserFollowing(
	username VARCHAR(31) NOT NULL,
	follower VARCHAR(31) NOT NULL,
	PRIMARY KEY( username, follower )
);

insert into UserFollowing VALUES( 'daniel', 'testman' );
insert into UserFollowing VALUES( 'daniel', 'christian' );
insert into UserFollowing VALUES( 'daniel', 'matt' );
insert into UserFollowing VALUES( 'daniel', 'nicholas' );
insert into UserFollowing VALUES( 'daniel', 'travas' );
insert into UserFollowing VALUES( 'christian', 'travas' );
insert into UserFollowing VALUES( 'matt', 'travas' );
insert into UserFollowing VALUES( 'nick', 'travas' );

CREATE TABLE UserCredibility(
	username VARCHAR(31) NOT NULL,
	credibility int NOT NULL,
	dateOfLastGrant timestamp,
	PRIMARY KEY( username )
);

insert into UserCredibility VALUES( 'daniel', 100, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'matt', 100, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'nick', -10000, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'christian', 100, '2018-04-25 23:59:59' );

CREATE TABLE UserBio(
	username VARCHAR(31) NOT NULL,
	bio VARCHAR(255) NOT NULL,
	PRIMARY KEY( username )
);

insert into UserBio VALUES( 'daniel','My name is Daniel Anderson, co-founder of Travas research and crypto currency enthusiast' );
insert into UserBio VALUES( 'christian','My name is Christian Skinner, I like buy Ethereum at the top and lose all my money' );
insert into UserBio VALUES( 'matt','My name is Matt Teeter, the contango master' );
insert into UserBio VALUES( 'nick','My name is Nicholass Skinner, I am a fucking queef!' );

CREATE TABLE ExchangeMarketIntersections(
	exchangeOne VARCHAR(31) NOT NULL,
	exchangeTwo VARCHAR(31) NOT NULL,
	markets VARCHAR(15000),
	PRIMARY KEY( exchangeOne, exchangeTwo )
);

