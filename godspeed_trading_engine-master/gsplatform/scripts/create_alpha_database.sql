drop DATABASE TravasAlpha;
CREATE DATABASE TravasAlpha;
use TravasAlpha;

CREATE TABLE ApiKeys (
    username VARCHAR(31) NOT NULL,
    exchange VARCHAR(31) NOT NULL,
    publicKey VARCHAR(255) NOT NULL,
    privateKey VARCHAR(255) NOT NULL,
    isActive Boolean NOT NULL,
    PRIMARY KEY( username, exchange )
);

CREATE TABLE UserLogin (
    username VARCHAR(31) NOT NULL,
    email VARCHAR(63) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY( username, email )
);

insert into UserLogin VALUES("daniel", "daniel@travas.io", "$2a$12$1nTR.gT6uSTVLEEyU8.gCO/FevEYJVIwYoJlNWy8Tk5fU03JnWM/y");
insert into UserLogin VALUES("matt", "matt@travas.io", "$2a$12$1nTR.gT6uSTVLEEyU8.gCO/FevEYJVIwYoJlNWy8Tk5fU03JnWM/y");
insert into UserLogin VALUES("christian", "christian@travas.io", "$2a$12$1nTR.gT6uSTVLEEyU8.gCO/FevEYJVIwYoJlNWy8Tk5fU03JnWM/y");
insert into UserLogin VALUES("nick", "nicholas@travas.io", "$2a$12$1nTR.gT6uSTVLEEyU8.gCO/FevEYJVIwYoJlNWy8Tk5fU03JnWM/y");
insert into UserLogin VALUES("alpha", "alpha@travas.io", "$2a$12$1nTR.gT6uSTVLEEyU8.gCO/FevEYJVIwYoJlNWy8Tk5fU03JnWM/y");

CREATE TABLE SessionTokens (
    username VARCHAR(31) NOT NULL,
    token VARCHAR(64) NOT NULL,
    lastApiCall Timestamp NOT NULL,
    PRIMARY KEY( username )
);

insert into SessionTokens VALUES("daniel","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("matt","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("christian","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("nick","EMPTY",'2018-03-08 23:59:59');
insert into SessionTokens VALUES("alpha","EMPTY",'2018-03-08 23:59:59');

CREATE TABLE UserSignup (
    username VARCHAR(31) NOT NULL,
    email VARCHAR(63) NOT NULL,
    verified BOOLEAN NOT NULL,
    signupDate timestamp NOT NULL,
    verificationString VARCHAR(255) NOT NULL,
    PRIMARY KEY( username, email )
);

insert into UserSignup VALUES("daniel", "daniel@travas.io", true, NOW( ), "1234" );
insert into UserSignup VALUES("christian", "christian@travas.io", true, NOW( ), "1234" );
insert into UserSignup VALUES("matt", "matt@travas.io", true, NOW( ), "1234" );
insert into UserSignup VALUES("nick", "nicholas@travas.io", true, NOW( ), "1234" );

CREATE TABLE TradingStrategies(
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyName VARCHAR(255) NOT NULL,
    buySignals VARCHAR(1023),
    sellSignals VARCHAR(1023),
    targets VARCHAR(255),
    stopLoss DECIMAL(4,4) NOT NULL,
    PRIMARY KEY( ID, username )
);

insert into TradingStrategies VALUES( 1, 'daniel', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'daniel', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 1, 'matt', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'matt', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 1, 'christian', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'christian', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 1, 'nick', 'SMA-CROSS-BUY', '[{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}]', '[{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}]', "[]", 00.05 );
insert into TradingStrategies VALUES( 2, 'nick', 'SMA-BUY', '[{signal:SMA-BUY,sma:20}]', '[{signal:SMA-SELL,sma:20}]', "[]", 00.05 );

CREATE TABLE DeletedTradingStrategies(
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyName VARCHAR(255) NOT NULL,
    buySignals VARCHAR(1023),
    sellSignals VARCHAR(1023),
    targets VARCHAR(255),
    stopLoss DECIMAL(2,2) NOT NULL,
    PRIMARY KEY( ID, username )
);

CREATE TABLE TradingSignalsLookup(
    ID INT NOT NULL AUTO_INCREMENT,
    signalName VARCHAR(63),
    parameters VARCHAR(511),
    PRIMARY KEY( ID )
);

insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-BUY', '[{name:"sma",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-CROSS-BUY', '[{name:"smaSmaller",datatype:"int"},{name:"smaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-BUY', '[{name:"ema",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-CROSS-BUY', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'BOLLINGER-BANDS-BUY', '[{name:"sma",datatype:"int"},{name:"std",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'LOCAL-MINIMA-BUY', '[{name:"windowSize",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'LOCAL-MAXIMA-SELL', '[{name:"windowSize",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-SELL', '[{name:"sma",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'SMA-CROSS-SELL', '[{name:"smaSmaller",datatype:"int"},{name:"smaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-SELL', '[{name:"ema",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'EMA-CROSS-SELL', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'BOLLINGER-BANDS-SELL', '[{name:"sma",datatype:"int"},{name:"std",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'TARGET-BUY', '[{name:"target",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'TARGET-SELL', '[{name:"target",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'RSI-BUY', '[{name:"period",datatype:"int"},{name:"threshold",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'RSI-SELL', '[{name:"period",datatype:"int"},{name:"threshold",datatype:"double"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'MACD-BUY', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"},{name:"emaSignal",datatype:"int"}]' );
insert into TradingSignalsLookup( signalName, parameters ) VALUES( 'MACD-SELL', '[{name:"emaSmaller",datatype:"int"},{name:"emaLarger",datatype:"int"},{name:"emaSignal",datatype:"int"}]' );

CREATE TABLE Access(
    username VARCHAR(64) NOT NULL,
    tier ENUM('free', 'premium', 'admin'),
    PRIMARY KEY( username )
);

INSERT INTO Access VALUES( "daniel", 'admin' );
INSERT INTO Access VALUES( "nick", 'admin' );
INSERT INTO Access VALUES( "christian", 'admin' );
INSERT INTO Access VALUES( "matt", 'admin' );
INSERT INTO Access VALUES( "garren", 'admin' );

CREATE TABLE Limits(
    service VARCHAR(64) NOT NULL,
    tier ENUM('free', 'premium', 'admin'),
    setLimit INT NOT NULL,
    PRIMARY KEY( service, tier, setLimit )
);

INSERT INTO Limits VALUES( "BotManager", 'free', 25 );
INSERT INTO Limits VALUES( "BotManager", 'premium', 100 );
INSERT INTO Limits VALUES("BotManager", 'admin', 1000 );
INSERT INTO Limits VALUES( "StrategyManager", 'free', 100 );
INSERT INTO Limits VALUES( "StrategyManager", 'premium', 500 );
INSERT INTO Limits VALUES( "StrategyManager", 'admin', 1000 );

CREATE TABLE UserBots(
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyID INT NOT NULL,
    botName VARCHAR(255) NOT NULL,
    exchange VARCHAR(127) NOT NULL,
    marketBase VARCHAR(15) NOT NULL,
    marketQuote VARCHAR(15) NOT NULL,
    locked BOOLEAN NOT NULL,
    running BOOLEAN NOT NULL,
    isPublic BOOLEAN NOT NULL,
    tradingLimit DOUBLE NOT NULL,
    intervalCandle VARCHAR(15) NOT NULL,
    PRIMARY KEY( ID, strategyID, username )   
);

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle) VALUES( "daniel", 1, "Alpha", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle) VALUES( "daniel", 2, "Omega", "gdax", "ETH", "BTC",  false, false, false, 0.5, '5m' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 1, "Beta", "gdax", "ETH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "christian", 2, "Zeta", "gdax", "BTC", "USD",  false, false, false, 0.5, '5m' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 1, "Eta", "gdax", "LTC", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "matt", 2, "Theta", "gdax", "LTC", "BTC",  false, false, false, 0.5, '5m' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 1, "Mu", "gdax", "BCH", "USD",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "nick", 2, "Nu", "gdax", "BCH", "BTC",  false, false, false, 0.5, '5m' );

insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 1, "Xi", "gdax", "BTC", "EUR",   false, false, false, 0.5, '1h' );
insert into UserBots (username,strategyId,botName,exchange,marketBase,marketQuote,locked,running,isPublic,tradingLimit,intervalCandle)  VALUES( "garren", 2, "Sigma", "gdax", "ETH", "EUR",  false, false, false, 0.5, '5m' );

CREATE TABLE UserDeletedBots( 
    ID INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(31) NOT NULL,
    strategyID INT NOT NULL,
    botName VARCHAR(255) NOT NULL,
    PRIMARY KEY( ID, strategyID, username )   
);


CREATE TABLE BotLeaderboards(
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( botID, username )
);

CREATE TABLE BotLeaderboardsDaily(
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( botID, username )
);

CREATE TABLE BotLeaderboardsWeekly(
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( botID, username )
);

CREATE TABLE BotLeaderboardsHistorical(
    dated timestamp,
    botID INT NOT NULL ,
    username VARCHAR(31),
    botName VARCHAR(63),
    exchange VARCHAR(33),
    market VARCHAR(33),
    totalROI DECIMAL(16,8) NOT NULL,
    networth DECIMAL(16,8) NOT NULL,
    numTrades INT NOT NULL,
    bestTrade DECIMAL(16,8) NOT NULL,
    numWins INT NOT NULL,
    numLosses INT NOT NULL,
    PRIMARY KEY( dated, botID, username )
);

CREATE TABLE BotLogs(
    botID INT NOT NULL,
    username VARCHAR(31) NOT NULL,
    action ENUM('BUY', 'SELL', 'HOLD'),
    networth DOUBLE(64,8) NOT NULL,
    quoteOwned DOUBLE(64,8) NOT NULL,
    assetsOwned DOUBLE(64,8) NOT NULL,
    lastPrice DOUBLE(64,8),
    signalStates VARCHAR(2055),
    dated timestamp NOT NULL,
    PRIMARY KEY( botID, username, dated )   
);

CREATE TABLE UserTickers(
    username VARCHAR(31) NOT NULL,
    ticker VARCHAR(511) NOT NULL,
    tickerBox int NOT NULL,
    PRIMARY KEY( username, tickerBox )
);

insert into UserTickers VALUES( 'daniel', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'daniel', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'daniel', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'daniel', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

insert into UserTickers VALUES( 'matt', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'matt', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'matt', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'matt', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

insert into UserTickers VALUES( 'christian', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'christian', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'christian', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'christian', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

insert into UserTickers VALUES( 'nick', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'nick', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'nick', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'nick', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

insert into UserTickers VALUES( 'garren', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'garren', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'garren', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'garren', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

insert into UserTickers VALUES( 'alpha', '{base:"ETH", quote:"USD", exchange:"gdax"}', 1 );
insert into UserTickers VALUES( 'alpha', '{base:"BTC", quote:"USD", exchange:"gdax"}', 2 );
insert into UserTickers VALUES( 'alpha', '{base:"LTC", quote:"USD", exchange:"gdax"}', 3 );
insert into UserTickers VALUES( 'alpha', '{base:"BCH", quote:"USD", exchange:"gdax"}', 4 );

CREATE TABLE InHouseSignals(
     id int NOT NULL AUTO_INCREMENT NOT NULL,
     signalName VARCHAR(255) NOT NULL,
     parameterValues VARCHAR(511) NOT NULL,
     PRIMARY KEY( id )
);

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-BUY", '{signal:SMA-BUY,sma:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-SELL", '{signal:SMA-SELL,sma:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:7,smaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:5,smaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:10,smaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:15,smaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:20,smaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:50,smaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:5,smaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:6,smaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:6,smaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-BUY", '{signal:SMA-CROSS-BUY,smaSmaller:9,smaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:7,smaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:5,smaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:10,smaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:15,smaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:20,smaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:50,smaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:5,smaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:6,smaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:6,smaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "SMA-CROSS-SELL", '{signal:SMA-CROSS-SELL,smaSmaller:9,smaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-BUY", '{signal:EMA-BUY,ema:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-SELL", '{signal:EMA-SELL,ema:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:7,emaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:5,emaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:10,emaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:15,emaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:20,emaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:50,emaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:5,emaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:6,emaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:6,emaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-BUY", '{signal:EMA-CROSS-BUY,emaSmaller:9,emaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:7,emaLarger:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:5,emaLarger:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:10,emaLarger:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:15,emaLarger:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:20,emaLarger:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:50,emaLarger:100}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:5,emaLarger:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:6,emaLarger:21}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:6,emaLarger:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( "EMA-CROSS-SELL", '{signal:EMA-CROSS-SELL,emaSmaller:9,emaLarger:27}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:21,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:20,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:14,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:30,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:50,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:21,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:20,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:14,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:30,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-BUY', '{signal:BOLLINGER-BANDS-BUY,sma:50,std:1.0}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:21,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:20,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:14,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:30,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:50,std:2.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:21,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:20,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:14,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:30,std:1.0}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'BOLLINGER-BANDS-SELL', '{signal:BOLLINGER-BANDS-SELL,sma:50,std:1.0}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-BUY', '{signal:LOCAL-MINIMA-BUY,windowSize:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:18}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:30}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'LOCAL-MINIMA-SELL', '{signal:LOCAL-MINIMA-SELL,windowSize:100}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.01}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.05}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.1}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.2}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.3}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.4}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:.5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-BUY', '{signal:TARGET-BUY,target:1.00}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.01}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.05}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.1}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.2}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.3}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.4}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:.5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'TARGET-SELL', '{signal:TARGET-SELL,target:1.00}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:5,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:10,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:15,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:25,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:50,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:5,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:10,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:15,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:25,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-BUY', '{signal:RSI-BUY,period:50,threshold:40}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:5,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:10,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:15,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:25,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:50,threshold:50}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:5,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:10,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:15,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:25,threshold:40}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'RSI-SELL', '{signal:RSI-SELL,period:50,threshold:40}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:12,emaLarger:26,emaSignal:9}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:10,emaLarger:20,emaSignal:7}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:15,emaLarger:30,emaSignal:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:9,emaLarger:18,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:8,emaLarger:16,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:18,emaLarger:35,emaSignal:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:20,emaLarger:40,emaSignal:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:25,emaLarger:50,emaSignal:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:30,emaLarger:60,emaSignal:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-BUY', '{signal:MACD-BUY,emaSmaller:7,emaLarger:14,emaSignal:4}' );

insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:12,emaLarger:26,emaSignal:9}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:10,emaLarger:20,emaSignal:7}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:15,emaLarger:30,emaSignal:10}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:9,emaLarger:18,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:8,emaLarger:16,emaSignal:5}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:18,emaLarger:35,emaSignal:12}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:20,emaLarger:40,emaSignal:15}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:25,emaLarger:50,emaSignal:20}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:30,emaLarger:60,emaSignal:25}' );
insert into InHouseSignals( signalName, parameterValues ) VALUES( 'MACD-SELL', '{signal:MACD-SELL,emaSmaller:7,emaLarger:14,emaSignal:4}' );

CREATE TABLE UserFollowing(
	username VARCHAR(31) NOT NULL,
	follower VARCHAR(31) NOT NULL,
	PRIMARY KEY( username, follower )
);

insert into UserFollowing VALUES( 'daniel', 'testman' );
insert into UserFollowing VALUES( 'daniel', 'christian' );
insert into UserFollowing VALUES( 'daniel', 'matt' );
insert into UserFollowing VALUES( 'daniel', 'nicholas' );
insert into UserFollowing VALUES( 'daniel', 'travas' );
insert into UserFollowing VALUES( 'christian', 'travas' );
insert into UserFollowing VALUES( 'matt', 'travas' );
insert into UserFollowing VALUES( 'nick', 'travas' );
insert into UserFollowing VALUES( 'alpha', 'travas' );

CREATE TABLE UserCredibility(
	username VARCHAR(31) NOT NULL,
	credibility int NOT NULL,
	dateOfLastGrant timestamp,
	PRIMARY KEY( username )
);

insert into UserCredibility VALUES( 'daniel', 100, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'matt', 100, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'nick', -10000, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'christian', 100, '2018-04-25 23:59:59' );
insert into UserCredibility VALUES( 'alpha', 100, '2018-04-25 23:59:59' );

CREATE TABLE UserBio(
	username VARCHAR(31) NOT NULL,
	bio VARCHAR(255) NOT NULL,
	PRIMARY KEY( username )
);

insert into UserBio VALUES( 'daniel','My name is Daniel Anderson, co-founder of Travas research and crypto currency enthusiast' );
insert into UserBio VALUES( 'christian','My name is Christian Skinner, I like buy Ethereum at the top and lose all my money' );
insert into UserBio VALUES( 'matt','My name is Matt Teeter, the contango master' );
insert into UserBio VALUES( 'nick','My name is Nicholass Skinner, I am a fucking queef!' );
insert into UserBio VALUES( 'alpha','I am AlPhA!.2.' );
