import sys
import os
import argparse

COUNTERS = [ 'BTC', 'ETH', 'BNB' ]

def make_pair( pair ):

	ending    = pair[-3:]
	endingTwo = pair[-4:]
	
	if ending in COUNTERS and endingTwo != "USDT":
		return pair[:-3] + "/" + ending

	return pair[:-4] + "/" + endingTwo

	
def main():

	parser = argparse.ArgumentParser( )
	parser.add_argument('--data_dir', type=str, default='')
	parser.add_argument('--table', type=str, default='1MinCandles')
	
	args = parser.parse_args()
	
	data_dir         = args.data_dir
	table            = args.table
	
	for filename in os.listdir( data_dir ):
	
		pair     = make_pair( filename.split('_')[0] )
		filepath = data_dir + "/" + filename
		
		if '.DS_Store' in filename or 'backtesting' in filename:
		    continue
		
		for line in open( filepath ).readlines( )[1:]:
			fields = line.split(',')
			query  = "INSERT into {} VALUES( {},{},{},{},{},{},{},{},{},{},{},{},{}"
			date                      = fields[0].strip()
			open_time                 = fields[1].strip()
			openV                     = fields[2].strip()
			high                      = fields[3].strip()
			low						  = fields[4].strip()
			closeV                    = fields[5].strip()
			volume                    = fields[6].strip()
			close_time                = fields[7].strip()
			quote_asset_volume        = fields[8].strip()
			num_of_trades             = fields[9].strip()
			taker_buy_base_asset_vol  = fields[10].strip()
			taker_buy_quote_asset_vol = fields[11].strip()
			query = query.format( table, "\'"+date+"\'", "\'"+pair+"\'", open_time, close_time, openV, high, low, closeV, volume, quote_asset_volume, num_of_trades, taker_buy_base_asset_vol, taker_buy_quote_asset_vol)
			print( query + " );" )
	
	


if __name__ == '__main__':

	main()
	