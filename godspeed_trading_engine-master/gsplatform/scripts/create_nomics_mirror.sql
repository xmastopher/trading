drop DATABASE nomicsmirror;
CREATE DATABASE nomicsmirror;
use nomicsmirror;

CREATE TABLE NomicsDailyCandles(
    exchange VARCHAR(31),
    market VARCHAR(31),
    base VARCHAR(31),
    quote VARCHAR(31),
    candles LONGTEXT,
    PRIMARY KEY(exchange,base,quote)
);

CREATE TABLE NomicsMarkets(
    exchange VARCHAR(31),
    market VARCHAR(31),
    base VARCHAR(31),
    quote VARCHAR(31),
    PRIMARY KEY(exchange,base,quote)
);

CREATE TABLE NomicsMarketsIntersections(
    exchangeOne VARCHAR(31),
    exchangeTwo VARCHAR(31),
    exchangeOneMarket VARCHAR(63),
    exchangeTwoMarket VARCHAR(63),
    base VARCHAR(31),
    quote VARCHAR(31),
    PRIMARY KEY(exchangeOne,exchangeTwo,base,quote)
);