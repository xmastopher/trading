CREATE DATABASE HistoricalBinanceData;
use historicalbinancedata;

CREATE TABLE 1MinCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 5MinCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 15MinCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 30MinCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 1HourCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 2HourCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 4HourCandles (
    date DATETIME NOT NULL,
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 6HourCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 12HourCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);

CREATE TABLE 1DayCandles (
    pair VARCHAR(255) NOT NULL,
    open_time VARCHAR(255) NOT NULL,
    close_time VARCHAR(255),
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    quote_asset_volume DOUBLE,
    num_of_trades INT,
    taker_buy_base_asset_volume DOUBLE,
    taker_buy_quote_asset_volume DOUBLE,
    PRIMARY KEY ( open_time, close_time, pair )
);
